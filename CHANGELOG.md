### v0.1 - Delete this script as it is now a rule in Makefile
### v0.1.0 - Publish STRAP application
### v0.1.1 - Add automation scripts for Makefile
### v0.1.2 - Automate creating users using Makefile
### v0.1.2-1 - Fix createusers.py script error
### v0.1.3 - Add a documentation for using Makefile
### v0.2.0 - Render applications' information
### v0.3.0 - Allow applications to be edited
### v0.3.1 - Notify user if their selected application id is not unique
### v0.4.0 - Allow users to delete applications
### v0.4.1 - Make changes in application's behavior
### v0.4.10 - Simulate application deployment using a fake command
### v0.4.11 - Implement stopping deployments functionality
### v0.4.12 - Containerize app in DEBUG mode
### v0.4.12-1 - Containerize app with DEBUG mode off
### v0.4.13 - Integrate Helm and Terraform into application container
### v0.4.2 - Enable pylint on custom files
### v0.4.3 - Add tests and set them up in CI
### v0.4.4 - Set up coverage test in CI
### v0.4.5 - Add unit tests, improve linting and coverage test result
### v0.4.5-1 - Set up selenium test on local machine
### v0.4.5-2 - Add selenium tests
### v0.4.5-3 - Make a Postgres container as the app's database
### v0.4.6 - Add an About modal to the application
### v0.4.6-1 - Fix unit test errors
### v0.4.7 - Add automation scripts for Makefile
### v0.4.8 - Automate creating users using Makefile
### v0.4.9 - Allow users to configure database for applications
### v0.4.9-1 - Render username on navigation bar
### v0.5.0 - Successfully deploy STRAPPER to a Kubernetes cluster for the first time
### v0.6.0 - Convert to an asgi app and update deployment log in near real time
### v0.6.1 - Set up Postgres to replace Redis as backend for channel layers
### v0.6.2 - Make a minor change in render messages
### v0.6.3 - Explain why 'MySQL' option is greyed out for Database field
### v1.0.0 - Implement state machine on issue #20 into STRAP
### v1.0.1 - Fix issue #19 - Print out field errors in application form if any
### v1.0.2 - Tackle issue #21: Form validation failures show messages to user
### v1.0.3 - Fix issue #22: Fix deployment log channel layer group
### v1.0.4 - Force failed deployments to get destroyed before the applications can be deleted as mentioned in issue #20
### v1.0.5 - Modify the script to update CHANGELOG.md
### v1.0.6 - Modify tests for forms and models
### v1.0.7 - Add tests for views
### v1.1.0 - Allow users to manage groups for each app
### v1.1.1 - Add an API endpoint for external apps to get groups for an user
### v1.2.0 - Allow group sharing between applications
### v1.3.0 - First deployable version to Kickstand
### v2.0.0 - First workable version on Kickstand
### v2.1.0 - Update Postgre info after succesful deployments and add a real-time app log
### v2.2.0 - Provide app terminals for users to execute commands within app pods
