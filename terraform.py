# pylint:
import sys
import os
import argparse
import json
import tempfile
import subprocess

from strapper.exceptions import DeploymentFailure, DestroyDeploymentFailure

BASE_DIR = '/tmp/terraform'

# Get the application name from command line argument -a
parser = argparse.ArgumentParser(description='Fake running terraform')
parser.add_argument('-a', '--app', required=True, help='Specify app name to run terraform on')
parser.add_argument('-d', '--destroy', required=False, default=0,
    help='Set this to a non-zero value will destroy \
    the resources for this app created by Terraform. Default is 0')
parser.add_argument('-r', '--revert', required=False, default=0,
    help='Set this to a non-zero value will revert \
    the resources created or modified for this app in the most recent Terraform apply. Default is 0')
parser.add_argument('-s', '--stop', required=False, default=0,
    help='Set this to a non-zero value will destroy \
    the resources created or modified for this app by Helm using Terraform. Default is 0')
parser.add_argument('-l', '--log', required=False, default=0,
    help='Log helm_release resource for an application. Default is 0')
parser.add_argument('-t', '--test', required=False, default=0,
    help='If set to a non-zero, the file is run in test mode. Default is 0')

args = vars(parser.parse_args())
app_id = args['app']

# destroy mode
destroy = args['destroy']

# revert mode
revert = args['revert']

# stop mode
stop = args['stop']

# log mode
log = args['log']

# test mode
test = args['test']

# Fake run terraform in unit test environment
if test:
    if app_id == 'successfulapp':
        if stop:
            print(f'Stopped app {app_id} successfully')
        elif destroy:
            print(f'Destroyed app {app_id} successfully')
        elif revert:
            print(f'Reverted app {app_id} successfully')
        else:
            deployed_log =\
f'''Deployed app {app_id} successfully
                
postgres = {{
    "databases" = [
        "{app_id}_prod",
        "{app_id}_dev",
    ]
    "roles" = {{
        "{app_id}_owner" = "ownerpassword"
        "{app_id}_ro" = "ropassword"
        "{app_id}_rw" = "rwpassword"
    }}
}}'''
            print(deployed_log)
    else:
        if stop:
            print(f'Failed to stop app {app_id}')
            raise DeploymentFailure(description=f'Failed to stop app {app_id}')
        if destroy:
            print(f'Failed to destroy app {app_id}')
            raise DeploymentFailure(description=f'Failed to destroy app {app_id}')
        if revert:
            print(f'Failed to revert app {app_id}')
            raise DeploymentFailure(description=f'Failed to revert app {app_id}')
        print(f'Failed to deploy app {app_id}')
        raise DeploymentFailure(description=f'Failed to deploy app {app_id}')
    sys.exit()

# Disable "wrong-import-position", "wrong-import-order", "ungrouped-imports" as
# these packages only need to be imported in a non-testing environment
# pylint:disable=C0413,C0411,C0412
import django
django.setup()

# TODO: Has to import this here as django.setup() needs to be called before
# Application model can be imported
# pylint: disable-next=C0413
from strapper.normal_users.models import Application, Deployment


app_path = f"{BASE_DIR}/{app_id}"

# if not (destroy or revert or stop or log):
# Use os.system here because there is no need to stop
# these processes when stopping a deployment as these processes
# don't make changes to Kubernetes resources
# Run 'terraform init'
os.system(f"cd {BASE_DIR} && terraform validate")
os.system(f"cd {BASE_DIR} && terraform init -upgrade")

# create Terraform variable definitions file in JSON
testing = False
app = Application.objects.get(identifier=app_id)
if revert:
    app.copy_last_successful_deployment()

tfvars = app.build_configuration()

print(f'tfvars: {tfvars}')
# Cannot use "with" because temporary file has to exist when running
# Terraform and can only be deleted afterwards.
# pylint: disable=consider-using-with
tmp = tempfile.NamedTemporaryFile(mode='r+', suffix='.tfvars.json', delete=False, dir=f'{BASE_DIR}')
print("Temporary Terraform variables file: %s", tmp.name)
json.dump(tfvars, tmp, ensure_ascii=False, indent=4)

# TODO: "plan" should be set elsewhere.  This command is the basis for
# either the plan or apply operation (and maybe destroy at some point).
# "plan" should create a plan file and "apply" should use this file.
action = "plan" if testing else "apply"
auto_approve = "-auto-approve" if not testing else ""
tmp.close()

if destroy:
    # Run 'terraform destroy'
    process = subprocess.Popen(
        ["terraform", f"-chdir={BASE_DIR}", "destroy", "-no-color", "-input=false", auto_approve,
        f"-var-file={tmp.name}", f"-state={app_id}.tfstate"]
    )

    process.wait(timeout=120)

    # Delete the temporary file
    os.remove(tmp.name)

    # Raise an error if destroying an app's deployment fails, which means return code is not 0
    if process.returncode != 0:
        raise DestroyDeploymentFailure(description=f'Failed to destroy deployment for {app.descriptive_name}')

    sys.exit()

elif revert:
    # Run "terraform apply"
    process = subprocess.Popen(
        ["terraform", f"-chdir={BASE_DIR}", action, "-no-color", "-input=false", auto_approve,
        f"-var-file={tmp.name}", f"-state={app_id}.tfstate"]
    )

    process.wait(timeout=120)

    # Delete the temporary file
    os.remove(tmp.name)

    # Raise an error if reverting an app's failed deployment fails, which means return code is not 0
    if process.returncode != 0:
        raise DestroyDeploymentFailure(description=f'Failed to revert deployment for {app.descriptive_name}')

    sys.exit()

elif stop:
    # Run 'terraform destroy'
    process = subprocess.Popen(
        ["terraform", f"-chdir={BASE_DIR}", "destroy", "-no-color", "-input=false", auto_approve,
        f"-var-file={tmp.name}", f"-state={app_id}.tfstate", "-target", "helm_release.app"]
    )

    process.wait(timeout=120)

    # Delete the temporary file
    os.remove(tmp.name)

    # Raise an error if destroying an app's deployment fails, which means return code is not 0
    if process.returncode != 0:
        raise DestroyDeploymentFailure(description=f'Failed to stop deployment for {app.descriptive_name}')

    sys.exit()

elif log:
    # Run 'terraform destroy'
    process = subprocess.Popen(
        ["terraform", f"-chdir={BASE_DIR}", "show", "helm_release.app",
        f"-var-file={tmp.name}", f"-state={app_id}.tfstate"]
    )

    process.wait(timeout=120)

    # Delete the temporary file
    os.remove(tmp.name)

    # Raise an error if destroying an app's deployment fails, which means return code is not 0
    if process.returncode != 0:
        raise DestroyDeploymentFailure(description=f'Failed to log deployment for {app.descriptive_name}')

    sys.exit()

else:
    # Run "terraform apply"
    process = subprocess.Popen(
        ["terraform", f"-chdir={BASE_DIR}", action, "-no-color", "-input=false", auto_approve,
        f"-var-file={tmp.name}", f"-state={app_id}.tfstate"]
    )

    # Add process id to the currently executing processes for this deployment
    # Get deployment to avoid race condition
    deployment = Deployment.objects.get(
        application=app
    )
    deployment.process_ids = str(deployment.process_ids or '') + f"{',' if deployment.process_ids else ''}{process.pid}"
    deployment.save()
    process.wait(timeout=120)

    # Delete the temporary file
    os.remove(tmp.name)

    # Raise an error if deploying an app fails, which means return code is not 0
    if process.returncode != 0:
        raise DeploymentFailure(description=f'Failed to deploy app {app.descriptive_name}')

    sys.exit()
