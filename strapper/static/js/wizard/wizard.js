import { appFormInit } from "../app_forms.js";
import { harborProjectFunctions } from "../harbor_projects.js";
import { appContainerFunctions } from "../app_containers.js";

const wizardInit = () => {
    appFormInit();
    harborProjectFunctions();
    appContainerFunctions();
}

wizardInit();