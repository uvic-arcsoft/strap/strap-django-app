import { autoSuggestTextInputsFunctions } from "./components.js";
import { displayMessageToast } from "./components.js";

// Handle app container creations
const appContainerCreationFunctions = () => {
    // Initially hide app container forms
    $('.app-container.create-form').hide();

    // Open app container form when clicking on "+ Create a container" button
    $('.app-container-btn.create').click(function() {
        const appContainersDisplay = $(this).closest('.app-containers-display');
        const appContainerCreateForm = appContainersDisplay.find('.app-container.create-form');
        appContainerCreateForm.fadeToggle();
        $(this).hide();
        appContainersDisplay.find('.no-container-text').fadeOut();
    })

    // Close app container creation form when clicking on the "Cancel" button
    $('.app-container-create-form-cancel').click(function() {
        $(this).closest('.create-form').fadeOut();
        const appContainersDisplay = $(this).closest('.app-containers-display');
        appContainersDisplay.find('.app-container-btn.create').fadeIn();
        appContainersDisplay.find('.no-container-text').fadeIn();
    })
    
    // Get all images in image registry repository when clicking on Image select tag
    $('.app-container.container-form select.harbor-project-image').each(function() {
        const appId = $(this).data('app-id');
        const imageSelectTag = $(this);
        const currentlySelectedImage = imageSelectTag.val();

        // Display a loading message
        const imageField = imageSelectTag.closest('.image-field');
        const loadingMessage = imageField.find('.loading-message');
        loadingMessage.removeClass('d-none');
        imageSelectTag.html(`<option disabled selected>Loading images from registry...</option>`);

        // Make an AJAX request to get all images in image registry repository
        $.ajax({
            url: `/harbor/get_images/${appId}`,
            type: 'GET',
            success: function(response) {
                console.log(response);
                // Create image options for the select tag
                const harborHostname = new URL($('input#harbor-url').val()).hostname;
                imageSelectTag.html(
                `
                <option disabled ${!currentlySelectedImage ? "selected" : ""}>Select an image from image registry</option>
                ${response.images.map(image => `<option value="${image}" ${currentlySelectedImage === image ? "selected" : ""}>${image.replace(`${harborHostname}/${appId}/`, "")}</option>`).join('')}
                `
                );

                // When select an image, update the hidden image and tag inputs to submit to backend
                const containerForm = imageSelectTag.closest('form');
                const imageInput = containerForm.find('.container-image');
                const tagInput = containerForm.find('.container-image-tag');

                imageSelectTag.off('change').change(function() {
                    const selectedImage = $(this).val();
                    const selectedImageParts = selectedImage.split(':');
                    const image = selectedImageParts.slice(0, -1).join(':');
                    const imageTag = selectedImage.split(':').at(-1);
                    imageInput.val(image);
                    tagInput.val(imageTag);
                });

                // Hide the loading message
                loadingMessage.addClass('d-none');
            },
            error: function(error) {
                console.log(error);
                imageSelectTag.html(`<option disabled selected>Failed to fetch images from image registry</option>`);

                // Hide the loading message
                loadingMessage.addClass('d-none');
            }
        })
    });
}

// Handle app container updates
export const appContainerUpdateFunctions = () => {
    // Initially hide app container forms
    $('.app-container.edit-form').hide();

    // Toggle app container edit form
    $('.app-container .edit-btn').click(function() {
        const containerId = $(this).data('container-id');
        const appContainersDisplay = $(this).closest('.app-containers-display');
        const appContainer = appContainersDisplay.find(`#app-container-${containerId}`);
        const appContainerEditForm = appContainersDisplay.find(`#app-container-form-${containerId}`);
        appContainer.fadeToggle();
        appContainerEditForm.fadeToggle();
    })

    // Close app container edit form
    $('.app-container-edit-form-cancel').click(function() {
        const containerId = $(this).data('container-id');
        const appContainersDisplay = $(this).closest('.app-containers-display');
        const appContainer = appContainersDisplay.find(`#app-container-${containerId}`);
        const appContainerEditForm = appContainersDisplay.find(`#app-container-form-${containerId}`);
        appContainer.fadeToggle();
        appContainerEditForm.fadeToggle();
    })

    /* Environment variables */
    // Update enviromnet variables hidden textarea
    const updateEnvironmentVariablesTextarea = (el) => {
        const environmentVariablesDisplay = el.closest('.environment-variables-display');
        let environmentVariables = "";
        environmentVariablesDisplay.find('.environment-variable').each(function() {
            const key = $(this).find('input.key').val();
            const value = $(this).find('input.value').val();
            if (key || value) {
                environmentVariables += `${key}==${value}\n`;
            }
        });
        environmentVariablesDisplay.closest('form').find('.container-environment-variables').val(environmentVariables);
    }

    // Update environment variables
    const attachUpdateEnvironmentVariableEventListener = () => {
        $('.environment-variable input').off('input').on('input', function() {
            updateEnvironmentVariablesTextarea($(this));
        });
    }

    // Remove environment variable
    const attachRemoveEnvironmentVariableEventListener = () => {
        $('.environment-variable .delete-btn').off('click').click(function() {
            const environmentVariable = $(this).closest('.environment-variable');
            
            // NOTE: Have to clean up the input first to update the hidden textarea
            environmentVariable.find('input').val('');
            updateEnvironmentVariablesTextarea($(this))
            environmentVariable.remove();
        });
    }

    // Add environment variable
    const addEnvironmentVariable = () => {
        $('.environment-variables-display .add-btn').off('click').click(function() {
            const environmentVariablesHolder = $(this).closest('.environment-variables-display').find('.environment-variables-holder');
            environmentVariablesHolder.append(`
            <div class="row mb-1 environment-variable">
                <div class="col">
                    <label class="mb-1 text-secondary fs-90percent">Key</label>
                    <input class="form-control key" placeholder="e.g. CLIENT_KEY" required>
                </div>
                <div class="col">
                    <label class="mb-1 text-secondary fs-90percent">Value</label>
                    <div class="auto-suggest">
                        <input class="form-control value" pattern='^[^"]*$' title="Please do not use double quotation marks in an environment variable value">
                        <span class="suggestions" class="d-none"></span>
                        <div class="mirror"></div>
                    </div>
                </div>
                <div class="col-1 d-flex flex-column justify-content-end">
                    <i class="fa-solid fa-trash mt-auto mb-3 icon-btn-dark delete-btn"></i>
                </div>
            </div>
            `);
            attachRemoveEnvironmentVariableEventListener();
            attachUpdateEnvironmentVariableEventListener();
            autoSuggestTextInputsFunctions();
        });
    }

    // Update environment variable when importing a .env file
    $('.environment-variables-display .env-file-input').on("change", function() {
        const fileInput = $(this);
        const file = this.files[0];

        // Only allow .env* files to be imported
        if (!file.name.startsWith('.env')) {
            displayMessageToast('Only .env* files are allowed', false);
            fileInput.val('');
            return;
        }

        const reader = new FileReader();
        reader.onload = function(e) {
            const content = e.target.result;
            const environmentVariablesHolder = fileInput.closest('.environment-variables-display').find('.environment-variables-holder');
            const lines = content.split('\n');
            let errorMessages = [];
            lines.forEach((line, lineIndex) => {
                // Ignore comments and blank lines
                const lineTrimmed = line.trim();
                if (lineTrimmed && !lineTrimmed.startsWith('#')) {
                    const parts = line.split('=', 2);
                    if (parts.length === 2) {
                        environmentVariablesHolder.append(`
                        <div class="row mb-1 environment-variable">
                            <div class="col">
                                <label class="mb-1 text-secondary fs-90percent">Key</label>
                                <input class="form-control key" value="${parts[0]}" required>
                            </div>
                            <div class="col">
                                <label class="mb-1 text-secondary fs-90percent">Value</label>
                                <div class="auto-suggest">
                                    <input class="form-control value" value="${parts[1]}" pattern='^[^"]*$' title="Please do not use double quotation marks in an environment variable value">
                                    <span class="suggestions" class="d-none"></span>
                                    <div class="mirror"></div>
                                </div>
                            </div>
                            <div class="col-1 d-flex flex-column justify-content-end">
                                <i class="fa-solid fa-trash mt-auto mb-3 icon-btn-dark delete-btn"></i>
                            </div>
                        </div>
                        `);
                    } else {
                        errorMessages.push(`<strong>Skipping line ${lineIndex + 1}</strong>: Invalid environment variable format`);
                    }
                }
            });
            if (errorMessages.length > 0) {
                displayMessageToast(errorMessages.join('<br>'), false);
            }
            updateEnvironmentVariablesTextarea(environmentVariablesHolder);
            attachRemoveEnvironmentVariableEventListener();
            attachUpdateEnvironmentVariableEventListener();
            autoSuggestTextInputsFunctions();
        }
        reader.readAsText(file);
        fileInput.val('');
    })

    attachRemoveEnvironmentVariableEventListener();
    addEnvironmentVariable();
    attachUpdateEnvironmentVariableEventListener();
}

const scan = (containerId, scanUrl) => {
    const appId = $('input#app-id').val();
    const deployBtn = $(`.deploy-btn.deploy-btn-${appId}`);
    const containerDiv = $(`#app-container-${containerId}`);

    // Display the loading spinner
    containerDiv.find('.scan-container-loading-text').removeClass('d-none');

    // Hide the status and failed messages if they're displayed
    containerDiv.find('.scan-container-status-text').addClass('d-none');
    containerDiv.find('.scan-container-failed-text').addClass('d-none');

    const severityLevels = {
        "Critical": [4, "text-red-700"],
        "Unknown": [3, "text-red-500"],
        "High": [3, "text-red-500"],
        "Medium": [2, "text-orange-400"],
        "Low": [1, "text-yellow-400"],
    }

    // Send an AJAX request to scan the container
    $.ajax({
        type: 'GET',
        url: scanUrl,
        success: function(response) {
            console.log(response);
            // Hide the loading spinner
            containerDiv.find('.scan-container-loading-text').addClass('d-none');

            if (response.status === 200) {
                // Display vulnerabilities for this container
                const vulnerabilities = response.vulnerabilities;
                const scanContainerStatusText = containerDiv.find('.scan-container-status-text');
                const scanContainerFailedText = containerDiv.find('.scan-container-failed-text');

                // Failed to scan vulnerabilities for this particular container
                if ("error" in vulnerabilities) {
                    scanContainerFailedText.removeClass('d-none');
                    return;
                }

                // No vulnerabilities
                if (vulnerabilities.length === 0) {
                    scanContainerStatusText.html(`
                        <i class="fa-solid fa-check me-2"></i>
                        <span class="text-success">No vulnerabilities found</span>
                    `);
                    scanContainerStatusText.removeClass('d-none');

                    // Enable app deployment if there are no vulnerabilities
                    if (response.is_deployable && deployBtn.hasClass('disabled')) {
                        deployBtn.removeClass('disabled');
                    }
                    return;
                }

                // Get the highest severity level
                let highestSeverityLevel = "Low";
                for (let i = 0; i < vulnerabilities.length; i++) {
                    const vulnerability = vulnerabilities[i];
                    if (severityLevels[vulnerability.severity][0] > severityLevels[highestSeverityLevel][0]) {
                        highestSeverityLevel = vulnerability.severity;
                    }
                    if (highestSeverityLevel === "Critical") {
                        break;
                    }
                }

                // Display number of vulnerabilities and the highest severity level
                scanContainerStatusText.html(`
                    <i class="fa-solid fa-exclamation-triangle me-2 ${severityLevels[highestSeverityLevel][1]}"></i>
                    <span class="${severityLevels[highestSeverityLevel][1]}">
                        ${vulnerabilities.length} vulnerabilities found with the highest severity level being <strong>${highestSeverityLevel}</strong>. 
                        More <a href="${scanReportUrl}" target="_blank">details</a>.
                    </span>
                `);
                scanContainerStatusText.removeClass('d-none');

                // Enable app deployment if the vulnerabilities are acceptable, i.e. below the configured level
                if (response.is_deployable && deployBtn.hasClass('disabled')) {
                    deployBtn.removeClass('disabled');
                }
            } else {
                // Hide the status text if it's displayed
                containerDiv.find('.scan-container-status-text').addClass('d-none');
                // Display a failed message on top of the container
                containerDiv.find('.scan-container-failed-text').removeClass('d-none');
            }
        },
        error: function(response) {
            console.log(response);
            // Hide the loading spinner
            containerDiv.find('.scan-container-loading-text').addClass('d-none');
            // Hide the status text if it's displayed
            containerDiv.find('.scan-container-status-text').addClass('d-none');
            // Display a failed message on top of the container
            containerDiv.find('.scan-container-failed-text').removeClass('d-none');
        }
    });
}

export const appContainerFunctions = () => {
    appContainerCreationFunctions();
    appContainerUpdateFunctions();

    // Prevent container form to be submitted with duplicate env var keys
    $('.container-form form').submit(function(e) {
        e.preventDefault();

        const environmentVariables = $(this).find('.container-environment-variables').val().split('\n');
        const keys = environmentVariables.map(envVar => envVar.split('==')[0]);
        const uniqueKeys = new Set(keys);
        if (keys.length !== uniqueKeys.size) {
            displayMessageToast('Environment variable keys must be unique', false);
            return;
        }

        $(this).off('submit').submit();
    });

    // Rescan container when clicking on "Retry" in a container failed scan message
    $('.container-rescan-btn').click(function(e) {
        e.preventDefault();
        const containerId = $(this).data('container-id');
        const scanUrl = $(this).data('scan-url');
        scan(containerId, scanUrl);
    });
}
