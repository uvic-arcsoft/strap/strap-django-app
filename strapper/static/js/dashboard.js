import { appFormInit } from "./app_forms.js";

const dashboardInit = () => {
    // Hide app creation form intially
    $('.app-create-form').hide();

    // Open app creation form when clicking on plus icon button on all applications modal
    $('.app-create-btn').click(function() {
        const appModal = $(this).closest('.app-modal');
        $(this).toggleClass('active');
        appModal.find('.app-create-form').fadeToggle();
    })

    // Close the app creation form when clicking on the form's "Cancel" button
    $('.app-create-cancel-btn').click(function() {
        const appModal = $(this).closest('.app-modal');
        appModal.find('.app-create-btn').toggleClass('active');
        $(this).closest('.app-create-form').fadeToggle();
    })

    appFormInit();
}

dashboardInit();