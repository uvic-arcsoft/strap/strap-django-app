// All functions for app form (edit/create)
const appFormFunctions = () => {
    let reservedAppNames = [];
    if ($('input#reserved-app-names').length > 0) {
        reservedAppNames = $('input#reserved-app-names').val().split('\n');
    }

    /* Display an error if user inputs an invalid identifier, which can be:
    - Idenfier already exists
    - Identifier doesn't have all lowercase letters, numbers and hyphens, or doesn't have a length at least 3 and no more than 16
    */ 
    $('.app-identifier-input').on('input', function() {
        const appIdRegex = /^[a-z][a-z0-9-]*[a-z0-9]$/;
        const existingIdentifiers = JSON.parse($(this).attr('data-existing-identifiers'));
        const curAppIdentifier = $(this).val();

        const isError = existingIdentifiers.includes(curAppIdentifier)
            || reservedAppNames.includes(curAppIdentifier)
            || !appIdRegex.test(curAppIdentifier)
            || curAppIdentifier.length < 3 || curAppIdentifier.length > 16;
        
        const errorText = $(this).parent().children('.text-danger');
        const submitBtn = $(this).parent().parent().find("button[type='submit']");
        if (isError) {
            // Flash error on input tag
            errorText.removeClass('d-none');
            $(this).addClass('error-input');
            if (existingIdentifiers.includes(curAppIdentifier) || reservedAppNames.includes(curAppIdentifier)) {
                errorText.text('Identifier is not available');
            } else if (curAppIdentifier.length < 3) {
                errorText.text('Identifier needs to have at least 3 characters');
            } else if (!appIdRegex.test(curAppIdentifier)) {
                errorText.text('Identifier must contain only alphanumeric characters and hyphens. It must start with an alphabet and end with an alphanumeric character.');
            } else {
                errorText.text("Identifier can't have more than 16 characters");
            }
            // Disable the submit button
            submitBtn.attr('disabled', true);
        } else {
            if (!errorText.hasClass('d-none')) {
                errorText.addClass('d-none');
            }
            if ($(this).hasClass('error-input')) {
                $(this).removeClass('error-input');
            }
            submitBtn.attr('disabled', false);
        }

        // Update domain name
        $('#app-domain-id').text(curAppIdentifier);
    })

    $('select.authentication').on("change", function() {
        // If authentication is set to "None", disable the authenticated routes and
        // unauthenticated routes input and simply set "/" as the only unauthenticated route
        const appForm = $(this).closest('form');
        if (!$(this).val()) {
            appForm.find('.routes-input input').attr("disabled", true);

            // Wipe out all authenticated routes
            appForm.find('.route-tag-container.authenticated-routes').empty();
            appForm.find('.route-hidden-input.authenticated-routes-input').val('');

            // Set "/" as the only unauthenticated route
            appForm.find('.route-tag-container.unauthenticated-routes').html(`
                <div class="route-tag mt-1"><span class="route-tag-path">/</span> <span class="route-tag-close disabled">&#10005;</span></div>
            `);
            appForm.find('.route-hidden-input.unauthenticated-routes-input').val('/');
        } else {
            appForm.find('.routes-input input').attr("disabled", false);
            appForm.find('route-tag-close').removeClass('disabled');
            routeTagDeleteEvent(appForm.find('.route-tag-container.unauthenticated-routes'));
        }
    })

    // If app form is not empty, notify users that changes made might not be saved
    // when they are closing the window (implement later if needed)
    /* $(window).on("beforeunload", function() {
        return confirm("Do you really want to close?"); 
    }) */
}

// Add a delete event for the newly created route tag
const routeTagDeleteEvent = routeContainer => {
    // Delete a route tag when clicking on "X" in that tag
    routeContainer.find('.route-tag:last .route-tag-close').click(function() {
        const routeTag = $(this).parent();
        const routeHiddenInput = routeTag.parent().parent().children('.route-hidden-input')
        let routeNewInput = routeHiddenInput.val().replace(routeTag.children('.route-tag-path').text(), '')
        routeHiddenInput.val(routeNewInput);
        routeTag.remove();
    })
}

// All functions for routes
const routeFunctions =  () => {
    $('.routes-input input').keyup(function() {
        // Disable 'Add route' button when the input for route is empty
        const addRouteBtn = $(this).parent().children('.btn')
        const curRoutes = $(this).parent().parent().find('.route-hidden-input').val().split(',')
        if ($(this).val() === "" || curRoutes.includes($(this).val())) {
            if (!addRouteBtn.hasClass('disabled')) {
                addRouteBtn.addClass('disabled');
            }
        } else {
            if (addRouteBtn.hasClass('disabled')) {
                addRouteBtn.removeClass('disabled');
            }
        }

        // Display a warning if the route is in the other list of routes
        // For example, if adding a route to authenticated routes, and that route is already in unauthenticated routes
        const type = $(this).data('type');
        const unauthRoutes = $(this).closest('form').find('input[name="unauthenticated_routes"]').val().split(',').filter(route => route);
        const authRoutes = $(this).closest('form').find('input[name="authenticated_routes"]').val().split(',').filter(route => route);
        if (type === "authenticated") {
            if (
            $(this).val() &&
            unauthRoutes.includes($(this).val()) &&
            // Don't show warning if the only route is unauthenticated and it's "/"
            // Happens when the authentication mode is first changed from "None" to a different mode
            ($(this).val() !== "/" || ($(this).val() === "/" && !(unauthRoutes.length === 1 && authRoutes.length === 0)))) {
                $('.alert-warning[data-role="authenticated-routes-alert"]').removeClass('d-none');
            } else {
                $('.alert-warning[data-role="authenticated-routes-alert"]').addClass('d-none');
            }
        } else {
            if ($(this).val() && authRoutes.includes($(this).val())) {
                $('.alert-warning[data-role="unauthenticated-routes-alert"]').removeClass('d-none');
            } else {
                $('.alert-warning[data-role="unauthenticated-routes-alert"]').addClass('d-none');
            }
        }
    })

    // Add a route tag when 'Add route' button is clicked
    $('.routes-input .btn').click(function() {
        const form = $(this).closest('form');
        const routeContainer = $(this).parent().parent().children('.route-tag-container')
        const routeInput = $(this).parent().children('input')
        const newRoute = routeInput.val();
        routeContainer.append(`<div class="route-tag mt-1"><span class="route-tag-path">${newRoute}</span> <span class="route-tag-close">&#10005;</span></div>`)
        routeTagDeleteEvent(routeContainer);

        // Add the new route to the hidden input
        const routeHiddenInput = $(this).parent().parent().children('.route-hidden-input')
        let routeHiddenInputVal = routeHiddenInput.val()
        routeHiddenInput.val(`${routeHiddenInputVal}${routeHiddenInputVal === '' ? '' : ','}${newRoute}`)

        // Reset route input
        routeInput.val("");

        // Disable "Add route" button
        $(this).addClass('disabled');

        // Hide warning if displayed
        form.find('.alert-warning').addClass('d-none');

        // Remove the route from the other list of routes if it's in there
        const type = routeInput.data('type');
        if (type === "authenticated") {
            const unauthRoutes = form.find('input[name="unauthenticated_routes"]').val().split(',');
            if (unauthRoutes.includes(newRoute)) {
                const newUnauthRoutes = unauthRoutes.filter(route => route && route !== newRoute);
                form.find('input[name="unauthenticated_routes"]').val(newUnauthRoutes.join(','));
                form.find('.route-tag-container.unauthenticated-routes').children().each(function() {
                    if ($(this).children('.route-tag-path').text() === newRoute) {
                        $(this).remove();
                    }
                })
            }
        } else {
            const authRoutes = form.find('input[name="authenticated_routes"]').val().split(',');
            if (authRoutes.includes(newRoute)) {
                const newAuthRoutes = authRoutes.filter(route => route && route !== newRoute);
                form.find('input[name="authenticated_routes"]').val(newAuthRoutes.join(','));
                form.find('.route-tag-container.authenticated-routes').children().each(function() {
                    if ($(this).children('.route-tag-path').text() === newRoute) {
                        $(this).remove();
                    }
                })
            }
        }
    })

    // Delete a route tag when clicking on "X" in that tag
    $('.route-tag .route-tag-close').click(function() {
        if (!$(this).hasClass('disabled')) {
            const routeTag = $(this).parent();
            const routeHiddenInput = routeTag.parent().parent().children('.route-hidden-input')
            let routeNewInput = routeHiddenInput.val().replace(routeTag.children('.route-tag-path').text(), '')
            routeHiddenInput.val(routeNewInput);
            routeTag.remove();
        }
    })
    
    // Make description a required field when user opts in to publish the app on STRAP main page
    $("input[name='published_on_strap']").change(function() {
        $("textarea[name='description']").attr("required", $(this).prop("checked"));
    })
}

const advancedConfigFunctions = () => {
    // Remove an IP address input field when clicking on the trash icon
    const ipRemoveBtnInit = () => {
        $('.allowed-ip-remove-btn').off('click').on('click', function() {
            $(this).closest('.allowed-ip-item').remove();
        });
    }
    ipRemoveBtnInit();

    /*** Limit IP access to app interactions ***/
    // Only display IP options if selecting 'Limit app access to certain IPs' option
    $('#app-advanced-config-content input[type="radio"]').click(function() {
        if ($(this).attr('id') === 'limit-ip-access') {
            $('#ip-options').removeClass('d-none');
        } else {
            $('#ip-options').addClass('d-none');
        }
    });

    // Only display the text input for IP addresses if selecting 'Others' under 'Limit app access to certain IPs' option
    $('#other-ips').change(function() {
        $('#allowed-ips-div').toggleClass('d-none');
    });

    // Add a new IP address input field when clicking on "Add IP address or network"
    $('#add-ip-input-btn').click(function() {
        $('#allowed-ips-container').append(
        `
        <div class="allowed-ip-item my-1">
            <div class="d-flex flex-row gap-2 align-items-center">
                <input 
                class="form-control"
                type="text"
                name="allowed_ips"
                pattern="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(/[0-9]{1,2})?$"
                placeholder="0.0.0.0/0"
                aria-label="Other IPs list">
                <i class="allowed-ip-remove-btn fa-solid fa-trash icon-btn-dark"></i>
            </div>
        </div>
        `
        );
        ipRemoveBtnInit();
    });
}

export const appFormInit = () => {
    appFormFunctions();
    routeFunctions();
    advancedConfigFunctions();
}
