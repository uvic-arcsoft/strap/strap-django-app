// All functions for app group managers
export const appGroupFunctions = () => {
    // Hide app group create and edit forms initially
    $('.app-group-form-card').hide();
    $('.app-group-edit-form-card').hide();

    // Hide conflict app modal
    const appConflictModalJquery = $('#groupFormAppConflictModal');
    var appConflictModalJavascript = new bootstrap.Modal(document.getElementById('groupFormAppConflictModal'))

    /* Group creating and editing forms */
    // Disable "Add users" field when "Add everyone" is checked 
    $('.app-group-form .is-everyone-checkbox').change(function() {
        const groupForm = $(this).closest('.app-group-form');
        const inputTagFieldInput = groupForm.find('.input-tag-field input');
        if ($(this).prop('checked')) {
            inputTagFieldInput.attr('disabled', true);
        } else {
            inputTagFieldInput.attr('disabled', false);
        }
    })

    // Toggle the create group form when clicking on the "+" button
    $('.app-group-create-form-btn').click(function() {
        const appGroupModal = $(this).closest('.app-modal');
        const groupCreateText = appGroupModal.find('.group-create-text');
        appGroupModal.find('.app-group-create-form-btn').toggleClass('active');
        appGroupModal.find('.app-group-form-card').fadeToggle();
        if (groupCreateText.length > 0) {
            groupCreateText.fadeToggle();
        }
    })

    // Close the group create form when clicking on a "Cancel" button
    $('.app-group-form-cancel-btn').click(function() {
        $(this).closest('.app-group-form-card').fadeOut();
        const appGroupModal = $(this).closest('.app-modal');
        const groupCreateText = appGroupModal.find('.group-create-text');
        appGroupModal.find('.app-group-create-form-btn').removeClass('active');
        if (groupCreateText.length > 0) {
            groupCreateText.fadeToggle();
        }
    })

    // Open the edit group form when clicking on the pen icon button
    $('.app-group-edit-form-btn').click(function() {
        const groupDisplay = $(this).closest('.group-display');
        groupDisplay.find(`.group-card`).fadeToggle();
        groupDisplay.find(`.app-group-edit-form-card`).fadeToggle();
    })

    // Close the group edit form when clicking on a "Cancel" button
    $('.app-group-edit-form-cancel-btn').click(function() {
        const groupDisplay = $(this).closest('.group-display');
        groupDisplay.find(`.group-card`).fadeToggle();
        groupDisplay.find(`.app-group-edit-form-card`).fadeToggle();
    })

    // Prompt an error if an user tries to create/change a group name that already exists in an app
    $('.app-group-form').on('submit', function(e) {
        e.preventDefault();
        const nameField = $(this).find('.group-name-field');
        const appField = $(this).find('.group-app-field');
        const initName = nameField.data('name');
        const selectedApps = appField.find('option:selected');
        let conflictApps = [];

        selectedApps.each(function() {
            const appGroups = $(this).data('groups');
            console.log(appGroups);
            if (nameField.val() !== initName && appGroups.includes(nameField.val())) {
                console.log('conflict app found');
                conflictApps.push($(this).text());
            }
        })

        if (conflictApps.length > 0) {
            const modalBody = appConflictModalJquery.find('.modal-body');
            modalBody.empty();
            modalBody.append(`
            Group <strong>${nameField.val()}</strong> already exists in the following apps:
            <ul class="mt-2">
            `)
            conflictApps.forEach(app => {
                modalBody.append(`<li>${app}</li>`)
            })
            modalBody.append('</ul>');
            // appConflictModal.removeClass('fade');
            appConflictModalJavascript.show();
            return false;
        }
        // console.log('No conflict app found. Submitting the form!')
        $(this).off('submit');
        $(this).submit();
    })

    /* Modal for adding existing groups to an app functionalities */
    // Show "No compatible group found" text
    $('.add-groups-to-app-modal').each(function() {
        const appName = $(this).data('app-id');
        const groupCards = $(this).find('.group-card');
        const groupShowcase = $(this).find('.group-showcase');
        if (groupCards.length === 0) {
            $(this).find('.helper-text').addClass('d-none');
            groupShowcase.empty();
            groupShowcase.append(`<p>There is no compatible group to add to <strong>${appName}</strong></p>`);
        }
    })

    // Select/deselect group cards by clicking on them
    $('.add-groups-to-app-modal .group-card').click(function() {
        const addGroupModal = $(this).closest('.add-groups-to-app-modal');
        const groupInput = addGroupModal.find('.group-field');
        const addBtn = addGroupModal.find('.add-btn');
        const groupId = $(this).data('group-id');
        let selectedGroups = groupInput.val().split(",");
        $(this).toggleClass('selected');
        if ($(this).hasClass('selected')) {
            selectedGroups.push(groupId);
        } else {
            selectedGroups = selectedGroups.filter(groupItemId => groupItemId != groupId);
        }
        groupInput.val(selectedGroups.join(","));
        if (selectedGroups.length > 1) {
            addBtn.removeClass('disabled');
        } else {
            addBtn.addClass('disabled');
        }
    })
}
