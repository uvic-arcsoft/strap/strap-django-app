// Add users to a role group
const addUsersToRoleGroup = (el) => {
    // Search users using the search bar
    const searchValue = el.val().toLowerCase();
    const searchUrl = el.data('search-url');
    const searchResults = el.closest('.user-search-bar').find('.search-results');
    
    const modal = $('#accessManagerModal');
    const form = modal.find('form');
    const editorsInput = form.find('input[name="editors"]');
    const viewersInput = form.find('input[name="viewers"]');
    const editors = editorsInput.val().split(',');
    const viewers = viewersInput.val().split(',');
    
    // Send an AJAX request to get all users that match the searched query
    $.ajax({
        type: 'GET',
        url: `${searchUrl}?search=${searchValue}`,
        success: function(data) {
            searchResults.html('');
            searchResults.addClass('show');

            // Hide users that are already in either the editors or viewers list
            const hiddenUsernames = [...new Set([...editors, ...viewers])];
            const availableUsers = data.users.filter(user => !hiddenUsernames.includes(user.username));

            // No user found
            if (availableUsers.length === 0) {
                searchResults.append(`
                <li><a class="dropdown-item disabled text-black" href="#">No user found</a></li>
                `)
                return;
            }

            availableUsers.forEach(user => {
                searchResults.append(`
                <li>
                    <a 
                    class="dropdown-item d-flex flex-row align-items-center gap-3"
                    href="#"
                    data-image="${user.image}"
                    data-first-name="${user.first_name}"
                    data-last-name="${user.last_name}"
                    data-username="${user.username}">
                        <img src="${user.image}" class="rounded-circle me-2" width="30" height="30">
                        <div>
                            <div class="fw-semibold">${user.first_name ? user.first_name + ' ' + user.last_name : user.username}</div>
                            <span div class="text-black-50">${user.username}</div>
                        </div>
                    </a>
                </li>
                `)
            })

            // Check if we are adding an editor or a viewer
            const mode = modal.find('select[name="mode"]').val();

            // Add a user to the editors or viewers list when clicking on a user
            searchResults.find('.dropdown-item').click(function(e) {
                e.preventDefault();
                const user = $(this);
                const username = user.data('username');
                const firstName = user.data('first-name');
                const fullName = firstName + ' ' + user.data('last-name');
                const image = user.data('image');

                // Add the user to the editors or viewers list
                if (mode === 'editor') {
                    editors.push(username);
                    editorsInput.val(editors.join(','));
                    modal.find('.modal-body').append(`
                    <div 
                        data-username="${ username }"
                        data-role="editor"
                        class="user-option d-flex flex-row px-4 py-2 align-items-center gap-3">
                        <div>
                            <img src="${ image }" width="40" height="40" class="rounded-circle" alt="profile image">
                        </div>
                        <div class="flex-grow-1">
                            <div class="fw-semibold full-name">${ firstName ? fullName : username }</div>
                            <div class="text-black-50">${ username }</div>
                        </div>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Editor
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item disabled text-black fw-semibold" href="#">
                                    <i class="fa-solid fa-pen me-2"></i>
                                    Editor
                                </a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item remove-access-btn" href="#">Remove access</a></li>
                                <li><a class="dropdown-item demote-btn" href="#">Demote to 'Viewer'</a></li>
                                <li><a class="dropdown-item disabled" href="#">Transfer ownership (Save first)</a></li>
                            </ul>
                        </div>
                    </div>
                    `)
                } else {
                    viewers.push(username);
                    viewersInput.val(viewers.join(','));
                    modal.find('.modal-body').append(`
                    <div 
                        data-username="${ username }"
                        data-role="viewer"
                        class="user-option d-flex flex-row px-4 py-2 align-items-center gap-3">
                        <div>
                            <img src="${ image }" width="40" height="40" class="rounded-circle" alt="profile image">
                        </div>
                        <div class="flex-grow-1">
                            <div class="fw-semibold full-name">${ firstName ? fullName : username }</div>
                            <div class="text-black-50">${ username }</div>
                        </div>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Viewer
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item disabled text-black fw-semibold" href="#">
                                    <i class="fa-solid fa-eye me-2"></i>
                                    Viewer
                                </a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item remove-access-btn" href="#">Remove access</a></li>
                                <li><a class="dropdown-item promote-btn" href="#">Promote to 'Editor'</a></li>
                            </ul>
                        </div>
                    </div>
                    `);
                }

                // Delete the user from the search results
                user.parent().remove();

                // Reinitialize the remove access function so that it can work on newly added users
                removeAccess();

                // Reinitialize the promote to editor function so that it can work on newly added users
                promoteToEditor();

                // Reinitialize the demote to viewer function so that it can work on newly added users
                demoteToViewer();
            })
        },
        error: function(error) {
            console.log(error);
            console.log('Fetch query returns error. Failed to search users.');

            searchResults.html('');
            searchResults.addClass('show');

            searchResults.append(`
            <li><a class="dropdown-item disabled text-black" href="#">Failed to search users</a></li>
            `)
        }
    })

    // Close the search results when clicking outside of the search bar
    $(document).click(function(e) {
        if (!$(e.target).closest('.user-search-bar').length) {
            $('.search-results').removeClass('show');
        }
    })
}

// Attach an event listener on "Remove access" button to remove access from an user to an app
const removeAccess = () => {
    $('#accessManagerModal .remove-access-btn').off('click').click(function() {
        console.log('Remove access clicked');
        const userOption = $(this).closest('.user-option');
        const username = userOption.data('username');
        const role = userOption.data('role');
        const editorsInput = $('#accessManagerModal form input[name="editors"]');
        const viewersInput = $('#accessManagerModal form input[name="viewers"]');
        const editors = editorsInput.val().split(',');
        const viewers = viewersInput.val().split(',');

        // Remove the user from the editors or viewers list
        if (role === 'editor') {
            const index = editors.indexOf(username);
            editors.splice(index, 1);
            editorsInput.val(editors.join(','));
        } else {
            const index = viewers.indexOf(username);
            viewers.splice(index, 1);
            viewersInput.val(viewers.join(','));
        }

        // Remove the user from the modal
        userOption.remove();
    });
}

// Promote a viewer to an editor
const promoteToEditor = () => {
    $('#accessManagerModal .promote-btn').off('click').click(function() {
        const userOption = $(this).closest('.user-option');
        const username = userOption.data('username');
        const editorsInput = $('#accessManagerModal form input[name="editors"]');
        const viewersInput = $('#accessManagerModal form input[name="viewers"]');
        const editors = editorsInput.val().split(',');
        const viewers = viewersInput.val().split(',');

        // Remove the user from the viewers list
        const index = viewers.indexOf(username);
        viewers.splice(index, 1);
        viewersInput.val(viewers.join(','));

        // Add the user to the editors list
        editors.push(username);
        editorsInput.val(editors.join(','));

        // Render the user as an editor
        const fullName = userOption.find('.full-name').text();
        const image = userOption.find('img').attr('src');

        userOption.empty();
        userOption.data('role', 'editor');
        userOption.append(`
        <div>
            <img src="${ image }" width="40" height="40" class="rounded-circle" alt="profile image">
        </div>
        <div class="flex-grow-1">
            <div class="fw-semibold full-name">${ fullName }</div>
            <div class="text-black-50">${ username }</div>
        </div>
        <div class="dropdown">
            <button class="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Editor
            </button>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item disabled text-black fw-semibold" href="#">
                    <i class="fa-solid fa-pen me-2"></i>
                    Editor
                </a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item remove-access-btn" href="#">Remove access</a></li>
                <li><a class="dropdown-item demote-btn" href="#">Demote to 'Viewer'</a></li>
                <li><a class="dropdown-item disabled" href="#">Transfer ownership (Save first)</a></li>
            </ul>
        </div>
        `);

        // Reinitialize the remove access function so that it can work on newly promoted users
        removeAccess();

        // Reinitialize the demote to viewer function so that it can work on newly promoted users
        demoteToViewer();
    });
}

// Demote an editor to a viewer
const demoteToViewer = () => {
    $('#accessManagerModal .demote-btn').off('click').click(function() {
        const userOption = $(this).closest('.user-option');
        const username = userOption.data('username');
        const editorsInput = $('#accessManagerModal form input[name="editors"]');
        const viewersInput = $('#accessManagerModal form input[name="viewers"]');
        const editors = editorsInput.val().split(',');
        const viewers = viewersInput.val().split(',');

        // Remove the user from the editors list
        const index = editors.indexOf(username);
        editors.splice(index, 1);
        editorsInput.val(editors.join(','));

        // Add the user to the viewers list
        viewers.push(username);
        viewersInput.val(viewers.join(','));

        // Render the user as a viewer
        const fullName = userOption.find('.full-name').text();
        const image = userOption.find('img').attr('src');

        userOption.empty();
        userOption.data('role', 'viewer');
        userOption.append(`
        <div>
            <img src="${ image }" width="40" height="40" class="rounded-circle" alt="profile image">
        </div>
        <div class="flex-grow-1">
            <div class="fw-semibold full-name">${ fullName }</div>
            <div class="text-black-50">${ username }</div>
        </div>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Viewer
            </button>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item disabled text-black fw-semibold" href="#">
                    <i class="fa-solid fa-eye me-2"></i>
                    Viewer
                </a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item remove-access-btn" href="#">Remove access</a></li>
                <li><a class="dropdown-item promote-btn" href="#">Promote to 'Editor'</a></li>
            </ul>
        </div>
        `);

        // Reinitialize the remove access function so that it can work on newly demoted users
        removeAccess();

        // Reinitialize the promote to editor function so that it can work on newly demoted users
        promoteToEditor();
    });
}

const transferAppOwnership = () => {
    // Set the new owner of the app to the selected user's username when clicking on "Transfer ownership" button
    $('#accessManagerModal .transfer-ownership-btn').click(function() {
        const newOwner = $(this).data('new-owner');
        $('#transferOwnershipWarningModal .new-owner-username').text(newOwner);
        $('#transferOwnershipWarningModal input[name="new_owner"]').val(newOwner);
    });
}

export const manageAppFunctions = () => {
    $('#accessManagerModal .user-search-bar input').keyup(function() {
        addUsersToRoleGroup($(this));
    });

    $('#accessManagerModal .user-search-bar input').click(function() {
        addUsersToRoleGroup($(this));
    });

    $('#accessManagerModal .submit-btn').click(function() {
        const form = $('#accessManagerModal form');
        form.submit();
    });

    // Initialize the remove access function
    removeAccess();

    // Initialize the promote to editor function
    promoteToEditor();

    // Initialize the demote to viewer function
    demoteToViewer();

    // Initialize the transfer app ownership function
    transferAppOwnership();
}
