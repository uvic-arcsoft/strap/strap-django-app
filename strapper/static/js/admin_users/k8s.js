// All interactions for K8s
export const k8sFunctions = () => {
    // Open K8s form
    $('.k8s-info .edit-btn').click(function() {
        const k8sInfo = $(this).closest('.k8s-info');
        const appModalBody = k8sInfo.closest('.app-modal-body');
        const k8sForm = appModalBody.find('.k8s-form');
        k8sInfo.toggleClass('d-none');
        k8sForm.toggleClass('d-none');
    });

    // Close K8s form
    $('.k8s-form .cancel-btn').click(function() {
        const k8sForm = $(this).closest('.k8s-form');
        const appModalBody = k8sForm.closest('.app-modal-body');
        const k8sInfo = appModalBody.find('.k8s-info');
        k8sInfo.toggleClass('d-none');
        k8sForm.toggleClass('d-none');
    });

    // Populate K8s request process modal with request info
    $('.k8s-resource-request-modal .view-details-btn').click(function() {
        const modal = $('#k8sResourceRequestModal');
        modal.find('form').attr('action', $(this).data('process-url'));
        modal.find('.app-name').text($(this).data('app-name'));
        modal.find('.submitted-by').text($(this).data('submitted-by'));
        modal.find('.last-updated').text($(this).data('last-updated'));
        modal.find('.replicas').val($(this).data('replicas'));
        modal.find('.replicas').data('requested-replicas', $(this).data('replicas'));
        modal.find('.justification').text($(this).data('justification'));
        modal.find('.current-quota-replicas').text($(this).data('current-quota-replicas'));
    });

    // Update UI if an admin adjusts a request
    $('#k8sResourceRequestModal .replicas').on('input', function() {
        const requestedReplicas = $(this).data('requested-replicas');
        const approveBtn = $('#k8sResourceRequestModal button[value="approve"]');

        if ($(this).val() != requestedReplicas) {
            approveBtn.text('Approve adjusted request');
        } else {
            approveBtn.text('Approve');
        }
    });
}
