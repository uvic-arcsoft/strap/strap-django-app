// All interactions for PostgreSQL database
const postgresqlFunctions = () => {
    // Open database form
    $('.database-info .edit-btn').click(function() {
        const databaseInfo = $(this).closest('.database-info');
        const appModalBody = databaseInfo.closest('.app-modal-body');
        const databaseForm = appModalBody.find('.database-form');
        databaseInfo.toggleClass('d-none');
        databaseForm.toggleClass('d-none');
    });

    // Close database form
    $('.database-form .cancel-btn').click(function() {
        const databaseForm = $(this).closest('.database-form');
        const appModalBody = databaseForm.closest('.app-modal-body');
        const databaseInfo = appModalBody.find('.database-info');
        databaseInfo.toggleClass('d-none');
        databaseForm.toggleClass('d-none');
    });
}

export const databaseFunctions = () => {
    postgresqlFunctions();
}