// All interactions for Keycloak
export const keycloakFunctions = () => {
    // Open Keycloak form
    $('.keycloak-info .edit-btn').click(function() {
        const keycloakInfo = $(this).closest('.keycloak-info');
        const appModalBody = keycloakInfo.closest('.app-modal-body');
        const keycloakForm = appModalBody.find('.keycloak-form');
        keycloakInfo.toggleClass('d-none');
        keycloakForm.toggleClass('d-none');
    });

    // Close Keycloak form
    $('.keycloak-form .cancel-btn').click(function() {
        const keycloakForm = $(this).closest('.keycloak-form');
        const appModalBody = keycloakForm.closest('.app-modal-body');
        const keycloakInfo = appModalBody.find('.keycloak-info');
        keycloakInfo.toggleClass('d-none');
        keycloakForm.toggleClass('d-none');
    });
}