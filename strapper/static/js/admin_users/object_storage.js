// All interactions for object storage
export const objectStorageFunctions = () => {
    // Open object storage form
    $('.object-storage-info .edit-btn').click(function() {
        const objectStorageInfo = $(this).closest('.object-storage-info');
        const appModalBody = objectStorageInfo.closest('.app-modal-body');
        const objectStorageForm = appModalBody.find('.object-storage-form');
        objectStorageInfo.toggleClass('d-none');
        objectStorageForm.toggleClass('d-none');
    });

    // Close object storage form
    $('.object-storage-form .cancel-btn').click(function() {
        const objectStorageForm = $(this).closest('.object-storage-form');
        const appModalBody = objectStorageForm.closest('.app-modal-body');
        const objectStorageInfo = appModalBody.find('.object-storage-info');
        objectStorageInfo.toggleClass('d-none');
        objectStorageForm.toggleClass('d-none');
    });
}