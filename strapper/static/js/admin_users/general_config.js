// All interactions for general configs
export const generalConfigFunctions = () => {
    // Open general config form
    $('.general-config-display .edit-btn').click(function() {
        const generalConfigDisplay = $(this).closest('.general-config-display');
        const appModalBody = generalConfigDisplay.closest('.app-modal-body');
        const generalConfigForm = appModalBody.find('.general-config-form');
        generalConfigDisplay.toggleClass('d-none');
        generalConfigForm.toggleClass('d-none');
    });

    // Close general config form
    $('.general-config-form .cancel-btn').click(function() {
        const generalConfigForm = $(this).closest('.general-config-form');
        const appModalBody = generalConfigForm.closest('.app-modal-body');
        const generalConfigDisplay = appModalBody.find('.general-config-display');
        generalConfigDisplay.toggleClass('d-none');
        generalConfigForm.toggleClass('d-none');
    });

    // Remove an IP value input when clicking on the trash icon
    const ipValueRemoveBtnInit = () => {
        $('#preset-ips-editor .remove-ip-btn').off('click').on('click', function() {
            $(this).closest('.row').remove();
        });
    }
    ipValueRemoveBtnInit();

    // Add IP value input when clicking on "+ Add IP address or network"
    const ipValueAddBtnInit = () => {
        $('#preset-ips-editor .add-ip-btn').off('click').on('click', function() {
            const ipOptionBlock = $(this).closest('.ip-option-block');
            const ipOptionValues = ipOptionBlock.find('.ip-option-values');
            ipOptionValues.append(`
            <div class="row my-1">
                <div class="col-6"></div>
                <div class="col-5">
                    <input
                    type="text"
                    name="preset_ip_value"
                    pattern="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(/[0-9]{1,2})?$"
                    class="form-control"
                    placeholder="0.0.0.0/0">
                </div>
                <div class="col-1 d-flex flex-column justify-content-center align-items-start">
                    <i class="remove-ip-btn fa-solid fa-trash icon-btn-dark my-auto"></i>
                </div>
            </div>
            `);
            ipValueRemoveBtnInit();
        });
    }
    ipValueAddBtnInit();

    // Remove IP option block when clicking on the "Delete option" button
    const ipOptionRemoveBtnInit = () => {
        $('#ip-option-blocks .btn-outline-danger').off('click').on('click', function() {
            $(this).closest('.ip-option-block').remove();
        });
    }
    ipOptionRemoveBtnInit();


    // Add IP option block when clicking on "+ Add IP option" button
    $('#add-ip-option-btn').click(function(e) {
        e.preventDefault();
        $('#ip-option-blocks').append(`
        <div class="ip-option-block mt-3 pt-3 border-top border-dark">
            <div class="d-flex flex-row justify-content-end">
                <button class="btn btn-sm btn-outline-danger ms-auto mb-2">
                    <i class="fa-solid fa-trash me-1"></i>
                    Delete option
                </button>
            </div>
            <div class="ip-option-values">
                <div class="row">
                    <label class="col-6 fs-90percent fw-light">Name</label>
                    <label class="col-5 fs-90percent fw-light">IP addresses/networks</label>
                    <div class="col-1"></div>
                </div>
                <div class="row my-1">
                    <div class="col-6">
                        <input type="text" name="preset_ip_name" class="form-control" placeholder="My VPN" required>
                    </div>
                    <div class="col-5">
                        <input
                            type="text"
                            name="preset_ip_value"
                            pattern="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(/[0-9]{1,2})?$"
                            class="form-control"
                            placeholder="0.0.0.0/0">
                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-6"></div>
                <div class="add-ip-btn col-5 fs-90percent text-btn-dark mt-2 w-fit">+ Add IP address or network</div>
                <div class="col-1"></div>
            </div>

            <input type="text" name="preset_ips" class="d-none">
        </div>
        `);
        ipValueAddBtnInit();
        ipOptionRemoveBtnInit();
    });

    // Preprocess data when submitting the form
    $('form.general-config-form').submit(function(e) {
        e.preventDefault();
        
        // Combine all IP values into a comma-separated string and set it to the hidden input
        $('.ip-option-block').each(function() {
            const ipValues = [];
            $(this).find('input[name="preset_ip_value"]').each(function() {
                if ($(this).val().trim() !== '') {
                    ipValues.push($(this).val());
                }
            });
            $(this).find('input[name="preset_ips"]').val(ipValues.join(','));
        })

        $(this).off('submit').submit();
    });
}
