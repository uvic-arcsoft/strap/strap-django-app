import { generalConfigFunctions } from "./general_config.js";
import { databaseFunctions } from "./database.js";
import { keycloakFunctions } from "./keycloak.js";
import { harborFunctions } from "./harbor.js";
import { objectStorageFunctions } from "./object_storage.js";
import { k8sFunctions } from "./k8s.js";

const dashboardInit = () => {
    generalConfigFunctions();
    databaseFunctions();
    keycloakFunctions();
    harborFunctions();
    objectStorageFunctions();
    k8sFunctions();
}

dashboardInit();