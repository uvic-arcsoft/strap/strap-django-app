// All interactions for Harbor
export const harborFunctions = () => {
    // Open Harbor form
    $('.harbor-info .edit-btn').click(function() {
        const harborInfo = $(this).closest('.harbor-info');
        const appModalBody = harborInfo.closest('.app-modal-body');
        const harborForm = appModalBody.find('.harbor-form');
        harborInfo.toggleClass('d-none');
        harborForm.toggleClass('d-none');
    });

    // Close Harbor form
    $('.harbor-form .cancel-btn').click(function() {
        const harborForm = $(this).closest('.harbor-form');
        const appModalBody = harborForm.closest('.app-modal-body');
        const harborInfo = appModalBody.find('.harbor-info');
        harborInfo.toggleClass('d-none');
        harborForm.toggleClass('d-none');
    });
}