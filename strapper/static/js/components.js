/* Input tags include an input field and a button 
Users type in the input field and click on the button to add the content
inside the input field as a tag. When sent, content of tags 
are submited together as a string separated by commas */
const inputTagComponentFunction = () => {
    // Add a delete event for the newly created input tag
    const inputTagDeleteEvent = tagContainer => {
        // Delete a input tag when clicking on "X" in that tag
        tagContainer.find('.input-tag:last .input-tag-close').click(function() {
            const inputTag = $(this).parent();
            const inputTagHiddenInput = inputTag.parent().parent().children('.input-tag-hidden-input')
            let inputTagNewInput = inputTagHiddenInput.val().replace(inputTag.children('.input-tag-content').text(), '')
            inputTagHiddenInput.val(inputTagNewInput);
            inputTag.remove();
        })
    }

    // Disable 'Add' button when the input for input tag field is empty
    $('.input-tag-field input').keyup(function() {
        const addInputTagBtn = $(this).parent().children('.btn')
        const curValue = $(this).parent().parent().find('.input-tag-hidden-input').val().split(',')
        if ($(this).val() === "" || curValue.includes($(this).val())) {
            if (!addInputTagBtn.hasClass('disabled')) {
                addInputTagBtn.addClass('disabled');
            }
        } else {
            if (addInputTagBtn.hasClass('disabled')) {
                addInputTagBtn.removeClass('disabled');
            }
        }
    })

    // Add an input tag when 'Add' button is clicked
    $('.input-tag-field .btn').click(function() {
        const inputTagContainer = $(this).parent().parent().children('.input-tag-container')
        const inputTagInput = $(this).parent().children('input')
        const newInputTag = inputTagInput.val();
        inputTagContainer.append(`<div class="input-tag mt-1"><span class="input-tag-content">${newInputTag}</span> <span class="input-tag-close">&#10005;</span></div>`)
        inputTagDeleteEvent(inputTagContainer);

        // Add the new route to the hidden input
        const inputTagHiddenInput = $(this).parent().parent().children('.input-tag-hidden-input')
        let inputTagHiddenInputVal = inputTagHiddenInput.val()
        inputTagHiddenInput.val(`${inputTagHiddenInputVal}${inputTagHiddenInputVal === '' ? '' : ','}${newInputTag}`)

        // Reset route input
        inputTagInput.val("");

        // Disable "Add route" button
        $(this).addClass('disabled');
    })

    // Delete an input tag when clicking on "X" in that tag
    $('.input-tag .input-tag-close').click(function() {
        const inputTag = $(this).parent();
        const inputTagHiddenInput = inputTag.parent().parent().children('.input-tag-hidden-input')
        let inputTagNewInput = inputTagHiddenInput.val().replace(inputTag.children('.input-tag-content').text(), '')
        inputTagHiddenInput.val(inputTagNewInput);
        inputTag.remove();
    })
}

/* Brief display show short summary of something with buttons 'See more'
and 'See less' to toggle all information related to that thing */
const briefDisplay = () => {
    // Hide hidden content and "See less" button intially
    $('.brief-display .hidden-content').addClass('d-none');
    $('.breif-display .see-less-btn').addClass('d-none');

    // Clicking "See more" shows the hidden content, "See less" button and hides the "See more" button
    $('.brief-display .see-more-btn').click(function() {
        const briefDisplay = $(this).closest('.brief-display');
        $(this).addClass('d-none');
        briefDisplay.find('.see-less-btn').removeClass('d-none');
        briefDisplay.find('.hidden-content').removeClass('d-none');
    })

    // Clicking "See less" hides the hidden content, "See less" button and shows the "See more" button
    $('.brief-display .see-less-btn').click(function() {
        const briefDisplay = $(this).closest('.brief-display');
        $(this).addClass('d-none');
        briefDisplay.find('.see-more-btn').removeClass('d-none');
        briefDisplay.find('.hidden-content').addClass('d-none');
    })
}

// Functionalities for app cards
const appCardFunctions = () => {
    // Toggle app edit form when clicking on pen icon button in an app card
    $('.app-edit-btn').click(function() {
        const appCard = $(this).closest('.app-card');
        $(this).closest('.icon-btn-case').addClass('d-none');
        appCard.find('.app-edit-form').fadeToggle();
        appCard.find('.app-summary').fadeToggle();
    })

    // Close app edit form and show app summary when clicking on "Cancel" button on the form
    $('.app-card-edit-cancel-btn').click(function() {
        const appCard = $(this).closest('.app-card');
        appCard.find('.icon-btn-case').removeClass('d-none');
        $(this).closest('.app-edit-form').fadeToggle();
        appCard.find('.app-summary').fadeToggle();
    })
}

// Password display with a button to show/hide the password
export const initPasswordDisplay = () => {
    $('.password-display .show-btn').off('click').click(function() {
        const passwordDisplay = $(this).parent('.password-display');
        passwordDisplay.find('.password').toggleClass('d-none');
        passwordDisplay.find('.text').toggleClass('d-none');
        if ($(this).text() === 'Show') {
            $(this).text('Hide');
        } else  {
            $(this).text('Show');
        }
    });

    // Copy password to clipboard when clicked
    $('.password-display pre.text code').off('click').click(function() {
        const password = $(this).text();
        navigator.clipboard.writeText(password).then(function() {
            displayMessageToast('Password copied to clipboard', true);
        }, function() {
            displayMessageToast('Failed to copy password to clipboard', false);
        });
    });
}

// Message toasts
const messageToast = document.getElementById('messageToast');
const toast = new bootstrap.Toast(messageToast, {
    delay: 20000
});
// Display message toast
export const displayMessageToast = (message, success=true) => {
    messageToast.querySelector('.toast-body').innerHTML = message;
    if (success) {
        messageToast.querySelector('.toast-header strong').textContent = 'Strapper (Successful)';
    } else {
        messageToast.querySelector('.toast-header strong').textContent = 'Strapper (Failed)';
    }
    toast.show();
}
// Display message toast if receive a message from backend
const message = $('#message-toast-content').data('message');
const success = $('#message-toast-content').data('success');
if (message) {
    displayMessageToast(message, success);
}

/*** Auto suggest text inputs/textareas for placeholder variables that will take on a value after a successful deployment ***/
// For example #DB_NAME#
export const autoSuggestTextInputsFunctions = () => {
    const words = [
        ["#DB_NAME#", "Database name"],
        ["#DB_OWNER_PW#", "Database owner password"], 
        ["#DB_RO_PW#", "Database read-only password"], 
        ["#DB_RW_PW#", "Database read-write password"], 
    ];

    $('.auto-suggest input').off('keyup').on('keyup', function(e) {
        const inputField = $(this);
        const autoSuggest = inputField.closest('.auto-suggest');
        const suggestions = autoSuggest.find('.suggestions');

        // Estimate cursor position - This is a simplistic approach
        const cursorPosition = inputField.get(0).selectionStart;
        const inputVal = inputField.val();
        const beforeCursor = inputVal.substring(0, cursorPosition);
        const afterCursor = inputVal.substring(cursorPosition);
        const lastChar = beforeCursor.charAt(beforeCursor.length - 1);

        // Calculate the length in pixel of the input text
        const mirror = autoSuggest.find('.mirror')
        mirror.text(inputVal);
        const inputTextWidth = mirror.width();

        if (lastChar === '#') {
            let suggestionsHTML = '';
            words.forEach(function(word) {
                suggestionsHTML += `<div><span class="insert-text">${word[0]}</span><span class="explain-text">${word[1]}</span></div>`;
            });
            suggestions.html(suggestionsHTML).show();
      
            // Adjust position based on a rough estimate of cursor position
            // This does not accurately place the box next to the cursor in the input field but provides a starting point
            suggestions.css({
              'left': `${inputField.offset().left + 20 + inputTextWidth}px`, // Adjust this based on your needs
              'top': `${inputField.offset().top + 5}px`
            });

            suggestions.children('div').off('click').on('click', function() {
                const wordToInsert = $(this).find('.insert-text').text();
                const newText = beforeCursor.slice(0, -1) + wordToInsert + afterCursor;
                inputField.val(newText).trigger('input');
                suggestions.hide();
                inputField.focus();
                // Set cursor position right after the inserted word (basic implementation)
                const newCursorPos = beforeCursor.length + wordToInsert.length -1;
                inputField.get(0).setSelectionRange(newCursorPos, newCursorPos);
            });
        } else if (e.key !== 'ArrowDown' && e.key !== 'ArrowUp') {
            suggestions.hide();
        }

        // Hide suggestions when clicking outside
        $(document).off('click.autoSuggestClick').on('click.autoSuggestClick', function(e) {
            if (!$(e.target).closest('.auto-suggest').length && !$(e.target).closest('.suggestions').length) {
                $('.suggestions').hide();
            }
        });
    });
}

// Advanced configurations toggle button
const advancedConfigInit = () => {
    $('.advanced-config').each(function() {
        const active = $(this).data('active');
        if (active) {
            $(this).find('.advanced-config-toggle').addClass('active');
        } else {
            $(this).find('.advanced-config-content').hide();
        }
    })

    $('.advanced-config-toggle').click(function() {
        $(this).toggleClass('active');
        $(this).closest('.advanced-config').find('.advanced-config-content').fadeToggle();
    });
}

// Initialize Bootstrap tooltips
const initBootstrapTooltips = () => {
    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
}

inputTagComponentFunction();
briefDisplay();
initPasswordDisplay();
appCardFunctions();
autoSuggestTextInputsFunctions();
advancedConfigInit();
initBootstrapTooltips();
