// All functions for K8s resources manager
export const appK8sResourcesManagerFunctions = () => {
    // Initially hide edit form
    $('.k8s-resources-manager .config-form').hide();

    // Show edit form, hide summary when clicking on 'Edit' button on the summary
    $('.k8s-resources-manager .edit-btn').click(function() {
        const k8sResourcesManager = $(this).closest('.k8s-resources-manager')
        const infoDisplay = $(this.closest('.info-display'));
        const configForm = k8sResourcesManager.find('.config-form');
        infoDisplay.hide();
        configForm.fadeIn();
    })

    // Hide edit form, show summary when clicking on 'Cancel' on the edit form
    $('.k8s-resources-manager .config-form .cancel-btn').click(function() {
        const k8sResourcesManager = $(this).closest('.k8s-resources-manager');
        const configForm = $(this).closest('.config-form');
        const infoDisplay = k8sResourcesManager.find('.info-display');
        infoDisplay.fadeIn();
        configForm.hide();
    })
}
