import { initPasswordDisplay, displayMessageToast } from "./components.js";

// Create a new image registry
const harborProjectCreationFunctions = () => {
    // Click on "Create project" button
    $('.harbor-modal .harbor-create-btn').click(function() {
        const appId = $(this).data('identifier');
        const createBtn = $(`.harbor-modal-${appId} .harbor-create-btn`);

        // Disable the button and show the spinner
        createBtn.addClass('disabled');
        createBtn.text('');
        createBtn.append(`Creating...<span class="spinner-border spinner-border-sm text-primary" role="status" aria-hidden="true"></span>`)

        // Send an AJAX request to create a new image registry
        $.ajax({
            type: 'POST',
            url: $(this).data('href'),
            data: {
                // CSRF token
                csrfmiddlewaretoken: $('input#csrf-token').val()
            },
            success: function(response) {
                console.log(response);

                // Display message toast
                displayMessageToast(response.message, true);
                
                // Success
                if (response.status === 200) {
                    // Remove the create button
                    // createBtn.remove();
                    const project = response.project;
                    const harborUrl = $("#harbor-url").val();

                    // Add instructions modal for the image registry
                    $('body').append(`
                    <div class="modal fade" id="harbor-instructions-modal-${ appId }" tabindex="-1" aria-labelledby="harbor-instructions-label" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="harbor-instructions-label">How to push images to your image registry</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <p>To push an image to your registry <strong>(${ project.project_name })</strong>, please do the followings:</p>
                                    <ol>
                                        <li>Log in to your image registry using the following command:</li>
                                        <pre class="bg-light p-2 mb-0"><code>docker login ${ harborUrl.replace("https://", "").replace("http://", "") } -u &#39;${ project.username }&#39;<br>Password: Type your password</code></pre>
                                        <div class="d-flex flex-row gap-2 my-2">
                                            <label>Your password:</label>
                                            <span class="password-display">
                                                <div class="password">****************</div>
                                                <pre class="text d-none"><code>${ project.password }</code></pre>
                                                <div class="show-btn">Show</div>
                                            </span>
                                        </div>
                                        <li>Tag your image with the project URL and the image name:</li>
                                        <pre class="bg-light p-2"><code>docker tag &lt;image&gt; ${ harborUrl.replace("https://", "").replace("http://", "") }/${ project.project_name }/&lt;image&gt;</code></pre>
                                        <li>Push the image to your registry:</li>
                                        <pre class="bg-light p-2"><code>docker push ${ harborUrl.replace("https://", "").replace("http://", "") }/${ project.project_name }/&lt;image&gt;</code></pre>
                                    </ol>
                                    <h5 class="mt-3">What is an image registry?</h5>
                                    <p>An image registry is like a private registry for your container images. You have to authenticate to push images to your image registry. In addition, once an image is pushed, it is scanned for vulnerabilities to ensure that only secure images get pushed to the registry.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Understood</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    `)

                    // Display image registry details
                    $(`.harbor-modal-${appId}`).each(function() {
                        const harborModalBody = $(this).find('.app-modal-body');
                        harborModalBody.empty();
                        harborModalBody.append(`
                        <div class="row mb-2">
                            <div class="col-5 fw-semibold">Name</div>
                            <div class="col-7 harbor-name-${ appId }">${ project.project_name }</div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-5 fw-semibold">Username</div>
                            <div class="col-7 harbor-username-${ appId }">${ project.username }</div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-5 fw-semibold">Password</div>
                            <div class="col-7 harbor-password-${ appId }">
                                <span class="password-display">
                                    <div class="password">****************</div>
                                    <pre class="text d-none"><code>${ project.password }</code></pre>
                                    <div class="show-btn">Show</div>
                                </span>
                            </div>
                        </div>
                        <div class="app-modal-btn-container">
                            <button type="button" class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#harbor-instructions-modal-${ appId }">Instructions</button>
                        </div>
                        `);
                    });
                    initPasswordDisplay();

                    // Hide "No image registry found" warning
                    $(`.no-harbor-project-warning-${ appId }`).remove();

                    // Show containers display
                    $(`.app-containers-display-${ appId }`).removeClass('d-none');
                } 
                // Failed
                else {
                    // Enable the button and hide the spinner
                    createBtn.removeClass('disabled');
                    createBtn.empty();
                    createBtn.text('Create project');
                }
            },
            error: function(response) {
                // Display message toast
                displayMessageToast("Something went wrong. Please try again or contact us for support.", false);

                // Enable the button and hide the spinner
                createBtn.removeClass('disabled');
                createBtn.empty();
                createBtn.text('Create project');
            }
        });
    });
}

// Handle image registry interactions
export const harborProjectFunctions = () => {
    harborProjectCreationFunctions();
}
