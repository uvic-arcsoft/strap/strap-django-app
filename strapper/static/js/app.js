import { appContainerFunctions } from "./app_containers.js";
import { appDeploymentFunctions } from "./app_deployments.js";
import { appFormInit } from "./app_forms.js";
import { appGroupFunctions } from "./app_groups.js";
import { appSummaryFunctions } from "./app_summary.js";
import { appK8sResourcesManagerFunctions } from "./app_k8s.js";
import { harborProjectFunctions } from "./harbor_projects.js";
import { manageAppFunctions } from "./manage_app.js";

// All functionalities for the dashboard tab
const appFunctions = () => {
    // Hide app creation form intially
    $('.app-create-form').hide();

    // Open app creation form when clicking on plus icon button on all applications modal
    $('.app-create-btn').click(function() {
        const appModal = $(this).closest('.app-modal');
        $(this).toggleClass('active');
        appModal.find('.app-create-form').fadeToggle();
    })

    // Close the app creation form when clicking on the form's "Cancel" button
    $('.app-create-cancel-btn').click(function() {
        const appModal = $(this).closest('.app-modal');
        appModal.find('.app-create-btn').toggleClass('active');
        $(this).closest('.app-create-form').fadeToggle();
    })

    // Configure delete modal when 'Delete' button is clicked
    $('.app-delete-btn').click(function() {
        const appIdentifier = $(this).data("identifier");
        const appName = $(this).data("name");
        
        // Set modal title
        $('#deleteModal .modal-title').text(`Delete ${appName}`);

        // Set modal body
        $('#deleteModal .modal-body .modal-body-text').html(`To confirm deleting ${appName}, type <strong>${appIdentifier}</strong>`);
    
        // Enable 'Confirm' button in delete modal if app identifier has been
        // typed correctly in the confirm input box
        const deleteConfirmInput = $('#deleteModal .modal-body input')
        deleteConfirmInput.off('keyup');
        deleteConfirmInput.keyup(function() {
            const deleteConfirmBtn = $(this).closest('.modal-content').find('.delete-modal-confirm-btn')
            if ($(this).val() === appIdentifier) {
                deleteConfirmBtn.removeClass('disabled');
                deleteConfirmBtn.attr('href', `/delete/${appIdentifier}/`);
            } else {
                if (!deleteConfirmBtn.hasClass('disabled')) {
                    deleteConfirmBtn.addClass('disabled');
                }
            }
        })
    })
}

// Send an API request to the backend to scan containers for vulnerabilities
const scanContainers = () => {
    const appId = $('input#app-id').val();
    const scanContainersUrl = $('input#scan-containers-url').val();
    const deployBtn = $(`.deploy-btn.deploy-btn-${appId}`);
    const unknownCriticality = $('input#unknown-criticality').val();

    let severityLevels = {
        "Critical": [4, "text-red-700"],
        "High": [3, "text-red-500"],
        "Medium": [2, "text-orange-400"],
        "Low": [1, "text-yellow-400"],
    }
    severityLevels['Unknown'] = severityLevels[unknownCriticality];

    // Send an AJAX request to scan containers
    $.ajax({
        type: 'GET',
        url: scanContainersUrl,
        success: function(response) {
            console.log(response);
            // Hide the loading spinner
            $('.scan-container-loading-text').addClass('d-none');
            if (response.status === 200) {
                // Hide the failed message if it's displayed
                $('.scan-container-failed-text').addClass('d-none');

                // Display vunerabilities for each container
                Object.entries(response.report).forEach(([containerId, vulnerabilities]) => {
                    const scanReportUrl = $(`#app-container-${containerId}`).data('scan-report-url');
                    const scanContainerStatusText = $(`#app-container-${containerId} .scan-container-status-text`);
                    // Failed to scan vulnerabilities for this particular container
                    if ("error" in vulnerabilities) {
                        $(`#app-container-${containerId} .scan-container-failed-text`).removeClass('d-none');
                        return;
                    }
                    
                    // No vulnerabilities
                    if (vulnerabilities.length === 0) {
                        scanContainerStatusText.html(`
                            <i class="fa-solid fa-check me-2"></i>
                            <span class="text-success">No vulnerabilities found</span>
                        `);
                        scanContainerStatusText.removeClass('d-none');

                        // Enable app deployment if there are no vulnerabilities
                        if (response.is_deployable && deployBtn.hasClass('disabled')) {
                            deployBtn.removeClass('disabled');
                        }
                        return;
                    }

                    // Get the highest severity level
                    let highestSeverityLevel = "Low";
                    for (let i = 0; i < vulnerabilities.length; i++) {
                        const vulnerability = vulnerabilities[i];
                        if (severityLevels[vulnerability.severity][0] > severityLevels[highestSeverityLevel][0]) {
                            highestSeverityLevel = vulnerability.severity;
                        }
                        if (highestSeverityLevel === "Critical") {
                            break;
                        }
                    }

                    // Display number of vulnerabilities and the highest severity level
                    scanContainerStatusText.html(`
                        <i class="fa-solid fa-exclamation-triangle me-2 ${severityLevels[highestSeverityLevel][1]}"></i>
                        <span class="${severityLevels[highestSeverityLevel][1]}">
                            ${vulnerabilities.length} vulnerabilities found with the highest severity level being <strong>${highestSeverityLevel}</strong>. 
                            More <a href="${scanReportUrl}" target="_blank">details</a>.
                        </span>
                    `);
                    scanContainerStatusText.removeClass('d-none');

                    // Enable app deployment if the vulnerabilities are acceptable, i.e. below the configured level
                    if (response.is_deployable) {
                        deployBtn.removeClass('disabled');
                    }
                });
            } else {
                // Hide the status text if it's displayed
                $('.scan-container-status-text').addClass('d-none');
                // Display a failed message on top of the container
                $('.scan-container-failed-text').removeClass('d-none');
            }
        },
        error: function(response) {
            console.log(response);
            // Hide the loading spinner
            $('.scan-container-loading-text').addClass('d-none');
            // Hide the status text if it's displayed
            $('.scan-container-status-text').addClass('d-none');
            // Display a failed message on top of the container
            $('.scan-container-failed-text').removeClass('d-none');
        }
    })
}

const appInit = () => {
    appFunctions();
    appContainerFunctions();
    appDeploymentFunctions();
    appFormInit();
    appGroupFunctions();
    appSummaryFunctions();
    appK8sResourcesManagerFunctions();
    harborProjectFunctions();
    manageAppFunctions();

    // Scan app containers when the page load
    scanContainers();
}

appInit();
