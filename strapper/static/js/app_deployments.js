import { appContainerUpdateFunctions } from "./app_containers.js";
import { autoSuggestTextInputsFunctions, initPasswordDisplay } from "./components.js";

export const appDeploymentFunctions = () => {
    // Initially hide all the alerts for app deployments
    $('.deploy-alert').hide();

    // Deploy an application when clicking on 'Deploy' button
    $('.deploy-btn').click(function() {
        
        const deployBtn = $('.deploy-btn');
        const appId = deployBtn.data('app-id');
        const stopBtn = $(`.deploy-stop-btn-${appId}`);

        // Check if the app is already deployed
        const isAppDeployed = $(`input.app-deployed-${appId}`).is(':checked');
        
        // Render a spinner and disable the "Deploy" button
        deployBtn.addClass('disabled');

        // Save the current texts on deploy and stop buttons. The buttons
        // will revert to these texts when the deployment is canceled
        window.localStorage.setItem(`${appId}-deploy-btn-text`, deployBtn.eq(0).text());
        window.localStorage.setItem(`${appId}-deploy-stop-btn-text`, stopBtn.eq(0).text());
        
        deployBtn.text('')
        if (!isAppDeployed) {
            deployBtn.append(`Deploying...<span class="spinner-border spinner-border-sm text-primary" role="status" aria-hidden="true"></span>`);
        } else {
            deployBtn.append(`Updating...<span class="spinner-border spinner-border-sm text-primary" role="status" aria-hidden="true"></span>`);
        }

        // Enable deployment log
        const deployLog = $(`.deploy-log-${appId}`);
        deployLog.empty();
        deployLog.append(`<a href="/deploy_log/${appId}" target="_blank">View</a>`);

        const successMessage = $(`.deploy-alert-success-${appId}`);
        const errorMessage = $(`.deploy-alert-error-${appId}`);
        const infoMessage = $(`.deploy-alert-info-${appId}`);

        // Enable the 'Stop' button, set the text to 'Cancel'
        stopBtn.text('Cancel');
        stopBtn.removeClass('disabled');

        // Disable "delete" and "edit" button for app summary when deploying the app
        const deleteBtn = $(`#${appId}-manage-tab-pane .app-delete-btn`);
        const editBtn = $(`#app-summary-${appId} .app-summary-edit-btn`)
        editBtn.addClass('disabled');
        deleteBtn.addClass('disabled');

        // Save the current state of the app to localstorage
        // The state is reverted back to this value when a deployment/update gets canceled
        const deployState = $(`.deploy-state-${appId}`)
        const deployStatus = $(`.deploy-status-${appId}`)
        window.localStorage.setItem(`${appId}-prev-state`, deployState.eq(0).text());

        // Change app state and status
        if (isAppDeployed) {
            deployState.text('deployed + started');
            deployStatus.text('updating');
        } else {
            deployState.text('started');
            deployStatus.text('deploying');
        }

        // Send the deploy request to Django backend
        $.ajax({
            type: 'GET',
            url: `/deploy/${appId}/`,
            success: function(response) {
                const exitCode = response.exit_code
                const deployed = response.deployed
                const message = response.message

                // If successfully deployed, render the success message
                if (exitCode === 0) {
                    if (deployed === 1) {
                        successMessage.text(message);
                        successMessage.show().delay(5000).fadeOut();
                        
                        // Update databases, database owner and initial password
                        // if this is the first successful deployment
                        if (!isAppDeployed) {
                            $.ajax({
                                type: 'GET',
                                url: `/app_info/${appId}/`,
                                data: {
                                    "X-Requested-With": "XMLHttpRequest"
                                },
                                success: function(response) {
                                    console.log(response);
                                    $(`.deploy-databases-${appId}`).text(response.databases ? response.databases : 'None');
                                    $(`.deploy-dbowner-${appId}`).text(response.dbowner ? response.dbowner : 'None');
                                    $(`.deploy-dbpassword-${appId}`).empty();
                                    $(`.deploy-dbpassword-${appId}`).append(
                                    response.dbpassword ? `
                                    <span class="password-display">
                                        <div class="password">****************</div>
                                        <pre class="text d-none"><code>${response.dbpassword}</code></pre>
                                        <div class="show-btn">Show</div>
                                    </span>
                                    ` : 'None'
                                    )
                                    if (response.dbpassword) {
                                        initPasswordDisplay();
                                    }
                                },
                                error: function(response) {
                                    console.log(response.errorMessage);
                                }
                            })
                        }
                    } else {
                        infoMessage.text(message);
                        infoMessage.show().delay(5000).fadeOut();
                    }
                    // Remove the spinner on 'Deploy' button and change text back to 'Deploy'/'Update'
                    deployBtn.empty();
                    deployBtn.text('Update');

                    deployBtn.removeClass("disabled");

                    // Display application deployment information
                    deployState.text('deployed');
                    deployStatus.text('succeeded');
                
                    // Change title of the stop button to 'Destroy'
                    // Clicking this button will destroy the resources for the app 
                    // created by Terraform
                    stopBtn.text('Stop');

                    // Update app URL
                    const appDomain = $('#strap-domain').val();
                    const appUrlHref = `${window.location.protocol}//${appId}.${appDomain}`;
                    const appUrl = $(`.app-url-${appId}`);
                    appUrl.empty();
                    appUrl.append(`<a href="${appUrlHref}" target="_blank">${appUrlHref}</a>`);

                    // Enable application log
                    const appLog = $(`.app-log-${appId}`);
                    appLog.empty();
                    appLog.append(`<a href="/app_log/${appId}/" target="_blank">View</a>`);

                    // Make app terminal available
                    const appTerminal = $(`.terminal-${appId}`);
                    appTerminal.empty();
                    appTerminal.text('');
                    appTerminal.append(`
                    <a href="/terminal/${appId}/" target="_blank">Open</a>
                    `)

                    // Enable "Edit" button
                    editBtn.removeClass("disabled");
                    
                    // App now has 1 active deployment
                    $(`input.app-deployed-${appId}`).prop('checked', true);
                }
                // Else render the error message
                // 0.5 indicates the deployment is canceled
                // This is when the deployment fails
                else if (exitCode !== 0.5){
                    errorMessage.text(message)
                    errorMessage.show().delay(5000).fadeOut();

                    // enable the 'Deploy'/'Update' button. Remove the spinner
                    // Display application deployment information - state and status
                    deployBtn.removeClass('disabled');
                    deployBtn.empty();
                    if (isAppDeployed) {
                        deployBtn.text('Update');
                        stopBtn.text('Revert');
                        deployState.text('deployed + failed');
                    }
                    // if the app has no active deployment
                    else {
                        deployBtn.text('Deploy');
                        stopBtn.text('Destroy');
                        deployState.text('failed');
                    }
                    deployStatus.text('failed');

                    // Enable "Edit" button
                    editBtn.removeClass('disabled');
                }
            },
            error: function(error) {
                console.log(error);
                const message = error.responseJSON && error.responseJSON.message ? error.responseJSON.message : 'Oops! Something went wrong. Please try again.';
                errorMessage.text(message);
                errorMessage.show().delay(5000).fadeOut();

                // enable the 'Deploy'/'Update' button. Remove the spinner
                // Display application deployment information - state and status
                deployBtn.removeClass('disabled');
                deployBtn.empty();
                if (isAppDeployed) {
                    deployBtn.text('Update');
                    stopBtn.text('Stop');
                    deployState.text('deployed + failed');
                }
                // if the app has no active deployment
                else {
                    deployBtn.text('Deploy');
                    // Disable 'Stop' button 
                    stopBtn.addClass('disabled');
                    stopBtn.text('Cancel');
                    
                    deployState.text('failed');
                    
                    // Enable "Delete" button
                    deleteBtn.removeClass('disabled');
                }
                deployStatus.text('failed');

                // Enable "Edit" button
                editBtn.removeClass('disabled');
            }
        })
    })

    $('.deploy-stop-btn').click(function() {

        const deployStopBtn = $('.deploy-stop-btn')
        const appId = deployStopBtn.data('app-id')
        const deployBtn = $(`.deploy-btn-${appId}`);
        
        // Check if the app is already deployed
        const isAppDeployed = $(`input.app-deployed-${appId}`).is(':checked');
        
        const deployState = $(`.deploy-state-${appId}`);
        const deployStatus = $(`.deploy-status-${appId}`);
        const oldDeployState = $(this).closest('.app-modal-body').find(`.deploy-state-${appId}`).text();
        const oldDeployStatus = $(this).closest('.app-modal-body').find(`.deploy-status-${appId}`).text();

        // Keep track of our action with the 'Stop'/'Cancel'/'Destroy' button
        let action = '';

        // Disable the Cancel/Stop/Destroy/Revert button
        deployStopBtn.addClass('disabled');
        // Disable the Deploy/Update button
        deployBtn.addClass('disabled');

        if ($(this).text() === 'Stop') {
            action = 'stop';
            // Render a spinner
            deployStopBtn.text('')
            deployStopBtn.append(`Stopping...<span class="spinner-border spinner-border-sm text-danger" role="status" aria-hidden="true"></span>`)
            // Change app state to "stopping", status to "stopping"
            deployState.text('stopped');
            deployStatus.text('stopping');
        } else if ($(this).text() === 'Destroy') {
            action = 'destroy';
            // Render a spinner
            deployStopBtn.text('')
            deployStopBtn.append(`Destroying...<span class="spinner-border spinner-border-sm text-danger" role="status" aria-hidden="true"></span>`)
            // Change app state to "None", status to "destroying"
            deployState.text('None');
            deployStatus.text('destroying');
        } else if ($(this).text() === 'Revert') {
            action = 'revert';
            // Render a spinner
            deployStopBtn.text('')
            deployStopBtn.append(`Reverting...<span class="spinner-border spinner-border-sm text-danger" role="status" aria-hidden="true"></span>`)
            // Change app state to "deployed", status to "reverting"
            deployState.text('deployed');
            deployStatus.text('reverting');
        } else {
            action = 'cancel';
            // Render a spinner
            deployStopBtn.text('')
            deployStopBtn.append(`Cancelling...<span class="spinner-border spinner-border-sm text-danger" role="status" aria-hidden="true"></span>`)
            // Change app state to whatever it was before deployed, status to "cancelling"
            deployStatus.text('cancelling');
        }

        // Disable "delete" and "edit" button for app summary when destroying/stopping the app
        const deleteBtn = $(`#${appId}-manage-tab-pane .app-delete-btn`);
        const editBtn = $(`#app-summary-${appId} .app-summary-edit-btn`)
        editBtn.addClass('disabled');
        deleteBtn.addClass('disabled');

        const successMessage = $(`.deploy-alert-success-${appId}`);
        const errorMessage = $(`.deploy-alert-error-${appId}`);
        const infoMessage = $(`.deploy-alert-info-${appId}`);

        // Send the stop request to Django backend
        $.ajax({
            type: 'GET',
            url: `/stop_deploy/${appId}/`,
            data: {
                "X-Requested-With": "XMLHttpRequest"
            },
            success: function(response) {
                const exitCode = response.exit_code
                const message = response.message
                const appConfigs = response.app_configs
                
                // If successfully stop the deployment, render the success message
                if (exitCode === 0) {
                    successMessage.text(message);
                    successMessage.show().delay(5000).fadeOut();
                
                    // Remove the spinner on 'Stop' button
                    deployStopBtn.empty();
                    // Enable 'Deploy' button
                    $(`.deploy-btn-${appId}`).removeClass('disabled');
                    
                    // Stop app pod on K8s cluster
                    if (action === 'stop') {
                        deployStopBtn.text('Destroy');
                        deployStopBtn.removeClass('disabled');
                        // Display application deployment information - state and status
                        $(`.deploy-state-${appId}`).text('stopped');
                        $(`.deploy-status-${appId}`).text('stopped');
                        
                        $(`.deploy-btn-${appId}`).text('Update');

                        // Enable "edit" button for app summary if app deployment is stopped/destroyed
                        editBtn.removeClass('disabled');

                        // Set app URL to "None"
                        const appUrl = $(`.app-url-${appId}`);
                        appUrl.empty();
                        appUrl.text('None');

                        // Set application log to "None"
                        const appLog = $(`.app-log-${appId}`);
                        appLog.empty();
                        appLog.text('None');

                        // Make app terminal unavailable
                        const appTerminal = $(`.terminal-${appId}`);
                        appTerminal.empty();
                        appTerminal.text('Unavaiable');
                    
                    // Cancel an ongoing deployment
                    } else if (action === 'cancel') {
                        // Display application deployment status
                        $(`.deploy-status-${appId}`).text('canceled');

                        // Enable "delete" and "edit" button for app summary if app deployment is stopped/destroyed
                        editBtn.removeClass('disabled');
                        deleteBtn.removeClass('disabled');

                        // Set app URL to "None"
                        const appUrl = $(`.app-url-${appId}`);
                        appUrl.empty();
                        appUrl.text('None');

                        // Set application log to "None"
                        const appLog = $(`.app-log-${appId}`);
                        appLog.empty();
                        appLog.text('None');

                        // Set state to what is was before deployed/updated
                        deployState.text(window.localStorage.getItem(`${appId}-prev-state`));

                        // Set stop and deploy buttons text to what they were before deployed
                        deployBtn.text(window.localStorage.getItem(`${appId}-deploy-btn-text`));
                        deployStopBtn.text(window.localStorage.getItem(`${appId}-deploy-stop-btn-text`));

                        // Enable stop/revert button if this app has an active deployment
                        if (isAppDeployed) {
                            deployStopBtn.removeClass('disabled');
                        }
                    
                    } else if (action === 'revert') {
                        deployStopBtn.text('Stop');
                        deployStopBtn.removeClass('disabled');
                        // Display application deployment information - state and status
                        $(`.deploy-state-${appId}`).text('deployed');
                        $(`.deploy-status-${appId}`).text('succeeded');

                        $(`.deploy-btn-${appId}`).text('Update');

                        // Enable "edit" button for app summary if app deployment is stopped/destroyed
                        editBtn.removeClass('disabled');
                        
                        // Update application fields with the new value
                        for (const[key, value] of Object.entries(appConfigs)) {
                            if (key === 'authenticated-routes' || key === 'unauthenticated-routes') {
                                $(`#app-summary-${appId} .${key}`).empty();
                                value.split(',').forEach(route => {
                                    if (route === '') {
                                        return;
                                    }
                                    $(`#app-summary-${appId} .${key}`).append(`<div class="route-tag mt-1">${route}</div>`)
                                })
                                $(`#config-app-form-${appId} .${key}`).empty();
                                value.split(',').forEach(route => {
                                    if (route === '') {
                                        return;
                                    }
                                    $(`#config-app-form-${appId} .${key}`).append(`<div class="route-tag mt-1"><span class="route-tag-path">${route}</span> <span class="route-tag-close">&#10005;</span></div>`)
                                })
                                $(`#config-app-form-${appId} .${key}-input`).val(value);
                            } else {
                                $(`#app-summary-${appId} .${key}`).text(value);
                                if (key === 'authentication') {
                                    $(`#config-app-form-${appId} .${key} option[value="${value}"]`).prop('selected', true);
                                } else {
                                    $(`#config-app-form-${appId} .${key}`).val(value);
                                    $(`#config-app-form-${appId} .${key}`).attr('value', value);
                                }
                            }
                        }

                        // Update application containers
                        const containers = appConfigs['containers'];
                        const containersHolder = $(`#${appId}-containers-tab-pane .app-containers-holder`);
                        const csrfToken = $('input#csrf-token').val();

                        containersHolder.empty();
                        for (const container of containers) {

                            // Environment variables in object format
                            const environmentVariables = {};
                            if (container['environment-variables'] !== "None") {
                                container['environment-variables'].replace('\r', '').split('\n').forEach(env => {
                                    if (env) {
                                        const [key, value] = env.split('==');
                                        environmentVariables[key] = value;
                                    }
                                })
                            }
                            

                            console.log(environmentVariables);

                            // Container display and edit form
                            containersHolder.append(
                            `
                            <div class="app-container" id="app-container-${ container.id }">
                                <div class="row mb-2">
                                    <div class="col-4 fw-semibold">Image</div>
                                    <div class="col-8 container-image">${ container.image }:${ container['image-tag'] }</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-4 fw-semibold">Exposed port</div>
                                    <div class="col-8 container-port">${ container.port }</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-4 fw-semibold">Runtime command</div>
                                    <div style="word-wrap: break-word;" class="col-8 container-command">${ container.command }</div>
                                </div>
                                <div class="row">
                                    <div class="col-4 fw-semibold">Environment variables</div>
                                    <div class="col-8"><pre class="mt-1 container-environment-variables">${ container['environment-variables'].replaceAll("==", "=") }</pre></div>
                                </div>
                                <div class="app-modal-btn-container">
                                    <button id="container-delete-modal-btn-${ container.id }" type="button" class="btn btn-outline-danger btn-sm delete-btn" data-bs-toggle="modal" data-bs-target="#containerDeleteModal-${ container.id }">Delete</button>
                                    <button type="button" class="btn btn-outline-primary btn-sm edit-btn" data-container-id="${ container.id }">Edit</button>
                                </div>
                            </div>
                            <div class="app-container edit-form" id="app-container-form-${ container.id }">
                                <form 
                                    action="/container/edit/${ appId }/${ container.id }/"
                                    data-container-id="${ container.id }"
                                    data-app-id="${ appId }"
                                    method="post">
                                    <input type="hidden" name="csrfmiddlewaretoken" value="${ csrfToken }">

                                    <h5 class="mb-3">Edit container ${ container.image }:${ container["image-tag"] }</h5>

                                    <div class="mb-3">
                                        <label class="form-label">Image (Required)</label>
                                        <input name="image" type="text" class="form-control container-image" value="${ container.image }" placeholder='e.g. repository/imagename' required>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Image tag</label>
                                        <input name="image_tag" type="text" class="form-control container-image-tag" maxlength="50" value="${ container["image-tag"] }" placeholder='e.g. latest (max 50 characters)'>
                                        <div class="form-text">Tag of the app image. Leaving this <strong>blank</strong> will set the tag to <strong>latest</strong></div>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Exposed port</label>
                                        <input name="port" type="number" class="form-control container-port" value="${ container.port === "None" ? "" : container.port }" placeholder='e.g. 4444' min='0' max='65536'>
                                        <div class="form-text">Container port</div>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Runtime command</label>
                                        <div class="auto-suggest">
                                            <input name="command" class='form-control container-command' type='text' value="${ container.command === "None" ? "" : container.command }" placeholder="e.g. docker-compose exec web python3 manage.py makemigrations"></input>
                                            <span class="suggestions" class="d-none"></span>
                                            <div class="mirror"></div>
                                        </div>
                                        <div class="form-text">Command to execute inside container</div>
                                    </div>

                                    <div class="mb-5 environment-variables-display">
                                        <label class="form-label">Environment variables</label>
                                        <div class="environment-variables-holder">
                                        ${Object.keys(environmentVariables).map(key => 
                                        `
                                        <div class="row mb-1 environment-variable">
                                            <div class="col">
                                                <label class="mb-1 text-secondary fs-90percent">Key</label>
                                                <input class="form-control key" value="${ key }" placeholder="e.g. CLIENT_KEY" required>
                                            </div>
                                            <div class="col">
                                                <label class="mb-1 text-secondary fs-90percent">Value</label>
                                                <div class="auto-suggest">
                                                    <input class="form-control value" value="${ environmentVariables[key] }">
                                                    <span class="suggestions" class="d-none"></span>
                                                    <div class="mirror"></div>
                                                </div>
                                            </div>
                                            <div class="col-1 d-flex flex-column justify-content-end">
                                                <i class="fa-solid fa-trash mt-auto mb-3 icon-btn-dark delete-btn"></i>
                                            </div>
                                        </div>
                                        `).join('')}
                                        </div>
                                        <button type="button" class="btn btn-sm btn-dark mt-3 add-btn"><i class="fa-solid fa-plus me-2"></i>Add another</button>
                                    </div>

                                    <div class="mb-3 d-none">
                                        <label class="form-label">Environment variables</label>
                                        <textarea name="environment_variables" class='form-control container-environment-variables' maxlength="6990" type='text' rows='10' placeholder="SECRET_KEY=thisisasupersecretkey">${ container['environment-variables'] === "None" ? "" : container['environment-variables'] }</textarea>
                                        <div class="form-text">Environment variables for runtime</div>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <div class="btn btn-danger app-container-edit-form-cancel" data-container-id="${ container.id }">Cancel</div>
                                </form>
                            </div>
                            `
                            )

                            // Container delete modal if not yet exists
                            if (!$(`#containerDeleteModal-${ container.id }`).length) {
                                $('body').append(
                                `
                                <div class="modal fade" id="containerDeleteModal-${ container.id }" tabindex="-1" aria-labelledby="containerDeleteModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="containerDeleteModalLabel">Delete container</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure you want to delete container <strong>${ container.image }:${ container["image-tag"] }</strong>?
                                                ${container.port == appConfigs['port'] ? `<div class="mt-2"><strong>Note:</strong> Your app (${ appConfigs['descriptive-name'] }) is exposing this container's port to external requests. Removing this container will result in no port being exposed for the app. You will have to set a new port for external requests to access the app.</div>` : ""}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                                <a href="/container/delete/${ appId }/${ container.id }/" class="btn btn-danger">Yes</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `
                                )
                            }
                        }
                    // Attach event listerners to app container cards
                    appContainerUpdateFunctions();
                    autoSuggestTextInputsFunctions();

                    // Destroy an entire all resources after stopping the app, including database and Keycloak
                    } else {
                        deployStopBtn.text('Cancel');
                        // Display application deployment information - state and status
                        $(`.deploy-state-${appId}`).text('None');
                        $(`.deploy-status-${appId}`).text('destroyed');
                        // Set database, db owner and password to 'None'
                        $(`.deploy-databases-${appId}`).text('None');
                        $(`.deploy-dbowner-${appId}`).text('None');
                        $(`.deploy-dbpassword-${appId}`).empty()
                        $(`.deploy-dbpassword-${appId}`).text('None');

                        $(`.deploy-btn-${appId}`).text('Deploy');

                        // Enable "delete" and "edit" button for app summary if app deployment is stopped/destroyed
                        editBtn.removeClass('disabled');
                        deleteBtn.removeClass('disabled');

                        // Set application log to "None"
                        const appLog = $(`.app-log-${appId}`);
                        appLog.empty();
                        appLog.text('None');

                        // App no longer has an active deployment
                        $(`input.app-deployed-${appId}`).prop('checked', false);
                    }
                }

                // Else render the error message
                else {
                    errorMessage.text(message)
                    errorMessage.show().delay(5000).fadeOut();
                
                    // Enable the Stop/Cancel/Destroy button
                    deployStopBtn.removeClass('disabled');
                    deployStopBtn.empty();
                    
                    if (action !== 'cancel') {
                        if (action === 'stop') {
                            deployStopBtn.text('Stop');
                        } else if (action === 'revert') {
                            deployStopBtn.text('Revert');
                        } else {
                            deployStopBtn.text('Destroy');
                        }
                        // Set state and status back to what they were
                        deployState.text(oldDeployState);
                        deployStatus.text(oldDeployStatus);
                    } else {
                        deployStopBtn.text('Cancel');
                    }
                    
                    // Enable "Edit" button
                    // TODO: Enable "Delete" button or no? Need to check if there is a previous deployment
                    // deleteBtn.removeClass('disabled');
                    editBtn.removeClass('disabled');
                }
                // Enable Deploy/Update button
                deployBtn.removeClass('disabled');
            },
            error: function(error) {
                console.log(error);
                const message = error.responseJSON && error.responseJSON.message ? error.responseJSON.message : `Failed to ${action} app deployment`;
                errorMessage.text(message);
                errorMessage.show().delay(5000).fadeOut();
            
                // Enable the Stop/Cancel/Destroy button
                deployStopBtn.removeClass('disabled');
                deployStopBtn.empty();

                if (action !== 'cancel') {
                    if (action === 'stop') {
                        deployStopBtn.text('Stop');
                    } else if (action === 'revert') {
                        deployStopBtn.text('Revert');
                    } else {
                        deployStopBtn.text('Destroy');
                    }
                    // Set state and status back to what they were
                    deployState.text(oldDeployState);
                    deployStatus.text(oldDeployStatus);
                } else {
                    deployStopBtn.text('Cancel');
                }

                // Enable "Edit" button
                // TODO: Enable "Delete" button or no? Need to check if there is a previous deployment
                // deleteBtn.removeClass('disabled');
                editBtn.removeClass('disabled');

                // Enable Deploy/Update button
                deployBtn.removeClass('disabled');
            }
        })
    })
}
