// All functions for app summaries
export const appSummaryFunctions = () => {
    // Intially hide edit form
    $('.app-edit-form').hide();

    // Show edit form, hide summary when clicking on 'Edit' button on the summary
    $('.app-summary-edit-btn').click(function() {
        const appSummary = $(this).closest('.app-summary')
        const appIdentifier = appSummary.data('app-id');
        const appEditForm = $(`#app-edit-form-${appIdentifier}`);
        appSummary.hide();
        appEditForm.fadeIn();
    })

    // Hide edit form, show summary when clicking on 'Cancel' on the edit form
    $('.app-summary-edit-cancel-btn').click(function() {
        const appEditForm = $(this).closest('.app-edit-form');
        const appIdentifier = appEditForm.data('app-id');
        const appSummary = $(`#app-summary-${appIdentifier}`);
        appEditForm.hide();
        appSummary.fadeIn();
    })
}
