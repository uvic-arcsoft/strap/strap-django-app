#!/bin/sh
# Migrate schema to the database
python manage.py migrate

# Start Gunicorn server
daphne -b 0.0.0.0 -p 8001 strapper.asgi:application --ping-timeout 25