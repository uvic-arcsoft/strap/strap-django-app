# pylint:
import os
from datetime import timedelta
from typing import Union, Any, Dict

from django.views import View
from django.urls import reverse
from django.http import JsonResponse, HttpRequest
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import redirect
from django.utils import timezone
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings

from normal_users.models import Application
from helpers.custom_json_response import (
    JsonResponseBadRequest,
    JsonResponseNotFound,
    JsonResponseForbidden,
    JsonResponseUnauthorized,
    JsonResponseOK
)
from .models import AppRoleGroup, TransferOwnershipRequest


class UserAPIView(View):
    """
    API for user queries
    """
    def get(self, request: HttpRequest) -> JsonResponse:
        """
        Get users
        """
        # Search users by first name, last name, or username
        # Receive 1 param:
        # - search: str - search query
        # Return a list of max 5 users that has either first name,
        # last name or username matching the search query
        if request.path == reverse('access:search_users'):
            # User must be authenticated
            if not request.user.is_authenticated:
                return JsonResponseUnauthorized("User not authenticated")

            # Search users by first name, last name, or username
            search = request.GET.get('search', None)
            if search is None:
                return JsonResponseBadRequest("Missing search query")

            users = User.objects.filter(
                Q(first_name__icontains=search) |
                Q(last_name__icontains=search) |
                Q(username__icontains=search))[:5]
            return JsonResponseOK(
                message="Users fetched successfully",
                users=[
                    {
                        'id': user.id,
                        'username': user.username,
                        'first_name': user.first_name,
                        'last_name': user.last_name,
                        'image': user.profile.get_image()
                    }
                    for user in users if user.username != request.user.username
                ]
            )

        return JsonResponseBadRequest("Invalid request")


class AccessAPIView(View):
    """
    API for access control
    """
    def post(self, request: HttpRequest) -> Union[JsonResponse, redirect]:
        """
        Update access for users to an app
        """
        # Update access for users to an app
        # Receive 3 params:
        # - app_id: str - app's identifier
        # - editors: str - comma-separated list of usernames of editors
        # - viewers: str - comma-separated list of usernames of viewers
        if request.path == reverse('access:update_app_access'):
            # User must be authenticated
            if not request.user.is_authenticated:
                return JsonResponseUnauthorized("User not authenticated")

            # Must provide app ID
            app = None
            app_id = request.POST.get('app_id', None)
            if app_id is None:
                return JsonResponseBadRequest("Missing app ID")
            try:
                # App must exist
                app = Application.objects.get(identifier=app_id)
            except Application.DoesNotExist:
                return JsonResponseNotFound("App does not exist")

            # User must the the owner of the app
            if app.owner.username != request.user.username:
                return JsonResponseForbidden("Unauthorized. User is not the app's owner")


            # Update app's editors
            editors = request.POST.get('editors', '').split(',')
            editor_group = AppRoleGroup.objects.get(app=app, role='editor')
            editor_group.users.clear()
            for editor in editors:
                if not editor:
                    continue
                try:
                    user = User.objects.get(username=editor)
                    editor_group.users.add(user)
                except User.DoesNotExist:
                    print(f"User {editor} does not exist to be added as an editor")
                    continue

            # Update app's viewers
            viewers = request.POST.get('viewers', []).split(',')
            viewer_group = AppRoleGroup.objects.get(app=app, role='viewer')
            viewer_group.users.clear()
            for viewer in viewers:
                if not viewer:
                    continue
                try:
                    user = User.objects.get(username=viewer)
                    viewer_group.users.add(user)
                except User.DoesNotExist:
                    print(f"User {viewer} does not exist to be added as a viewer")
                    continue

            request.session['active_tab'] = 'manage'
            request.session['success'] = True
            request.session['message'] = f"Update access for app <strong>{app.descriptive_name}</strong> successfully!"
            return redirect(request.META.get('HTTP_REFERER'))

        # App owner sends an email request to a new owner
        # for app ownership transfer
        # Receive 2 params:
        # - app_id: str - app's identifier
        # - new_owner: str - username of the new owner
        if request.path == reverse('access:email_ownership_transfer_request'):
            # User must authenticated
            if not request.user.is_authenticated:
                return JsonResponseUnauthorized("User not authenticated")

            # Must provide app ID
            app = None
            app_id = request.POST.get('app_id', None)
            if app_id is None:
                return JsonResponseBadRequest("Missing app ID")
            try:
                # App must exist
                app = Application.objects.get(identifier=app_id)
            except Application.DoesNotExist:
                return JsonResponseNotFound("App does not exist")

            # User must be the owner of the app
            if app.owner.username != request.user.username:
                return JsonResponseForbidden("Unauthorized. User is not the apps owner")

            # Must provide new owner
            new_owner = request.POST.get('new_owner', None)
            if new_owner is None:
                return JsonResponseBadRequest("Missing new owner username")

            # New owner must exist
            try:
                new_owner = User.objects.get(username=new_owner)
            except User.DoesNotExist:
                return JsonResponseNotFound("New owner does not exist")

            # Check if a transfer ownership request already exists for app
            transfer_request = None
            if TransferOwnershipRequest.objects.filter(app=app).exists():
                # If yes, update the request's receiver to the new owner
                transfer_request = TransferOwnershipRequest.objects.get(app=app)
                transfer_request.receiver = new_owner
                transfer_request.save()
            else:
                # If no, create a new request
                transfer_request = TransferOwnershipRequest.objects.create(
                    app=app,
                    receiver=new_owner,
                    accepted=False,
                    expiration_date=timezone.now() + timedelta(days=TransferOwnershipRequest.expiration_period)
                )

            # Send an email to the new owner to notify them of the request
            base_url = f"{request.scheme}://{request.get_host()}"
            email_html = render_to_string('access/emails/app_transfer_ownership.html', {
                'transfer_request': transfer_request,
                'base_url': base_url
            })
            if not settings.TESTING:
                send_mail(
                    f"Transfer ownership request for app {app.descriptive_name}",
                    "",     # Empty message body because we are using HTML content
                    os.environ.get('EMAIL_SENDER'),
                    [new_owner.username],
                    fail_silently=False,
                    html_message=email_html,
                )

            request.session['message'] = (f"{app.descriptive_name} transfer ownership request sent to "
                                          f"<strong>{new_owner.username}</strong>")
            request.session['success'] = True

            # Open the manage tab
            request.session['active_tab'] = 'manage'

            return redirect(request.META.get('HTTP_REFERER'))

        return JsonResponseBadRequest("Invalid request")


class TransferOwnershipAPIView(View):
    """
    API for transfer ownership requests
    """
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        """
        Accept or decline a transfer ownership request
        """
        app_id = kwargs.get('app_id', None)

        # Accept a transfer ownership request
        if request.path == reverse('access:accept_ownership_transfer_request', kwargs={'app_id': app_id}):
            # User must be authenticated
            if not request.user.is_authenticated:
                return JsonResponseUnauthorized("User not authenticated")

            # App id must be provided as a path parameter
            if app_id is None:
                return JsonResponseBadRequest("Missing app ID")

            # App must exist
            app = None
            try:
                app = Application.objects.get(identifier=app_id)
            except Application.DoesNotExist:
                return JsonResponseNotFound("App does not exist")

            # A transfer ownership request must exist for the app
            transfer_request = None
            try:
                transfer_request = TransferOwnershipRequest.objects.get(app=app)
            except TransferOwnershipRequest.DoesNotExist:
                return JsonResponseNotFound("Transfer ownership request does not exist")

            # Request must not be expired
            if transfer_request.is_expired():
                return JsonResponseBadRequest("Transfer ownership request has expired")

            # User must be the receiver of the request
            if transfer_request.receiver.username != request.user.username:
                return JsonResponseForbidden(
                    "Unauthorized. User is not the receiver of the request"
                )

            # Make the current owner an editor of the app and remove the current user from the editor group
            editor_group = AppRoleGroup.objects.get(app=app, role='editor')
            editor_group.users.add(app.owner)
            editor_group.users.remove(request.user)

            # Make the receiver the new owner of the app
            app.owner = transfer_request.receiver
            app.save()

            # Delete the request after it has been accepted
            transfer_request.delete()

            # Set success message
            request.session['success'] = True
            request.session['message'] = (f"Ownership of app <strong>{app.descriptive_name}</strong> "
                                          f"transferred to <strong>{app.owner.username}</strong>")

            return redirect(reverse('get_app', kwargs={'identifier': app_id}))

        # Delete a transfer ownership request
        # This can be done in 2 ways:
        # - The app owner cancels the ownership transfer
        # - The receiver of the request declines the ownership transfer
        if (request.path in [
            reverse('access:cancel_ownership_transfer_request', kwargs={'app_id': app_id}),
            reverse('access:decline_ownership_transfer_request', kwargs={'app_id': app_id})
            ]):
            # User must be authenticated
            if not request.user.is_authenticated:
                return JsonResponseUnauthorized("User not authenticated")

            # App id must be provided as a path parameter
            if app_id is None:
                return JsonResponseBadRequest("Missing app ID")

            # App must exist
            app = None
            try:
                app = Application.objects.get(identifier=app_id)
            except Application.DoesNotExist:
                return JsonResponseNotFound("App does not exist")

            # A transfer ownership request must exist for the app
            transfer_request = None
            try:
                transfer_request = TransferOwnershipRequest.objects.get(app=app)
            except TransferOwnershipRequest.DoesNotExist:
                return JsonResponseNotFound("Transfer ownership request does not exist")

            # Request must not be expired
            if transfer_request.is_expired():
                return JsonResponseBadRequest("Transfer ownership request has expired")

            # User must be the owner of the app or the receiver of the request
            if (request.user.username not in [app.owner.username, transfer_request.receiver.username]):
                return JsonResponseForbidden(
                    "Unauthorized. User is not the owner or receiver of the request"
                )

            # Delete the request
            transfer_request.delete()

            # Set success message for app owner in case of request cancellation
            if request.user.username == app.owner.username:
                request.session['success'] = True
                request.session['message'] = ("Ownership transfer request for app <strong>"
                                            f"{app.descriptive_name}</strong> cancelled")
                request.session['active_tab'] = request.GET.get('active_tab', None)
                return redirect(request.META.get('HTTP_REFERER'))

            # Set success message for receiver in case of request decline
            request.session['success'] = True
            request.session['message'] = ("Ownership transfer request for app <strong>"
                                        f"{app.descriptive_name}</strong> declined")
            return redirect(reverse('dashboard'))

        return JsonResponseBadRequest("Invalid request")
