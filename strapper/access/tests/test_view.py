# pylint:
from datetime import timedelta

from django.test import TestCase, Client, override_settings
from django.contrib.auth.models import User
from django.templatetags.static import static
from django.urls import reverse
from django.utils import timezone

from normal_users.models import Application
from access.models import Profile, AppRoleGroup, TransferOwnershipRequest

# pylint: disable=no-name-in-module
# TODO: Figure out why no-name-in-module is being raised
# when the code is working fine
from strapper.test_settings import TEST_SETTINGS
# pylint: enable=no-name-in-module

@override_settings(**TEST_SETTINGS)
class ViewTestCase(TestCase):
    def setUp(self):
        # Create 3 clients: app owner, an unauthenticated client and an unauthorized client
        self.app_owner = User.objects.create_user(
            username='app_owner',
            password='app_owner_password'
        )
        Profile.objects.create(user=self.app_owner)
        self.app_owner_client = Client(HTTP_X_FORWARDED_USER="app_owner")

        self.unauthenticated_user = User.objects.create_user(
            username='unauthenticated_user',
            password='unauthenticated_user_password',
            first_name='Unauthenticated',
            last_name='User'
        )
        Profile.objects.create(user=self.unauthenticated_user)
        self.unauthenticated_client = Client()

        self.unauthorized_user = User.objects.create_user(
            username='unauthorized_user',
            password='unauthorized_user_password',
            first_name='Unauthorized',
            last_name='User'
        )
        Profile.objects.create(user=self.unauthorized_user)
        self.unauthorized_client = Client(HTTP_X_FORWARDED_USER="unauthorized_user")

        # Create an application
        self.app = Application.objects.create(
            identifier='test_app',
            descriptive_name='Test Application',
            description='This is a test application',
            owner=self.app_owner
        )
        self.editor_group = AppRoleGroup.objects.get(
            app=self.app,
            role='editor'
        )
        self.viewer_group = AppRoleGroup.objects.get(
            app=self.app,
            role='viewer'
        )

        # Log in the app owner and the unauthorized user
        self.app_owner_client.get(reverse('login'), follow=True)
        self.unauthorized_client.get(reverse('login'), follow=True)


class UserAPIViewTest(ViewTestCase):
    """
    Test cases for UserAPIView
    """
    def test_search_users(self):
        """
        Test searching users by a query
        The returned users should have the query in their first name, last name or username
        and should not contain the user making the request
        """
        # Test when search query is missing
        response = self.app_owner_client.get(reverse('access:search_users'), follow=True)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {'status': 400, 'title': 'Bad Request', 'message': 'Missing search query'})

        # Test when search query is provided but no users are found
        response = self.app_owner_client.get(reverse('access:search_users') + '?search=someone', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'status': 200, 'title': 'OK', 'message': 'Users fetched successfully', 'users': []
        })

        # Test when search query matches the user making the request
        response = self.app_owner_client.get(
            reverse('access:search_users') + f'?search={self.app_owner.username}', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'status': 200, 'title': 'OK', 'message': 'Users fetched successfully', 'users': []
        })

        # Test when search query matches 2 users that are not the user making the request
        response = self.app_owner_client.get(reverse('access:search_users') + '?search=user', follow=True)
        self.assertEqual(response.status_code, 200)
        result = response.json()
        self.assertIn({
            'id': self.unauthenticated_user.id,
            'username': self.unauthenticated_user.username,
            'first_name': self.unauthenticated_user.first_name,
            'last_name': self.unauthenticated_user.last_name,
            'image': static(Profile.DEFAULT_IMAGE)
        }, result['users'])
        self.assertIn({
            'id': self.unauthorized_user.id,
            'username': self.unauthorized_user.username,
            'first_name': self.unauthorized_user.first_name,
            'last_name': self.unauthorized_user.last_name,
            'image': static(Profile.DEFAULT_IMAGE)
        }, result['users'])

class AccessAPIViewTest(ViewTestCase):
    """
    Test cases for AccessAPIView
    """
    def test_update_app_access(self):
        """
        Test updating access for users to an app
        """
        # Test when app ID is missing
        response = self.app_owner_client.post(
            reverse('access:update_app_access'),
            follow=True,
            HTTP_REFERER=reverse("get_app", args=[self.app.identifier]))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {'status': 400, 'title': 'Bad Request', 'message': 'Missing app ID'})

        # Test when app ID is provided but app does not exist
        response = self.app_owner_client.post(
            reverse('access:update_app_access'), {'app_id': 'nonexistent'},
            follow=True,
            HTTP_REFERER=reverse("get_app", args=[self.app.identifier]))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {'status': 404, 'title': 'Not Found', 'message': 'App does not exist'})

        # Test when app ID is provided but user is not the owner of the app
        response = self.unauthorized_client.post(
            reverse('access:update_app_access'), {'app_id': self.app.identifier},
            follow=True,
            HTTP_REFERER=reverse("get_app", args=[self.app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': "Unauthorized. User is not the app's owner"
        })

        # Create 2 users for testing. One is an editor and the other is a viewer
        editor = User.objects.create_user(
            username='editor',
            password='editor_password',
            first_name='Editor',
            last_name='User'
        )

        viewer = User.objects.create_user(
            username='viewer',
            password='viewer_password',
            first_name='Viewer',
            last_name='User'
        )

        # Test when app ID is provided and user is the owner of the app
        response = self.app_owner_client.post(
            reverse('access:update_app_access'),
            {
                'app_id': self.app.identifier,
                'editors': f'{editor.username}',
                'viewers': f'{viewer.username}'
            },
            follow=True,
            HTTP_REFERER=reverse("get_app", args=[self.app.identifier])
        )
        # Should succeed and redirect to app's page
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/app.html')
        context_data = response.context_data
        self.assertEqual(context_data['active_tab'], 'manage')
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            f"Update access for app <strong>{self.app.descriptive_name}</strong> successfully!")

        # Check if the editor and viewer are added to the app's editor and viewer groups respectively
        editor_group = AppRoleGroup.objects.get(app=self.app, role='editor')
        self.assertEqual(editor_group.users.count(), 1)
        self.assertIn(editor, editor_group.users.all())

        viewer_group = AppRoleGroup.objects.get(app=self.app, role='viewer')
        self.assertEqual(viewer_group.users.count(), 1)
        self.assertIn(viewer, viewer_group.users.all())

    def test_email_ownership_transfer_request(self):
        """
        Test sending an email to request ownership transfer of an app.
        Email is disabled for testing, but a request must be created properly
        """
        with self.settings(TESTING=True):
            # Test when app ID is missing
            response = self.app_owner_client.post(
                reverse('access:email_ownership_transfer_request'),
                follow=True,
                HTTP_REFERER=reverse("get_app", args=[self.app.identifier]))
            self.assertEqual(response.status_code, 400)
            self.assertEqual(response.json(), {'status': 400, 'title': 'Bad Request', 'message': 'Missing app ID'})

            # Test when app ID is provided but app does not exist
            response = self.app_owner_client.post(
                reverse('access:email_ownership_transfer_request'), {'app_id': 'nonexistent'},
                follow=True,
                HTTP_REFERER=reverse("get_app", args=[self.app.identifier]))
            self.assertEqual(response.status_code, 404)
            self.assertEqual(response.json(), {'status': 404, 'title': 'Not Found', 'message': 'App does not exist'})

            # Test when app ID is provided but user is not the owner of the app
            response = self.unauthorized_client.post(
                reverse('access:email_ownership_transfer_request'), {'app_id': self.app.identifier},
                follow=True,
                HTTP_REFERER=reverse("get_app", args=[self.app.identifier]))
            self.assertEqual(response.status_code, 403)
            self.assertEqual(response.json(), {
                'status': 403, 'title': 'Forbidden', 'message': 'Unauthorized. User is not the apps owner'
            })

            # Test when app ID is provided and user is the owner of the app
            # but missing new owner's username
            response = self.app_owner_client.post(
                reverse('access:email_ownership_transfer_request'),
                {'app_id': self.app.identifier},
                follow=True,
                HTTP_REFERER=reverse("get_app", args=[self.app.identifier])
            )
            self.assertEqual(response.status_code, 400)
            self.assertEqual(response.json(), {
                'status': 400, 'title': 'Bad Request', 'message': 'Missing new owner username'
            })

            # Test when app ID is provided and user is the owner of the app
            # but new owner's username does not exist
            response = self.app_owner_client.post(
                reverse('access:email_ownership_transfer_request'),
                {'app_id': self.app.identifier, 'new_owner': 'nonexistent'},
                follow=True,
                HTTP_REFERER=reverse("get_app", args=[self.app.identifier])
            )
            self.assertEqual(response.status_code, 404)
            self.assertEqual(response.json(), {
                'status': 404, 'title': 'Not Found', 'message': 'New owner does not exist'
            })

            # Test when app ID is provided and user is the owner of the app
            # and new owner's username exists. Should be valid
            new_owner = User.objects.create_user(
                username='new_owner',
                password='new_owner_password',
                first_name='New',
                last_name='Owner'
            )
            Profile.objects.create(user=new_owner)
            response = self.app_owner_client.post(
                reverse('access:email_ownership_transfer_request'),
                {'app_id': self.app.identifier, 'new_owner': new_owner.username},
                follow=True,
                HTTP_REFERER=reverse("get_app", args=[self.app.identifier])
            )

            # Should succeed and redirect to app's page
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'normal_users/app.html')
            context_data = response.context_data
            self.assertEqual(context_data['active_tab'], 'manage')
            self.assertEqual(context_data['success'], True)
            self.assertEqual(
                context_data['message'],
                (f"{self.app.descriptive_name} transfer ownership request sent to "
                                          f"<strong>{new_owner.username}</strong>"))

            # Check if a transfer ownership request is created
            self.assertTrue(TransferOwnershipRequest.objects.filter(app=self.app).exists())

            # Check information of the newly created transfer ownership request
            transfer_request = TransferOwnershipRequest.objects.get(app=self.app)
            self.assertEqual(transfer_request.receiver, new_owner)
            self.assertFalse(transfer_request.accepted)
            self.assertFalse(transfer_request.is_expired())
            self.assertEqual(transfer_request.days_left(), TransferOwnershipRequest.expiration_period)


class TransferOwnershipAPIView(ViewTestCase):
    """
    Test cases for TransferOwnershipAPIView
    """
    def test_accept_ownership_transfer_request(self):
        """
        Test accepting a transfer ownership request
        """
        # Test providing a non-existent app ID. Expecting 404
        response = self.unauthorized_client.get(
            reverse('access:accept_ownership_transfer_request', kwargs={'app_id': 'nonexistent'}),
            follow=True
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {'status': 404, 'title': 'Not Found', 'message': 'App does not exist'})

        # Test providing a valid app ID but app has no transfer ownership request. Expecting 404
        response = self.unauthorized_client.get(
            reverse('access:accept_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {
            'status': 404, 'title': 'Not Found', 'message': 'Transfer ownership request does not exist'
        })

        # Test providing a valid app ID but app has an expired transfer ownership request. Expecting 400
        transfer_request = TransferOwnershipRequest.objects.create(
            app=self.app,
            receiver=self.unauthorized_user,
            expiration_date=timezone.now() - timedelta(hours=1)
        )
        response = self.unauthorized_client.get(
            reverse('access:accept_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {
            'status': 400, 'title': 'Bad Request', 'message': 'Transfer ownership request has expired'
        })

        # Test providing a valid app ID but user is not the receiver of the transfer ownership request. Expecting 403
        transfer_request.expiration_date = timezone.now() + timedelta(days=TransferOwnershipRequest.expiration_period)
        transfer_request.save()
        response = self.app_owner_client.get(
            reverse('access:accept_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.json(),
            {'status': 403, 'title': 'Forbidden', 'message': 'Unauthorized. User is not the receiver of the request'})

        # Test providing a valid app ID and user is the receiver of the transfer ownership request. Expecting 200
        response = self.unauthorized_client.get(
            reverse('access:accept_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/app.html')
        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            f"Ownership of app <strong>{self.app.descriptive_name}</strong> transferred to "
            f"<strong>{self.unauthorized_user.username}</strong>")

        # The transfer ownership request should be deleted
        self.assertFalse(TransferOwnershipRequest.objects.filter(app=self.app).exists())

        # The app owner should be the user who accepted the transfer ownership request
        self.app.refresh_from_db()
        self.assertEqual(self.app.owner, self.unauthorized_user)

    def test_decline_ownership_transfer_request(self):
        """
        Test declining a transfer ownership request
        """
        # Test providing a non-existent app ID. Expecting 404
        response = self.unauthorized_client.get(
            reverse('access:decline_ownership_transfer_request', kwargs={'app_id': 'nonexistent'}),
            follow=True,
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {'status': 404, 'title': 'Not Found', 'message': 'App does not exist'})

        # Test providing a valid app ID but app has no transfer ownership request. Expecting 404
        response = self.unauthorized_client.get(
            reverse('access:decline_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {
            'status': 404, 'title': 'Not Found', 'message': 'Transfer ownership request does not exist'
        })

        # Test providing a valid app ID but app has an expired transfer ownership request. Expecting 400
        transfer_request = TransferOwnershipRequest.objects.create(
            app=self.app,
            receiver=self.unauthorized_user,
            expiration_date=timezone.now() - timedelta(hours=1)
        )
        response = self.unauthorized_client.get(
            reverse('access:decline_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {
            'status': 400, 'title': 'Bad Request', 'message': 'Transfer ownership request has expired'
        })

        # Test providing a valid app ID and user is the receiver of the transfer ownership request. Expecting 200
        transfer_request.expiration_date = timezone.now() + timedelta(days=TransferOwnershipRequest.expiration_period)
        transfer_request.save()
        response = self.unauthorized_client.get(
            reverse('access:decline_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/dashboard/index.html')
        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            f"Ownership transfer request for app <strong>{self.app.descriptive_name}</strong> declined")

        # The transfer ownership request should be deleted
        self.assertFalse(TransferOwnershipRequest.objects.filter(app=self.app).exists())

        # The app owner should remain the same
        self.app.refresh_from_db()
        self.assertEqual(self.app.owner, self.app_owner)

    def test_cancel_ownership_transfer_request(self):
        """
        Test cancelling a transfer ownership request
        """
        # Test providing a non-existent app ID. Expecting 404
        response = self.app_owner_client.get(
            reverse('access:cancel_ownership_transfer_request', kwargs={'app_id': 'nonexistent'}),
            follow=True,
            HTTP_REFERER=reverse("get_app", args=[self.app.identifier])
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {'status': 404, 'title': 'Not Found', 'message': 'App does not exist'})

        # Test providing a valid app ID but app has no transfer ownership request. Expecting 404
        response = self.app_owner_client.get(
            reverse('access:cancel_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True,
            HTTP_REFERER=reverse("get_app", args=[self.app.identifier])
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {
            'status': 404, 'title': 'Not Found', 'message': 'Transfer ownership request does not exist'
        })

        # Test providing a valid app ID but app has an expired transfer ownership request. Expecting 400
        transfer_request = TransferOwnershipRequest.objects.create(
            app=self.app,
            receiver=self.unauthorized_user,
            expiration_date=timezone.now() - timedelta(hours=1)
        )
        response = self.app_owner_client.get(
            reverse('access:cancel_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True,
            HTTP_REFERER=reverse("get_app", args=[self.app.identifier])
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {
            'status': 400, 'title': 'Bad Request', 'message': 'Transfer ownership request has expired'
        })

        # Test providing a valid app ID and user is the owner of the app. Expecting 200
        transfer_request.expiration_date = timezone.now() + timedelta(days=TransferOwnershipRequest.expiration_period)
        transfer_request.save()
        response = self.app_owner_client.get(
            reverse('access:cancel_ownership_transfer_request', kwargs={'app_id': self.app.identifier}),
            follow=True,
            HTTP_REFERER=reverse("get_app", args=[self.app.identifier])
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/app.html')
        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            f"Ownership transfer request for app <strong>{self.app.descriptive_name}</strong> cancelled")

        # The transfer ownership request should be deleted
        self.assertFalse(TransferOwnershipRequest.objects.filter(app=self.app).exists())

        # The app owner should remain the same
        self.app.refresh_from_db()
        self.assertEqual(self.app.owner, self.app_owner)
