# pylint:
from datetime import timedelta

from django.test import TestCase
from django.contrib.auth.models import User
from django.templatetags.static import static
from django.utils import timezone

from normal_users.models import Application
from access.models import Profile, AppRoleGroup, TransferOwnershipRequest


class ProfileModelTest(TestCase):
    """
    Test cases for Profile model
    This model holds additional information about a user
    """

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            password='testpassword'
        )

        self.profile = Profile.objects.create(
            user=self.user,
            bio='This is a test bio',
            image='test_image.jpg'
        )

    def test_profile_creation(self):
        """
        Test Profile model creation
        """
        self.assertEqual(self.profile.user.username, 'testuser')
        self.assertEqual(self.profile.bio, 'This is a test bio')
        self.assertEqual(self.profile.image, 'test_image.jpg')

    def test_profile_str(self):
        """
        Test Profile model string representation (__str__)
        """
        self.assertEqual(str(self.profile), 'testuser Profile')

    def test_profile_to_dict(self):
        """
        Test Profile model to_dict method
        """
        self.assertEqual(self.profile.to_dict(), {
            'bio': 'This is a test bio',
            'image': 'test_image.jpg'
        })

    def test_profile_get_image(self):
        """
        Test Profile model get_image method
        """
        # Test when image is set
        self.assertEqual(self.profile.get_image(), 'test_image.jpg')

        # Test when image is not set
        self.profile.image = None
        self.profile.save()
        self.assertEqual(self.profile.get_image(), static(Profile.DEFAULT_IMAGE))


class AppRoleGroupModelTest(TestCase):
    """
    Test cases for AppRoleGroup model
    This model stores users that have a certain role in an app (editor/viewer)
    """

    def setUp(self):
        # Create 3 users with 3 roles: app owner, editor, viewer
        self.user = User.objects.create_user(
            username='appowner',
            password='appownerpassword'
        )
        self.editor = User.objects.create_user(
            username='editor',
            password='editorpassword'
        )
        self.viewer = User.objects.create_user(
            username='viewer',
            password='viewerpassword'
        )

        self.app = Application.objects.create(
            identifier='testapp',
            descriptive_name='Test Application',
            description='This is a test application',
            owner=self.user
        )

        self.editor_group = AppRoleGroup.objects.create(
            app=self.app,
            role='editor'
        )
        self.editor_group.users.add(self.editor)

        self.viewer_group = AppRoleGroup.objects.create(
            app=self.app,
            role='viewer'
        )
        self.viewer_group.users.add(self.viewer)

    def test_app_role_group_creation(self):
        """
        Test AppRoleGroup model creation
        """
        self.assertEqual(self.editor_group.app.descriptive_name, 'Test Application')
        self.assertEqual(self.editor_group.role, 'editor')
        self.assertEqual(self.editor_group.users.count(), 1)
        self.assertEqual(self.viewer_group.app.descriptive_name, 'Test Application')
        self.assertEqual(self.viewer_group.role, 'viewer')
        self.assertEqual(self.viewer_group.users.count(), 1)

    def test_app_role_group_str(self):
        """
        Test AppRoleGroup model string representation (__str__)
        """
        self.assertEqual(str(self.editor_group), 'Test Application - editor')
        self.assertEqual(str(self.viewer_group), 'Test Application - viewer')


class TransferOwnershipRequestModelTest(TestCase):
    """
    Test cases for TransferOwnershipRequest model
    This model represents a request to transfer ownership of an app
    """

    def setUp(self):
        self.user = User.objects.create_user(
            username='appowner',
            password='appownerpassword'
        )

        self.receiver = User.objects.create_user(
            username='receiver',
            password='receiverpassword'
        )

        self.app = Application.objects.create(
            identifier='testapp',
            descriptive_name='Test Application',
            description='This is a test application',
            owner=self.user
        )

        self.request = TransferOwnershipRequest.objects.create(
            app=self.app,
            receiver=self.receiver,
            expiration_date=timezone.now() + timedelta(days=TransferOwnershipRequest.expiration_period)
        )

    def test_transfer_ownership_request_creation(self):
        """
        Test TransferOwnershipRequest model creation
        """
        self.assertEqual(self.request.app.descriptive_name, 'Test Application')
        self.assertEqual(self.request.receiver.username, 'receiver')
        self.assertFalse(self.request.accepted)

    def test_transfer_ownership_request_str(self):
        """
        Test TransferOwnershipRequest model string representation (__str__)
        """
        self.assertEqual(str(self.request), 'Test Application Ownership Transfer Request')

    def test_transfer_ownership_request_is_expired(self):
        """
        Test TransferOwnershipRequest model is_expired method
        """
        self.assertFalse(self.request.is_expired())

        # Change expiration date to a past date
        self.request.expiration_date = timezone.now() - timedelta(hours=1)
        self.request.save()
        self.assertTrue(self.request.is_expired())

    def test_transfer_ownership_request_days_left(self):
        """
        Test TransferOwnershipRequest model days_left method
        """
        self.assertEqual(self.request.days_left(), TransferOwnershipRequest.expiration_period)

        # Change expiration date to a past date
        self.request.expiration_date = timezone.now() - timedelta(hours=1)
        self.request.save()
        self.assertEqual(self.request.days_left(), 0)
