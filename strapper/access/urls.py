# pylint:
from django.urls import path
from . import views

app_name = 'access'

urlpatterns = [
    path('search_users/', views.UserAPIView.as_view(), name='search_users'),
    path('update_app_access/', views.AccessAPIView.as_view(), name='update_app_access'),
    path(
        'email_ownership_transfer_request/',
        views.AccessAPIView.as_view(),
        name='email_ownership_transfer_request'),
    path(
        'accept_ownership_transfer_request/<slug:app_id>/',
        views.TransferOwnershipAPIView.as_view(),
        name='accept_ownership_transfer_request'),
    path(
        'cancel_ownership_transfer_request/<slug:app_id>/',
        views.TransferOwnershipAPIView.as_view(),
        name='cancel_ownership_transfer_request'),
    path(
        'decline_ownership_transfer_request/<slug:app_id>/',
        views.TransferOwnershipAPIView.as_view(),
        name='decline_ownership_transfer_request'),
]
