# pylint:
from django.contrib import admin
from .models import Profile, AppRoleGroup, TransferOwnershipRequest

# Register your models here.
admin.site.register(Profile)
admin.site.register(AppRoleGroup)
admin.site.register(TransferOwnershipRequest)
