# pylint: disable=invalid-sequence-index,too-many-ancestors
# - invalid-sequence-index: sequence index is a Django model IntegerField,
#   which is an int
# - too-many-ancestors: inheriting from Django's model classes and we have
#   no control over ancestors or depth of inheritance
from django.db import models
from django.contrib.auth.models import User
from django.templatetags.static import static
from django.utils import timezone

from normal_users.models import Application

class Roles(models.TextChoices):
    EDITOR = 'editor', 'Editor'
    VIEWER = 'viewer', 'Viewer'

class Profile(models.Model):
    """
    Richer information about an user
    """
    DEFAULT_IMAGE = 'img/hamster_on_rocket.webp'

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, default='', blank=True)
    image = models.CharField(max_length=500, null=True, blank=True)

    class Meta:
        db_table = 'profiles'

    def __str__(self):
        return f'{self.user.username} Profile'

    def to_dict(self):
        return {
            'bio': self.bio,
            'image': self.image
        }

    def get_image(self):
        """
        Return path to user's profile image
        """
        return self.image if self.image else static(self.DEFAULT_IMAGE)

class AppRoleGroup(models.Model):
    """
    Store users that have a certain role in an app (editor/viewer)
    """
    app = models.ForeignKey(Application, on_delete=models.CASCADE)
    role = models.CharField(max_length=20, choices=Roles.choices)
    users = models.ManyToManyField(User)

    class Meta:
        db_table = 'app_role_groups'

    def __str__(self):
        return f'{self.app.descriptive_name} - {self.role}'

class TransferOwnershipRequest(models.Model):
    """
    A request to transfer ownership of an app
    """

    expiration_period = 30      # in days

    app = models.OneToOneField(Application, on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, on_delete=models.CASCADE)
    accepted = models.BooleanField(default=False)
    expiration_date = models.DateTimeField()

    class Meta:
        db_table = 'transfer_ownership_requests'

    def __str__(self):
        return f'{self.app.descriptive_name} Ownership Transfer Request'

    def is_expired(self):
        """
        Check if the request has expired. Delete it if it has
        """
        if self.expiration_date < timezone.now():
            self.delete()
            return True
        return False

    def days_left(self):
        """
        Return the number of days (rounded up) left before the request expires
        """
        return (self.expiration_date - timezone.now()).days + 1
