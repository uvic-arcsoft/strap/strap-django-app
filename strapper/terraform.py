# pylint:
import sys
import os
import argparse
import json
import subprocess
import environ

from exceptions import DeploymentFailure, DestroyDeploymentFailure

import django
django.setup()

# NOTE: settings is only avaiable after django.setup() is called
# pylint: disable=wrong-import-position
from django.conf import settings

# Change the line below if need to specify the path of .env file
# For example: environ.Env.read_env(os.path.join(BASE_DIR, '.env'))
environ.Env.read_env()

# TODO: Has to import this here as django.setup() needs to be called before
# Application model can be imported
# pylint: disable-next=C0413
from normal_users.models import Application, Deployment
from admin_users.models import (GeneralConfig, DatabaseConfig, KeycloakConfig,
                                ObjectStorageConfig, K8sConfig)

BASE_DIR = '/tmp/terraform'

# Get the application name from command line argument -a
parser = argparse.ArgumentParser(description='Fake running terraform')
parser.add_argument('-a', '--app', required=True, help='Specify app name to run terraform on')
parser.add_argument('-d', '--destroy', required=False, default=0,
    help='Set this to a non-zero value will destroy \
    the resources for this app created by Terraform. Default is 0')
parser.add_argument('-r', '--revert', required=False, default=0,
    help='Set this to a non-zero value will revert \
    the resources created or modified for this app in the most recent Terraform apply. Default is 0')
parser.add_argument('-s', '--stop', required=False, default=0,
    help='Set this to a non-zero value will destroy \
    the resources created or modified for this app by Helm using Terraform. Default is 0')
parser.add_argument('-l', '--log', required=False, default=0,
    help='Log helm_release resource for an application. Default is 0')
parser.add_argument('-t', '--test', required=False, default=0,
    help='If set to a non-zero, the file is run in test mode. Default is 0')

args = vars(parser.parse_args())
app_id = args['app']

# destroy mode
destroy = args['destroy']

# revert mode
revert = args['revert']

# stop mode
stop = args['stop']

# log mode
log = args['log']

# test mode
test = args['test']

# Fake run terraform in unit test environment
if test:
    if app_id == 'successfulapp':
        if stop:
            print(f'Stopped app {app_id} successfully')
        elif destroy:
            print(f'Destroyed app {app_id} successfully')
        elif revert:
            print(f'Reverted app {app_id} successfully')
        else:
            deployed_log =\
f'''Deployed app {app_id} successfully
                
postgres = {{
    "databases" = [
        "{app_id}_prod",
        "{app_id}_dev",
    ]
    "roles" = {{
        "{app_id}_owner" = "ownerpassword"
        "{app_id}_ro" = "ropassword"
        "{app_id}_rw" = "rwpassword"
    }}
}}'''
            print(deployed_log)
    else:
        if stop:
            print(f'Failed to stop app {app_id}')
            raise DeploymentFailure(description=f'Failed to stop app {app_id}')
        if destroy:
            print(f'Failed to destroy app {app_id}')
            raise DeploymentFailure(description=f'Failed to destroy app {app_id}')
        if revert:
            print(f'Failed to revert app {app_id}')
            raise DeploymentFailure(description=f'Failed to revert app {app_id}')
        print(f'Failed to deploy app {app_id}')
        raise DeploymentFailure(description=f'Failed to deploy app {app_id}')
    sys.exit()

# All admin configurations
general_config = GeneralConfig.get_instance()
database_config = DatabaseConfig.get_instance(database_pk="psql")
keycloak_config = KeycloakConfig.get_instance()
object_storage_config_strap = ObjectStorageConfig.get_instance(object_storage_pk="strap")
k8s_config = K8sConfig.get_instance()

app_path = f"{BASE_DIR}/{app_id}"

# create Terraform variable definitions file in JSON
testing = False
app = Application.objects.get(identifier=app_id)
if revert:
    app.copy_last_successful_deployment()

tfvars = app.build_configuration()
deploy_vars = {
    # "source": "git::https://gitlab.com/uvic-arcsoft/strap/terraform.git?depth=1&ref=manage-harbor-separately",
    # "helm_chart": "https://gitlab.com/uvic-arcsoft/strap/helm/-/package_files/74937592/download?ref=i3-multiple-containers-app", # pylint: disable=line-too-long
    # "helm_chart": "/usr/src/app/helm",
    "source": general_config.terraform_module,
    "helm_chart": general_config.helm_chart,
    "image_pull_policy": "Always",
    "domain": general_config.domain,
    "database_host": database_config.database_host,
    "database_name": database_config.database_name,
    "database_username": database_config.database_username,
    "database_password": database_config.database_password,
    # "keycloak_realm": "",
    "keycloak_client_id": keycloak_config.keycloak_client_id,
    "keycloak_client_secret": keycloak_config.keycloak_client_secret,
    "keycloak_url": keycloak_config.keycloak_url,
    "keycloak_base_path": keycloak_config.keycloak_base_path,
    "kubeconfig": f"{os.environ.get('KUBE_CONFIG_DIR', '~/.kube')}/config"
}
deploy_vars.update(tfvars)
app_tfconfig = {
    "module": {
        "strap": deploy_vars
    },
    "output": {
        "postgres": {
            "value": "${module.strap.postgres}"
        },
        # "registry": {
        #     "value": "${module.strap.registry}",
        #     "sensitive": True
        # }
    }
}

print(f'tfvars: {tfvars}')
# Cannot use "with" because temporary file has to exist when running
# Terraform and can only be deleted afterwards.
# pylint: disable=consider-using-with
config_file = open(f"{app_path}/app.tf.json", "w", encoding="utf-8")
print("Terraform config file: %s", config_file.name)
json.dump(app_tfconfig, config_file, ensure_ascii=False, indent=4)
config_file.close()

# Write Terraform config file for storing state files in S3 in Minio server
if os.environ.get("TERRAFORM_BACKEND_ENABLED", "False") == "True":
    # Export neccesary environment variables for Terraform backend
    # based on the configurations we have in Strapper
    # There environment variables only last for the current process and its subprocesses
    os.environ["AWS_ACCESS_KEY_ID"] = object_storage_config_strap.object_storage_access_key
    os.environ["AWS_SECRET_ACCESS_KEY"] = object_storage_config_strap.object_storage_secret_key
    os.environ["AWS_ENDPOINT_URL_S3"] = object_storage_config_strap.object_storage_url
    os.environ["AWS_REGION"] = object_storage_config_strap.object_storage_region

    backend_tfconfig = {
        "terraform": {
            "backend": {
                "s3": {
                    "bucket": object_storage_config_strap.object_storage_bucket,
                    "key": f"{app_id}.tfstate",
                    "skip_region_validation": "true",
                    "skip_requesting_account_id": "true",
                    "use_path_style": "true",
                    "skip_credentials_validation": "true",
                    "skip_metadata_api_check": "true",
                    "skip_s3_checksum": "true"
                }
            }
        }
    }
    backend_config_file = open(f"{app_path}/backend.tf.json", "w", encoding="utf-8")
    json.dump(backend_tfconfig, backend_config_file, ensure_ascii=False, indent=4)
    backend_config_file.close()

# Write a kube config file if not exists
kubeconfig_dir = os.environ.get('KUBE_CONFIG_DIR', '~/.kube')
kubeconfig_path = f'{kubeconfig_dir}/config'
if not os.path.exists(kubeconfig_dir):
    os.mkdir(kubeconfig_dir)
kubeconfig_file = open(kubeconfig_path, 'w', encoding="utf-8")
kubeconfig_file.write(
    f"""
apiVersion: v1
clusters:
- cluster:
    insecure-skip-tls-verify: true
    server: {k8s_config.k8s_server_url}
  name: kickstand-dev
contexts:
- context:
    cluster: kickstand-dev
    user: minhto
  name: minhto@kickstand-dev
current-context: minhto@kickstand-dev
kind: Config
preferences: {{}}
users:
- name: minhto
  user:
    client-certificate-data: {k8s_config.k8s_client_certificate}
    client-key-data: {k8s_config.k8s_client_key}
    """
)
kubeconfig_file.close()

# if not (destroy or revert or stop or log):
# Use os.system here because there is no need to stop
# these processes when stopping a deployment as these processes
# don't make changes to Kubernetes resources
# Run 'terraform init'
init_result = os.system(f"cd {app_path} && terraform init -upgrade -force-copy")
# If init fails, try running with "-reconfigure" flag
if init_result != 0:
    print("Failed to initialize Terraform. Trying again with -reconfigure flag")
    init_result = os.system(f"cd {app_path} && terraform init -upgrade -force-copy -reconfigure")
    if init_result != 0:
        raise DeploymentFailure(description=f'Failed to initialize Terraform for {app.descriptive_name}')

# Validate terraform configs
os.system(f"cd {app_path} && terraform validate")

# TODO: "plan" should be set elsewhere.  This command is the basis for
# either the plan or apply operation (and maybe destroy at some point).
# "plan" should create a plan file and "apply" should use this file.
action = "plan" if testing else "apply"
auto_approve = "-auto-approve" if not testing else ""

if destroy:
    # Run 'terraform destroy'
    process = subprocess.Popen(
        ["terraform", f"-chdir={app_path}", "destroy", "-no-color", "-input=false", auto_approve,
        f"-state={app_id}.tfstate"]
    )

    process.wait(timeout=settings.TERRAFORM_TIMEOUT)

    # Raise an error if destroying an app's deployment fails, which means return code is not 0
    if process.returncode != 0:
        raise DestroyDeploymentFailure(description=f'Failed to destroy deployment for {app.descriptive_name}')

    sys.exit()

elif revert:
    # Run "terraform apply"
    process = subprocess.Popen(
        ["terraform", f"-chdir={app_path}", action, "-no-color", "-input=false", auto_approve,
        f"-state={app_id}.tfstate"]
    )

    process.wait(timeout=settings.TERRAFORM_TIMEOUT)

    # Raise an error if reverting an app's failed deployment fails, which means return code is not 0
    if process.returncode != 0:
        raise DestroyDeploymentFailure(description=f'Failed to revert deployment for {app.descriptive_name}')

    sys.exit()

elif stop:
    # Run 'terraform destroy'
    process = subprocess.Popen(
        ["terraform", f"-chdir={app_path}", "destroy", "-no-color", "-input=false", auto_approve,
        f"-state={app_id}.tfstate", "-target", "module.strap.helm_release.app"]
    )

    process.wait(timeout=settings.TERRAFORM_TIMEOUT)

    # Raise an error if destroying an app's deployment fails, which means return code is not 0
    if process.returncode != 0:
        raise DestroyDeploymentFailure(description=f'Failed to stop deployment for {app.descriptive_name}')

    sys.exit()

elif log:
    # Run 'terraform show'
    process = subprocess.Popen(
        ["terraform", f"-chdir={app_path}", "show", "helm_release.app",
        f"-state={app_id}.tfstate"]
    )

    process.wait(timeout=settings.TERRAFORM_TIMEOUT)

    # Raise an error if destroying an app's deployment fails, which means return code is not 0
    if process.returncode != 0:
        raise DestroyDeploymentFailure(description=f'Failed to log deployment for {app.descriptive_name}')

    sys.exit()

else:
    # Run "terraform apply"
    if testing:
        process = subprocess.Popen(
            ["terraform", f"-chdir={app_path}", action, "-no-color", "-input=false",
            f"-state={app_id}.tfstate"]
        )
    else:
        process = subprocess.Popen(
            ["terraform", f"-chdir={app_path}", action, "-no-color", "-input=false", auto_approve,
            f"-state={app_id}.tfstate"]
        )

    # Add process id to the currently executing processes for this deployment
    # Get deployment to avoid race condition
    deployment = Deployment.objects.get(
        application=app
    )
    deployment.process_ids = str(deployment.process_ids or '') + f"{',' if deployment.process_ids else ''}{process.pid}"
    deployment.save()
    process.wait(timeout=settings.TERRAFORM_TIMEOUT)

    # Raise an error if deploying an app fails, which means return code is not 0
    if process.returncode != 0 or testing:
        raise DeploymentFailure(description=f'Failed to deploy app {app.descriptive_name}')

    sys.exit()
