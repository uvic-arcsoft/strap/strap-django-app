# pylint:
import os
import json
import environ

from django.test import TestCase, Client, override_settings
from django.contrib.auth.models import User
from django.urls import reverse

from normal_users.models import Application
from admin_users.models import HarborConfig

# pylint: disable=no-name-in-module
# TODO: Figure out why no-name-in-module is being raised
# when the code is working fine
from strapper.test_settings import TEST_SETTINGS
# pylint: enable=no-name-in-module

# Load the environment variables
env = environ.Env.read_env()

# Webhook test cases
@override_settings(**TEST_SETTINGS)
class WebhookViewTestCase(TestCase):
    # General setup
    def setUp(self):
        self.user = User.objects.create_user(username='user@example.org', password='testpassword')

        # Create 1 app
        self.app = Application.objects.create(
            identifier='testapp',
            descriptive_name='Test App',
            description='This is an app for testing',
            port=1234,
            authentication='uvic',
            is_secure=True,
            owner=self.user
        )

        # Create 1 container for the app
        self.container = self.app.container_set.create(
            image='unit/testapp',
            image_tag='latest',
            port=1234,
            command='python testapp.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

    # NOTE: This webhook is not in use
    # def test_image_pushed_view(self):
    #     response = self.client.post('/webhook/registry/image_pushed/')
    #     self.assertEqual(response.status_code, 200)

    # Send an email to app's owner when detecting at least one severe vulnerability
    # in the app's image
    def test_image_high_vulnerabilities_notification(self):
        # Without EMAIL_HOST_USER set, should receive a 200 OK response
        # with a message indicating that email notification is disabled
        with self.settings(EMAIL_HOST_USER=None):
            client = Client(HTTP_AUTHORIZATION=os.environ.get('REGISTRY_WEBHOOK_SECRET'))
            response = client.post(reverse('webhook_image_high_vulnerabilities'))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                response.content,
                b'EMAIL_HOST_USER is not set in settings.py. Email notification is disabled.'
            )

        client = Client()
        # Without the authorization header
        # Expected: 401 Unauthorized
        response = client.post(reverse('webhook_image_high_vulnerabilities'))
        self.assertEqual(response.status_code, 401)

        client = Client(HTTP_AUTHORIZATION=os.environ.get('REGISTRY_WEBHOOK_SECRET'))

        # Configure Harbor
        harbor_config = HarborConfig.get_instance()
        harbor_config.severity_threshold = "High"
        # Valid email template
        harbor_config.vuln_notification_email_template = """
            <p>Hi {{ name }},</p>
            <p>We have detected severe vulnerabilities in the container images used by your application, {{ app_name }}. Please fix the followings as soon as possible:</p>
            <ul>
                {% for resource_url, vulnerabilities in vuln_images.items %}
                    <li>
                        <strong>{{ resource_url }}</strong>: 
                        {% for severity, num_found in vulnerabilities.items %}
                            {{ num_found }} {{severity}} vulnerabilities{% if not forloop.last %}, {% endif %}
                        {% endfor %}
                    </li>
                {% endfor %}
            </ul>
            <p>Please either fix the vulnerabilities or use a different image to upgrade your app. If you have any questions, feel free to contact us at {{ email_sender }}.</p>
            <p>
            Best regards,<br>
            <strong>UVic ARCSoftware team</strong>
            </p>
        """
        harbor_config.save()

        # Send an invalid webhook payload
        # Expected: 500 Internal Server Error
        invalid_payload = {
            'invalid': 'payload'
        }
        response = client.post(
            reverse('webhook_image_high_vulnerabilities'),
            data=json.dumps(invalid_payload),
            content_type='application/json',
            follow=True)
        self.assertEqual(response.status_code, 500)
        self.assertIn(b'Invalid webhook data. Missing keys', response.content)

        # No severe vulnerabilities payload
        # Expected: 200 OK
        valid_payload = {
            'event_data': {
                'resources': [
                    {
                        'resource_url': 'unit/testapp:latest',
                        'scan_overview': {
                            'application/vnd.security.vulnerability.report; version=1.1': {
                                'severity': 'Low',
                                'summary': { 
                                    'summary': {
                                        'Low': 1,
                                        'Medium': 0,
                                        'High': 0,
                                        'Critical': 0
                                    }
                                }
                            }
                        }
                    }
                ]
            }
        }
        response = client.post(reverse('webhook_image_high_vulnerabilities'),
                                data=json.dumps(valid_payload),
                                content_type='application/json',
                                follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.content,
            b'No severely vulnerable artifacts detected. No need to notify developer.'
        )

        # Severe vulnerabilities detected
        # Expected: 200 OK
        valid_payload['event_data']['resources'][0]['scan_overview'][
            'application/vnd.security.vulnerability.report; version=1.1'
        ]['severity'] = 'Critical'
        valid_payload['event_data']['resources'][0]['scan_overview'][
            'application/vnd.security.vulnerability.report; version=1.1'
        ]['summary']['summary'] = {
            'Low': 1,
            'Medium': 2,
            'High': 3,
            'Critical': 4
        }
        response = client.post(reverse('webhook_image_high_vulnerabilities'),
                                data=json.dumps(valid_payload),
                                content_type='application/json',
                                follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.content,
            b"Contacted Test App's owner successfully!"
        )

        # Send a payload with a non-existing image
        # Expected: 404 Not Found
        valid_payload['event_data']['resources'][0]['resource_url'] = 'unit/non-existing:latest'
        response = client.post(reverse('webhook_image_high_vulnerabilities'),
                                data=json.dumps(valid_payload),
                                content_type='application/json',
                                follow=True)
        self.assertEqual(response.status_code, 404)
        self.assertIn(b'Container not found with image', response.content)

        # Use an invalid email template. Wrong Jinja syntax (missing closing tag)
        # Expected: 500 Internal Server Error
        valid_payload['event_data']['resources'][0]['resource_url'] = 'unit/testapp:latest'
        harbor_config.vuln_notification_email_template = """
            {% if name %}
            <p>Hi {{ name }},</p>
            <p>We have detected severe vulnerabilities in the container images used by your application, {{ app_name }}. Please fix the followings as soon as possible:</p>
        """
        harbor_config.save()

        response = client.post(reverse('webhook_image_high_vulnerabilities'),
                                data=json.dumps(valid_payload),
                                content_type='application/json',
                                follow=True)
        self.assertEqual(response.status_code, 500)
        self.assertIn(b"Error occurred while interpolating email template", response.content)
