# pylint:
from django.test import TestCase
from normal_users.forms import (ApplicationForm, K8sResourcesForm,
                    K8sResourcesRequestForm, ContainerForm, GroupForm)

# Test cases for ApplicationForm
class ApplicationFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = ApplicationForm({
            'identifier': 'myapp',
            'descriptive_name': 'My Application',
            'description': 'This is my application',
            'port': 1234,
            'authentication': 'uvic',
            'database': 'PostgreSQL',
            'authenticated_routes': '/login, /admin',
            'unauthenticated_routes': '/'
        })
        self.assertEqual(form.is_valid(), True)

    # Form should be invalid
    def test_invalid_form(self):
        # Missing descriptive_name
        form = ApplicationForm({
            'identifier': 'myapp',
            'description': 'This is my application',
            'container_image': 'image/myapp',
            'container_port': 1234,
            'authentication': 'Uvic', # This field is also invalid
            'runtime_command': 'python validtestapp.py',
            'environment_variables': 'VAR1=123\nVAR2=thisissecret',
        })
        self.assertEqual(form.is_valid(), False)

# Test cases for K8sResourcesForm
class K8sResourcesFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = K8sResourcesForm({
            'replicas': 3,
        })
        self.assertEqual(form.is_valid(), True)

    # Form should be invalid
    def test_invalid_form(self):
        # Missing replicas
        form = K8sResourcesForm({})
        self.assertEqual(form.is_valid(), False)

# Test cases for K8sResourcesRequestForm
class K8sResourcesRequestFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = K8sResourcesRequestForm({
            'replicas': 3,
            'justification': 'Need more replicas for myapp',
        })
        self.assertEqual(form.is_valid(), True)

    # Form should be invalid
    def test_invalid_form(self):
        # Missing replicas
        form = K8sResourcesRequestForm({
            'justification': 'Need more replicas for myapp',
        })
        self.assertEqual(form.is_valid(), False)

# Test cases for ContainerForm
class ContainerFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = ContainerForm({
            'image': 'image/myapp',
            'image_tag': 'latest',
            'container_port': 1234,
            'command': 'python validtestapp.py',
            'environment_variables': 'VAR1=123\nVAR2=thisissecret',
        })
        self.assertEqual(form.is_valid(), True)

    # Form should be invalid
    def test_invalid_form(self):
        # Missing container image
        form = ContainerForm({
            'port': 1234,
            'command': 'python validtestapp.py',
            'environment_variables': 'VAR1=123\nVAR2=thisissecret',
        })
        self.assertEqual(form.is_valid(), False)

# Test cases for GroupForm
class GroupFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = GroupForm({
            'name': 'mygroup',
            'is_everyone': False,
            'users': 'user@example.org, admin@example.org',
            'description': 'This is my group',
        })
        self.assertEqual(form.is_valid(), True)

    # Form should be invalid
    def test_invalid_form(self):
        # Missing name
        form = GroupForm({
            'is_everyone': False,
            'users': 'user@example.org, admin@example.org',
            'description': 'This is my group',
        })
        self.assertEqual(form.is_valid(), False)
