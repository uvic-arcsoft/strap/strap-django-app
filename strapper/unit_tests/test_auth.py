# pylint:
from django.test import TestCase, Client, override_settings
from django.contrib.auth.models import User
from django.urls import reverse

from exceptions import BadConfig

# pylint: disable=no-name-in-module
# TODO: Figure out why no-name-in-module is being raised
# when the code is working fine
from strapper.test_settings import TEST_SETTINGS
# pylint: enable=no-name-in-module

admin_user_override_settings = {
    **TEST_SETTINGS,
    'CONFIG': {
        **TEST_SETTINGS['CONFIG'],
        'AUTHX_USER_OVERRIDE': 'admin@example.org'
    }
}

# General setup for authentication tests
class AuthenticationTestCase(TestCase):
    def setUp(self):
        # This client is generic, will be overriden by children
        self.client = Client()

        # Create two users in the testing database
        self.fake_user = User.objects.create(username="user@example.org", password="testinguser")
        self.fake_admin = User.objects.create(username="admin@example.org", password="testingadmin", is_staff=True)

    # Visit login page
    @override_settings(**TEST_SETTINGS)
    def visit_login(self):
        return self.client.get("/login/", follow=True)

    # Visit login page with an overriding user
    @override_settings(**admin_user_override_settings)
    def visit_login_with_overriding_user(self):
        return self.client.get("/login/", follow=True)

    # Visit user dashboard
    @override_settings(**TEST_SETTINGS)
    def visit_user_dashboard(self):
        return self.client.get("/", follow=True)

    # Visit admin dashboard
    @override_settings(**TEST_SETTINGS)
    def visit_admin_dashboard(self):
        return self.client.get("/admin/", follow=True)

    # Logout
    @override_settings(**TEST_SETTINGS)
    def logout(self):
        return self.client.get("/logout/", follow=True)

# Test cases for unauthenticated users
class UnauthenticatedUserTestCases(AuthenticationTestCase):
    def setUp(self):
        super().setUp()
        self.client = Client()

    # Visit login page
    def test_unauthenticated_access_login_page(self):
        response = self.visit_login()
        self.assertEqual(response.templates[0].name, "login.html")

    # Visit login with an overriding user
    def test_unauthenticated_with_overriding_user(self):
        response = self.visit_login_with_overriding_user()
        self.assertRedirects(response, expected_url='/')

    # Visit user dashboard
    def test_unauthenticated_access_user_dashboard(self):
        response = self.visit_user_dashboard()
        self.assertRedirects(response, expected_url=reverse("welcome"))

    # Visit admin dashboard
    def test_unauthenticated_access_admin_dashboard(self):
        response = self.visit_admin_dashboard()
        self.assertRedirects(response, expected_url=reverse("welcome"))

    # Logout
    def test_unauthenticated_logout(self):
        response = self.logout()
        self.assertEqual(response.status_code, 200)


# Test cases for unauthorized users
class UnauthorizedUserTestCases(AuthenticationTestCase):
    def setUp(self):
        super().setUp()
        self.client = Client(HTTP_X_FORWARDED_USER="donotexist@example.org")

    # Visit login page
    def test_unauthorized_user_access_login_page(self):
        response = self.visit_login()
        self.assertEqual(response.templates[0].name, "wizard/getting_started.html")

    # Visit user dashboard
    def test_unauthorized_user_access_user_dashboard(self):
        response = self.visit_user_dashboard()
        self.assertRedirects(response, expected_url=reverse("welcome"))

    # Visit admin dashboard
    def test_unauthorized_user_access_admin_dashboard(self):
        response = self.visit_admin_dashboard()
        self.assertRedirects(response, expected_url=reverse("welcome"))

    # Logout
    def test_unauthorized_user_logout(self):
        response = self.logout()
        self.assertEqual(response.status_code, 200)


# Test cases for authorized users
class AuthorizedUserTestCases(AuthenticationTestCase):
    def setUp(self):
        super().setUp()
        self.client = Client(HTTP_X_FORWARDED_USER="user@example.org")
        self.visit_login()

    # Visit login page
    def test_authorized_user_access_login_page(self):
        response = self.visit_login()
        self.assertRedirects(response, expected_url="/")

    # Visit user dashboard
    def test_authorized_user_access_user_dashboard(self):
        response = self.visit_user_dashboard()
        self.assertEqual(response.status_code, 200)

    # Visit admin dashboard
    def test_authorized_user_access_admin_dashboard(self):
        response = self.visit_admin_dashboard()
        self.assertRedirects(response, expected_url="/")

    # Logout (Not sure why this fail)
    def test_authorized_user_logout(self):
        response = self.logout()
        self.assertEqual(response.status_code, 200)

# Test cases for authorized admins
class AuthorizedAdminTestCases(AuthenticationTestCase):
    def setUp(self):
        super().setUp()
        self.client = Client(HTTP_X_FORWARDED_USER="admin@example.org")
        self.visit_login()

    # Visit login page
    def test_authorized_admin_access_login_page(self):
        response = self.visit_login()
        self.assertRedirects(response, expected_url="/")

    # Visit user dashboard
    def test_authorized_admin_access_user_dashboard(self):
        response = self.visit_user_dashboard()
        self.assertEqual(response.status_code, 200)

    # Visit admin dashboard
    def test_authorized_admin_access_admin_dashboard(self):
        response = self.visit_admin_dashboard()
        self.assertEqual(response.status_code, 200)

    # Logout (Not sure why this fail)
    def test_unauthorized_admin_logout(self):
        response = self.logout()
        self.assertEqual(response.status_code, 200)


# Some tests for the settings in development only
bad_config_settings = {
    **TEST_SETTINGS,
    'CONFIG': {
        **TEST_SETTINGS['CONFIG'],
        'AUTHX_HTTP_HEADER_USER': None
    }
}

non_existing_user_settings = {
    **TEST_SETTINGS,
    'CONFIG': {
        **TEST_SETTINGS['CONFIG'],
        'AUTHX_USER_OVERRIDE': 'donotexist@example.org'
    }
}

class AuthenticationBySettingsTestCases(TestCase):
    def setUp(self):
        super().setUp()
        self.client = Client()

    # Set CONFIG['AUTHX_HTTP_HEADER_USER'] = None
    @override_settings(**bad_config_settings)
    def test_login_with_bad_config(self):
        with self.assertRaises(BadConfig):
            self.client.get('/login/', follow=True)

    # Set CONFIG['AUTHX_USER_OVERRIDE'] to a non-existing user
    @override_settings(**non_existing_user_settings)
    def test_login_with_non_existing_user_in_settings(self):
        response = self.client.get('/login/', follow=True)
        self.assertEqual(response.status_code, 200)
