# pylint:
import json

import re

from django.test import TestCase
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.core.exceptions import ValidationError

from admin_users.models import GeneralConfig, HarborConfig
from normal_users.models import (Application, Group, K8sInstance, Route,
    Deployment, Container, K8sContainer, HarborProject, ApplicationQuota,
    ApplicationQuotaRequest, RequestStatusTypes, Notification, NotificationTypes,
    NotificationStatusTypes)

# General setup for model tests
class ModelTestCase(TestCase):
    def setUp(self):
        # Create 3 users with 4 roles to the main app: app's owner, editor, viewer and unauthz user
        self.owner = User.objects.create(username='owner@example.org', password='ownerpwd')
        self.editor = User.objects.create(username='editor@example.org', password='editorpwd')
        self.viewer = User.objects.create(username='viewer@example.org', password='viewerpwd')
        self.unauthz = User.objects.create(username='unauthz@example.org', password='unauthzpwd')

        # Create an application with 2 routes and 2 containers
        self.main_app = Application.objects.create(
            identifier='testapp',
            descriptive_name='Test App',
            description='This is an app for testing',
            port=1234,
            authentication='uvic',
            # database should be default to 'PostgresSQL'
            allowed_ips='192.168.1.1,192.168.0.1',
            replicas=2,
            owner=self.owner
        )

        # Populate roles for the main app
        self.main_app.approlegroup_set.get(role="editor").users.add(self.editor)
        self.main_app.approlegroup_set.get(role="viewer").users.add(self.viewer)

        self.main_app_auth_route = Route.objects.create(route='/login', authenticated=True, application=self.main_app)
        self.main_app_auth_route2 = Route.objects.create(route='/admin', authenticated=True, application=self.main_app)
        self.main_app_unauth_route = Route.objects.create(route='/', authenticated=False, application=self.main_app)

        self.main_app_container = self.main_app.container_set.create(
            image='unit/testapp',
            image_tag='latest',
            port=1234,
            command='python testapp.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )
        self.main_app_container2 = self.main_app.container_set.create(
            image='unit/testapp2',
            image_tag='latest',
            port=1235,
            command='python testapp2.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

        # Test if the instances are created properly
        self.assertEqual(Application.objects.count(), 1)
        self.assertEqual(Route.objects.count(), 3)
        self.assertEqual(Container.objects.count(), 2)

        # Create a K8s resource quota for the main app
        self.main_app.applicationquota.replicas = 2
        self.main_app.applicationquota.save()

        # Populate test data for app request
        self.main_app.applicationquotarequest.replicas=3
        self.main_app.applicationquotarequest.justification='Need more replicas for testapp'
        self.main_app.applicationquotarequest.submitted_by=self.owner
        self.main_app.applicationquotarequest.status=RequestStatusTypes.PENDING
        self.main_app.applicationquotarequest.admin_reply='I approve this request'
        self.main_app.applicationquotarequest.save()


# Test cases for model Application
class ApplicationModelTestCases(ModelTestCase):
    # Create a valid instance
    def test_create_valid(self):
        app = Application.objects.create(
            identifier='validtestapp',
            descriptive_name='Valid Test App',
            description='This app should be valid',
            port=1234,
            authentication='Uvic',
            database='None',
            allowed_ips='192.12.12.23/24,146.23.45.12/24',
            owner=self.owner
        )
        self.assertEqual(Application.objects.count(), 2)
        self.assertEqual(app.identifier, 'validtestapp')
        self.assertEqual(app.descriptive_name, 'Valid Test App')
        self.assertEqual(app.description, 'This app should be valid')
        self.assertEqual(app.port, 1234)
        self.assertEqual(app.authentication, 'Uvic')
        self.assertEqual(app.database, 'None')
        self.assertEqual(app.allowed_ips, '192.12.12.23/24,146.23.45.12/24')
        self.assertEqual(app.replicas, 1)
        self.assertEqual(app.owner, self.owner)

    # Create an invalid instance
    def test_create_invalid(self):
        with self.assertRaises(IntegrityError):
            Application.objects.create(
                identifier='testapp',   # Identifier already in used
                descriptive_name='Valid Test App',
                description='This app should be valid',
                port=1234,
                authentication='Uvic',
                owner=self.owner
            )

            # The new app should not be created
            self.assertEqual(Application.objects.count(), 1)

    # Test representation of Application objects
    def test_repr(self):
        self.assertEqual(repr(self.main_app), 'Test App')

    # Test helper function create_routes
    def test_create_routes(self):
        self.main_app.create_routes(['/admin', '/new_auth', '/new_path'], authenticated=True)
        self.main_app.create_routes(['/new_unauth'], authenticated=False)
        self.assertEqual(self.main_app.route_set.count(), 6)
        self.assertEqual(self.main_app.routes_to_str(authenticated=True), '/admin,/login,/new_auth,/new_path')
        self.assertEqual(self.main_app.routes_to_str(authenticated=False), '/,/new_unauth')

        # Cannot create empty route
        self.main_app.create_routes([''], authenticated=True)
        self.assertEqual(self.main_app.route_set.count(), 6)

        # Automatically prepend '/' to the route if it is not present
        self.main_app.create_routes(['path_without_slash'], authenticated=True)
        self.assertEqual(self.main_app.route_set.count(), 7)
        self.assertEqual(
            self.main_app.routes_to_str(authenticated=True),
            '/admin,/login,/new_auth,/new_path,/path_without_slash')

    # Test helper function has_route
    def test_has_route(self):
        self.assertEqual(self.main_app.has_route('/login'), True)
        self.assertEqual(self.main_app.has_route('/admin'), True)
        self.assertEqual(self.main_app.has_route('/new_auth'), False)
        self.assertEqual(self.main_app.has_route('/'), True)
        self.assertEqual(self.main_app.has_route('/new_unauth'), False)

    # Test helper function routes_to_str
    def test_routes_to_str(self):
        self.assertEqual(self.main_app.routes_to_str(authenticated=True), '/admin,/login')
        self.assertEqual(self.main_app.routes_to_str(authenticated=False), '/')

    # Test get_allowed_ips helper method
    def test_get_allowed_ips(self):
        self.assertEqual(self.main_app.get_allowed_ips()["others"], ['192.168.1.1', '192.168.0.1'])
        my_vpn = {
            "My VPN": ['1.1.1.1/1', '2.2.2.2/2']
        }
        general_config = GeneralConfig.get_instance()
        general_config.preset_ips = json.dumps(my_vpn)
        general_config.save()

        self.main_app.allowed_ips = '192.168.1.1,192.168.0.1,1.1.1.1/1,2.2.2.2/2'
        self.main_app.save()

        self.assertEqual(self.main_app.get_allowed_ips()["My VPN"], ['1.1.1.1/1', '2.2.2.2/2'])
        self.assertEqual(self.main_app.get_allowed_ips()["others"], ['192.168.1.1', '192.168.0.1'])


    # Test has_advaced_config helper method
    def test_has_advanced_config(self):
        self.assertEqual(self.main_app.has_advanced_config(), True)
        self.main_app.allowed_ips = None
        self.main_app.save()
        self.assertEqual(self.main_app.has_advanced_config(), False)

    # Test helper function build_configuration
    def test_build_configuration(self):
        config = self.main_app.build_configuration()
        self.assertEqual(config['app_name'], 'testapp')
        self.assertEqual(config['app_port'], 1234)
        self.assertEqual(config['authentication'], 'uvic')
        self.assertEqual(config['authenticated_routes'], ['/admin', '/login'])
        self.assertEqual(config['unauthenticated_routes'], ['/'])
        self.assertEqual(config['allowed_ips'], ['192.168.1.1', '192.168.0.1'])
        self.assertEqual(config['replicas'], 2)

        container1 = config['containers'][0]
        self.assertEqual(container1['image'], 'unit/testapp')
        self.assertEqual(container1['tag'], 'latest')
        self.assertEqual(container1['port'], 1234)
        self.assertEqual(container1['command'], ['python', 'testapp.py'])
        self.assertEqual(container1['environment'], {"VAR1": '"123"', "VAR2": '"thisissecret"'})

        container2 = config['containers'][1]
        self.assertEqual(container2['image'], 'unit/testapp2')
        self.assertEqual(container2['tag'], 'latest')
        self.assertEqual(container2['port'], 1235)
        self.assertEqual(container2['command'], ['python', 'testapp2.py'])
        self.assertEqual(container2['environment'], {"VAR1": '"123"', "VAR2": '"thisissecret"'})

    # Test helper function detect_config_change
    def test_detect_config_change(self):
        # This is a trivial case as this app currently has no K8sInstance
        # The function should return True in this case
        self.assertEqual(self.main_app.detect_config_change(), True)

    # Test helper function get_js_config
    def test_get_js_config(self):
        config = self.main_app.get_js_config()
        self.assertEqual(config['descriptive-name'], 'Test App')
        self.assertEqual(config['port'], 1234)
        self.assertEqual(config['authentication'], 'uvic')
        self.assertEqual(config['authenticated-routes'], '/admin,/login')
        self.assertEqual(config['unauthenticated-routes'], '/')
        self.assertEqual(config['allowed-ips'], ['192.168.1.1', '192.168.0.1'])
        self.assertEqual(config['replicas'], 2)
        self.assertEqual(config['containers'][0]['image'], 'unit/testapp')
        self.assertEqual(config['containers'][0]['image-tag'], 'latest')
        self.assertEqual(config['containers'][0]['port'], 1234)
        self.assertEqual(config['containers'][0]['command'], 'python testapp.py')
        self.assertEqual(config['containers'][0]['environment-variables'], 'VAR1==123\nVAR2==thisissecret')
        self.assertEqual(config['containers'][1]['image'], 'unit/testapp2')
        self.assertEqual(config['containers'][1]['image-tag'], 'latest')
        self.assertEqual(config['containers'][1]['port'], 1235)
        self.assertEqual(config['containers'][1]['command'], 'python testapp2.py')
        self.assertEqual(config['containers'][1]['environment-variables'], 'VAR1==123\nVAR2==thisissecret')

    # Test helper function copy_last_successful_deployment
    def test_copy_last_successful_deployment(self):
        # This is a trivial case as this app currently has no K8sInstance
        # The current app configurations should not be changed
        self.main_app.copy_last_successful_deployment()
        self.assertEqual(self.main_app.identifier, 'testapp')
        self.assertEqual(self.main_app.descriptive_name, 'Test App')
        self.assertEqual(self.main_app.description, 'This is an app for testing')
        self.assertEqual(self.main_app.port, 1234)
        self.assertEqual(self.main_app.authentication, 'uvic')
        self.assertEqual(self.main_app.database, 'None')
        self.assertEqual(self.main_app.allowed_ips, '192.168.1.1,192.168.0.1')
        self.assertEqual(self.main_app.replicas, 2)
        self.assertEqual(self.main_app.owner, self.owner)

        self.assertEqual(self.main_app.container_set.count(), 2)
        container1 = self.main_app.container_set.first()
        self.assertEqual(container1.image, 'unit/testapp')
        self.assertEqual(container1.image_tag, 'latest')
        self.assertEqual(container1.port, 1234)
        self.assertEqual(container1.command, 'python testapp.py')
        self.assertEqual(container1.environment_variables, 'VAR1==123\nVAR2==thisissecret')

        container2 = self.main_app.container_set.last()
        self.assertEqual(container2.image, 'unit/testapp2')
        self.assertEqual(container2.image_tag, 'latest')
        self.assertEqual(container2.port, 1235)
        self.assertEqual(container2.command, 'python testapp2.py')
        self.assertEqual(container2.environment_variables, 'VAR1==123\nVAR2==thisissecret')

    # Test helper function get_user_role
    # Get the role of a user for an app
    def test_get_user_role(self):
        self.assertEqual(self.main_app.get_user_role(self.owner), 'owner')
        self.assertEqual(self.main_app.get_user_role(self.editor), 'editor')
        self.assertEqual(self.main_app.get_user_role(self.viewer), 'viewer')
        self.assertEqual(self.main_app.get_user_role(self.unauthz), 'none')

    # Test helper function check_quota
    # Check if app has enough quota for a certain upgrade
    def test_check_quota(self):
        self.assertEqual(self.main_app.check_quota(replicas=2), True)
        self.assertEqual(self.main_app.check_quota(replicas=3), False)


# Test cases for model Container
class ContainerModelTestCases(ModelTestCase):
    # Create a valid instance
    def test_create_valid(self):
        container = Container.objects.create(
            image='container/testapp',
            # image_tag should be default to 'latest'
            port=1236,
            command='python testapp.py',
            environment_variables='VAR1==123\nVAR2==thisissecret',
            application=self.main_app
        )
        self.assertEqual(Container.objects.count(), 3)
        self.assertEqual(container.image, 'container/testapp')
        self.assertEqual(container.image_tag, 'latest')
        self.assertEqual(container.port, 1236)
        self.assertEqual(container.command, 'python testapp.py')
        self.assertEqual(container.environment_variables, 'VAR1==123\nVAR2==thisissecret')
        self.assertEqual(container.application, self.main_app)

    # Create an invalid instance
    def test_create_invalid(self):
        with self.assertRaises(IntegrityError):
            Container.objects.create(
                # Missing application
                image='unit/testapp',
                image_tag='latest',
                port=1234,
                command='python testapp.py',
                environment_variables='VAR1==123\nVAR2==thisissecret'
            )
            self.assertEqual(Container.objects.count(), 2)

    # Test representation of Container objects
    def test_repr(self):
        self.assertEqual(repr(self.main_app_container), 'unit/testapp:latest')

    # Test string representation of Container objects
    def test_str(self):
        self.assertEqual(str(self.main_app_container), 'unit/testapp:latest')

    # Test get_environment_variables helper method
    def test_get_environment_variables(self):
        self.assertEqual(
            self.main_app_container.get_environment_variables(),
            {'VAR1': '"123"', 'VAR2': '"thisissecret"'}
        )

    # test get_environment_variables_without_quote helper method
    def test_get_environment_variables_without_quote(self):
        self.assertEqual(
            self.main_app_container.get_environment_variables_without_quote(),
            {'VAR1': '123', 'VAR2': 'thisissecret'}
        )

    # Test environment_variables_to_str helper method
    def test_environment_variables_to_str(self):
        self.assertEqual(
            self.main_app_container.environment_variables_to_str(),
            'VAR1==123, VAR2==thisissecret'
        )


# Test cases for model ApplicationQuota
class ApplicationQuotaModelTestCases(ModelTestCase):
    # Test create a valid ApplicationQuota instance
    # A quota should be created for each application
    def test_create_valid(self):
        quota = ApplicationQuota.objects.get(application=self.main_app)
        self.assertEqual(ApplicationQuota.objects.count(), 1)
        self.assertEqual(quota.application, self.main_app)
        self.assertEqual(quota.replicas, 2)

    # Test representation of ApplicationQuota objects
    def test_repr(self):
        quota = ApplicationQuota.objects.get(application=self.main_app)
        self.assertEqual(repr(quota), 'Test App Quota')

    # Test string representation of ApplicationQuota objects
    def test_str(self):
        quota = ApplicationQuota.objects.get(application=self.main_app)
        self.assertEqual(str(quota), 'Test App Quota')


# Test cases for model ApplicationQuotaRequest
class ApplicationQuotaRequestModelTestCases(ModelTestCase):
    def setUp(self):
        super().setUp()
        self.request = self.main_app.applicationquotarequest

    # Test create a valid ApplicationQuotaRequest instance
    def test_create_valid(self):
        self.assertEqual(ApplicationQuotaRequest.objects.count(), 1)
        self.assertEqual(self.request.application, self.main_app)
        self.assertEqual(self.request.replicas, 3)
        self.assertEqual(self.request.justification, 'Need more replicas for testapp')
        self.assertEqual(self.request.submitted_by, self.owner)
        self.assertEqual(self.request.status, RequestStatusTypes.PENDING)
        self.assertEqual(self.request.admin_reply, 'I approve this request')

    # Test create an invalid ApplicationQuotaRequest instance
    def test_create_invalid(self):
        with self.assertRaises(IntegrityError):
            ApplicationQuotaRequest.objects.create(
                # Missing application
                replicas=5,
                justification='Need more replicas for testapp',
                submitted_by=self.owner
            )
            self.assertEqual(ApplicationQuotaRequest.objects.count(), 1)

    # Test representation of ApplicationQuotaRequest objects
    def test_repr(self):
        self.assertEqual(repr(self.request), 'Test App Quota Request')

    # Test string representation of ApplicationQuotaRequest objects
    def test_str(self):
        self.assertEqual(str(self.request), 'Test App Quota Request')

    # Test get_submitter_name helper method
    # Get the name or email if name doens't exist of the user who submitted the request
    def test_get_submitter_name(self):
        self.assertEqual(self.request.get_submitter_name(), 'owner@example.org')

        self.owner.first_name = 'John'
        self.owner.last_name = 'Doe'
        self.owner.save()
        self.assertEqual(self.request.get_submitter_name(), 'John Doe')


# Test cases for model Notification
class NotificationModelTestCases(ModelTestCase):
    def setUp(self):
        super().setUp()

        self.request = self.main_app.applicationquotarequest

        self.notification = Notification.objects.create(
            user = self.owner,
            html = 'Your quota request has been approved',
            url = '#',
            type = NotificationTypes.ApplicationQuotaRequest,
            related_model_pk = self.request.id,
            status = NotificationStatusTypes.NEW
        )

    # Test create a valid Notification instance
    def test_create_valid(self):
        self.assertEqual(Notification.objects.count(), 1)
        self.assertEqual(self.notification.user, self.owner)
        self.assertEqual(self.notification.html, 'Your quota request has been approved')
        self.assertEqual(self.notification.url, '#')
        self.assertEqual(self.notification.type, NotificationTypes.ApplicationQuotaRequest)
        self.assertEqual(self.notification.related_model_pk, self.request.id)
        self.assertEqual(self.notification.status, NotificationStatusTypes.NEW)

    # Test create an invalid Notification instance
    def test_create_invalid(self):
        with self.assertRaises(IntegrityError):
            Notification.objects.create(
                # Missing user
                html = 'Your quota request has been approved',
                url = '#',
                type = NotificationTypes.ApplicationQuotaRequest,
                related_model_pk = self.request.id,
                status = NotificationStatusTypes.NEW
            )
            self.assertEqual(Notification.objects.count(), 1)

    # Test representation of Notification objects
    def test_repr(self):
        name = (self.notification.user.get_full_name()
            if self.notification.user.get_full_name() else self.notification.user.username)
        message = re.sub(r'<.*?>', '', self.notification.html)[:60]
        self.assertEqual(repr(self.notification), f"{name} at {self.notification.created_at}: {message}")

    # Test string representation of Notification objects
    def test_str(self):
        self.assertEqual(str(self.notification), repr(self.notification))


# Test cases for model HarborProject
class HarborProjectModelTestCases(ModelTestCase):
    def setUp(self):
        super().setUp()

        self.harbor_project = HarborProject.objects.create(
            application = self.main_app,
            project_name = "test_project",
            username = "harbor_user",
            password = "testpassword",
            images=""
        )

        # Create some configs that are used in the HarborProject model
        HarborConfig.objects.create(
            harbor_url = "https://harbor.example.org",
            harbor_username = "admin",
            harbor_password = "adminpassword"
        )

    # Test create a valid HarborProject instance
    def test_create_valid(self):
        self.assertEqual(HarborProject.objects.count(), 1)
        self.assertEqual(self.harbor_project.application, self.main_app)
        self.assertEqual(self.harbor_project.project_name, "test_project")
        self.assertEqual(self.harbor_project.username, "harbor_user")
        self.assertEqual(self.harbor_project.password, "testpassword")
        self.assertEqual(self.harbor_project.images, "")

    # Test create an invalid HarborProject instance
    def test_create_invalid(self):
        with self.assertRaises(IntegrityError):
            HarborProject.objects.create(
                # Missing application
                project_name = "test_project",
                username = "harbor_user",
                password = "testpassword",
                images=""
            )
            self.assertEqual(HarborProject.objects.count(), 0)

    # Test representation of HarborProject objects
    def test_repr(self):
        self.assertEqual(repr(self.harbor_project), 'test_project')

    # Test string representation of HarborProject objects
    def test_str(self):
        self.assertEqual(str(self.harbor_project), 'test_project')

    # Test to_json method
    # Should return a dictionary describing the HarborProject instance
    def test_to_json(self):
        self.assertEqual(
            self.harbor_project.to_json(),
            {
                'project_name': 'test_project',
                'username': 'harbor_user',
                'password': 'testpassword',
                'images': []
            }
        )

    # Test get_images_from_harbor method
    # Should return a list of images for this project from the Harbor API
    def test_get_images_from_harbor(self):
        # Return an empty list because the Harbor API is not accessible
        # in the test environment
        self.assertIn('errors', self.harbor_project.get_images_from_harbor())

    # Test build_image_credentials method
    # Should return a dictionary with the credentials for the Harbor project
    def test_build_image_credentials(self):
        self.assertEqual(
            self.harbor_project.build_image_credentials(),
            {
                'registry': 'https://harbor.example.org',
                'username': 'harbor_user',
                'password': 'testpassword'
            }
        )


# Test cases for model Group
class GroupModelTestCases(ModelTestCase):
    def setUp(self):
        super().setUp()

        self.group = Group.objects.create(
            name="Test Group",
            is_everyone=False,
            users="user@example.org,owner@example.org",
            created_by=self.owner
        )
        self.group.application.add(self.main_app)

    # Test create a valid Group instance
    def test_create_valid(self):
        new_group = Group.objects.create(
            name="Test Users",
            is_everyone=False,
            users="user@example.org,owner@example.org",
            created_by=self.owner
        )
        new_group.application.add(self.main_app)
        self.assertEqual(Group.objects.count(), 2)
        self.assertEqual(new_group.application.first(), self.main_app)
        self.assertEqual(new_group.name, "Test Users")
        self.assertEqual(new_group.is_everyone, False)
        self.assertEqual(new_group.users, "user@example.org,owner@example.org")
        self.assertEqual(new_group.created_by, self.owner)

    # Test create an invalid Group instance
    def test_create_invalid(self):
        with self.assertRaises(IntegrityError):
            Group.objects.create(
                name=None,
                is_everyone=False,
                users="user@example.org,owner@example.org"
            )
            self.assertEqual(Group.objects.count(), 1)

    # Test __repr__ helper method
    def test_repr(self):
        self.assertEqual(
            repr(self.group),
            f"Group {self.group.name}")

    # Test get_users helper method
    def test_get_users(self):
        self.assertEqual(
            self.group.get_users(),
            ['user@example.org','owner@example.org']
        )

    # Test check_user method
    def test_check_user(self):
        self.assertEqual(self.group.check_user('user@example.org'), True)
        self.assertEqual(self.group.check_user('user1@example.org'), False)
        self.group.is_everyone = True
        self.group.save()
        self.assertEqual(self.group.check_user('user@example.org'), True)
        self.assertEqual(self.group.check_user('user1@example.org'), True)

# Test cases for model K8sInstance
class K8sInstanceModelTestCases(ModelTestCase):
    def setUp(self):
        super().setUp()

        # Create an Application, some Routes and a K8sInstance for test cases for this model
        # These test cases will change the app configurations so they shouldn't
        # interact with self.main_app created above
        self.app = Application.objects.create(
            identifier='myapp',
            descriptive_name='My Application',
            description='This is my application',
            port=4321,
            authentication='uvic_social',
            database='PostgresSQL',
            allowed_ips='192.164.2.1,192.163.1.2',
            replicas=2,
            owner=self.owner
        )

        self.app.route_set.create(route='/login', authenticated=True)
        self.app.route_set.create(route='/admin', authenticated=True)
        self.app.route_set.create(route='/new_auth', authenticated=True)
        self.app.route_set.create(route='/', authenticated=False)
        self.app.route_set.create(route='/new_unauth', authenticated=False)

        self.k8sinstance = K8sInstance.objects.create(
            application=self.app,
            descriptive_name='My Application',
            port=1234,
            authentication='uvic',
            authenticated_routes_str=',/login,/admin',
            unauthenticated_routes_str=',/',
            allowed_ips='192.168.1.1,192.168.0.1',
        )

    # The creation above for the K8sInstance should be valid
    # Test the field contents of the instance
    def test_fields_valid(self):
        self.assertEqual(self.k8sinstance.application, self.app)
        self.assertEqual(self.k8sinstance.descriptive_name, 'My Application')
        self.assertEqual(self.k8sinstance.port, 1234)
        self.assertEqual(self.k8sinstance.authentication, 'uvic')
        self.assertEqual(self.k8sinstance.authenticated_routes_str, ',/login,/admin')
        self.assertEqual(self.k8sinstance.unauthenticated_routes_str, ',/')
        self.assertEqual(self.k8sinstance.allowed_ips, '192.168.1.1,192.168.0.1')

    # Create an invalid instance
    def test_create_invalid(self):
        with self.assertRaises(IntegrityError):
            # Missing application, invalid authentication input
            K8sInstance.objects.create(
                descriptive_name='Invalid instance',
                authentication='doesnotexist',
            )
            self.assertEqual(K8sInstance.objects.count(), 1)

    # Test representation of K8sInstance objects
    def test_repr(self):
        self.assertEqual(repr(self.k8sinstance), 'My Application')

    # Test Application's helper method detect_config_change
    # The method returns True if there is any difference between the current
    # app config and the K8sInstance related to that app
    def test_application_detect_config_change(self):
        # Since there are differences between the app and the
        # K8sInstance, this method should return True
        self.assertEqual(self.app.detect_config_change(), True)

    # Test Application's helper method copy_last_successful_deployment
    # The method changes the Application field values to its K8sInstance field values
    def test_application_copy_last_successful_deployment(self):
        self.app.copy_last_successful_deployment()
        self.assertEqual(self.app.descriptive_name, 'My Application')
        self.assertEqual(self.app.port, 1234)
        self.assertEqual(self.app.authentication, 'uvic')

        # Routes of the Application should also be converted to routes included
        # in the 'authenticated_routes_str' and 'unauthenticated_routes_str' of K8sInstance
        self.assertEqual(len(self.app.route_set.filter(authenticated=True)), 2)
        self.assertEqual(len(self.app.route_set.filter(authenticated=False)), 1)
        # Note the string has the routes reversed from what the K8sInstace has
        # a Routes created for the Application are sorted by the 'route' field
        self.assertEqual(self.app.routes_to_str(authenticated=True), '/admin,/login')
        self.assertEqual(self.app.routes_to_str(authenticated=False), '/')
        self.assertEqual(self.app.allowed_ips, '192.168.1.1,192.168.0.1')
        self.assertEqual(self.app.replicas, 1)

# Test cases for model K8sContainer
class K8sContainerModelTestCases(ModelTestCase):
    def setUp(self):
        super().setUp()

        # Create an Application, some Containers and a K8sContainer for test cases for this model
        # These test cases will change the app configurations so they shouldn't
        # interact with self.main_app created above
        self.app = Application.objects.create(
            identifier='myapp',
            descriptive_name='My Application',
            description='This is my application',
            port=4321,
            authentication='uvic_social',
            database='PostgresSQL',
            owner=self.owner
        )

        self.app.container_set.create(
            image='container/myapp',
            image_tag='latest',
            port=4321,
            command='python myapp.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )
        self.app.container_set.create(
            image='container/myapp2',
            image_tag='latest',
            port=4322,
            command='python myapp2.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

        self.k8sinstance = K8sInstance.objects.create(
            application=self.app,
            descriptive_name='My Application',
            port=1234,
            authentication='uvic',
            authenticated_routes_str=',/login,/admin',
            unauthenticated_routes_str=',/'
        )

        self.k8scontainer = K8sContainer.objects.create(
            k8sinstance=self.k8sinstance,
            container = self.app.container_set.first(),
            image='container/k8scontainer',
            image_tag='latest',
            port=6000,
            command='python myapp.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

    # The creation above for the K8sContainer should be valid
    # Test the field contents of the instance
    def test_fields_valid(self):
        self.assertEqual(self.k8scontainer.k8sinstance, self.k8sinstance)
        self.assertEqual(self.k8scontainer.container, self.app.container_set.first())
        self.assertEqual(self.k8scontainer.image, 'container/k8scontainer')
        self.assertEqual(self.k8scontainer.image_tag, 'latest')
        self.assertEqual(self.k8scontainer.port, 6000)
        self.assertEqual(self.k8scontainer.command, 'python myapp.py')
        self.assertEqual(self.k8scontainer.environment_variables, 'VAR1==123\nVAR2==thisissecret')

    # Create an invalid instance
    def test_create_invalid(self):
        with self.assertRaises(IntegrityError):
            # Missing application
            K8sContainer.objects.create(
                image='container/k8scontainer',
                image_tag='latest',
                port=6000,
                command='python myapp.py',
                environment_variables='VAR1==123\nVAR2==thisissecret'
            )
            self.assertEqual(K8sContainer.objects.count, 1)

    # Test representation of K8sContainer objects
    def test_repr(self):
        self.assertEqual(repr(self.k8scontainer), 'container/k8scontainer:latest')

    # Test string representation of K8sContainer objects
    def test_str(self):
        self.assertEqual(str(self.k8scontainer), 'container/k8scontainer:latest')

    # Test application's copy_last_successful_deployment method
    # The method should change the Application's container field values to its K8sContainer field values
    def test_application_copy_last_successful_deployment(self):
        self.app.copy_last_successful_deployment()
        self.assertEqual(self.app.container_set.count(), 1)
        container = self.app.container_set.first()
        self.assertEqual(container.image, 'container/k8scontainer')
        self.assertEqual(container.image_tag, 'latest')
        self.assertEqual(container.port, 6000)
        self.assertEqual(container.command, 'python myapp.py')
        self.assertEqual(container.environment_variables, 'VAR1==123\nVAR2==thisissecret')


# Test cases for model Route
class RouteModelTestCases(ModelTestCase):
    # Create a valid instance
    def test_create_valid(self):
        route = Route.objects.create(
            route='/admin',
            authenticated=True,
            application=self.main_app
        )
        self.assertEqual(Route.objects.count(), 4)
        self.assertEqual(route.route, '/admin')
        self.assertEqual(route.authenticated, True)
        self.assertEqual(route.application, self.main_app)

    # Create an invalid instance
    def test_create_invalid(self):
        with self.assertRaises(IntegrityError):
            # Missing application
            Route.objects.create(
                route='/admin',
                authenticated=True
            )
            self.assertEqual(Route.objects.count(), 2)

    # Test representation of Route objects
    def test_repr(self):
        self.assertEqual(repr(self.main_app_auth_route), '/login')
        self.assertEqual(repr(self.main_app_unauth_route), '/')


# Test cases for model Deployment
class DeploymentModelTestCases(ModelTestCase):
    # Create a valid instance with all fields specified
    def test_create_valid_all_fields_specified(self):
        deployment = Deployment.objects.create(
            application=self.main_app,
            log_file='strap-test.log',
            state=0,
            process_ids='1'
            # prev_state should be default to 0
        )
        self.assertEqual(deployment.application, self.main_app)
        self.assertEqual(deployment.log_file, 'strap-test.log')
        self.assertEqual(deployment.state, 0)
        self.assertEqual(deployment.process_ids, '1')
        self.assertEqual(deployment.prev_state, 0)
        self.assertEqual(Deployment.objects.count(), 1)

    # Specify only application, which is the only required field
    def test_create_valid_app_field_specified(self):
        deployment = Deployment.objects.create(
            application=self.main_app
        )
        self.assertEqual(deployment.application, self.main_app)
        self.assertEqual(deployment.log_file, '')
        self.assertEqual(deployment.state, 0)
        self.assertEqual(deployment.process_ids, None)
        self.assertEqual(Deployment.objects.count(), 1)

    # Create an invalid instance
    def test_create_invalid(self):
        deployment = Deployment()
        deployment.application = self.main_app
        deployment.save()
        try:
            deployment.full_clean()
            assert False
        except ValidationError:
            assert True

    # Test getting state in string form
    def test_get_state(self):
        deployment = Deployment.objects.create(application=self.main_app)
        self.assertEqual(deployment.get_state(), 'None')

        deployment.state = 1
        deployment.save()
        self.assertEqual(deployment.get_state(), 'started')

        deployment.state = 2
        deployment.save()
        self.assertEqual(deployment.get_state(), 'deployed')

        deployment.state = 3
        deployment.save()
        self.assertEqual(deployment.get_state(), 'stopped')

        deployment.state = 4
        deployment.save()
        self.assertEqual(deployment.get_state(), 'failed')

        deployment.state = 5
        deployment.save()
        self.assertEqual(deployment.get_state(), 'deployed + started')

        deployment.state = 6
        deployment.save()
        self.assertEqual(deployment.get_state(), 'deployed + failed')

    # Test getting status of a deployment
    def test_get_status(self):
        deployment = Deployment.objects.create(application=self.main_app)
        self.assertEqual(deployment.get_status(), 'None')

        deployment.status = 1
        deployment.save()
        self.assertEqual(deployment.get_status(), 'deploying')

        deployment.status = 2
        deployment.save()
        self.assertEqual(deployment.get_status(), 'succeeded')

        deployment.status = 3
        deployment.save()
        self.assertEqual(deployment.get_status(), 'failed')

        deployment.status = 4
        deployment.save()
        self.assertEqual(deployment.get_status(), 'canceling')

        deployment.status = 5
        deployment.save()
        self.assertEqual(deployment.get_status(), 'canceled')

        deployment.status = 6
        deployment.save()
        self.assertEqual(deployment.get_status(), 'stopping')

        deployment.status = 7
        deployment.save()
        self.assertEqual(deployment.get_status(), 'stopped')

        deployment.status = 8
        deployment.save()
        self.assertEqual(deployment.get_status(), 'destroying')

        deployment.status = 9
        deployment.save()
        self.assertEqual(deployment.get_status(), 'destroyed')

        deployment.status = 10
        deployment.save()
        self.assertEqual(deployment.get_status(), 'updating')

        deployment.status = 11
        deployment.save()
        self.assertEqual(deployment.get_status(), 'reverting')

    # Test if a deployment is stoppable
    def test_is_stoppable(self):
        deployment = Deployment.objects.create(application=self.main_app)
        self.assertEqual(deployment.is_stoppable(), False)

        deployment.state = 1
        deployment.save()
        self.assertEqual(deployment.is_stoppable(), True)

        deployment.state = 2
        deployment.save()
        self.assertEqual(deployment.is_stoppable(), True)

        deployment.state = 3
        deployment.save()
        self.assertEqual(deployment.is_stoppable(), True)

        deployment.state = 4
        deployment.save()
        self.assertEqual(deployment.is_stoppable(), True)

        deployment.state = 5
        deployment.save()
        self.assertEqual(deployment.is_stoppable(), True)

        deployment.state = 6
        deployment.save()
        self.assertEqual(deployment.is_stoppable(), True)
