# pylint:
import json

from django.test import TestCase, Client, override_settings
from django.contrib.auth.models import User
from django.urls import reverse
from normal_users.models import Application, Route, Deployment
# pylint: disable=no-name-in-module
# TODO: Figure out why no-name-in-module is being raised
# when the code is working fine
from strapper.test_settings import TEST_SETTINGS
# pylint: enable=no-name-in-module

# General set up for view test cases
@override_settings(**TEST_SETTINGS)
class HelperViewTestCase(TestCase):
    def setUp(self):
        # Create 3 users with 3 roles to the main app: app's owner, editor and viewer
        self.user = User.objects.create(username='owner@example.org', password='ownerpwd')
        self.editor = User.objects.create(username='editor@example.org', password='editorpwd')
        self.viewer = User.objects.create(username='viewer@example.org', password='viewerpwd')

        self.owner_client = Client(HTTP_X_FORWARDED_USER="owner@example.org")
        self.owner_client.get("/login/", follow=True)

        self.editor_client = Client(HTTP_X_FORWARDED_USER="editor@example.org")
        self.editor_client.get("/login/", follow=True)

        self.viewer_client = Client(HTTP_X_FORWARDED_USER="viewer@example.org")
        self.viewer_client.get("/login/", follow=True)

        # This user doesn't own any app, therefore cannot access any
        # urls that are specific to an app such as deletion and deployment url
        self.unauthz_user = User.objects.create(
            username='unauthz@example.org',
            password='unauthzpwd',
            is_staff=True
        )

        self.unauthz_client = Client(HTTP_X_FORWARDED_USER="unauthz@example.org")
        self.unauthz_client.get("/login/", follow=True)

        # Create an application and 2 routes
        self.main_app = Application.objects.create(
            identifier='testapp',
            descriptive_name='Test App',
            description='This is an app for testing',
            port=1234,
            authentication='uvic',
            owner=self.user
        )

        # Populate roles for the main app
        self.main_app.approlegroup_set.get(role="editor").users.add(self.editor)
        self.main_app.approlegroup_set.get(role="viewer").users.add(self.viewer)

        self.main_app_auth_route = Route.objects.create(
            route='/login',
            authenticated=True,
            application=self.main_app
        )
        self.main_app_unauth_route = Route.objects.create(
            route='/',
            authenticated=False,
            application=self.main_app
        )

        # Create 2 containers for the application
        self.main_app_container = self.main_app.container_set.create(
            image='unit/testapp',
            image_tag='latest',
            port=1234,
            command='python testapp.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )
        self.main_app_container2 = self.main_app.container_set.create(
            image='unit/testapp2',
            image_tag='latest',
            port=1235,
            command='python testapp2.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

        # Create a group for the application
        self.main_app_group = self.main_app.group_set.create(
            name='Main app original group',
            is_everyone=False,
            users='owner@example.org,editor@example.org',
            created_by=self.user
        )

        # Test if the instances are created properly
        self.assertEqual(Application.objects.count(), 1)
        self.assertEqual(Route.objects.count(), 2)
        self.assertEqual(self.main_app.container_set.count(), 2)
        self.assertEqual(self.main_app.group_set.count(), 1)

    def test_deployment_app_info_view(self):
        """ Return the followings for an app deployment:
            - Databases
            - DB owner
            - DB initial password
        """
        # Test normal GET. Should not be allowed
        response = self.owner_client.get(
            reverse('app_info', args=[self.main_app.identifier]),
            follow=True)
        content = json.loads(response.content)
        self.assertEqual(content['status'], 400)
        self.assertEqual(content['message'], "Can't access this URL directly")

        # Test AJAX GET
        # Unauthorized user tries to get the app info. Shoud receive a JSON response with a 403 status
        response = self.unauthz_client.get(
            reverse('app_info', args=[self.main_app.identifier]),
            follow=True,
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = json.loads(response.content)
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], "User is unauthorized to access this url")

        # Viewer tries to get the app info. Shoud receive a JSON response with a 403 status
        response = self.viewer_client.get(
            reverse('app_info', args=[self.main_app.identifier]),
            follow=True,
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = json.loads(response.content)
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], "User is unauthorized to access this url")

        # Deployment doesn't exist for app
        response = self.owner_client.get(
            reverse('app_info', args=[self.main_app.identifier]),
            follow=True,
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = json.loads(response.content)
        self.assertEqual(content['status'], 404)
        self.assertEqual(content['message'], "There is no deployment for this app")

        # Create a deployment for the app
        Deployment.objects.create(
            application=self.main_app,
            state=2,
            status=2,
            databases='testapp_dev,testapp_prod',
            db_owner='testapp_owner',
            db_password='testapp_password'
        )

        # Editor tries to get the app info. Shoud be valid
        response = self.editor_client.get(
            reverse('app_info', args=[self.main_app.identifier]),
            follow=True,
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = json.loads(response.content)
        self.assertEqual(content['status'], 200)
        self.assertEqual(content['databases'], 'testapp_dev,testapp_prod')
        self.assertEqual(content['dbowner'], 'testapp_owner')
        self.assertEqual(content['dbpassword'], 'testapp_password')
        self.assertEqual(content['app_url'], self.main_app.get_url())

        # User has access to app
        response = self.owner_client.get(
            reverse('app_info', args=[self.main_app.identifier]),
            follow=True,
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = json.loads(response.content)
        self.assertEqual(content['status'], 200)
        self.assertEqual(content['databases'], 'testapp_dev,testapp_prod')
        self.assertEqual(content['dbowner'], 'testapp_owner')
        self.assertEqual(content['dbpassword'], 'testapp_password')
        self.assertEqual(content['app_url'], self.main_app.get_url())
