# pylint:
import os
import json
import shutil

from django.test import TestCase, Client, override_settings
from django.contrib.auth.models import User
from django.urls import reverse

from normal_users.models import (Application, Group, Route, Deployment, K8sInstance,
                                 K8sContainer, ApplicationQuotaRequest,
                                 RequestStatusTypes, Notification, NotificationTypes,
                                 NotificationStatusTypes)
from admin_users.models import GeneralConfig

# pylint: disable=no-name-in-module
# TODO: Figure out why no-name-in-module is being raised
# when the code is working fine
from strapper.test_settings import TEST_SETTINGS
# pylint: enable=no-name-in-module

# General set up for view test cases
@override_settings(**TEST_SETTINGS)
class ViewTestCase(TestCase):
    def setUp(self):
        # Create 3 users with 3 roles to the main app: app's owner, editor and viewer
        self.admin = User.objects.create(username='admin@example.org', password='adminpwd', is_staff=True)
        self.owner = User.objects.create(username='owner@example.org', password='ownerpwd')
        self.editor = User.objects.create(username='editor@example.org', password='editorpwd')
        self.viewer = User.objects.create(username='viewer@example.org', password='viewerpwd')

        self.owner_client = Client(HTTP_X_FORWARDED_USER="owner@example.org")
        self.owner_client.get("/login/", follow=True)

        self.editor_client = Client(HTTP_X_FORWARDED_USER="editor@example.org")
        self.editor_client.get("/login/", follow=True)

        self.viewer_client = Client(HTTP_X_FORWARDED_USER="viewer@example.org")
        self.viewer_client.get("/login/", follow=True)

        # Create a user that has no access to the main app
        self.unauthz_user = User.objects.create(
            username='unauthz@example.org',
            password='unauthzpwd',
        )

        self.unauthz_client = Client(HTTP_X_FORWARDED_USER="unauthz@example.org")
        self.unauthz_client.get("/login/", follow=True)

        # Create an application and 2 routes
        self.main_app = Application.objects.create(
            identifier='testapp',
            descriptive_name='Test App',
            description='This is an app for testing',
            port=1234,
            authentication='uvic',
            is_secure=True,
            allowed_ips='192.168.1.1,192.168.0.1',
            owner=self.owner
        )

        self.main_app_auth_route = Route.objects.create(
            route='/login',
            authenticated=True,
            application=self.main_app
        )
        self.main_app_unauth_route = Route.objects.create(
            route='/',
            authenticated=False,
            application=self.main_app
        )

        # Populate roles for the main app
        self.main_app.approlegroup_set.get(role="editor").users.add(self.editor)
        self.main_app.approlegroup_set.get(role="viewer").users.add(self.viewer)

        # Create 2 containers for the application
        self.main_app_container = self.main_app.container_set.create(
            image='unit/testapp',
            image_tag='latest',
            port=1234,
            command='python testapp.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )
        self.main_app_container2 = self.main_app.container_set.create(
            image='unit/testapp2',
            image_tag='latest',
            port=1235,
            command='python testapp2.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

        # Create a group for the application
        self.main_app_group = self.main_app.group_set.create(
            name='Main app original group',
            is_everyone=False,
            users='owner@example.org,viewer@example.org',
            created_by=self.owner
        )

        # Test if the instances are created properly
        self.assertEqual(Application.objects.count(), 1)
        self.assertEqual(Route.objects.count(), 2)
        self.assertEqual(self.main_app.container_set.count(), 2)
        self.assertEqual(self.main_app.group_set.count(), 1)

        # Give main app a quota of 3 replicas
        self.main_app.applicationquota.replicas = 3
        self.main_app.applicationquota.save()

# Test cases for Dashboard view
class DashboardViewTestCases(ViewTestCase):
    """
    Test visiting the dashboard page
    """
    def test_get(self):
        # Owner visits the dashboard
        response = self.owner_client.get(reverse('dashboard'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/dashboard/index.html')
        context_data = response.context_data
        self.assertEqual(len(context_data['owned_applications']), 1)
        self.assertEqual(context_data['owned_applications'][0], self.main_app)
        self.assertEqual(len(context_data['editor_applications']), 0)
        self.assertEqual(len(context_data['viewer_applications']), 0)
        existing_identifiers = json.loads(context_data['existing_identifiers'])
        self.assertEqual(len(existing_identifiers), 1)
        self.assertEqual(existing_identifiers[0], 'testapp')
        self.assertEqual(context_data['message'], None)
        self.assertEqual(context_data['success'], None)

        # Editor visits the dashboard
        response = self.editor_client.get(reverse('dashboard'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/dashboard/index.html')
        context_data = response.context_data
        self.assertEqual(len(context_data['owned_applications']), 0)
        self.assertEqual(len(context_data['editor_applications']), 1)
        self.assertEqual(len(context_data['viewer_applications']), 0)

        # Viewer visits the dashboard
        response = self.viewer_client.get(reverse('dashboard'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/dashboard/index.html')
        context_data = response.context_data
        self.assertEqual(len(context_data['owned_applications']), 0)
        self.assertEqual(len(context_data['editor_applications']), 0)
        self.assertEqual(len(context_data['viewer_applications']), 1)

    def test_post_create_valid(self):
        """
        # Test POST method of Dashboard, creating a new application (valid)
        """
        my_vpn = {
            "My VPN": ['1.1.1.1/1', '2.2.2.2/2']
        }
        general_config = GeneralConfig.get_instance()
        general_config.preset_ips = json.dumps(my_vpn)
        general_config.save()

        # Create a new application called 'Test App 2'
        response = self.owner_client.post(reverse('dashboard'), {
            'identifier': 'testapp2',
            'descriptive_name': 'Test App 2',
            'description': 'This is another app for testing',
            'authentication': 'uvic',
            'database': 'PostgreSQL',
            'owner': self.owner,
            'authenticated_routes': '/login/,/admin/',
            'unauthenticated_routes': '/',
            'ip_access': 'limit',
            'other_ips': 'on',
            'My VPN': 'on',
            'allowed_ips': ['192.168.1.2', '192.168.0.2']
        }, follow=True, HTTP_REFERER=reverse('dashboard'))

        # Test response info
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/app.html')

        # There should be a new application and 3 routes created
        self.assertEqual(Application.objects.count(), 2)
        self.assertEqual(Route.objects.count(), 5)

        # Test information of the new app
        new_app = Application.objects.all()[0]
        self.assertEqual(new_app.identifier, 'testapp2')
        self.assertEqual(new_app.descriptive_name, 'Test App 2')
        self.assertEqual(new_app.description, 'This is another app for testing')
        self.assertEqual(new_app.port, None)
        self.assertEqual(new_app.authentication, 'uvic')
        self.assertEqual(new_app.database, 'PostgreSQL')
        self.assertIn('My VPN', new_app.get_allowed_ips())
        self.assertIn('1.1.1.1/1', new_app.get_allowed_ips()['My VPN'])
        self.assertIn('2.2.2.2/2', new_app.get_allowed_ips()['My VPN'])
        self.assertIn('192.168.1.2', new_app.get_allowed_ips()['others'])
        self.assertIn('192.168.0.2', new_app.get_allowed_ips()['others'])
        self.assertEqual(new_app.owner, self.owner)

        # Test routes of the new app
        self.assertEqual(len(new_app.route_set.all()), 3)
        self.assertEqual(len(new_app.route_set.filter(route='/login/', authenticated=True)), 1)
        self.assertEqual(len(new_app.route_set.filter(route='/admin/', authenticated=True)), 1)
        self.assertEqual(len(new_app.route_set.filter(route='/', authenticated=False)), 1)

        # Test context of the view
        context_data = response.context_data
        self.assertEqual(context_data['message'], 'App <strong>Test App 2</strong> created successfully.')
        self.assertTrue(context_data['success'])
        self.assertEqual(len(context_data['owned_applications']), 2)

    def test_post_create_invalid(self):
        """
        # Test POST method of Dashboard, creating a new application (invalid)
        """
        # This is should be invalid because identifier 'testapp' already in use
        response = self.owner_client.post(reverse('dashboard'), {
            'identifier': 'testapp',
            'descriptive_name': 'Test App 2',
            'description': 'This is another app for testing',
            'authentication': 'Uvic', # invalid as 'Uvic' is not an option (should be 'uvic')
            'owner': self.owner,
            'authenticated_routes': '/login/,/admin/',
            'unauthenticated_routes': '/'
        }, follow=True, HTTP_REFERER=reverse('dashboard'))

        # Expects no new application or route created
        self.assertEqual(Application.objects.count(), 1)
        self.assertEqual(Route.objects.count(), 2)

        # Test context of the view
        context_data = response.context_data
        self.assertEqual(len(context_data['owned_applications']), 1)
        self.assertEqual(len(json.loads(context_data['existing_identifiers'])), 1)
        self.assertEqual(context_data['message'], "Attempt to create app failed. \
            App form was not filled out properly. This might due to an application error, which has \
            been logged. For further support, please create an issue <a target='_blank' \
            href='https://gitlab.com/uvic-arcsoft/strap-django-app/-/issues'>here</a>")
        self.assertFalse(context_data['success'])

    def test_post_edit_valid(self):
        """
        Test POST method of Dashboard, editing an existing application (valid)
        """
        # Edit 'Test App' into 'Test App 2' with many different information
        response = self.owner_client.post(reverse('edit_app', args=[self.main_app.identifier]), {
            'identifier': 'testapp',
            'descriptive_name': 'Test App 2',
            'description': 'This is another app for testing',
            'authentication': 'uvic',
            'database': 'PostgreSQL',
            'owner': self.owner,
            'authenticated_routes': '/login/,/admin/',
            'unauthenticated_routes': '/',
            'ip_access': 'limit',
            'other_ips': 'on',
            'allowed_ips': ['192.168.1.2', '192.168.0.2']
        }, follow=True, HTTP_REFERER=reverse('dashboard'))

        # Test response info
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/app.html')

        # There should not be any new application created
        # There should be 3 routes in total, all belong to the 'Test App'
        self.assertEqual(Application.objects.count(), 1)
        self.assertEqual(Route.objects.count(), 3)

        self.main_app = Application.objects.all()[0]

        # Check information of the edited version of the existing app
        self.assertEqual(self.main_app.identifier, 'testapp')
        self.assertEqual(self.main_app.descriptive_name, 'Test App 2')
        self.assertEqual(self.main_app.description, 'This is another app for testing')
        self.assertEqual(self.main_app.authentication, 'uvic')
        self.assertIn('192.168.1.2', self.main_app.get_allowed_ips()['others'])
        self.assertIn('192.168.0.2', self.main_app.get_allowed_ips()['others'])
        self.assertEqual(self.main_app.owner, self.owner)

        # Test routes of the new app
        self.assertEqual(len(self.main_app.route_set.all()), 3)
        self.assertEqual(len(self.main_app.route_set.filter(route='/login/', authenticated=True)), 1)
        self.assertEqual(len(self.main_app.route_set.filter(route='/admin/', authenticated=True)), 1)
        self.assertEqual(len(self.main_app.route_set.filter(route='/', authenticated=False)), 1)

        # Test context of the view
        context_data = response.context_data
        self.assertEqual(context_data['message'], 'App <strong>Test App 2</strong> edited successfully.')
        self.assertTrue(context_data['success'])
        self.assertEqual(len(context_data['owned_applications']), 1)

        # Editor tries to edit the app. Should be valid
        response = self.editor_client.post(reverse('edit_app', args=[self.main_app.identifier]), {
            'identifier': 'testapp',
            'descriptive_name': 'Test App 2',
            'description': 'This is another app for testing',
            'authentication': 'uvic',
            'database': 'PostgreSQL',
            'owner': self.owner,
            'authenticated_routes': '/login/,/admin/',
            'unauthenticated_routes': '/'
        }, follow=True, HTTP_REFERER=reverse('dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/app.html')

        # Viewers tries to edit the app. Should receive a JSON response with 403 status
        response = self.viewer_client.post(reverse('edit_app', args=[self.main_app.identifier]), {
            'identifier': 'testapp',
            'descriptive_name': 'Test App 2',
            'description': 'This is another app for testing',
            'authentication': 'uvic',
            'database': 'PostgreSQL',
            'owner': self.owner,
            'authenticated_routes': '/login/,/admin/',
            'unauthenticated_routes': '/'
        }, follow=True, HTTP_REFERER=reverse('dashboard'))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to edit this app'
        })

        # Unauthz user tries to edit the app. Should receive a JSON response with 403 status
        response = self.unauthz_client.post(reverse('edit_app', args=[self.main_app.identifier]), {
            'identifier': 'testapp',
            'descriptive_name': 'Test App 2',
            'description': 'This is another app for testing',
            'authentication': 'uvic',
            'database': 'PostgreSQL',
            'owner': self.owner,
            'authenticated_routes': '/login/,/admin/',
            'unauthenticated_routes': '/'
        }, follow=True, HTTP_REFERER=reverse('dashboard'))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to edit this app'
        })

    def test_post_edit_invalid(self):
        """
        Test POST method of Dashboard, editing an existing application (invalid)
        """
        # This is should be invalid because descriptive_name cannot be omitted
        response = self.owner_client.post(reverse('edit_app', args=[self.main_app.identifier]), {
            'identifier': 'testapp',
            'description': 'This is another app for testing',
            'port': 2345,
            'authentication': 'Uvic', # invalid as 'Uvic' is not an option (should be 'uvic')
            'owner': self.owner,
            'authenticated_routes': '/login/,/admin/',
            'unauthenticated_routes': '/'
        }, follow=True, HTTP_REFERER=reverse('dashboard'))

        # Expects no new application or route created
        self.assertEqual(Application.objects.count(), 1)
        self.assertEqual(Route.objects.count(), 2)

        app = Application.objects.all()[0]

        # The information of the existing app should remain unchanged
        self.assertEqual(self.main_app.identifier, app.identifier)
        self.assertEqual(self.main_app.descriptive_name, app.descriptive_name)
        self.assertEqual(self.main_app.description, app.description)
        self.assertEqual(self.main_app.port, app.port)
        self.assertEqual(self.main_app.authentication, app.authentication)
        self.assertEqual(self.main_app.owner, app.owner)

        # Test context of the view
        context_data = response.context_data
        self.assertEqual(len(context_data['owned_applications']), 1)
        self.assertEqual(len(json.loads(context_data['existing_identifiers'])), 1)
        self.assertEqual(context_data['message'], "Attempt to edit app failed. \
            App form was not filled out properly. This might due to an application error, which has \
            been logged. For further support, please create an issue <a target='_blank' \
            href='https://gitlab.com/uvic-arcsoft/strap-django-app/-/issues'>here</a>")
        self.assertFalse(context_data['success'])

# Test cases for ApplicationView
class ApplicationViewTestCases(ViewTestCase):
    # Test GET method of ApplicationView, getting an existing application
    def test_get_valid(self):
        response = self.owner_client.get(
            reverse('get_app', args=[self.main_app.identifier]),
            follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/app.html')
        context_data = response.context_data
        self.assertEqual(context_data['application'], self.main_app)
        self.assertEqual(context_data['owned_applications'][0], self.main_app)
        self.assertEqual(len(context_data['containers']), 2)
        self.assertEqual(context_data['groups'][0], self.main_app_group)
        self.assertEqual(context_data['owned_groups'][0], self.main_app_group)
        self.assertEqual(context_data['deployment'], None)
        self.assertEqual(context_data['k8sinstance'], None)
        self.assertEqual(context_data['status'], 200)

        # Editor tries to visit the app. Should be valid
        response = self.editor_client.get(
            reverse('get_app', args=[self.main_app.identifier]),
            follow=True)
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)

        # Viewer tries to visit the app. Should be valid
        response = self.viewer_client.get(
            reverse('get_app', args=[self.main_app.identifier]),
            follow=True)
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)

        # Unauthz user tries to visit the app. Should receive a JSON response with 403 status
        response = self.unauthz_client.get(
            reverse('get_app', args=[self.main_app.identifier]),
            follow=True)
        context_data = response.context_data
        self.assertEqual(context_data['status'], 403)
        self.assertEqual(context_data['message'], 'User is unauthorized to access this url')

    def test_get_invalid(self):
        """
        Test GET method of ApplicationView, getting a non-existing application
        """
        response = self.owner_client.get(reverse('get_app', args=['doesnotexist']), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/app.html')
        context_data = response.context_data
        self.assertEqual(context_data['status'], 404)
        self.assertEqual(context_data['message'], 'Application does not exist')


# Test cases for ApplicationDeleteView
class ApplicationDeleteViewTestCases(ViewTestCase):
    def test_delete_valid_with_no_deployment(self):
        """
        Test GET method, deleting an existing application (valid)
        This application has never been deployed
        """
        with self.settings(TESTING=True):
            # Editor tries to delete the app. Should receive a JSON response with 403 status
            response = self.editor_client.get(
                reverse('delete_app', args=[self.main_app.identifier]),
                follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
            self.assertEqual(response.status_code, 403)
            self.assertEqual(response.json(), {
                'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
            })

            # Viewer tries to delete the app. Should receive a JSON response with 403 status
            response = self.viewer_client.get(
                reverse('delete_app', args=[self.main_app.identifier]),
                follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
            self.assertEqual(response.status_code, 403)
            self.assertEqual(response.json(), {
                'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
            })

            # Unauthz user tries to delete the app. Should receive a JSON response with 403 status
            response = self.unauthz_client.get(
                reverse('delete_app', args=[self.main_app.identifier]),
                follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
            self.assertEqual(response.status_code, 403)
            self.assertEqual(response.json(), {
                'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
            })

        # Owner deletes the app. Should be valid
        response = self.owner_client.get(
            reverse('delete_app', args=[self.main_app.identifier]),
            follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        # Test response info
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/dashboard/index.html')

        # The application and the related routes should be deleted
        self.assertEqual(Application.objects.count(), 0)
        self.assertEqual(Route.objects.count(), 0)

        # Test context of the view
        context_data = response.context_data
        self.assertEqual(context_data['message'], 'App <strong>Test App</strong> deleted successfully')
        self.assertTrue(context_data['success'])
        self.assertEqual(len(context_data['owned_applications']), 0)
        self.assertEqual(len(json.loads(context_data['existing_identifiers'])), 0)

    def test_delete_valid_with_deployment(self):
        """
        Test GET method, deleting an existing application (valid)
        This application has been deployed before and the deployment
        is in a 'None' state now
        """
        # Create a deployment in a 'None' state for this app
        Deployment.objects.create(
            application=self.main_app,
            log_file='testapp.log',
            state=0,
            status=9,
        )
        # Delete the app
        response = self.owner_client.get(
            reverse('delete_app', args=[self.main_app.identifier]),
            follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        # Test response info
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'normal_users/dashboard/index.html')

        # The application and the related routes should be deleted
        self.assertEqual(Application.objects.count(), 0)
        self.assertEqual(Route.objects.count(), 0)

        # Test context of the view
        context_data = response.context_data
        self.assertEqual(context_data['message'], 'App <strong>Test App</strong> deleted successfully')
        self.assertTrue(context_data['success'])
        self.assertEqual(len(context_data['owned_applications']), 0)
        self.assertEqual(len(json.loads(context_data['existing_identifiers'])), 0)

    def test_get_invalid(self):
        """
        Test GET method, deleting a non-existing application (invalid)
        This might be an application that the current client doesn't have access to
        """
        # App with identifier 'doesnotexist' does not exist
        response = self.owner_client.get(
            reverse('delete_app', args=['doesnotexist']),
            follow=True, HTTP_REFERER=reverse('dashboard'))

        # Test response info
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })

        # The application and the related routes should not be deleted
        self.assertEqual(Application.objects.count(), 1)
        self.assertEqual(Route.objects.count(), 2)


# Test cases for ContainerAPIView
class ContainerAPIViewTestCase(ViewTestCase):
    def test_post_create_container(self):
        """
        Test POST method, create a new container for an application
        """
        # Owner creates a new container for the main app. Should be valid
        response = self.owner_client.post(
            reverse('create_container', args=[self.main_app.identifier]),
            {
                'image': 'unit/testapp3',
                'image_tag': 'latest',
                'port': 1236,
                'is_main': True,
                'command': 'python testapp3.py',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret'
            }, follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        # Test the new container
        self.assertEqual(self.main_app.container_set.count(), 3)
        new_container = self.main_app.container_set.get(image='unit/testapp3')
        self.assertEqual(new_container.image_tag, 'latest')
        self.assertEqual(new_container.port, 1236)
        # App should have main container's port
        self.main_app.refresh_from_db()
        self.assertEqual(self.main_app.port, 1236)
        self.assertEqual(new_container.command, 'python testapp3.py')
        self.assertEqual(new_container.environment_variables, 'VAR1==123\nVAR2==thisissecret')

        # Test context of the view
        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            f"Create container <strong>{new_container}</strong> successfully.")

        # Editor tries to create a new container for the app. Should be valid
        response = self.editor_client.post(
            reverse('create_container', args=[self.main_app.identifier]),
            {
                'image': 'unit/testapp4',
                'image_tag': 'latest',
                'port': 1236,
                'is_main': True,
                'command': 'python testapp4.py',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret'
            }, follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.main_app.container_set.count(), 4)

        # Viewer tries to create a new container for the app. Should receive a JSON response with 403 status
        response = self.viewer_client.post(
            reverse('create_container', args=[self.main_app.identifier]),
            {
                'image': 'unit/testapp4',
                'image_tag': 'latest',
                'port': 1236,
                'is_main': True,
                'command': 'python testapp4.py',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret'
            }, follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })

        # Unauthz user tries to create a new container for the app. Should receive a JSON response with 403 status
        response = self.unauthz_client.post(
            reverse('create_container', args=[self.main_app.identifier]),
            {
                'image': 'unit/testapp4',
                'image_tag': 'latest',
                'port': 1236,
                'is_main': True,
                'command': 'python testapp4.py',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret'
            }, follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(),  {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })

    def test_post_edit_container(self):
        """
        Test POST method, edit an existing container for an application
        """
        # Owner edits the first container of the main app. Should be valid
        container = self.main_app.container_set.first()
        response = self.owner_client.post(
            reverse('edit_container', args=[self.main_app.identifier, container.id]),
            {
                'image': 'unit/testapp',
                'image_tag': '1.0.0',
                'port': 4000,
                'is_main': True,
                'command': 'python testapp.py --edit',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue'
            }, follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        # Test the edited container
        self.assertEqual(self.main_app.container_set.count(), 2)
        container = self.main_app.container_set.first()
        self.assertEqual(container.image_tag, '1.0.0')
        self.assertEqual(container.port, 4000)
        # App should have main container's port
        self.main_app.refresh_from_db()
        self.assertEqual(self.main_app.port, 4000)
        self.assertEqual(container.command, 'python testapp.py --edit')
        self.assertEqual(container.environment_variables, 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue')

        # Test context of the view
        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            f"Edit container <strong>{container}</strong> successfully.")

        # Editor tries to edit the container. Should be valid
        response = self.editor_client.post(
            reverse('edit_container', args=[self.main_app.identifier, container.id]),
            {
                'image': 'unit/testapp',
                'image_tag': '2.0.0',
                'port': 4000,
                'is_main': True,
                'command': 'python testapp.py --edit',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue'
            }, follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 200)
        container = self.main_app.container_set.first()
        self.assertEqual(container.image_tag, '2.0.0')

        # Viewer tries to edit the container. Should receive a JSON response with 403 status
        response = self.viewer_client.post(
            reverse('edit_container', args=[self.main_app.identifier, container.id]),
            {
                'image': 'unit/testapp',
                'image_tag': '3.0.0',
                'port': 4000,
                'is_main': True,
                'command': 'python testapp.py --edit',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue'
            }, follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        container.refresh_from_db()
        self.assertEqual(container.image_tag, '2.0.0')

        # Unauthz user tries to edit the container. Should receive a JSON response with 403 status
        response = self.unauthz_client.post(
            reverse('edit_container', args=[self.main_app.identifier, container.id]),
            {
                'image': 'unit/testapp',
                'image_tag': '3.0.0',
                'port': 4000,
                'is_main': True,
                'command': 'python testapp.py --edit',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue'
            }, follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(),  {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        container.refresh_from_db()
        self.assertEqual(container.image_tag, '2.0.0')

    def test_ajax_post_create_container(self):
        """
        Test AJAX POST method, create a new container for an application
        """
        # Owner creates a new container for the main app. Should be valid
        response = self.owner_client.post(
            reverse('create_container', args=[self.main_app.identifier]),
            {
                'image': 'unit/testapp3',
                'image_tag': 'latest',
                'port': 1236,
                'command': 'python testapp3.py',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        # Test the new container
        self.assertEqual(response.status_code, 201)
        self.assertEqual(self.main_app.container_set.count(), 3)
        new_container = self.main_app.container_set.get(image='unit/testapp3')
        self.assertEqual(new_container.image_tag, 'latest')
        self.assertEqual(new_container.port, 1236)
        self.assertEqual(new_container.command, 'python testapp3.py')
        self.assertEqual(new_container.environment_variables, 'VAR1==123\nVAR2==thisissecret')

        # Test response content
        content = response.json()
        self.assertEqual(content['message'], 'Create container <strong>unit/testapp3:latest</strong> successfully.')
        container_content = content['container']
        self.assertEqual(container_content['id'], new_container.id)
        self.assertEqual(container_content['image'], new_container.image)
        self.assertEqual(container_content['image_tag'], new_container.image_tag)
        self.assertEqual(container_content['port'], new_container.port)
        self.assertEqual(container_content['command'], new_container.command)
        self.assertEqual(container_content['environment_variables'], new_container.environment_variables)

        # Editor tries to create a new container for the app. Should be valid
        response = self.editor_client.post(
            reverse('create_container', args=[self.main_app.identifier]),
            {
                'image': 'unit/testapp4',
                'image_tag': 'latest',
                'port': 1236,
                'command': 'python testapp4.py',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(self.main_app.container_set.count(), 4)

        # Viewer tries to create a new container for the app. Should receive a JSON response with 403 status
        response = self.viewer_client.post(
            reverse('create_container', args=[self.main_app.identifier]),
            {
                'image': 'unit/testapp4',
                'image_tag': 'latest',
                'port': 1236,
                'command': 'python testapp4.py',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(),  {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })

        # Unauthz user tries to create a new container for the app. Should receive a JSON response with 403 status
        response = self.unauthz_client.post(
            reverse('create_container', args=[self.main_app.identifier]),
            {
                'image': 'unit/testapp4',
                'image_tag': 'latest',
                'port': 1236,
                'command': 'python testapp4.py',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(),  {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })

    def test_ajax_post_edit_container(self):
        """
        Test AJAX POST method, edit an existing container for an application
        """
        container = self.main_app.container_set.first()
        # Owner edits the first container of the main app. Should be valid
        response = self.owner_client.post(
            reverse('edit_container', args=[self.main_app.identifier, container.id]),
            {
                'image': 'unit/testapp',
                'image_tag': '1.0.0',
                'port': 4000,
                'command': 'python testapp.py --edit',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        # Test the edited container
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.main_app.container_set.count(), 2)
        container = self.main_app.container_set.first()
        self.assertEqual(container.image_tag, '1.0.0')
        self.assertEqual(container.port, 4000)
        self.assertEqual(container.command, 'python testapp.py --edit')
        self.assertEqual(container.environment_variables, 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue')

        # Test response content
        content = response.json()
        self.assertEqual(content['message'], 'Edit container <strong>unit/testapp:1.0.0</strong> successfully.')
        container_content = content['container']
        self.assertEqual(container_content['id'], container.id)
        self.assertEqual(container_content['image'], container.image)
        self.assertEqual(container_content['image_tag'], container.image_tag)
        self.assertEqual(container_content['port'], container.port)
        self.assertEqual(container_content['command'], container.command)
        self.assertEqual(container_content['environment_variables'], container.environment_variables)

        # Editor tries to edit the container. Should be valid
        response = self.editor_client.post(
            reverse('edit_container', args=[self.main_app.identifier, container.id]),
            {
                'image': 'unit/testapp',
                'image_tag': '2.0.0',
                'port': 4000,
                'command': 'python testapp.py --edit',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        container.refresh_from_db()
        self.assertEqual(container.image_tag, '2.0.0')

        # Viewer tries to edit the container. Should receive a JSON response with 403 status
        response = self.viewer_client.post(
            reverse('edit_container', args=[self.main_app.identifier, container.id]),
            {
                'image': 'unit/testapp',
                'image_tag': '3.0.0',
                'port': 4000,
                'command': 'python testapp.py --edit',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        container.refresh_from_db()
        self.assertEqual(container.image_tag, '2.0.0')

        # Unauthz user tries to edit the container. Should receive a JSON response with 403 status
        response = self.unauthz_client.post(
            reverse('edit_container', args=[self.main_app.identifier, container.id]),
            {
                'image': 'unit/testapp',
                'image_tag': '3.0.0',
                'port': 4000,
                'command': 'python testapp.py --edit',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(),  {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        container.refresh_from_db()
        self.assertEqual(container.image_tag, '2.0.0')

    def test_get_delete_container(self):
        """
        Test GET method, delete an existing container for an application
        """
        container = self.main_app.container_set.first()
        # Unauthz user tries to delete the container. Should receive a JSON response with 403 status
        response = self.unauthz_client.get(
            reverse('delete_container', args=[self.main_app.identifier, container.id]),
            follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        self.assertEqual(self.main_app.container_set.count(), 2)

        # Viewer tries to delete the container. Should receive a JSON response with 403 status
        response = self.viewer_client.get(
            reverse('delete_container', args=[self.main_app.identifier, container.id]),
            follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(),  {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        self.assertEqual(self.main_app.container_set.count(), 2)

        # Editor tries to delete the container. Should be valid
        response = self.editor_client.get(
            reverse('delete_container', args=[self.main_app.identifier, container.id]),
            follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.main_app.container_set.count(), 1)
        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
f"Delete container <strong>{container}</strong> for app <strong>{self.main_app.descriptive_name}</strong> successfully")

        # Owner deletes the container. Should be valid
        container = self.main_app.container_set.first()
        response = self.owner_client.get(
            f'/container/delete/testapp/{container.id}/', follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(self.main_app.container_set.count(), 0)
        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
f"Delete container <strong>{container}</strong> for app <strong>{self.main_app.descriptive_name}</strong> successfully")

    def test_ajax_get_delete_container(self):
        """
        # Test AJAX GET method, delete an existing container for an application
        """

        container = self.main_app.container_set.first()
        # Unauthz user tries to delete the container. Should receive a JSON response with 403 status
        response = self.unauthz_client.get(
            reverse('delete_container', args=[self.main_app.identifier, container.id]),
            follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        self.assertEqual(self.main_app.container_set.count(), 2)

        # Viewer tries to delete the container. Should receive a JSON response with 403 status
        response = self.viewer_client.get(
            reverse('delete_container', args=[self.main_app.identifier, container.id]),
            follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(),  {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        self.assertEqual(self.main_app.container_set.count(), 2)

        # Editor tries to delete the container. Should be valid
        response = self.editor_client.get(
            reverse('delete_container', args=[self.main_app.identifier, container.id]),
            follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.main_app.container_set.count(), 1)

        # Owner deletes the container. Should be valid
        container = self.main_app.container_set.first()
        response = self.owner_client.get(
            f'/container/delete/testapp/{container.id}/', follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.main_app.container_set.count(), 0)
        content = response.json()
        self.assertEqual(content['message'],
f"Delete container <strong>{container}</strong> for app <strong>{self.main_app.descriptive_name}</strong> successfully")

    def test_post_edit_non_existing_container(self):
        """
        Test POST method, edit a non-existing container for an application
        """
        non_existing_id = self.main_app.container_set.last().id + 1
        response = self.owner_client.post(
            reverse('edit_container', args=[self.main_app.identifier, non_existing_id]),
            {
                'image': 'unit/testapp',
                'image_tag': '1.0.0',
                'port': 4000,
                'command': 'python testapp.py --edit',
                'environment_variables': 'VAR1==123\nVAR2==thisissecret\nNEWVAR==newvalue'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        # Test response content
        content = response.json()
        self.assertEqual(response.status_code, 403)
        self.assertEqual(content['message'], 'User is unauthorized to access container')

    def test_get_delete_non_existing_container(self):
        """
        Test GET method, delete a non-existing container for an application
        """
        non_existing_id = self.main_app.container_set.last().id + 1
        response = self.owner_client.get(
            reverse('delete_container', args=[self.main_app.identifier, non_existing_id]),
            follow=True, HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        # Test response content
        content = response.json()
        self.assertEqual(response.status_code, 403)
        self.assertEqual(content['message'], 'User is unauthorized to access this url')

# Test cases for HarborAPIView
class HarborAPIViewTestCase(ViewTestCase):
    def setUp(self):
        super().setUp()

        # Create an app that is meant for a successful deployment
        self.successful_app = Application.objects.create(
            identifier='successfulapp',
            descriptive_name='Successful App',
            description='This is an app that will be deployed successfully',
            port=1234,
            authentication='uvic',
            owner=self.owner
        )
        self.successful_app.approlegroup_set.get(role="editor").users.add(self.editor)
        self.successful_app.approlegroup_set.get(role="viewer").users.add(self.viewer)

        # Create an app that is meant for a failed deployment
        self.failed_app = Application.objects.create(
            identifier='failedapp',
            descriptive_name='Failed App',
            description='This is an app that will fail to deploy',
            port=1234,
            authentication='uvic',
            owner=self.owner
        )
        self.failed_app.approlegroup_set.get(role="editor").users.add(self.editor)
        self.failed_app.approlegroup_set.get(role="viewer").users.add(self.viewer)

    def tearDown(self):
        # Remove logging directory created by Terraform process
        app_ids = ['successfulapp', 'failedapp']
        for app_id in app_ids:
            if os.path.exists(f"/tmp/terraform/{app_id}"):
                shutil.rmtree(f"/tmp/terraform/{app_id}")

    def test_post_create_harbor_project(self):
        """
        Test POST method, create a new harbor project for an application
        """
        # Successful
        response = self.owner_client.post(
            reverse('create_harbor_project', args=[self.successful_app.identifier]),
            follow=True, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        print(content)
        self.assertEqual(content['status'], 200)
        self.assertEqual(content['message'], 'Created a Harbor project successfully.')

        # Failed
        response = self.owner_client.post(
            reverse('create_harbor_project', args=[self.failed_app.identifier]),
            follow=True, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(content['status'], 400)
        self.assertEqual(
            content['message'],
            'Failed to create a Harbor project. Please try again or contact us for support.')

        # Editor tries to create a new harbor project for the app. Should be able to invoke
        # the API but receive a JSON response with 400 status since the app is supposed to fail
        response = self.editor_client.post(
            reverse('create_harbor_project', args=[self.failed_app.identifier]),
            follow=True, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(content['status'], 400)
        self.assertEqual(
            content['message'],
            'Failed to create a Harbor project. Please try again or contact us for support.')

        # Viewer tries to create a new harbor project for the app. Should receive a JSON response with 403 status
        response = self.viewer_client.post(
            reverse('create_harbor_project', args=[self.successful_app.identifier]),
            follow=True, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], 'User is unauthorized to access this url')

        # Unauthz user tries to create a new harbor project for the app. Should receive a JSON response with 403 status
        response = self.unauthz_client.post(
            reverse('create_harbor_project', args=[self.successful_app.identifier]),
            follow=True, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], 'User is unauthorized to access this url')

# Test cases for GroupView
class GroupViewTestCases(ViewTestCase):
    # This function runs before each test case in this class
    def setUp(self):
        super().setUp()

        # Create a group
        self.group = Group.objects.create(
            name = "Test Group",
            is_everyone = True,
            users = "user@example.org,owner@example.org",
            created_by=self.owner
        )
        self.group.application.add(self.main_app)

    def test_post_create_group_valid(self):
        """
        Test POST method: Create a group with a valid form
        """
        # Owner creates a new group for the main app. Should be valid
        response = self.owner_client.post(
            reverse('create_group', args=[self.main_app.identifier]),
            {
                'name': 'Test Users',
                'is_everyone': False,
                'users': 'user1@example.org, user2@example.org',
                'application': [self.main_app.identifier]
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Group.objects.count(), 3)

        new_group = self.main_app.group_set.get(name='Test Users')
        self.assertEqual(new_group.is_everyone, False)
        self.assertEqual(new_group.users, 'user1@example.org, user2@example.org')
        self.assertEqual(new_group.application.first(), self.main_app)

        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            f"Create group <strong>{new_group.name}</strong> successfully.")

        # Editor tries to create a new group for the app. Should be valid
        response = self.editor_client.post(
            reverse('create_group', args=[self.main_app.identifier]),
            {
                'name': 'Test Editors',
                'is_everyone': True,
                'users': '',
                'application': [self.main_app.identifier]
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Group.objects.count(), 4)

        # Viewer tries to create a new group for the app. Should receive a JSON response with 403 status
        response = self.viewer_client.post(
            reverse('create_group', args=[self.main_app.identifier]),
            {
                'name': 'Test Viewers',
                'is_everyone': True,
                'users': '',
                'application': [self.main_app.identifier]
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        self.assertEqual(Group.objects.count(), 4)

        # Unauthz user tries to create a new group for the app. Should receive a JSON response with 403 status
        response = self.unauthz_client.post(
            reverse('create_group', args=[self.main_app.identifier]),
            {
                'name': 'Test Unauthz',
                'is_everyone': True,
                'users': '',
                'application': [self.main_app.identifier]
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })
        self.assertEqual(Group.objects.count(), 4)

    def test_post_create_group_invalid(self):
        """
        Test POST method: Create a group with an invalid form
        """
        response = self.owner_client.post(
            reverse('create_group', args=[self.main_app.identifier]),
            {
                # Missing name
                'is_everyone': False,
                'users': 'user1@example.org, user2@example.org'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Group.objects.count(), 2)

        context_data = response.context_data
        self.assertEqual(context_data['success'], False)
        self.assertEqual(
            context_data['message'],
"Failed to create a group. Please fill out the group form properly.")

    def test_post_edit_group_valid(self):
        """
        Test POST method: Edit a group with a valid form
        """
        # Owner edits the group. Should be valid
        response = self.owner_client.post(
            reverse('edit_group', args=[self.main_app.identifier, self.group.id]),
            {
                'name': 'Test Users',
                'is_everyone': False,
                'users': 'user1@example.org, user2@example.org',
                'application': [self.main_app.identifier]
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Group.objects.count(), 2)

        new_group = self.main_app.group_set.get(name='Test Users')
        self.assertEqual(new_group.id, self.group.id)
        self.assertEqual(new_group.is_everyone, False)
        self.assertEqual(new_group.users, 'user1@example.org, user2@example.org')
        self.assertEqual(new_group.created_by, self.owner)

        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            f"Edit group <strong>{new_group.name}</strong> successfully.")

    def test_post_edit_group_invalid(self):
        """
        Test POST method: Edit a group with a invalid form
        """
        response = self.owner_client.post(
            reverse('edit_group', args=[self.main_app.identifier, self.group.id]),
            {
                # Missing name
                'is_everyone': False,
                'users': 'user1@example.org, user2@example.org'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Group.objects.count(), 2)

        group = self.main_app.group_set.get(id=self.group.id)
        self.assertEqual(group.name, "Test Group")
        self.assertEqual(group.is_everyone, True)
        self.assertEqual(group.users, "user@example.org,owner@example.org")

        context_data = response.context_data
        self.assertEqual(context_data['success'], False)
        self.assertEqual(
            context_data['message'],
        (f"Failed to edit group <strong>{group.name}</strong>."
        " Please fill out the group form properly."))

    def test_post_create_group_unauthz(self):
        """
        Test POST method: Create a group without access to application
        """
        response = self.unauthz_client.post(
            reverse('create_group', args=[self.main_app.identifier]),
            {
                'name': 'Test Users',
                'is_everyone': False,
                'users': 'user1@example.org, user2@example.org'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })

    def test_post_edit_non_existing_group(self):
        """
        Test Post method: Edit a non-existing group
        """
        non_existing_id = self.group.id + 1
        response = self.owner_client.post(
            reverse('edit_group', args=[self.main_app.identifier, non_existing_id]),
            {
                'name': 'Test Users',
                'is_everyone': False,
                'users': 'user1@example.org, user2@example.org'
            }, follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })

    def test_get_delete_a_group(self):
        """
        Test GET method: Delete a group
        """
        response = self.owner_client.get(
            reverse('delete_group', args=[self.main_app.identifier, self.group.id]),
            follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Group.objects.count(), 1)

        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            f"Delete group <strong>{self.group.name}</strong> successfully")

    def test_get_delete_a_group_unauthz(self):
        """
        Test GET method: Delete a group without access to app
        """
        response = self.unauthz_client.get(
            reverse('delete_group', args=[self.main_app.identifier, self.group.id]),
            follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })

    def test_get_delete_a_non_existing_group_unauthz(self):
        """
        Test GET method: Delete a non-existing group
        """
        non_existing_id = self.group.id + 1
        response = self.owner_client.get(
            reverse('delete_group', args=[self.main_app.identifier, non_existing_id]),
            follow=True,
            HTTP_REFERER=reverse('get_app', args=[self.main_app.identifier]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {
            'status': 403, 'title': 'Forbidden', 'message': 'User is unauthorized to access this url'
        })

# Tests for GroupAPIView
class GroupAPIViewTestCase(ViewTestCase):
    # This function runs before each test case in this class
    def setUp(self):
        super().setUp()

        # Create a group
        self.group = self.main_app.group_set.create(
            name = "Test Group",
            is_everyone = False,
            users = "user@example.org,owner@example.org"
        )

    def test_get_all_groups_for_user_with_required_param(self):
        """
        Test GET method: Get all groups for an user with the required parameter 'user'
        """
        response = self.owner_client.get(
            f'/api/group/get_groups_for_user/{self.main_app.identifier}/',
            {'user': 'user@example.org'})
        self.assertEqual(response.status_code, 200)
        content = response.json()
        self.assertEqual(content['groups'], ['Test Group'])

        response = self.owner_client.get(
            f'/api/group/get_groups_for_user/{self.main_app.identifier}/',
            {'user': 'user1@example.org'})
        self.assertEqual(response.status_code, 200)
        content = response.json()
        self.assertEqual(content['groups'], [])

    def test_get_all_groups_for_user_without_required_param(self):
        """
        Test GET method: Get all groups for an user without the required parameter 'user'
        """
        response = self.owner_client.get(
            f'/api/group/get_groups_for_user/{self.main_app.identifier}/')
        self.assertEqual(response.status_code, 400)
        content = response.json()
        self.assertEqual(content['message'], "Bad request. An user email is required.")

    def test_get_all_groups_for_user_for_non_existing_app(self):
        """
        Test GET method: Get all groups for an user for a non-existing app
        """
        response = self.owner_client.get(
            '/api/group/get_groups_for_user/nonexist/',
            {'user': 'user@example.org'})
        self.assertEqual(response.status_code, 404)
        content = response.json()
        self.assertEqual(content['message'], "Requested app does not exist")

    def test_get_all_users_for_group_with_required_param(self):
        """
        Test GET method: Get users for a group with the required parameter 'group'
        """
        response = self.owner_client.get(
            f'/api/group/get_users_for_group/{self.main_app.identifier}/',
            {'group': 'Test Group'}
        )
        self.assertEqual(response.status_code, 200)
        content = response.json()
        self.assertEqual(content['users'], ['user@example.org', 'owner@example.org'])

    def test_get_all_users_for_group_without_required_param(self):
        """
        Test GET method: Get users for a group without the required parameter 'group'
        """
        response = self.owner_client.get(
            f'/api/group/get_users_for_group/{self.main_app.identifier}/',
        )
        self.assertEqual(response.status_code, 400)
        content = response.json()
        self.assertEqual(content['message'], "Bad request. An group name is required.")

    def test_get_all_users_for_group_for_non_existing_app(self):
        """
        Test GET method: Get users for a group for a non-existing app
        """
        response = self.owner_client.get(
            '/api/group/get_users_for_group/nonexist/',
            {'group': 'Test Group'}
        )
        self.assertEqual(response.status_code, 404)
        content = response.json()
        self.assertEqual(content['message'], 'Requested app does not exist')

# Setup for all deployment related Django views
class DeploymentRelatedViewTestCase(ViewTestCase):
    def setUp(self):
        super().setUp()

        # Create an app that is meant for a successful deployment
        self.successful_app = Application.objects.create(
            identifier='successfulapp',
            descriptive_name='Successful App',
            description='This is an app that will be deployed successfully',
            port=1234,
            authentication='uvic',
            is_secure=True,
            allowed_ips='192.168.1.2,192.168.0.2',
            owner=self.owner
        )
        self.successful_app.approlegroup_set.get(role="editor").users.add(self.editor)
        self.successful_app.approlegroup_set.get(role="viewer").users.add(self.viewer)

        self.successful_app.route_set.create(route='/login', authenticated=True)
        self.successful_app.route_set.create(route='/admin', authenticated=True)
        self.successful_app.route_set.create(route='/', authenticated=False)

        self.successful_app.container_set.create(
            image='unit/successfulapp',
            image_tag='1.0.0',
            port=1234,
            command='python successfulapp.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

        self.successful_app.container_set.create(
            image='unit/successfulapp2',
            image_tag='latest',
            port=1235,
            command='python successfulapp2.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

        # Create an app that is meant for a failed deployment
        self.failed_app = Application.objects.create(
            identifier='failedapp',
            descriptive_name='Failed App',
            description='This is an app that will fail to deploy',
            port=1234,
            authentication='uvic',
            is_secure=True,
            owner=self.owner
        )
        self.failed_app.approlegroup_set.get(role="editor").users.add(self.editor)
        self.failed_app.approlegroup_set.get(role="viewer").users.add(self.viewer)

        self.failed_app.route_set.create(route='/login', authenticated=True)
        self.failed_app.route_set.create(route='/admin', authenticated=True)
        self.failed_app.route_set.create(route='/', authenticated=False)

        self.failed_app.container_set.create(
            image='unit/failedapp',
            image_tag='1.0.0',
            port=1234,
            command='python failedapp.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

        self.failed_app.container_set.create(
            image='unit/failedapp2',
            image_tag='latest',
            port=1235,
            command='python failedapp2.py',
            environment_variables='VAR1==123\nVAR2==thisissecret'
        )

    def tearDown(self):
        # Remove logging directories created by Terraform process
        app_ids = ['testapp', 'successfulapp', 'failedapp']
        for app_id in app_ids:
            if os.path.exists(f"/tmp/terraform/{app_id}"):
                shutil.rmtree(f"/tmp/terraform/{app_id}")

# pylint: disable=W0105
# Test cases for ApplicationDeploymentView
class ApplicationDeploymentViewTestCases(DeploymentRelatedViewTestCase):
    def test_ajax_get_deploy_valid_state_machine(self):
        """
        Test AJAX GET method, initiate a valid deployment for an application
        'valid deployment' means the app deployment is not 'started' or 'deployed'
        Test the state machine for deployment ("stop/revert/destroy" action not included)
        """
        # None -> Started -> Deployed
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        # A new Deployment should be created for the Application
        # Test info of the new Deployment
        self.assertEqual(Deployment.objects.count(), 1)
        deployment = Deployment.objects.all()[0]
        self.assertEqual(deployment.application, self.successful_app)
        self.assertEqual(deployment.get_state(), 'deployed')
        self.assertEqual(deployment.get_status(), 'succeeded')
        self.assertEqual(
            deployment.log_file,
            f'/tmp/terraform/{self.successful_app.identifier}/strap-{self.successful_app.identifier}.log'
        )
        self.assertEqual(deployment.process_ids, None)

        # A new K8sInstance should be created for the Application
        # Test info of the new K8sInstance
        self.assertEqual(K8sInstance.objects.count(), 1)
        k8sinstance = K8sInstance.objects.all()[0]
        self.assertEqual(k8sinstance.application, self.successful_app)
        self.assertEqual(k8sinstance.descriptive_name, 'Successful App')
        self.assertEqual(k8sinstance.port, 1234)
        self.assertEqual(k8sinstance.authentication, 'uvic')
        self.assertEqual(k8sinstance.authenticated_routes_str, '/admin,/login')
        self.assertEqual(k8sinstance.unauthenticated_routes_str, '/')
        self.assertEqual(k8sinstance.allowed_ips, '192.168.1.2,192.168.0.2')

        # 2 new instances of K8sContainer should be created for the K8sInstance
        # Test info of the new K8sContainers
        self.assertEqual(K8sContainer.objects.count(), 2)
        k8scontainers = K8sContainer.objects.all()
        self.assertEqual(k8scontainers[0].k8sinstance, k8sinstance)
        self.assertEqual(k8scontainers[0].image, 'unit/successfulapp')
        self.assertEqual(k8scontainers[0].image_tag, '1.0.0')
        self.assertEqual(k8scontainers[0].port, 1234)
        self.assertEqual(k8scontainers[0].command, 'python successfulapp.py')
        self.assertEqual(k8scontainers[0].environment_variables, 'VAR1==123\nVAR2==thisissecret')

        self.assertEqual(k8scontainers[1].k8sinstance, k8sinstance)
        self.assertEqual(k8scontainers[1].image, 'unit/successfulapp2')
        self.assertEqual(k8scontainers[1].image_tag, 'latest')
        self.assertEqual(k8scontainers[1].port, 1235)
        self.assertEqual(k8scontainers[1].command, 'python successfulapp2.py')
        self.assertEqual(k8scontainers[1].environment_variables, 'VAR1==123\nVAR2==thisissecret')

        content = response.json()
        self.assertEqual(content['exit_code'], 0)
        self.assertEqual(content['deployed'], 1)
        self.assertEqual(content['message'], f'App {self.successful_app.descriptive_name} deployed successfully')

        # Deployed -> Update -> No Changes -> Deployed
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        content = response.json()
        self.assertEqual(content['exit_code'], 0)
        self.assertEqual(content['deployed'], 0)
        self.assertEqual(content['message'], ('No changes detected'
            ' in the app configuration to initiate a new deployment'))

        # Deployed -> Update -> Deployed (with updated version)
        first_container = self.successful_app.container_set.first()
        first_container.image_tag = '2.0.0'
        first_container.save()
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        # Deployment should have "deployed" state and "succeeded" status
        deployment = Deployment.objects.get(application=self.successful_app)
        self.assertEqual(deployment.get_state(), "deployed")
        self.assertEqual(deployment.get_status(), "succeeded")

        # Related K8sContainer should have image tag updated
        first_k8scontainer = first_container.k8scontainer
        self.assertEqual(first_k8scontainer.image_tag, '2.0.0')

        content = response.json()
        self.assertEqual(content['exit_code'], 0)
        self.assertEqual(content['deployed'], 1)
        self.assertEqual(content['message'], f'App {self.successful_app.descriptive_name} deployed successfully')


        # None -> Failed
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.failed_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        # A new Deployment should be created for the Application
        # Test info of the new Deployment
        self.assertEqual(Deployment.objects.count(), 2)
        deployment = Deployment.objects.get(application=self.failed_app)
        self.assertEqual(deployment.application, self.failed_app)
        self.assertEqual(deployment.get_state(), 'failed')
        self.assertEqual(deployment.get_status(), 'failed')
        self.assertEqual(
            deployment.log_file,
            f'/tmp/terraform/{self.failed_app.identifier}/strap-{self.failed_app.identifier}.log'
        )
        self.assertEqual(deployment.process_ids, None)

        # There shouldn't be a new K8sInstance created for this application
        self.assertEqual(K8sInstance.objects.count(), 1)

        # There shouldn't be any new K8sContainer created for this application
        self.assertEqual(K8sContainer.objects.count(), 2)

        content = response.json()
        self.assertEqual(content['exit_code'], 1)
        self.assertEqual(content['deployed'], 0)
        self.assertEqual(content['message'], f'Failed to deploy app {self.failed_app.descriptive_name}')

        # Stopped -> Update -> Failed -> Stopped
        deployment.state = 3
        deployment.status = 6
        deployment.save()
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.failed_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        deployment = Deployment.objects.get(application=self.failed_app)
        self.assertEqual(deployment.state, 3)

    def test_ajax_get_deploy_invalid(self):
        """
        Test AJAX GET method, initiate a invalid deployment for an application
        'invalid deployment' means the app deployment is 'started' or 'deployed'
        """
        # state is 'started'
        deployment = Deployment.objects.create(
            application=self.main_app,
            log_file=f'/tmp/strap-{self.main_app.identifier}.log',
            state=1     # State is set to 'started'
        )

        response = self.owner_client.get(f'/deploy/{self.main_app.identifier}/', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(Deployment.objects.count(), 1)
        self.assertEqual(content['exit_code'], -1)
        self.assertEqual(content['message'], f'App {self.main_app.descriptive_name} is being deployed')

        # state is 'deployed + started'
        deployment.state = 5
        deployment.save()

        response = self.owner_client.get(f'/deploy/{self.main_app.identifier}/', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(Deployment.objects.count(), 1)
        self.assertEqual(content['exit_code'], -1)
        self.assertEqual(content['message'], f'App {self.main_app.descriptive_name} is being deployed')

    def test_user_get_with_different_roles(self):
        """
        Test an unauthorized user accessing deployment url for testapp
        'unauthorized user' means user is not the owner of the app
        """
        # Unauthorized user tries to deploy the app. Should receive a JSON response with 403 status
        response = self.unauthz_client.get(
            f'/deploy/{self.main_app.identifier}/',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(Deployment.objects.count(), 0)
        self.assertEqual(content['exit_code'], -1)
        self.assertEqual(content['message'], 'User is unauthorized to deploy this app')

        # Viewer tries to deploy the app. Should receive a JSON response with 403 status
        response = self.viewer_client.get(
            f'/deploy/{self.main_app.identifier}/',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(Deployment.objects.count(), 0)
        self.assertEqual(content['exit_code'], -1)
        self.assertEqual(content['message'], 'User is unauthorized to deploy this app')

        # Editor tries to deploy the app. Should be valid
        response = self.editor_client.get(
            f'/deploy/{self.main_app.identifier}/',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(Deployment.objects.count(), 1)
        self.assertNotEqual(content['exit_code'], -1)
        self.assertNotEqual(content['message'], 'User is unauthorized to deploy this app')

    def test_get(self):
        """
        Test normal GET for this view
        Should return 400 as this view cannot be accessed directly
        """
        # Normal GET
        response = self.owner_client.get(
            f'/deploy/{self.main_app.identifier}/')
        content = response.json()
        self.assertEqual(Deployment.objects.count(), 0)
        self.assertEqual(content['status'], 400)
        self.assertEqual(content['message'], 'Bad request. Cannot access this view directly.')

# Test cases for ApplicationDeploymentLogView
class ApplicationDeploymentLogViewTestCases(DeploymentRelatedViewTestCase):
    def test_get_authorized(self):
        """
        User has access to this log view i.e. User owes the app
        """
        # Log should be 'No deployment for this app found' at first
        response = self.owner_client.get(reverse('deploy_log_app', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 404)
        self.assertEqual(context_data['log'], 'No deployment for this app found')

        # Deploy an app
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        # Owner should be able to access the log view
        response = self.owner_client.get(reverse('deploy_log_app', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)
        self.assertIn(f'Deployed app {self.successful_app.identifier} successfully', context_data['log'])

        # Editor should be able to access the log view
        response = self.editor_client.get(reverse('deploy_log_app', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)
        self.assertIn(f'Deployed app {self.successful_app.identifier} successfully', context_data['log'])

        # Viewer should be able to access the log view
        response = self.viewer_client.get(reverse('deploy_log_app', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)
        self.assertIn(f'Deployed app {self.successful_app.identifier} successfully', context_data['log'])

    # User doesn't have access to this log view i.e. User doesn't owe the app
    def test_get_unauthorized(self):
        response = self.unauthz_client.get(reverse('deploy_log_app', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 403)
        self.assertEqual(context_data['log'], 'User is unauthorized to access this url')

# Test cases for StopApplicationDeploymentView
class StopApplicationDeploymentViewTestCases(DeploymentRelatedViewTestCase):
    def test_ajax_get_valid_state_machine(self):
        """
        Test a valid AJAX GET
        'valid' means the deployment is not in a 'None' state
        and user have authorization to stop the app deployment
        Test state machine of the 'stop/revert/destroy' action
        """
        # Deploy an app
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(K8sInstance.objects.count(), 1)

        # state is 'deployed', status is 'succeeded'
        deployment = Deployment.objects.get(application=self.successful_app)
        self.assertEqual(deployment.get_state(), 'deployed')
        self.assertEqual(deployment.get_status(), 'succeeded')

        # Deployed -> Stopping -> Stopped
        response = self.owner_client.get(
            reverse('stop_deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(K8sInstance.objects.count(), 1)

        # Test context
        content = response.json()
        self.assertEqual(content['exit_code'], 0)
        self.assertEqual(content['message'], 'Stop deployment for app Successful App successfully')

        # state is 'stopped', status is 'stopped'
        deployment = Deployment.objects.get(application=self.successful_app)
        self.assertEqual(deployment.get_state(), 'stopped')
        self.assertEqual(deployment.get_status(), 'stopped')

        # Stopped -> Destroyed -> None
        response = self.owner_client.get(
            reverse('stop_deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(K8sInstance.objects.count(), 0)

        # Test context
        content = response.json()
        self.assertEqual(content['exit_code'], 0)
        self.assertEqual(content['message'], 'Destroy deployment for app Successful App successfully')

        # state is 'destroyed', status is 'destroyed'
        deployment = Deployment.objects.get(application=self.successful_app)
        self.assertEqual(deployment.get_state(), 'None')
        self.assertEqual(deployment.get_status(), 'destroyed')

        # Deploy the app again
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        # Put the app into a "deployed + failed" state
        # Can't do this through the ApplicationDeploymentView in unit test environment
        first_container = self.successful_app.container_set.first()
        first_container.image_tag = '2.0.0'
        first_container.save()

        deployment = Deployment.objects.get(application=self.successful_app)
        deployment.state = 6
        deployment.status = 3
        deployment.save()

        # Revert the app back to the most previous successful deployment
        # Deployed + failed -> Reverting -> deployed
        response = self.owner_client.get(
            reverse('stop_deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        deployment = Deployment.objects.get(application=self.successful_app)
        self.assertEqual(deployment.get_state(), 'deployed')
        self.assertEqual(deployment.get_status(), 'succeeded')

        # Cancel a started deployment
        # Started -> Canceled -> None
        deployment = Deployment.objects.get(application=self.successful_app)
        deployment.state = 1
        deployment.status = 1
        deployment.process_ids='1,2'
        deployment.save()

        k8sinstance = K8sInstance.objects.get(application=self.successful_app)
        k8sinstance.delete()

        response = self.owner_client.get(
            reverse('stop_deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        deployment = Deployment.objects.get(application=self.successful_app)
        self.assertEqual(deployment.get_state(), 'None')
        self.assertEqual(deployment.get_status(), 'canceling')

        self.owner_client.get(
            reverse('deploy_app', args=[self.failed_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        # Failed to stop a deployed app will revert the app state backt to "deployed"
        # Deployed -> stopping -> failed -> deployed
        self.owner_client.get(
            reverse('deploy_app', args=[self.failed_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        deployment = Deployment.objects.get(application=self.failed_app)
        deployment.state = 2
        deployment.status = 2
        deployment.save()

        response = self.owner_client.get(
            reverse('stop_deploy_app', args=[self.failed_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        deployment = Deployment.objects.get(application=self.failed_app)
        self.assertEqual(deployment.get_state(), 'deployed')
        self.assertEqual(deployment.get_status(), 'succeeded')

        # Failed to destroy a stopped app will revert the app back to a "stopped" state
        # Stopped -> destroyed -> failed -> stopped
        deployment = Deployment.objects.get(application=self.failed_app)
        deployment.state = 3
        deployment.status = 7
        deployment.save()

        K8sInstance.objects.create(
            application=self.failed_app,
            descriptive_name=self.failed_app.descriptive_name,
            port=self.failed_app.port,
            authentication=self.failed_app.authentication,
            authenticated_routes_str=self.failed_app.routes_to_str(authenticated=True),
            unauthenticated_routes_str=self.failed_app.routes_to_str(authenticated=False)
        )

        response = self.owner_client.get(
            reverse('stop_deploy_app', args=[self.failed_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        deployment = Deployment.objects.get(application=self.failed_app)
        self.assertEqual(deployment.get_state(), 'stopped')
        self.assertEqual(deployment.get_status(), 'stopped')

        # Failed to revert a "deployed + failed" app back to a "deployed" state
        # deployed + failed -> revert -> failed -> deployed + failed
        deployment = Deployment.objects.get(application=self.failed_app)
        deployment.state = 6
        deployment.status = 3
        deployment.save()

        response = self.owner_client.get(
            reverse('stop_deploy_app', args=[self.failed_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        deployment = Deployment.objects.get(application=self.failed_app)
        self.assertEqual(deployment.get_state(), 'deployed + failed')
        self.assertEqual(deployment.get_status(), 'failed')

    def test_user_with_different_roles_stop_deployment(self):
        """
        Test unauthorized user stopping an app deployment that
        they don't have access to
        """
        # Create a deployment for the successful app
        self.owner_client.get(
            reverse('deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        # Unauthorized user tries to stop the app deployment. Should receive a JSON response with 403 status
        response = self.unauthz_client.get(
            reverse('stop_deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(content['exit_code'], -1)
        self.assertEqual(content['message'], 'User is not authorized to stop this app deployment')

        # Viewer tries to stop the app deployment. Should receive a JSON response with 403 status
        response = self.viewer_client.get(
            reverse('stop_deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(content['exit_code'], -1)
        self.assertEqual(content['message'], 'User is not authorized to stop this app deployment')

        # Editor tries to stop the app deployment. Should be valid
        response = self.editor_client.get(
            reverse('stop_deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertNotEqual(content['exit_code'], -1)
        self.assertNotEqual(content['message'], 'User is not authorized to stop this app deployment')

    def test_ajax_get_invalid(self):
        """
        Test an invalid AJAX GET. Should return 400 status code and an error message
        'invalid' means the deployment is in a 'None' state
        """
        # 'None' state
        Deployment.objects.create(
            application=self.successful_app,
            log_file=f'{self.successful_app}.log',
            state=0,
            status=0
        )
        response = self.owner_client.get(
            reverse('stop_deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(content['status'], 400)
        self.assertEqual(content['message'], "Bad request. Can't stop this app deployment.")

    def test_stop_non_existing_deployment(self):
        """
        Test stopping a non-existing deployment
        Should return status code 404 and an error message
        """
        response = self.owner_client.get(
            reverse('stop_deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.json()
        self.assertEqual(content['status'], 404)
        self.assertEqual(content['message'], "There is no deployment for this app")

    def test_get(self):
        """
        Test accessing the url directly. This url cannot be accessed directly
        Should return a status code 400 and an error message
        """
        response = self.owner_client.get(reverse('stop_deploy_app', args=[self.successful_app.identifier]))
        content = response.json()
        self.assertEqual(content['status'], 400)
        self.assertEqual(content['message'], "Cannot access this page directly")

# Test cases for ApplicationLogView
class ApplicationLogViewTestCases(DeploymentRelatedViewTestCase):
    def test_get(self):
        """
        Test GET method
        Display an app log
        """
        # User doesn't have access to the app log
        response = self.unauthz_client.get(reverse('app_log', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 403)
        self.assertEqual(context_data['log'], 'User is unauthorized to access this url')

        # No deployment for the app yet
        response = self.owner_client.get(reverse('app_log', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 404)
        self.assertEqual(context_data['log'], 'App is not deployed; therefore, has no log.')

        # Deploy an app
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        # Owner should be able to access the log view
        response = self.owner_client.get(reverse('app_log', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)
        self.assertEqual(context_data['log'], '')

        # Editor should be able to access the log view
        response = self.editor_client.get(reverse('app_log', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)
        self.assertEqual(context_data['log'], '')

        # Viewer should be able to access the log view
        response = self.viewer_client.get(reverse('app_log', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)
        self.assertEqual(context_data['log'], '')

# Test cases for ApplicationTerminalView
class ApplicationTerminalViewTestCases(DeploymentRelatedViewTestCase):
    def test_get(self):
        """
        Test GET method
        Display an app terminal
        """
        # User doesn't have access to the app terminal
        response = self.unauthz_client.get(reverse('terminal', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 403)
        self.assertEqual(context_data['log'], 'User is unauthorized to access this url')

        # No deployment for the app yet
        response = self.owner_client.get(reverse('terminal', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 404)
        self.assertEqual(context_data['log'], 'App is not deployed; therefore, terminal is unavailable.')

        # Deploy an app
        response = self.owner_client.get(
            reverse('deploy_app', args=[self.successful_app.identifier]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        # Owner should be able to access the terminal view
        response = self.owner_client.get(reverse('terminal', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)
        self.assertEqual(context_data['log'],
f'Terminal for {self.successful_app.descriptive_name} is active. Type in the prompt below to run commands.'
' Press "Enter" when you want to execute a command.')

        # Editor should be able to access the terminal view
        response = self.editor_client.get(reverse('terminal', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)
        self.assertEqual(context_data['log'],
f'Terminal for {self.successful_app.descriptive_name} is active. Type in the prompt below to run commands.'
' Press "Enter" when you want to execute a command.')

        # Viewer should be able to access the terminal view
        response = self.viewer_client.get(reverse('terminal', args=[self.successful_app.identifier]))
        context_data = response.context_data
        self.assertEqual(context_data['status'], 200)
        self.assertEqual(context_data['log'],
f'Terminal for {self.successful_app.descriptive_name} is active. Type in the prompt below to run commands.'
' Press "Enter" when you want to execute a command.')


# Test cases for K8sResourcesView
class K8sResourcesViewTestCases(ViewTestCase):
    """
    Test updating k8s resources for an app
    """
    def test_post_update_k8s_resources(self):
        """
        Test POST method
        Update k8s resources for an app
        """
        # Not found: App doesn't exist
        response = self.owner_client.post(
            reverse('update_k8s_resources', kwargs={'identifier': 'doesnotexist'}),
            {
                'replicas': 3
            }, follow=True)
        self.assertEqual(response.status_code, 404)
        content = response.json()
        self.assertEqual(content['message'], 'App not found')

        # Unauthorized: User is unauthorized to update k8s resources
        response = self.viewer_client.post(
            reverse('update_k8s_resources', kwargs={'identifier': self.main_app.identifier}),
            {
                'replicas': 3
            }, follow=True)
        self.assertEqual(response.status_code, 403)
        content = response.json()
        self.assertEqual(content['message'], 'You are not authorized to update K8s resources for this app')

        # Bad request: Missing required parameter 'replicas'
        response = self.owner_client.post(
            reverse('update_k8s_resources', kwargs={'identifier': self.main_app.identifier}),
            {}, follow=True)
        self.assertEqual(response.status_code, 400)

        # Bad request: Requested replicas exceeds app's quota
        response = self.owner_client.post(
            reverse('update_k8s_resources', kwargs={'identifier': self.main_app.identifier}),
            {
                'replicas': 4
            }, follow=True)
        self.assertEqual(response.status_code, 400)
        content = response.json()
        self.assertEqual(content['message'], 'Quota exceeded')

        # Valid: Update k8s resources for an app
        response = self.owner_client.post(
            reverse('update_k8s_resources', kwargs={'identifier': self.main_app.identifier}),
            {
                'replicas': 3
            }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.main_app.refresh_from_db()
        self.assertEqual(self.main_app.replicas, 3)

        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(context_data['message'], 'K8s resources updated successfully')


# Test cases for K8sResourcesRequestView
class K8sResourcesRequestViewTestCases(ViewTestCase):
    def test_get_cancel_k8s_resource_request(self):
        """
        Test GET method
        Cancel a k8s resource request
        """
        # Set k8s resource request for main app as PENDING
        k8s_request = self.main_app.applicationquotarequest
        k8s_request.status=RequestStatusTypes.PENDING
        k8s_request.save()

        # Not found: App doesn't exist
        response = self.owner_client.get(
            reverse('cancel_k8s_resource_request', kwargs={'identifier': 'doesnotexist'}),
            follow=True)
        self.assertEqual(response.status_code, 404)
        content = response.json()
        self.assertEqual(content['message'], 'App not found')

        # Unauthorized: User is unauthorized to cancel k8s resource request
        response = self.viewer_client.get(
            reverse('cancel_k8s_resource_request', kwargs={'identifier': self.main_app.identifier}),
            follow=True)
        self.assertEqual(response.status_code, 403)
        content = response.json()
        self.assertEqual(content['message'], 'You are not authorized to cancel resource requests for this app')

        # Valid: Cancel a k8s resource request
        response = self.owner_client.get(
            reverse('cancel_k8s_resource_request', kwargs={'identifier': self.main_app.identifier}),
            follow=True)
        self.assertEqual(response.status_code, 200)
        request = ApplicationQuotaRequest.objects.get(application=self.main_app)
        self.assertEqual(request.status, RequestStatusTypes.NONE)

        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(context_data['message'], 'K8s resource request canceled successfully')

    def test_post_request_k8s_resources(self):
        """
        Test POST method
        Request k8s resources for an app
        """
        # Not found: App doesn't exist
        response = self.owner_client.post(
            reverse('request_k8s_resources', kwargs={'identifier': 'doesnotexist'}),
            {
                'replicas': 4,
                'justification': 'Need more replicas'
            }, follow=True)
        self.assertEqual(response.status_code, 404)
        content = response.json()
        self.assertEqual(content['message'], 'App not found')

        # Unauthorized: User is unauthorized to request k8s resources
        response = self.viewer_client.post(
            reverse('request_k8s_resources', kwargs={'identifier': self.main_app.identifier}),
            {
                'replicas': 4,
                'justification': 'Need more replicas'
            }, follow=True)
        self.assertEqual(response.status_code, 403)
        content = response.json()
        self.assertEqual(content['message'], 'You are not authorized to request K8s resources for this app')

        # Bad request: Missing required parameter 'replicas'
        response = self.owner_client.post(
            reverse('request_k8s_resources', kwargs={'identifier': self.main_app.identifier}),
            {
                'justification': 'Need more replicas'
            }, follow=True)
        self.assertEqual(response.status_code, 400)

        # Valid: Create a new request
        response = self.owner_client.post(
            reverse('request_k8s_resources', kwargs={'identifier': self.main_app.identifier}),
            {
                'replicas': 4,
                'justification': 'Need more replicas'
            }, follow=True)
        self.assertEqual(response.status_code, 200)

        context_data = response.context_data
        self.assertEqual(context_data['success'], True)
        self.assertEqual(
            context_data['message'],
            "K8s resources request submitted successfully. Waiting for an admin's approval")

        # Check info of the new request
        self.main_app.refresh_from_db()
        request = self.main_app.applicationquotarequest
        self.assertEqual(request.replicas, 4)
        self.assertEqual(request.justification, 'Need more replicas')
        self.assertEqual(request.status, RequestStatusTypes.PENDING)
        self.assertEqual(request.submitted_by, self.owner)

        # Check if notifications are created for admins
        # Since there is only one admin, there should be only one notification
        notification = Notification.objects.get(user=self.admin)
        self.assertEqual(
            notification.html,
            f"New resource request for app <strong>{self.main_app.descriptive_name}</strong>")
        self.assertEqual(notification.url, f"{reverse('admin_home_page')}?active_tab=k8s")
        self.assertEqual(notification.type, NotificationTypes.ApplicationQuotaRequest)
        self.assertEqual(notification.related_model_pk, request.id)
        self.assertEqual(notification.status, NotificationStatusTypes.NEW)

        # Valid: Update an existing request
        response = self.owner_client.post(
            reverse('request_k8s_resources', kwargs={'identifier': self.main_app.identifier}),
            {
                'replicas': 5,
                'justification': 'Need more replicas'
            }, follow=True)
        self.assertEqual(response.status_code, 200)

        # Check info of the updated request
        self.main_app.refresh_from_db()
        request = self.main_app.applicationquotarequest
        self.assertEqual(request.replicas, 5)
        self.assertEqual(request.justification, 'Need more replicas')
        self.assertEqual(request.status, RequestStatusTypes.PENDING)
        self.assertEqual(request.submitted_by, self.owner)

        # Check if notifications are created for admins
        # There should be an additional notification for the updated request
        notifications = Notification.objects.filter(user=self.admin)
        self.assertEqual(notifications.count(), 2)
        notification = notifications.first()
        self.assertEqual(
            notification.html,
            f"Updated resource request for app <strong>{self.main_app.descriptive_name}</strong>")
        self.assertEqual(notification.url, f"{reverse('admin_home_page')}?active_tab=k8s")
        self.assertEqual(notification.type, NotificationTypes.ApplicationQuotaRequest)
        self.assertEqual(notification.related_model_pk, request.id)
        self.assertEqual(notification.status, NotificationStatusTypes.NEW)
