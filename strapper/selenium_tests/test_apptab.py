## pylint: disable=use-dict-literal
# TODO: Come back to fix use-dict-literal later
import os

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import override_settings

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

# address of container running selenium
selenium_url = os.environ.get('SELENIUM_URL', 'http://selenium:4444/wd/hub')

# base application URL
app_url = os.environ.get('APP_URL', 'http://django:8001')

# All tests for 'Create new' tab
@override_settings(
    ALLOWED_HOSTS=['*'],
    CONFIG={
        'AUTHX_USER_OVERRIDE': 'selenium@example.org',
        'AUTHZ_USERS': ['user@example.org', 'user1@example.org'],
        'AUTHZ_ADMINS': ['admin1@example.org', 'admin2@example.org']
    },
    TESTING=True
)  # Overwrite settings
class AppTabTest(StaticLiveServerTestCase):
    # This function forces that page to reload
    # Add this function to the beginning of every new test
    def visitDashboard(self):
        self.selenium.get(f'{app_url}/')

    @classmethod
    def setUpClass(cls):
        # Ignore ChromeDriver issues
        options = webdriver.ChromeOptions()
        options.headless = True
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--no-sandox')
        options.add_argument('--whitelisted-ips')
        cls.selenium = webdriver.Remote(
            command_executor = selenium_url,
            options=options
        )

        # Wait for STRAP initial authentication
        cls.wait = WebDriverWait(cls.selenium, 5)
        cls.selenium.get(f'{app_url}/')

        # Visit the dashboard page
        try:
            cls.wait.until(lambda selenium: selenium.current_url == f"{app_url}/")
        except TimeoutException as e:
            cls.selenium.quit()

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
    
    # Test title of page
    def test_title(self):
        return

    # Read information of an app. This function is NOT a TEST.
    # Returns a dictionary where key is an app field and value is the value for that field
    # For example, {"identifier": "testapp", "descriptive_name": "Test App", ...}
    def readAppInfo(self, app_summary):
        rows = app_summary.find_elements(By.CLASS_NAME, 'row')
        app_info = {}

        app_info['identifier'] = rows[0].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
        app_info['descriptive_name'] = rows[1].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
        app_info['description'] = rows[2].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
        app_info['container_image'] = rows[3].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
        app_info['image_tag'] = rows[4].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
        app_info['container_port'] = rows[5].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
        app_info['authentication'] = rows[6].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')

        app_info['authenticated_routes'] = []
        authenticated_route_tag_container = rows[7].find_element(By.CLASS_NAME, 'authenticated-routes')
        authenticated_route_tags = authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
        for tag in authenticated_route_tags:
            app_info['authenticated_routes'].append(tag.get_attribute('innerText'))
        
        app_info['unauthenticated_routes'] = []
        unauthenticated_route_tag_container = rows[8].find_element(By.CLASS_NAME, 'unauthenticated-routes')
        unauthenticated_route_tags = unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
        for tag in unauthenticated_route_tags:
            app_info['unauthenticated_routes'].append(tag.get_attribute('innerText'))

        app_info['runtime_command'] = rows[9].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
        app_info['environment_variables'] = rows[10].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
        app_info['database'] = rows[11].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
        return app_info

    # Test app creation form
    def createAppTest(self, identifier, descriptive_name, description,\
        container_image, image_tag="latest", container_port=80, authentication="",\
        authenticated_routes=[], unauthenticated_routes=[], runtime_command="", environment_variables="", database="None"):
        
        # Refresh the page
        self.visitDashboard()

        # Check if an application with this identifier already exists
        app_exist = len(self.selenium.find_elements(By.ID, f'{identifier}-tab-pane')) > 0

        # Refresh the page
        self.visitDashboard()

        # Open the 'Dashboard' tab
        dashboard_tab_btn = self.selenium.find_element(By.ID, 'dashboard-tab')
        dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
        self.selenium.execute_script('arguments[0].click()', dashboard_tab_btn)
        assert 'active' in dashboard_tab.get_attribute('class')

        # Create form should be hidden until clicking on the "+" button
        create_app_form = dashboard_tab.find_element(By.CLASS_NAME, 'app-create-form')
        assert 'display: none;' in create_app_form.get_attribute('style')
        create_app_btn = dashboard_tab.find_element(By.CLASS_NAME, 'app-create-btn')
        self.selenium.execute_script('arguments[0].click()', create_app_btn)
        assert 'display: none;' not in create_app_form.get_attribute('style')

        # Test contraints (uniqueness, length and characters) of identifier field
        identifier_field = create_app_form.find_element(By.NAME, 'identifier')
        identifier_field_error_message = create_app_form.find_elements(By.CLASS_NAME, 'text-danger')[0]
        assert 'd-none' in identifier_field_error_message.get_attribute('class')
        
        identifier_field.send_keys('te')
        assert 'error-input' in identifier_field.get_attribute('class')
        assert 'd-none' not in identifier_field_error_message.get_attribute('class')
        assert 'Identifier needs to have at least 3 characters' in\
            identifier_field_error_message.get_attribute('innerText')
        
        identifier_field.clear()
        identifier_field.send_keys('tes%')
        assert 'error-input' in identifier_field.get_attribute('class')
        assert 'd-none' not in identifier_field_error_message.get_attribute('class')
        assert 'Identifier only contains lowercase letters, numbers and hyphens' in\
            identifier_field_error_message.get_attribute('innerText')

        identifier_field.clear()
        identifier_field.send_keys(identifier)
        if app_exist:
            assert 'error-input' in identifier_field.get_attribute('class')
            assert 'd-none' not in identifier_field_error_message.get_attribute('class')
            assert 'Identifier already exists' in\
                identifier_field_error_message.get_attribute('innerText')
            return
        assert 'error-input' not in identifier_field.get_attribute('class')
        assert 'd-none' in identifier_field_error_message.get_attribute('class')

        # Fill in descriptive name, description, container image, image tag, container port
        # and select an authentication mode
        descriptive_name_field = create_app_form.find_element(By.NAME, 'descriptive_name')
        descriptive_name_field.send_keys(descriptive_name)

        description_field = create_app_form.find_element(By.NAME, 'description')
        description_field.send_keys(description)

        container_image_field = create_app_form.find_element(By.NAME, 'container_image')
        container_image_field.send_keys(container_image)

        image_tag_field = create_app_form.find_element(By.NAME, 'image_tag')
        image_tag_field.send_keys(image_tag)

        container_port_field = create_app_form.find_element(By.NAME, 'container_port')
        container_port_field.send_keys(container_port)

        authentication_field = Select(create_app_form.find_element(By.NAME, 'authentication'))
        authentication_field.select_by_value(authentication)

        route_input = create_app_form.find_elements(By.CLASS_NAME, 'routes-input')
        
        # Test authenticated route
        authenticated_route_input = route_input[0]
        authenticated_route_input_field = authenticated_route_input.find_element(By.TAG_NAME, 'input')
        authenticated_route_input_btn = authenticated_route_input.find_element(By.CLASS_NAME, 'btn')
        authenticated_route_tag_container = create_app_form.find_element(By.CSS_SELECTOR, '.route-tag-container.authenticated-routes')
        assert 'disabled' in authenticated_route_input_btn.get_attribute('class')
        
        # Add authenticated routes
        for route in authenticated_routes:
            authenticated_route_input_field.send_keys(route)
            assert 'disabled' not in authenticated_route_input_btn.get_attribute('class')
            self.selenium.execute_script('arguments[0].click()', authenticated_route_input_btn)
            assert 'disabled' in authenticated_route_input_btn.get_attribute('class')
            assert not authenticated_route_input.get_attribute("value")
        assert len(authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')) == len(authenticated_routes)

        # Test unauthenticated route
        unauthenticated_route_input = route_input[1]
        unauthenticated_route_input_field = unauthenticated_route_input.find_element(By.TAG_NAME, 'input')
        unauthenticated_route_input_btn = unauthenticated_route_input.find_element(By.CLASS_NAME, 'btn')
        unauthenticated_route_tag_container = create_app_form.find_element(By.CSS_SELECTOR, '.route-tag-container.unauthenticated-routes')
        assert 'disabled' in unauthenticated_route_input_btn.get_attribute('class')

        # Add unauthenticated routes
        for route in unauthenticated_routes:
            unauthenticated_route_input_field.send_keys(route)
            assert 'disabled' not in unauthenticated_route_input_btn.get_attribute('class')
            self.selenium.execute_script('arguments[0].click()', unauthenticated_route_input_btn)
            assert 'disabled' in unauthenticated_route_input_btn.get_attribute('class')
            assert not unauthenticated_route_input.get_attribute("value")
        # unauthenticated_route_input_field.send_keys('/logout')
        # self.selenium.execute_script('arguments[0].click()', unauthenticated_route_input_btn)
        assert len(unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')) == len(unauthenticated_routes)

        # Fill in runtime command, environment variables and select a database
        runtime_command_field = create_app_form.find_element(By.NAME, 'runtime_command')
        runtime_command_field.send_keys(runtime_command)

        environment_variables_field = create_app_form.find_element(By.NAME, 'environment_variables')
        environment_variables_field.send_keys(environment_variables)

        database_field = Select(create_app_form.find_element(By.NAME, 'database'))
        database_field.select_by_value(database)

        # Submit the form, check if a new app has been created
        submit_btn = create_app_form.find_element(By.CSS_SELECTOR, "button[type='submit']")
        self.selenium.execute_script('arguments[0].click()', submit_btn)

        # Wait for the page to refresh, check the success message content
        try:
            self.wait.until(lambda selenium: selenium.current_url == f"{app_url}/")
        except TimeoutException:
            print("Page does not refresh after submit app creation form")
        success_message = self.selenium.find_element(By.CLASS_NAME, "alert")
        assert "alert-success" in success_message.get_attribute("class")
        assert f"App {descriptive_name} created successfully" in success_message.get_attribute('innerText')

        # Check if a new tab for the new app exists
        assert len(self.selenium.find_elements(By.ID, f"{identifier}-tab-pane")) == 1

        app_tab = self.selenium.find_element(By.CSS_SELECTOR, f'#{identifier}-tab-pane')
        # Test new values of the edited application
        app_summary = app_tab.find_element(By.CSS_SELECTOR, '.app-summary')
        app_info = self.readAppInfo(app_summary)
        assert app_info['identifier'] == identifier
        assert app_info['descriptive_name'] == descriptive_name
        assert app_info['description'] == description
        assert app_info['container_image'] == container_image
        assert app_info['image_tag'] == image_tag
        assert app_info['container_port'] == str(container_port)
        if not authentication:
            assert app_info['authentication'] == 'None'
        elif authentication == 'uvic':
            assert app_info['authentication'] == 'Uvic'
        else:
            assert app_info['authentication'] == 'Uvic + Social'
        assert sorted(app_info['authenticated_routes']) == sorted(app_info['authenticated_routes'])
        assert sorted(app_info['unauthenticated_routes']) == sorted(app_info['unauthenticated_routes'])
        assert app_info['runtime_command'] == "None" if not runtime_command else runtime_command
        assert app_info['environment_variables'] == "None" if not environment_variables else environment_variables
        assert app_info['database'] == database
    
    # Test editing app
    # If app doesn't exist, do nothing
    def editAppTest(self, identifier, descriptive_name, description,\
        container_image, image_tag="latest", container_port=80, authentication="",\
        authenticated_routes=[], unauthenticated_routes=[], runtime_command="", environment_variables="", database="None"):
        # Refresh the page
        self.visitDashboard()
        try:
            app_tab = self.selenium.find_element(By.ID, f'{identifier}-tab-pane')
        except:
            print(f"App with identifier {identifier} doesn't exist")
            return
        
        app_summary = app_tab.find_element(By.CLASS_NAME, 'app-summary')
        edit_btn = app_summary.find_element(By.CLASS_NAME, 'app-summary-edit-btn')
        edit_form = app_tab.find_element(By.CLASS_NAME, 'app-edit-form')
        edit_form_cancel_btn = edit_form.find_element(By.CLASS_NAME, 'app-summary-edit-cancel-btn')

        # Test opening the closing edit form
        assert "display: none;" in edit_form.get_attribute("style")
        self.selenium.execute_script('arguments[0].click()', edit_btn)
        self.wait.until(lambda selenium: "opacity" not in edit_form.get_attribute("style"))
        assert "display: none;" not in edit_form.get_attribute("style")
        self.selenium.execute_script('arguments[0].click()', edit_form_cancel_btn)
        self.wait.until(lambda selenium: "display: none;" in edit_form.get_attribute("style"))
        self.selenium.execute_script('arguments[0].click()', edit_btn)
        self.wait.until(lambda selenium: "opacity" not in edit_form.get_attribute("style"))

        # Test current values of edit form fields and fill in new values
        app_info = self.readAppInfo(app_summary)

        identifier_field = edit_form.find_element(By.NAME, 'identifier')
        assert identifier_field.get_attribute("value") == app_info['identifier']

        descriptive_name_field = edit_form.find_element(By.NAME, 'descriptive_name')
        assert descriptive_name_field.get_attribute("value") == app_info['descriptive_name']
        descriptive_name_field.clear()
        descriptive_name_field.send_keys(descriptive_name)

        description_field = edit_form.find_element(By.NAME, 'description')
        assert description_field.get_attribute("value") == app_info['description']
        description_field.clear()
        description_field.send_keys(description)

        container_image_field = edit_form.find_element(By.NAME, 'container_image')
        assert container_image_field.get_attribute("value") == app_info['container_image']
        container_image_field.clear()
        container_image_field.send_keys(container_image)

        image_tag_field = edit_form.find_element(By.NAME, 'image_tag')
        assert image_tag_field.get_attribute("value") == app_info['image_tag']
        image_tag_field.clear()
        image_tag_field.send_keys(image_tag)

        container_port_field = edit_form.find_element(By.NAME, 'container_port')
        assert container_port_field.get_attribute("value") == app_info['container_port']
        container_port_field.clear()
        container_port_field.send_keys(container_port)

        authentication_field = Select(edit_form.find_element(By.NAME, 'authentication'))
        if app_info['authentication'] != 'None':
            assert authentication_field.first_selected_option.get_attribute("value") == app_info['authentication']
        else:
            assert not authentication_field.first_selected_option.get_attribute("value")
        authentication_field.select_by_value(authentication)

        route_input = edit_form.find_elements(By.CLASS_NAME, 'routes-input')
        
        # Test authenticated route
        authenticated_route_input = route_input[0]
        authenticated_route_input_field = authenticated_route_input.find_element(By.TAG_NAME, 'input')
        authenticated_route_input_btn = authenticated_route_input.find_element(By.CLASS_NAME, 'btn')
        authenticated_route_tag_container = edit_form.find_element(By.CSS_SELECTOR, '.route-tag-container.authenticated-routes')
        assert 'disabled' in authenticated_route_input_btn.get_attribute('class')

        authenticated_route_tags = authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
        for route_tag in authenticated_route_tags:
            route = route_tag.find_element(By.CLASS_NAME, 'route-tag-path')
            delete_btn = route_tag.find_element(By.CLASS_NAME, 'route-tag-close')
            assert route.get_attribute('innerText') in app_info['authenticated_routes']
            self.selenium.execute_script('arguments[0].click()', delete_btn)

        authenticated_route_tags = authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
        assert len(authenticated_route_tags) == 0

        # Add authenticated routes
        for route in authenticated_routes:
            authenticated_route_input_field.send_keys(route)
            assert 'disabled' not in authenticated_route_input_btn.get_attribute('class')
            self.selenium.execute_script('arguments[0].click()', authenticated_route_input_btn)
            assert 'disabled' in authenticated_route_input_btn.get_attribute('class')
            assert not authenticated_route_input.get_attribute("value")
        assert len(authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')) == len(authenticated_routes)

        # Test unauthenticated route
        unauthenticated_route_input = route_input[1]
        unauthenticated_route_input_field = unauthenticated_route_input.find_element(By.TAG_NAME, 'input')
        unauthenticated_route_input_btn = unauthenticated_route_input.find_element(By.CLASS_NAME, 'btn')
        unauthenticated_route_tag_container = edit_form.find_element(By.CSS_SELECTOR, '.route-tag-container.unauthenticated-routes')
        assert 'disabled' in unauthenticated_route_input_btn.get_attribute('class')

        unauthenticated_route_tags = unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
        for route_tag in unauthenticated_route_tags:
            route = route_tag.find_element(By.CLASS_NAME, 'route-tag-path')
            delete_btn = route_tag.find_element(By.CLASS_NAME, 'route-tag-close')
            assert route.get_attribute('innerText') in app_info['unauthenticated_routes']
            self.selenium.execute_script('arguments[0].click()', delete_btn)

        unauthenticated_route_tags = unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
        assert len(unauthenticated_route_tags) == 0

        # Add unauthenticated routes
        for route in unauthenticated_routes:
            unauthenticated_route_input_field.send_keys(route)
            assert 'disabled' not in unauthenticated_route_input_btn.get_attribute('class')
            self.selenium.execute_script('arguments[0].click()', unauthenticated_route_input_btn)
            assert 'disabled' in unauthenticated_route_input_btn.get_attribute('class')
            assert not unauthenticated_route_input.get_attribute("value")
        assert len(unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')) == len(unauthenticated_routes)

        runtime_command_field = edit_form.find_element(By.NAME, 'runtime_command')
        assert runtime_command_field.get_attribute("value") == '' if app_info['runtime_command'] == 'None' else app_info['runtime_command']
        runtime_command_field.clear()
        runtime_command_field.send_keys(runtime_command)

        environment_variables_field = edit_form.find_element(By.NAME, 'environment_variables')
        assert environment_variables_field.get_attribute("value") == '' if app_info['environment_variables'] == 'None' else app_info['environment_variables']
        environment_variables_field.clear()
        environment_variables_field.send_keys(environment_variables)

        database_field = Select(edit_form.find_element(By.NAME, 'database'))
        assert database_field.first_selected_option.get_attribute("value") == app_info['database']
        database_field.select_by_value(database)

        # Submit the edit form
        submit_btn = edit_form.find_element(By.CSS_SELECTOR, "button[type='submit']")
        self.selenium.execute_script('arguments[0].click()', submit_btn)

        # Wait for the page to refresh, cue is a success message showing up
        # Check the success message content
        try:
            self.wait.until(
                lambda selenium:
                self.selenium.find_element(By.CLASS_NAME, "alert")
            )
        except TimeoutException:
            print("Page does not refresh after submiting group create form")
        success_message = self.selenium.find_element(By.CLASS_NAME, "alert")
        assert "alert-success" in success_message.get_attribute("class")
        assert f"App {descriptive_name} edited successfully" in success_message.get_attribute('innerText')

        app_tab = self.selenium.find_element(By.ID, f'{identifier}-tab-pane')
        # Test new values of the edited application
        app_summary = app_tab.find_element(By.CSS_SELECTOR, '.app-summary')
        app_info = self.readAppInfo(app_summary)
        assert app_info['identifier'] == identifier
        assert app_info['descriptive_name'] == descriptive_name
        assert app_info['description'] == description
        assert app_info['container_image'] == container_image
        assert app_info['image_tag'] == image_tag
        assert app_info['container_port'] == str(container_port)
        if not authentication:
            assert app_info['authentication'] == 'None'
        elif authentication == 'uvic':
            assert app_info['authentication'] == 'Uvic'
        else:
            assert app_info['authentication'] == 'Uvic + Social'
        assert sorted(app_info['authenticated_routes']) == sorted(app_info['authenticated_routes'])
        assert sorted(app_info['unauthenticated_routes']) == sorted(app_info['unauthenticated_routes'])
        assert app_info['runtime_command'] == runtime_command
        assert app_info['environment_variables'] == environment_variables
        assert app_info['database'] == database

    # Test deleting app
    # If app doesn't exist, do nothing
    def deleteAppTest(self, identifier):
        # Refresh the page
        self.visitDashboard()

        try:
            app_tab = self.selenium.find_element(By.ID, f'{identifier}-tab-pane')
        except:
            print(f"App with identifier {identifier} doesn't exist")
            return

        delete_btn = app_tab.find_element(By.CLASS_NAME, 'app-summary-delete-btn')
        delete_modal = self.selenium.find_element(By.ID, 'deleteModal')
        identifier_input = delete_modal.find_element(By.TAG_NAME, 'input')
        confirm_btn = delete_modal.find_element(By.CLASS_NAME, 'delete-modal-confirm-btn')

        # Open the app delete modal
        assert 'show' not in delete_modal.get_attribute("class")
        self.selenium.execute_script('arguments[0].click()', delete_btn)
        self.wait.until(lambda selenium: 'show' in delete_modal.get_attribute("class"))

        # "Confirm" button should be disabled until app identifier is typed
        assert 'disabled' in confirm_btn.get_attribute("class")
        identifier_input.send_keys(identifier)
        assert 'disabled' not in confirm_btn.get_attribute("class")
        self.selenium.execute_script('arguments[0].click()', confirm_btn)

        # Wait for the page to refresh, check the success message content
        try:
            self.wait.until(lambda selenium: selenium.current_url == f"{app_url}/")
        except TimeoutException:
            print("Page does not refresh after submit app creation form")
        success_message = self.selenium.find_element(By.CLASS_NAME, "alert")
        assert "alert-success" in success_message.get_attribute("class")
        assert f"deleted successfully" in success_message.get_attribute('innerText')
        assert len(self.selenium.find_elements(By.ID, f'{identifier}-tab-pane')) == 0
    
    # Test deploying app
    def deployAppTest(self, identifier):
        # Refresh the page
        self.visitDashboard()

        try:
            app_tab = self.selenium.find_element(By.ID, f'{identifier}-tab-pane')
        except:
            print(f"App with identifier {identifier} doesn't exist")
            return

        deployment_modal = app_tab.find_element(By.CLASS_NAME, f'deploy-modal-{identifier}')
        deploy_btn = deployment_modal.find_element(By.ID, f'deploy-btn-{identifier}')
        deploy_state = deployment_modal.find_element(By.ID, f'deploy-state-{identifier}')
        deploy_status = deployment_modal.find_element(By.ID, f'deploy-status-{identifier}')
        deploy_log = deployment_modal.find_element(By.ID, f'deploy-log-{identifier}')
        app_log = deployment_modal.find_element(By.ID, f'app-log-{identifier}')

        assert deploy_state.get_attribute("innerText") == ''
        assert deploy_status.get_attribute("innerText") == ''
        assert deploy_log.get_attribute("innerText") == 'None'
        assert app_log.get_attribute("innerText") == 'None'

        self.selenium.execute_script('arguments[0].click()', deploy_btn)
        assert "Deploying" in deploy_btn.get_attribute("innerText")
        assert "View" in deploy_log.get_attribute("innerText")
        assert "View" in app_log.get_attribute("innerText")

        self.wait.until(lambda selenium: "Update" in deploy_btn.get_attribute("innerText"))
        assert "deployed" in deploy_state.get_attribute("innerText")
        assert "succeeded" in deploy_status.get_attribute("innerText")

    
    # Run all tests for app tabs
    # Run all tests in a single function so that database doesn't reset after each test
    def test_all(self):
        # Create app My Application with identifier 'myapp'.
        self.createAppTest(identifier='myapp', descriptive_name='My Application',\
            description=('My application provides a basic functionality of an app. '
            'It is created for testing either with Selenium or manual testing. It is a very useful application.'),\
            container_image='archie/myapp', authenticated_routes=["/", "/amin", "/about"],\
            unauthenticated_routes=["/login", "/logout"])

        # Edit app "My Application"
        self.editAppTest(identifier='myapp', descriptive_name='My Application',\
            description='My application provides a basic functionality of an app.',\
            container_image='archie/myapp', image_tag="1.0", container_port=4001,\
            authentication="uvic_social", authenticated_routes=["/", "/admin"],\
            unauthenticated_routes=["/login"], runtime_command="python myapp.py",\
            environment_variables="VAR1=var1\nVAR2=var2", database="PostgresSQL")
        
        # Delete app "My Application"
        self.deleteAppTest(identifier="myapp")

        # Create app Successful Application with identifier successful'.
        self.createAppTest(identifier='successfulapp', descriptive_name='Successful Application',\
            description=('This app will be deployed successfully'),\
            container_image='archie/successfulapp')
        
        # Deploy Successful Application
        # TODO: Need to comment this test out because haven't figured out how to run the deployment in
        # a test mode
        # Need to figure out how to override settings
        # self.deployAppTest(identifier="successfulapp")
