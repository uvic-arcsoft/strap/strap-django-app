# ## pylint:
# import os

# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
# from django.test import override_settings
# from django.contrib.auth.models import User

# from selenium import webdriver
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.select import Select
# from selenium.common.exceptions import TimeoutException, NoSuchElementException
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.common.keys import Keys

# from normal_users.models import Application

# # address of container running selenium
# selenium_url = os.environ.get('SELENIUM_URL', 'http://selenium:4444/wd/hub')

# # base application URL
# app_url = 'http://django:8001'

# # All tests for 'Create new' tab
# @override_settings(
#     ALLOWED_HOSTS=['*'],
#     CONFIG={
#         'AUTHX_USER_OVERRIDE': 'selenium@example.org',
#         'AUTHZ_USERS': ['user@example.org', 'user1@example.org'],
#         'AUTHZ_ADMINS': ['admin1@example.org', 'admin2@example.org']
#     }
# )  # Overwrite settings
# class DashboardTabTest(StaticLiveServerTestCase):
#     # This function forces that page to reload
#     # Add this function to the beginning of every new test
#     def visitDashboard(self):
#         self.selenium.get(f'{app_url}/')

#     @classmethod
#     def setUpClass(cls):
#         # Ignore ChromeDriver issues
#         options = webdriver.ChromeOptions()
#         options.headless = True
#         options.add_experimental_option('excludeSwitches', ['enable-logging'])
#         options.add_argument('--disable-dev-shm-usage')
#         options.add_argument('--no-sandox')
#         options.add_argument('--whitelisted-ips')
#         cls.selenium = webdriver.Remote(
#             command_executor = selenium_url,
#             options=options
#         )

#         # Wait for STRAP initial authentication
#         cls.wait = WebDriverWait(cls.selenium, 5)
#         cls.selenium.get(f'{app_url}/')
#         try:
#             cls.wait.until(lambda selenium: selenium.current_url == f"{app_url}/")
#         except TimeoutException as e:
#             cls.selenium.quit()

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()

#     # Test title of page
#     def test_title(self):
#         print("Calling test_title of DashboardTabTest")
#         # Refresh the page
#         self.visitDashboard()
#         assert 'Dashboard' in self.selenium.title

#     # Read information of an app. This function is NOT a TEST.
#     # Returns a dictionary where key is an app field and value is the value for that field
#     # For example, {"identifier": "testapp", "descriptive_name": "Test App", ...}
#     def readAppInfo(self, app_summary):
#         rows = app_summary.find_elements(By.CLASS_NAME, 'row')
#         app_info = {}

#         app_info['identifier'] = rows[0].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
#         app_info['descriptive_name'] = rows[1].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
#         app_info['description'] = rows[2].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
#         app_info['container_image'] = rows[3].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
#         app_info['image_tag'] = rows[4].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
#         app_info['container_port'] = rows[5].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
#         app_info['authentication'] = rows[6].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')

#         app_info['authenticated_routes'] = []
#         authenticated_route_tag_container = rows[7].find_element(By.CLASS_NAME, 'authenticated-routes')
#         authenticated_route_tags = authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
#         for tag in authenticated_route_tags:
#             app_info['authenticated_routes'].append(tag.get_attribute('innerText'))
        
#         app_info['unauthenticated_routes'] = []
#         unauthenticated_route_tag_container = rows[8].find_element(By.CLASS_NAME, 'unauthenticated-routes')
#         unauthenticated_route_tags = unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
#         for tag in unauthenticated_route_tags:
#             app_info['unauthenticated_routes'].append(tag.get_attribute('innerText'))

#         app_info['runtime_command'] = rows[9].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
#         app_info['environment_variables'] = rows[10].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
#         app_info['database'] = rows[11].find_elements(By.TAG_NAME, 'div')[1].get_attribute('innerText')
#         return app_info

#     # Test app creation form
#     def createAppTest(self, identifier, descriptive_name, description,\
#         container_image, image_tag="latest", container_port=80, authentication="",\
#         authenticated_routes=[], unauthenticated_routes=[], runtime_command="", environment_variables="", database="None"):
        
#         # Refresh the page
#         self.visitDashboard()

#         # Check if an application with this identifier already exists
#         app_exist = len(self.selenium.find_elements(By.ID, f'{identifier}-tab-pane')) > 0

#         # Refresh the page
#         self.visitDashboard()

#         # Open the 'Dashboard' tab
#         dashboard_tab_btn = self.selenium.find_element(By.ID, 'dashboard-tab')
#         dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
#         self.selenium.execute_script('arguments[0].click()', dashboard_tab_btn)
#         assert 'active' in dashboard_tab.get_attribute('class')

#         # Create form should be hidden until clicking on the "+" button
#         create_app_form = dashboard_tab.find_element(By.CLASS_NAME, 'app-create-form')
#         assert 'display: none;' in create_app_form.get_attribute('style')
#         create_app_btn = dashboard_tab.find_element(By.CLASS_NAME, 'app-create-btn')
#         self.selenium.execute_script('arguments[0].click()', create_app_btn)
#         assert 'display: none;' not in create_app_form.get_attribute('style')

#         # Test contraints (uniqueness, length and characters) of identifier field
#         identifier_field = create_app_form.find_element(By.NAME, 'identifier')
#         identifier_field_error_message = create_app_form.find_elements(By.CLASS_NAME, 'text-danger')[0]
#         assert 'd-none' in identifier_field_error_message.get_attribute('class')
        
#         identifier_field.send_keys('te')
#         assert 'error-input' in identifier_field.get_attribute('class')
#         assert 'd-none' not in identifier_field_error_message.get_attribute('class')
#         assert 'Identifier needs to have at least 3 characters' in\
#             identifier_field_error_message.get_attribute('innerText')
        
#         identifier_field.clear()
#         identifier_field.send_keys('tes%')
#         assert 'error-input' in identifier_field.get_attribute('class')
#         assert 'd-none' not in identifier_field_error_message.get_attribute('class')
#         assert 'Identifier only contains lowercase letters, numbers and hyphens' in\
#             identifier_field_error_message.get_attribute('innerText')

#         identifier_field.clear()
#         identifier_field.send_keys(identifier)
#         if app_exist:
#             assert 'error-input' in identifier_field.get_attribute('class')
#             assert 'd-none' not in identifier_field_error_message.get_attribute('class')
#             assert 'Identifier already exists' in\
#                 identifier_field_error_message.get_attribute('innerText')
#             return
#         assert 'error-input' not in identifier_field.get_attribute('class')
#         assert 'd-none' in identifier_field_error_message.get_attribute('class')

#         # Fill in descriptive name, description, container image, image tag, container port
#         # and select an authentication mode
#         descriptive_name_field = create_app_form.find_element(By.NAME, 'descriptive_name')
#         descriptive_name_field.send_keys(descriptive_name)

#         description_field = create_app_form.find_element(By.NAME, 'description')
#         description_field.send_keys(description)

#         container_image_field = create_app_form.find_element(By.NAME, 'container_image')
#         container_image_field.send_keys(container_image)

#         image_tag_field = create_app_form.find_element(By.NAME, 'image_tag')
#         image_tag_field.send_keys(image_tag)

#         container_port_field = create_app_form.find_element(By.NAME, 'container_port')
#         container_port_field.send_keys(container_port)

#         authentication_field = Select(create_app_form.find_element(By.NAME, 'authentication'))
#         authentication_field.select_by_value(authentication)

#         route_input = create_app_form.find_elements(By.CLASS_NAME, 'routes-input')
        
#         # Test authenticated route
#         authenticated_route_input = route_input[0]
#         authenticated_route_input_field = authenticated_route_input.find_element(By.TAG_NAME, 'input')
#         authenticated_route_input_btn = authenticated_route_input.find_element(By.CLASS_NAME, 'btn')
#         authenticated_route_tag_container = create_app_form.find_element(By.CSS_SELECTOR, '.route-tag-container.authenticated-routes')
#         assert 'disabled' in authenticated_route_input_btn.get_attribute('class')
        
#         # Add authenticated routes
#         for route in authenticated_routes:
#             authenticated_route_input_field.send_keys(route)
#             assert 'disabled' not in authenticated_route_input_btn.get_attribute('class')
#             self.selenium.execute_script('arguments[0].click()', authenticated_route_input_btn)
#             assert 'disabled' in authenticated_route_input_btn.get_attribute('class')
#             assert not authenticated_route_input.get_attribute("value")
#         assert len(authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')) == len(authenticated_routes)

#         # Test unauthenticated route
#         unauthenticated_route_input = route_input[1]
#         unauthenticated_route_input_field = unauthenticated_route_input.find_element(By.TAG_NAME, 'input')
#         unauthenticated_route_input_btn = unauthenticated_route_input.find_element(By.CLASS_NAME, 'btn')
#         unauthenticated_route_tag_container = create_app_form.find_element(By.CSS_SELECTOR, '.route-tag-container.unauthenticated-routes')
#         assert 'disabled' in unauthenticated_route_input_btn.get_attribute('class')

#         # Add unauthenticated routes
#         for route in unauthenticated_routes:
#             unauthenticated_route_input_field.send_keys(route)
#             assert 'disabled' not in unauthenticated_route_input_btn.get_attribute('class')
#             self.selenium.execute_script('arguments[0].click()', unauthenticated_route_input_btn)
#             assert 'disabled' in unauthenticated_route_input_btn.get_attribute('class')
#             assert not unauthenticated_route_input.get_attribute("value")
#         # unauthenticated_route_input_field.send_keys('/logout')
#         # self.selenium.execute_script('arguments[0].click()', unauthenticated_route_input_btn)
#         assert len(unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')) == len(unauthenticated_routes)

#         # Fill in runtime command, environment variables and select a database
#         runtime_command_field = create_app_form.find_element(By.NAME, 'runtime_command')
#         runtime_command_field.send_keys(runtime_command)

#         environment_variables_field = create_app_form.find_element(By.NAME, 'environment_variables')
#         environment_variables_field.send_keys(environment_variables)

#         database_field = Select(create_app_form.find_element(By.NAME, 'database'))
#         database_field.select_by_value(database)

#         # Submit the form, check if a new app has been created
#         submit_btn = create_app_form.find_element(By.CSS_SELECTOR, "button[type='submit']")
#         self.selenium.execute_script('arguments[0].click()', submit_btn)

#         # Wait for the page to refresh, check the success message content
#         try:
#             self.wait.until(lambda selenium: selenium.current_url == f"{app_url}/")
#         except TimeoutException:
#             print("Page does not refresh after submit app creation form")
#         success_message = self.selenium.find_element(By.CLASS_NAME, "alert")
#         assert "alert-success" in success_message.get_attribute("class")
#         assert f"App {descriptive_name} created successfully" in success_message.get_attribute('innerText')

#         # Check if a new tab for the new app exists
#         assert len(self.selenium.find_elements(By.ID, f"{identifier}-tab-pane")) == 1

#         app_card = self.selenium.find_element(By.CSS_SELECTOR, f'#dashboard-tab-pane .{identifier}-app-card')
#         # Test new values of the edited application
#         app_summary = app_card.find_element(By.CSS_SELECTOR, '.app-summary')
#         app_info = self.readAppInfo(app_summary)
#         assert app_info['identifier'] == identifier
#         assert app_info['descriptive_name'] == descriptive_name
#         assert app_info['description'] == description
#         assert app_info['container_image'] == container_image
#         assert app_info['image_tag'] == image_tag
#         assert app_info['container_port'] == str(container_port)
#         if not authentication:
#             assert app_info['authentication'] == 'None'
#         elif authentication == 'uvic':
#             assert app_info['authentication'] == 'Uvic'
#         else:
#             assert app_info['authentication'] == 'Uvic + Social'
#         assert sorted(app_info['authenticated_routes']) == sorted(app_info['authenticated_routes'])
#         assert sorted(app_info['unauthenticated_routes']) == sorted(app_info['unauthenticated_routes'])
#         assert app_info['runtime_command'] == runtime_command
#         assert app_info['environment_variables'] == environment_variables
#         assert app_info['database'] == database

#     # Test "See more"/"See less" on the each app card on Applications widget on the dashboard tab
#     def seeMoreSeeLessBtnTest(self, identifier):
#         # Refresh the page
#         self.visitDashboard()

#         dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
#         app_content = dashboard_tab.find_element(By.CSS_SELECTOR, f'.{identifier}-app-card .brief-display')
#         hidden_content = app_content.find_element(By.CLASS_NAME, 'hidden-content')
#         see_more_btn = app_content.find_element(By.CLASS_NAME, "see-more-btn")
#         see_less_btn = app_content.find_element(By.CLASS_NAME, "see-less-btn")

#         assert 'd-none' in hidden_content.get_attribute("class")

#         # Clicking on "See more" button shows the hidden content and "See less" button
#         # and hides "See more" button
#         self.selenium.execute_script('arguments[0].click()', see_more_btn)
#         assert 'd-none' not in hidden_content.get_attribute("class")
#         assert 'd-none' not in see_less_btn.get_attribute("class")
#         assert 'd-none' in see_more_btn.get_attribute("class")

#         # Clicking on "See less" button hides the hidden content and "See less" button
#         # and shows "See more" button
#         self.selenium.execute_script('arguments[0].click()', see_less_btn)
#         assert 'd-none' in hidden_content.get_attribute("class")
#         assert 'd-none' in see_less_btn.get_attribute("class")
#         assert 'd-none' not in see_more_btn.get_attribute("class")

#     # Test editing app
#     # If app doesn't exist, do nothing
#     def editAppTest(self, identifier, descriptive_name, description,\
#         container_image, image_tag="latest", container_port=80, authentication="",\
#         authenticated_routes=[], unauthenticated_routes=[], runtime_command="", environment_variables="", database="None"):
#         # Refresh the page
#         self.visitDashboard()

#         # Open the dashboard tab
#         self.selenium.find_element(By.ID, 'dashboard-tab').click()
        
#         try:
#             app_card = self.selenium.find_element(By.CSS_SELECTOR, f'#dashboard-tab-pane .{identifier}-app-card')
#         except:
#             print(f"App with identifier {identifier} doesn't exist")
#             return
        
#         pen_icon_btn = app_card.find_element(By.CLASS_NAME, 'app-edit-btn')
#         edit_form = app_card.find_element(By.CLASS_NAME, 'app-edit-form')
#         edit_form_cancel_btn = edit_form.find_element(By.CLASS_NAME, 'app-card-edit-cancel-btn')

#         # Test opening the closing edit form
#         assert "display: none;" in edit_form.get_attribute("style")
#         self.selenium.execute_script('arguments[0].click()', pen_icon_btn)
#         self.wait.until(lambda selenium: "opacity" not in edit_form.get_attribute("style"))
#         assert "display: none;" not in edit_form.get_attribute("style")
#         self.selenium.execute_script('arguments[0].click()', edit_form_cancel_btn)
#         self.wait.until(lambda selenium: "display: none;" in edit_form.get_attribute("style"))
#         self.selenium.execute_script('arguments[0].click()', pen_icon_btn)
#         self.wait.until(lambda selenium: "opacity" not in edit_form.get_attribute("style"))

#         # Test current values of edit form fields and fill in new values
#         app_summary = app_card.find_element(By.CSS_SELECTOR, '.app-summary')
#         app_info = self.readAppInfo(app_summary)

#         identifier_field = edit_form.find_element(By.NAME, 'identifier')
#         assert identifier_field.get_attribute("value") == app_info['identifier']

#         descriptive_name_field = edit_form.find_element(By.NAME, 'descriptive_name')
#         assert descriptive_name_field.get_attribute("value") == app_info['descriptive_name']
#         descriptive_name_field.clear()
#         descriptive_name_field.send_keys(descriptive_name)

#         description_field = edit_form.find_element(By.NAME, 'description')
#         assert description_field.get_attribute("value") == app_info['description']
#         description_field.clear()
#         description_field.send_keys(description)

#         container_image_field = edit_form.find_element(By.NAME, 'container_image')
#         assert container_image_field.get_attribute("value") == app_info['container_image']
#         container_image_field.clear()
#         container_image_field.send_keys(container_image)

#         image_tag_field = edit_form.find_element(By.NAME, 'image_tag')
#         assert image_tag_field.get_attribute("value") == app_info['image_tag']
#         image_tag_field.clear()
#         image_tag_field.send_keys(image_tag)

#         container_port_field = edit_form.find_element(By.NAME, 'container_port')
#         assert container_port_field.get_attribute("value") == app_info['container_port']
#         container_port_field.clear()
#         container_port_field.send_keys(container_port)

#         authentication_field = Select(edit_form.find_element(By.NAME, 'authentication'))
#         if app_info['authentication'] != 'None':
#             assert authentication_field.first_selected_option.get_attribute("value") == app_info['authentication']
#         else:
#             assert not authentication_field.first_selected_option.get_attribute("value")
#         authentication_field.select_by_value(authentication)

#         route_input = edit_form.find_elements(By.CLASS_NAME, 'routes-input')
        
#         # Test authenticated route
#         authenticated_route_input = route_input[0]
#         authenticated_route_input_field = authenticated_route_input.find_element(By.TAG_NAME, 'input')
#         authenticated_route_input_btn = authenticated_route_input.find_element(By.CLASS_NAME, 'btn')
#         authenticated_route_tag_container = edit_form.find_element(By.CSS_SELECTOR, '.route-tag-container.authenticated-routes')
#         assert 'disabled' in authenticated_route_input_btn.get_attribute('class')

#         authenticated_route_tags = authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
#         for route_tag in authenticated_route_tags:
#             route = route_tag.find_element(By.CLASS_NAME, 'route-tag-path')
#             delete_btn = route_tag.find_element(By.CLASS_NAME, 'route-tag-close')
#             assert route.get_attribute('innerText') in app_info['authenticated_routes']
#             self.selenium.execute_script('arguments[0].click()', delete_btn)

#         authenticated_route_tags = authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
#         assert len(authenticated_route_tags) == 0

#         # Add authenticated routes
#         for route in authenticated_routes:
#             authenticated_route_input_field.send_keys(route)
#             assert 'disabled' not in authenticated_route_input_btn.get_attribute('class')
#             self.selenium.execute_script('arguments[0].click()', authenticated_route_input_btn)
#             assert 'disabled' in authenticated_route_input_btn.get_attribute('class')
#             assert not authenticated_route_input.get_attribute("value")
#         assert len(authenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')) == len(authenticated_routes)

#         # Test unauthenticated route
#         unauthenticated_route_input = route_input[1]
#         unauthenticated_route_input_field = unauthenticated_route_input.find_element(By.TAG_NAME, 'input')
#         unauthenticated_route_input_btn = unauthenticated_route_input.find_element(By.CLASS_NAME, 'btn')
#         unauthenticated_route_tag_container = edit_form.find_element(By.CSS_SELECTOR, '.route-tag-container.unauthenticated-routes')
#         assert 'disabled' in unauthenticated_route_input_btn.get_attribute('class')

#         unauthenticated_route_tags = unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
#         for route_tag in unauthenticated_route_tags:
#             route = route_tag.find_element(By.CLASS_NAME, 'route-tag-path')
#             delete_btn = route_tag.find_element(By.CLASS_NAME, 'route-tag-close')
#             assert route.get_attribute('innerText') in app_info['unauthenticated_routes']
#             self.selenium.execute_script('arguments[0].click()', delete_btn)

#         unauthenticated_route_tags = unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')
#         assert len(unauthenticated_route_tags) == 0

#         # Add unauthenticated routes
#         for route in unauthenticated_routes:
#             unauthenticated_route_input_field.send_keys(route)
#             assert 'disabled' not in unauthenticated_route_input_btn.get_attribute('class')
#             self.selenium.execute_script('arguments[0].click()', unauthenticated_route_input_btn)
#             assert 'disabled' in unauthenticated_route_input_btn.get_attribute('class')
#             assert not unauthenticated_route_input.get_attribute("value")
#         assert len(unauthenticated_route_tag_container.find_elements(By.CLASS_NAME, 'route-tag')) == len(unauthenticated_routes)

#         runtime_command_field = edit_form.find_element(By.NAME, 'runtime_command')
#         assert runtime_command_field.get_attribute("value") == app_info['runtime_command']
#         runtime_command_field.clear()
#         runtime_command_field.send_keys(runtime_command)

#         environment_variables_field = edit_form.find_element(By.NAME, 'environment_variables')
#         assert environment_variables_field.get_attribute("value") == app_info['environment_variables']
#         environment_variables_field.clear()
#         environment_variables_field.send_keys(environment_variables)

#         database_field = Select(edit_form.find_element(By.NAME, 'database'))
#         assert database_field.first_selected_option.get_attribute("value") == app_info['database']
#         database_field.select_by_value(database)

#         # Submit the edit form
#         submit_btn = edit_form.find_element(By.CSS_SELECTOR, "button[type='submit']")
#         self.selenium.execute_script('arguments[0].click()', submit_btn)

#         # Wait for the page to refresh, cue is app tab pane being opened
#         # Check the success message content
#         try:
#             self.wait.until(
#                 lambda selenium: "show active" in\
#                 self.selenium.find_element(By.ID, f'{identifier}-tab-pane').get_attribute("class")
#             )
#         except TimeoutException:
#             print("Page does not refresh after submit app edit form")
#         success_message = self.selenium.find_element(By.CLASS_NAME, "alert")
#         assert "alert-success" in success_message.get_attribute("class")
#         assert f"App {descriptive_name} edited successfully" in success_message.get_attribute('innerText')

#         app_card = self.selenium.find_element(By.CSS_SELECTOR, f'#dashboard-tab-pane .{identifier}-app-card')
#         # Test new values of the edited application
#         app_summary = app_card.find_element(By.CSS_SELECTOR, '.app-summary')
#         app_info = self.readAppInfo(app_summary)
#         assert app_info['identifier'] == identifier
#         assert app_info['descriptive_name'] == descriptive_name
#         assert app_info['description'] == description
#         assert app_info['container_image'] == container_image
#         assert app_info['image_tag'] == image_tag
#         assert app_info['container_port'] == str(container_port)
#         if not authentication:
#             assert app_info['authentication'] == 'None'
#         elif authentication == 'uvic':
#             assert app_info['authentication'] == 'Uvic'
#         else:
#             assert app_info['authentication'] == 'Uvic + Social'
#         assert sorted(app_info['authenticated_routes']) == sorted(app_info['authenticated_routes'])
#         assert sorted(app_info['unauthenticated_routes']) == sorted(app_info['unauthenticated_routes'])
#         assert app_info['runtime_command'] == runtime_command
#         assert app_info['environment_variables'] == environment_variables
#         assert app_info['database'] == database

#     # Test deleting app
#     # If app doesn't exist, do nothing
#     def deleteAppTest(self, identifier):
#         # Refresh the page
#         self.visitDashboard()

#         try:
#             app_card = self.selenium.find_element(By.CSS_SELECTOR, f'#dashboard-tab-pane .{identifier}-app-card')
#         except:
#             print(f"App with identifier {identifier} doesn't exist")
#             return
#         trash_icon_btn = app_card.find_element(By.CLASS_NAME, 'app-summary-delete-btn')
#         delete_modal = self.selenium.find_element(By.ID, 'deleteModal')
#         identifier_input = delete_modal.find_element(By.TAG_NAME, 'input')
#         confirm_btn = delete_modal.find_element(By.CLASS_NAME, 'delete-modal-confirm-btn')

#         # Open the app delete modal
#         assert 'show' not in delete_modal.get_attribute("class")
#         self.selenium.execute_script('arguments[0].click()', trash_icon_btn)
#         self.wait.until(lambda selenium: 'show' in delete_modal.get_attribute("class"))

#         # "Confirm" button should be disabled until app identifier is typed
#         assert 'disabled' in confirm_btn.get_attribute("class")
#         identifier_input.send_keys(identifier)
#         assert 'disabled' not in confirm_btn.get_attribute("class")
#         self.selenium.execute_script('arguments[0].click()', confirm_btn)

#         # Wait for the page to refresh, check the success message content
#         try:
#             self.wait.until(lambda selenium: selenium.current_url == f"{app_url}/")
#         except TimeoutException:
#             print("Page does not refresh after submit app creation form")
#         success_message = self.selenium.find_element(By.CLASS_NAME, "alert")
#         assert "alert-success" in success_message.get_attribute("class")
#         assert f"deleted successfully" in success_message.get_attribute('innerText')
#         assert len(self.selenium.find_elements(By.CSS_SELECTOR, f'#dashboard-tab-pane .{identifier}-app-card')) == 0
    
#     # Args: Group id
#     # Returns: info about the group in a dictionary
#     def readGroupInfo(self, group_id):
#         # Refresh the page
#         self.visitDashboard()

#         # Open the dashboard tab
#         self.selenium.find_element(By.ID, 'dashboard-tab').click()
#         dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
#         group_card = dashboard_tab.find_element(By.CLASS_NAME, f"group-card-{group_id}")

#         group_name = group_card.find_element(By.CLASS_NAME, 'group-name').get_attribute("innerText")
#         group_apps = group_card.find_element(By.CLASS_NAME, 'group-apps').get_attribute("innerText")
#         group_app_list = []
#         if group_apps:
#             if ', ' in group_apps:
#                 group_app_list = group_apps.split(', ')
#             else:
#                 group_app_list = [group_apps]

#         is_everyone = False
#         group_user_list = []
#         group_users = group_card.find_element(By.CLASS_NAME, 'group-users')
#         if group_users.get_attribute("innerText") == "All authenticated users":
#             is_everyone = True
#             data_users = group_users.get_attribute("data-users")
#             for user in data_users.split(','):
#                 if user:
#                     group_user_list.append(user)
#         elif group_users.get_attribute("innerText") != "No one":
#             for tag in group_card.find_elements(By.CSS_SELECTOR, '.group-users .tag'):
#                 group_user_list.append(tag.get_attribute("innerText"))
        
#         return {
#             'name': group_name,
#             'is_everyone': is_everyone,
#             'apps': group_app_list,
#             'users': group_user_list
#         }


#     # Test for creating groups
#     # Returns the newly created group id
#     def createGroupTest(self, name, is_everyone=False, users=[], apps=[]):
#         # Refresh the page
#         self.visitDashboard()

#         # Open the dashboard tab
#         self.selenium.find_element(By.ID, 'dashboard-tab').click()

#         dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
#         group_create_btn = dashboard_tab.find_element(By.CLASS_NAME, 'app-group-create-form-btn')
#         group_create_form_card = dashboard_tab.find_element(By.CLASS_NAME, 'app-group-form-card')
#         group_create_form = group_create_form_card.find_element(By.CLASS_NAME, "app-group-form")

#         # Toggle group create form with "+" button
#         self.selenium.execute_script('arguments[0].click()', group_create_btn)
#         self.wait.until(lambda selenium: EC.visibility_of_element_located(group_create_form_card))

#         self.selenium.execute_script('arguments[0].click()', group_create_btn)
#         self.wait.until(lambda selenium: "display: none" in group_create_form_card.get_attribute("style"))

#         self.selenium.execute_script('arguments[0].click()', group_create_btn)
#         self.wait.until(lambda selenium: EC.visibility_of_element_located(group_create_form_card))

#         name_field = group_create_form.find_element(By.NAME, 'name')
#         name_field.send_keys(name)

#         is_everyone_field = group_create_form.find_element(By.NAME, 'is_everyone')
#         if is_everyone:
#             self.selenium.execute_script('arguments[0].click()', is_everyone_field)
        
#         # Test group's users
#         user_input = group_create_form.find_element(By.CLASS_NAME, 'input-tag-field')
#         user_input_field = user_input.find_element(By.TAG_NAME, 'input')
#         user_input_btn = user_input.find_element(By.CLASS_NAME, 'btn')
#         user_tag_container = group_create_form.find_element(By.CSS_SELECTOR, '.input-tag-container')
#         assert 'disabled' in user_input_btn.get_attribute('class')
        
#         # Insert users
#         for user in users:
#             user_input_field.send_keys(user)
#             assert 'disabled' not in user_input_btn.get_attribute('class')
#             self.selenium.execute_script('arguments[0].click()', user_input_btn)
#             assert 'disabled' in user_input_btn.get_attribute('class')
#             assert not user_input_field.get_attribute("value")
#         assert len(user_tag_container.find_elements(By.CLASS_NAME, 'input-tag')) == len(users)

#         application_field = Select(group_create_form.find_element(By.NAME, 'application'))
#         for app in apps:
#             application_field.select_by_visible_text(app)
#         assert len(application_field.all_selected_options) == len(apps)

#         submit_btn = group_create_form.find_element(By.CSS_SELECTOR, "button[type='submit']")
#         self.selenium.execute_script('arguments[0].click()', submit_btn)

#         # Wait for the page to refresh, cue is a success message showing up
#         # Check the success message content
#         try:
#             self.wait.until(
#                 lambda selenium:
#                 self.selenium.find_element(By.CLASS_NAME, "alert")
#             )
#         except TimeoutException:
#             print("Page does not refresh after submiting group create form")
#         success_message = self.selenium.find_element(By.CLASS_NAME, "alert")
#         assert "alert-success" in success_message.get_attribute("class")
#         assert f"Create group '{name}' successfully." in success_message.get_attribute('innerText')

#         # Open the dashboard tab
#         self.selenium.find_element(By.ID, 'dashboard-tab').click()

#         # Test newly created group information
#         new_group_card = self.selenium.find_elements(By.CLASS_NAME, 'group-card')[0]
        
#         group_name = new_group_card.find_element(By.CLASS_NAME, 'group-name').get_attribute("innerText")
#         assert group_name == name

#         group_apps = new_group_card.find_element(By.CLASS_NAME, 'group-apps').get_attribute("innerText")
#         for app in apps:
#             assert app in group_apps
        
#         if is_everyone:
#             assert new_group_card.find_element(By.CLASS_NAME, "group-users").get_attribute("innerText") == "All authenticated users"
#         else:
#             if len(users) > 0:
#                 group_users = []
#                 for tag in new_group_card.find_elements(By.CSS_SELECTOR, '.group-users .tag'):
#                     group_users.append(tag.get_attribute("innerText"))
#                 assert sorted(users) == sorted(group_users)
#             else:
#                 assert new_group_card.find_element(By.CLASS_NAME, "group-users").get_attribute("innerText") == "No one"

#         group_id = new_group_card.get_attribute("class").split(" ")[-1].split("-")[-1]
#         return (group_id, name)
    
#     # Test editing a group
#     def editGroupTest(self, group_id, name, is_everyone=False, users=[], apps=[]):
#         group_info = self.readGroupInfo(group_id)
        
#         # Refresh the page
#         self.visitDashboard()

#         # Open the dashboard tab
#         self.selenium.find_element(By.ID, 'dashboard-tab').click()

#         dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
#         group_card = dashboard_tab.find_element(By.CLASS_NAME, f"group-card-{group_id}")
#         group_edit_form_card = dashboard_tab.find_element(By.CLASS_NAME, f"app-group-edit-form-card-{group_id}")
#         edit_btn = group_card.find_element(By.CLASS_NAME, "app-group-edit-form-btn")

#         # Open group edit form with pen icon button
#         self.selenium.execute_script('arguments[0].click()', edit_btn)
#         self.wait.until(lambda selenium: EC.visibility_of_element_located(group_edit_form_card))

#         # Close the edit form by clicking on "Cancel" button
#         cancel_btn = group_edit_form_card.find_element(By.CLASS_NAME, 'app-group-edit-form-cancel-btn')
#         self.selenium.execute_script('arguments[0].click()', cancel_btn)
#         self.wait.until(lambda selenium: "display: none;" in group_edit_form_card.get_attribute("style"))

#         self.selenium.execute_script('arguments[0].click()', edit_btn)
#         self.wait.until(lambda selenium: EC.visibility_of_element_located(group_edit_form_card))

#         # Check current values of the group edit form and change them to the new values
#         name_field = group_edit_form_card.find_element(By.NAME, 'name')
#         assert group_info['name'] == name_field.get_attribute("value")
#         name_field.clear()
#         name_field.send_keys(name)

#         is_everyone_field = group_edit_form_card.find_element(By.NAME, 'is_everyone')
#         assert group_info['is_everyone'] == is_everyone_field.is_selected()

#         # Temporarily uncheck "Include any authenticated user" to enable the text field for adding members
#         if is_everyone_field.is_selected():
#             self.selenium.execute_script('arguments[0].click()', is_everyone_field)

#         # Test members
#         user_input = group_edit_form_card.find_element(By.CLASS_NAME, 'input-tag-field')
#         user_input_field = user_input.find_element(By.TAG_NAME, 'input')
#         user_input_btn = user_input.find_element(By.CLASS_NAME, 'btn')
#         user_tag_container = group_edit_form_card.find_element(By.CSS_SELECTOR, '.input-tag-container')
#         assert 'disabled' in user_input_btn.get_attribute('class')

#         # Remove all current members
#         user_tags = user_tag_container.find_elements(By.CLASS_NAME, 'input-tag')
#         for user_tag in user_tags:
#             user = user_tag.find_element(By.CLASS_NAME, 'input-tag-content')
#             delete_btn = user_tag.find_element(By.CLASS_NAME, 'input-tag-close')
#             assert user.get_attribute('innerText') in group_info['users']
#             self.selenium.execute_script('arguments[0].click()', delete_btn)

#         user_tags = user_tag_container.find_elements(By.CLASS_NAME, 'input-tag')
#         assert len(user_tags) == 0

#         # Add new members
#         for user in users:
#             user_input_field.send_keys(user)
#             assert 'disabled' not in user_input_btn.get_attribute('class')
#             self.selenium.execute_script('arguments[0].click()', user_input_btn)
#             assert 'disabled' in user_input_btn.get_attribute('class')
#             assert not user_input.get_attribute("value")
#         assert len(user_tag_container.find_elements(By.CLASS_NAME, 'input-tag')) == len(users)

#         if is_everyone != is_everyone_field.is_selected():
#             self.selenium.execute_script('arguments[0].click()', is_everyone_field)

#         application_field = Select(group_edit_form_card.find_element(By.NAME, 'application'))
#         for app_option in application_field.all_selected_options:
#             assert app_option.get_attribute("innerText") in group_info["apps"]
#         application_field.deselect_all()
#         for app in apps:
#             application_field.select_by_visible_text(app)
#         assert len(application_field.all_selected_options) == len(apps)

#         submit_btn = group_edit_form_card.find_element(By.CSS_SELECTOR, "button[type='submit']")
#         self.selenium.execute_script('arguments[0].click()', submit_btn)

#         # Wait for the page to refresh, cue is app tab pane being opened
#         # Check the success message content
#         try:
#             self.wait.until(
#                 lambda selenium:
#                 self.selenium.find_element(By.CLASS_NAME, "alert")
#             )
#         except TimeoutException:
#             print("Page does not refresh after submiting group edit form")
#         success_message = self.selenium.find_element(By.CLASS_NAME, "alert")
#         assert "alert-success" in success_message.get_attribute("class")
#         assert f"Edit group '{name}' successfully." in success_message.get_attribute('innerText')

#         group_info = self.readGroupInfo(group_id)
#         assert group_info['name'] == name
#         assert group_info['is_everyone'] == is_everyone
#         assert sorted(group_info['users']) == sorted(users)
#         assert sorted(group_info['apps']) == sorted(apps)

#         return (group_id, name)
    
#     # Test deleting group
#     # This function is also used to clean up groups created during tests
#     def deleteGroupTest(self, group_id, name):
#         # Refresh the page
#         self.visitDashboard()

#         # Open the dashboard tab
#         self.selenium.find_element(By.ID, 'dashboard-tab').click()
#         dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
#         group_card = dashboard_tab.find_element(By.CLASS_NAME, f"group-card-{group_id}")
#         trash_icon_btn = group_card.find_element(By.CLASS_NAME, "fa-trash")
#         delete_modal = self.selenium.find_element(By.ID, f'groupDeleteModal-{group_id}')

#         self.selenium.execute_script('arguments[0].click()', trash_icon_btn)
#         self.wait.until(lambda selenium: 'show' in delete_modal.get_attribute('class'))

#         modal_title = delete_modal.find_element(By.CLASS_NAME, "modal-title")
#         assert f"Delete group '{name}'" in modal_title.get_attribute("innerText")

#         confirm_btn = delete_modal.find_element(By.CLASS_NAME, 'btn-danger')
#         self.selenium.execute_script('arguments[0].click()', confirm_btn)

#         # Wait for the page to refresh, cue is app tab pane being opened
#         # Check the success message content
#         try:
#             self.wait.until(
#                 lambda selenium:
#                 self.selenium.find_element(By.CLASS_NAME, "alert")
#             )
#         except TimeoutException:
#             print("Page does not refresh after submiting group edit form")
#         success_message = self.selenium.find_element(By.CLASS_NAME, "alert")
#         assert "alert-success" in success_message.get_attribute("class")
#         assert f"Delete group '{name}' successfully" in success_message.get_attribute('innerText')

#         try:
#             dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
#             group_card = dashboard_tab.find_element(By.CLASS_NAME, f"group-card-{group_id}")
#             print(f"Deleting group {name} failed")
#             assert False
#         except NoSuchElementException:
#             assert True

#     # Run all tests for 'Dashboard' tab
#     # Run all tests in a single function so that database doesn't reset after each test
#     def test_all(self):
#         print("Calling test_all of DashboardTabTest")
#         # Create app My Application with identifier 'myapp'.
#         self.createAppTest(identifier='myapp', descriptive_name='My Application',\
#             description='My application provides a basic functionality of an app.\
#             It is created for testing either with Selenium or manual testing. It is a very useful application.',\
#             container_image='archie/myapp', authenticated_routes=["/", "/amin", "/about"],\
#             unauthenticated_routes=["/login", "/logout"])

#         # Try creating app My Application with identifier 'myapp' again to test the uniqueness of identifier
#         self.createAppTest(identifier='myapp', descriptive_name='My Application',\
#             description='My application provides a basic functionality of an app.',\
#             container_image='archie/myapp')

#         # Create app Selenium App with identifier 'seleniumapp'
#         self.createAppTest(identifier='seleniumapp', descriptive_name='Selenium Application',\
#             description='This app is created by Selenium',\
#             container_image='archie/seleniumapp', image_tag="1.0", container_port=8001,\
#             authentication="uvic_social", authenticated_routes=["/", "/admin"],\
#             unauthenticated_routes=["/login"], runtime_command="python seleniumapp.py",\
#             environment_variables="VAR1=var1\nVAR2=var2", database="PostgresSQL")

#         # There should be 2 applications in the Applications widge of Dashboard tab
#         # dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
#         # assert len(dashboard_tab.find_elements(By.CLASS_NAME, 'app-card')) == 2

#         self.seeMoreSeeLessBtnTest(identifier='myapp')

#         self.editAppTest(identifier='myapp', descriptive_name='My Application',\
#             description='My application provides a basic functionality of an app.',\
#             container_image='archie/myapp', image_tag="1.0", container_port=4001,\
#             authentication="uvic_social", authenticated_routes=["/", "/admin"],\
#             unauthenticated_routes=["/login"], runtime_command="python myapp.py",\
#             environment_variables="VAR1=var1\nVAR2=var2", database="PostgresSQL")

#         (myapp_group_id, myapp_group_name) = self.createGroupTest(name="Users", is_everyone=False, users=['user@example.org', 'admin@example.org'], apps=["My Application"])

#         (myapp_group_id, myapp_group_name) = self.editGroupTest(group_id=myapp_group_id, name="Admins", is_everyone=True, users=["user@example.org", "admin@example.org"], apps=["My Application", "Selenium Application"])

#         self.deleteGroupTest(myapp_group_id, myapp_group_name)
#         # Test app deletion and clean up the created apps at the same time
#         self.deleteAppTest('myapp')
#         self.deleteAppTest('seleniumapp')

#         # dashboard_tab = self.selenium.find_element(By.ID, 'dashboard-tab-pane')
#         # assert len(dashboard_tab.find_elements(By.CLASS_NAME, 'app-card')) == 0
