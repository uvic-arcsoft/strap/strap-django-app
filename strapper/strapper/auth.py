# pylint:
import jwt

from django.contrib.auth.models import User
from django.conf import settings
from django.shortcuts import redirect
from django.urls import reverse
from django.views import View
from django.contrib.auth import login, logout

from django_editablecontent.views import EditableContentView

from wizard.models import Wizard
from access.models import Profile

from exceptions import BadConfig

USER_DNE = -1

def authenticate(data):
    """
    This function authenticates users by check the HTTP request header. The HTTP request header,
    namely 'X-Forwarded-User', contains username of the user. If the username is found in Django's user
    database, the user is authenticated. Else, the user is not. Users in user database is created by the superuser
    throught '/admin' page manually for TESTING PURPOSES.

    Args:
        data: HTTP request headers

    Returns:
        Tuple:
            - User Id: an int if user is authenticated, None if not authenticated
            - admin: True if user is authenticated and an admin, else False.
            - first_login: True if user is authenticated and logging in for the first time, else False.
    """
    # For testing, can edit 'AUTHX_USER_OVERRIDE' to replace the HTTP request headers in settings.py
    # if settings.DEBUG and settings.CONFIG.get('AUTHX_USER_OVERRIDE'):
    if settings.CONFIG.get('AUTHX_USER_OVERRIDE', None):
        username = settings.CONFIG.get('AUTHX_USER_OVERRIDE')
        try:
            user = User.objects.get(username=username)
            return (user.id, False)
        except User.DoesNotExist:
            user = User.objects.create(username=username, password=f"strap{username}", is_staff=False)
            Profile.objects.create(user=user)
            return (user.id, True)

    # header should be 'X-Forwarded-User'
    header = settings.CONFIG.get('AUTHX_HTTP_HEADER_USER', None)
    if header is None:
        raise BadConfig('Missing definition for HTTP request header container user ID')

    # read the username passed in the header
    username = data.get(header, None)

    if username is None:
        # raise BadCall('Missing HTTP request header for user authentication')
        return (USER_DNE, False)

    # Authenticate and check if user is an admin
    first_login = False
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        user = User.objects.create(username=username, password=f"strap{username}", is_staff=False)
        first_login = True
        Profile.objects.create(user=user)

    # Populate user profile with ID Token received from authentication service
    id_token = data.get(settings.CONFIG.get('AUTHX_HTTP_HEADER_ID_TOKEN', None), None)
    if id_token:
        id_token = id_token.replace("Bearer ", "")
        user_info = jwt.decode(id_token, options={"verify_signature": False})
        user.first_name = user_info.get('given_name', '')
        user.last_name = user_info.get('family_name', '')
        user.email = user_info.get('email', '')
        user.save()

        user.profile.image = user_info.get('picture', '')
        user.profile.save()


    return (user.id, first_login)

# Login Page: Unauthenticated users and first-time visiters will have to
# get through this view
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class LoginView(EditableContentView):
    template_name = "login.html"
    editable_content_names = settings.GLOBAL_EDITABLE_CONTENTS_NAMES

    def dispatch(self, request, *args, **kwargs):
        user = None
        if request.user.is_authenticated:
            user = request.user

            # If user has started a wizard, redirect to the wizard page
            if Wizard.objects.filter(user=user).exists() and user.wizard.is_active:
                return redirect(reverse('wizard_view', kwargs={'step': user.wizard.step}))

            return redirect('/')

        # authenticate the user and check if the user is an admin
        (user_id, first_login) = authenticate(request.headers)

        if user_id and user_id > 0 and user_id != USER_DNE:
            user = User.objects.get(id=user_id)
            login(request, user)

            # Redirect to wizard welcome page if user is logging in for the first time
            if first_login:
                return redirect(reverse('getting_started'))

            # Redirect to the wizard page if user has started a wizard
            if Wizard.objects.filter(user=user).exists() and user.wizard.is_active:
                return redirect(reverse('wizard_view', kwargs={'step': user.wizard.step}))

            return redirect("/")

        return super().dispatch(request, *args, **kwargs)

# Logging users out
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class LogoutView(View):
    # Disable argument-differ as other arguments aren't used in the method
    # pylint: disable-next=arguments-differ
    def dispatch(self, request):
        logout(request)

        # For TESTING ONLY
        if settings.DEBUG:
            settings.CONFIG['AUTHX_USER_OVERRIDE'] = ""

        return redirect(reverse('welcome'))
