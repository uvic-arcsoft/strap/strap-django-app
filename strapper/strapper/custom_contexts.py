# pylint:
from django.conf import settings
from django.middleware.csrf import get_token
import version

from admin_users.models import GeneralConfig, HarborConfig
from normal_users.models import NotificationStatusTypes

# Get current user information
def get_user(request):
    # Decide the status of the notification bell based on the user's notifications
    # Precendence: New -> Pending -> None
    notification_bell_status = NotificationStatusTypes.NONE
    if request.user.is_authenticated and request.user.is_staff:
        if request.user.notification_set.filter(status=NotificationStatusTypes.NEW).exists():
            notification_bell_status = NotificationStatusTypes.NEW
        elif request.user.notification_set.filter(status=NotificationStatusTypes.PENDING).exists():
            notification_bell_status = NotificationStatusTypes.PENDING

    return {
        'strapper_user': request.user if request.user.is_authenticated else None,
        'strapper_is_admin': request.user.is_staff,
        'notification_bell_status': notification_bell_status
    }


# Set up configurations specified in settings.py
# W0613 is disabled as functions that populate global context in Django
# needs to take request as an argument
# pylint: disable-next=W0613
def project_setup(request):
    project_config = settings.CONFIG
    return {
        'project_title': project_config['PROJECT_TITLE'],
        'project_version': version.version,
        'project_homepage': project_config['PROJECT_HOMEPAGE'],
        'about_content': project_config['ABOUT_CONTENT'],
        'strap_domain': GeneralConfig.get_instance().domain,
        'csrf_token': get_token(request),
        'harbor_url': HarborConfig.get_instance().harbor_url,
    }
