# pylint:
"""strapper URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from . import auth

urlpatterns = [
    path('admin-django/', admin.site.urls),
    path('login/', auth.LoginView.as_view(), name='login'),
    path('logout/', auth.LogoutView.as_view(), name='logout'),
    path('admin/', include('admin_users.urls'), name='admin_users'),
    path('content/', include('django_editablecontent.urls'), name='editablecontent'),
    path('fileupload/', include('django_fileupload.urls'), name='fileupload'),
    path('help/', include('help.urls'), name='help'),
    path('wizard/', include('wizard.urls'), name='wizard'),
    path('access/', include('access.urls'), name='access'),
    path('', include('normal_users.urls'), name='normal_users'),
]

# Add URL patterns for serving media files during development
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
