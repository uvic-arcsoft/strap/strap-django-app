# pylint:
from django.conf import settings

TEST_SETTINGS = {
    'DEBUG': True,
    'TESTING': True,
    'CONFIG': {
        'AUTHX_USER_OVERRIDE': None,
        'AUTHX_HTTP_HEADER_USER': 'X-Forwarded-User',
        'AUTHX_HTTP_HEADER_ID_TOKEN': 'X-Forwarded-Id-Token',
        'PROJECT_TITLE': 'STRAP',
        'PROJECT_VERSION': 'v0.4.6',
        'PROJECT_HOMEPAGE': '',
        'ABOUT_CONTENT': '<p>This is an about content</p>'
    },
    'EMAIL_HOST_USER': 'myuser',
    'MIDDLEWARE': [
        middleware for middleware in settings.MIDDLEWARE
        if middleware != 'middleware.block_external_urls_middleware.BlockExternalUrlsMiddleware'
    ]
}
