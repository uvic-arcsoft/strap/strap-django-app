# pylint: disable=invalid-sequence-index,too-many-ancestors
#   invalid-sequence-index: sequence index is a Django model
#   IntegerField, which is an int
#   too-many-ancestors: inheriting from Django's model classes
#   and we have no control over ancestors or depth of inheritance

import os
from typing import Dict, List
import json
import time
import shlex
import re
from urllib.parse import quote_plus as urlencode, urlparse
import requests

from kubernetes import client, config as kube_config, stream

from django.db import models
from django.core import validators
from django.contrib.auth.models import User
from django.conf import settings

from admin_users.models import GeneralConfig, HarborConfig

class AuthTypes(models.TextChoices):
    UVIC = 'uvic', 'Uvic'
    UVIC_SOCIAL = 'uvic_social', 'Uvic + Social'

class DatabaseTypes(models.TextChoices):
    NONE = 'None', 'None'
    POSTGRESQL = 'PostgreSQL', 'PostgreSQL'
    MYSQL = 'MySQL', 'MySQL'

# An application configuration created by users
class Application(models.Model):
    identifier = models.CharField(max_length=16, primary_key=True)
    descriptive_name = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=500, blank=True, default="")
    authentication = models.CharField(max_length=15, choices=AuthTypes.choices,
        null=True, blank=True)
    database = models.CharField(max_length=20, choices=DatabaseTypes.choices, default=DatabaseTypes.NONE, blank=True)
    port = models.IntegerField(null=True, blank=True,
        validators = [validators.MinValueValidator(0), validators.MaxValueValidator(65536)])
    is_secure = models.BooleanField(default=False)
    published_on_strap = models.BooleanField(default=False)
    replicas = models.IntegerField(null=False, blank=False, default=1)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    last_updated = models.DateTimeField(auto_now=True)

    # Empty means all IPs are allowed. IPs are stored as a comma-separated list
    allowed_ips = models.CharField(max_length=300, null=True, blank=True)

    # Exempt from blocking in case vulnerability scan shows vulnerabilities
    # above threshold
    vuln_blocking_exempt = models.BooleanField(default=False)

    class Meta:
        db_table = 'application'
        ordering = ['-last_updated']

    def save(self, *args, **kwargs):
        """
        Ensure an app always has 2 role groups - editor and viewer
        """
        super().save(*args, **kwargs)

        # Create role groups if they don't exist
        if not self.approlegroup_set.filter(role='editor').exists():
            self.approlegroup_set.create(role='editor')
        if not self.approlegroup_set.filter(role='viewer').exists():
            self.approlegroup_set.create(role='viewer')

        # Create a quota if it doesn't exist
        if not ApplicationQuota.objects.filter(application=self).exists():
            ApplicationQuota.objects.create(application=self)

        # Create a quota request, set to None state if it doesn't exist
        if not hasattr(self, 'applicationquotarequest'):
            ApplicationQuotaRequest.objects.create(application=self)

    def delete(self, *args, **kwargs):
        """
        Delete all role groups of the app before deleting the app
        """
        for group in self.approlegroup_set.all():
            group.delete()
        super().delete(*args, **kwargs)

    def __repr__(self):
        return self.descriptive_name

    def is_deployable(self):
        """
        Determine if applition is deployable. An app is deployable if:
        - Its containers have no vulnerabilities higher than the threshold OR
          it is exempt from blocking due to vulnerabilities found
        - It has at least one container
        - It has a port, meaning it has a main container
        """
        return ((self.is_secure or self.vuln_blocking_exempt)
                and self.port and self.container_set.count() > 0)

    # Check if the app has a certain route
    def has_route(self, route):
        return self.route_set.filter(route=route).exists()

    def create_routes(self, route_list, authenticated=True):
        """
        Create routes (if not exist already) for the application
        Args:
            - route_list: list of routes (str) to create (might already exist)
            - authenticated: boolean, True if the routes are authenticated, False otherwise
        Returns: None
        """
        for route in route_list:
            if route and not self.has_route(route):
                if not route.startswith('/'):
                    route = '/' + route
                self.route_set.create(route=route, authenticated=authenticated)

    # Convert all routes (authenticated or unauthenticated) into a string
    # where routes are separated by a comma ','
    def routes_to_str(self, authenticated=True):
        return ','.join(route.route for route in list(self.route_set.filter(authenticated=authenticated)))

    # Similar to routes_to_str but only return authenticated routes
    # NOTE: Must keep this method to be used in Django templates
    def authenticated_routes_to_str(self):
        return self.routes_to_str(authenticated=True)

    # Similar to routes_to_str but only return unauthenticated routes
    # NOTE: Must keep this method to be used in Django templates
    def unauthenticated_routes_to_str(self):
        return self.routes_to_str(authenticated=False)

    # Returns a dictionary of allowed IPs separated by categories
    # Key: either a preset name or "others"
    # Value: a list of IPs
    def get_allowed_ips(self) -> Dict[str, List[str]]:
        if not self.allowed_ips:
            return {}

        allowed_ips = {}
        general_config = GeneralConfig.get_instance()
        preset_ips = general_config.get_preset_ips()
        for name in preset_ips.keys():
            if set(preset_ips[name]).issubset(self.allowed_ips.split(',')):
                allowed_ips[name] = preset_ips[name]

        # Combine all preset IPs into a list
        preset_ips_list = [ip for ips in preset_ips.values() for ip in ips]
        other_ips = [ip for ip in self.allowed_ips.split(',') if ip not in preset_ips_list]
        if len(other_ips):
            allowed_ips['others'] = other_ips
        return allowed_ips

    # Returns True if app has an advanced config set, False otherwise
    def has_advanced_config(self) -> bool:
        advanced_config = False
        if self.allowed_ips:
            advanced_config = True

        return advanced_config

    # Convert app configurations into a dictionary
    def build_configuration(self):
        tfvars = {}

        # General configurations
        tfvars['app_name'] = self.identifier
        tfvars['app_port'] = self.port if self.port else 80
        tfvars['authentication'] = self.authentication
        tfvars['authenticated_routes'] = (
            self.routes_to_str(authenticated=True).split(',')
            if len(self.route_set.filter(authenticated=True)) > 0
            else None
        )
        tfvars['unauthenticated_routes'] = (
            self.routes_to_str(authenticated=False).split(',')
            if len(self.route_set.filter(authenticated=False)) > 0
            else None
        )
        tfvars['database'] = self.database if self.database != 'None' else None
        tfvars['allowed_ips'] = self.allowed_ips.split(',') if self.allowed_ips else []
        tfvars['replicas'] = self.replicas

        # Containers
        containers = []
        for container in self.container_set.all():
            env_vars = container.get_environment_variables()
            container_dict = {
                'image': container.image,
                'tag': container.image_tag,
                'port': container.port if container.port else 80,
                'command': shlex.split(container.command) if container.command else None,
                'environment': env_vars if len(env_vars) != 0 else None,
                'shared_volume_path': "/tmp/mnt_shared_volume"
            }
            containers.append(container_dict)
        tfvars['containers'] = containers
        tfvars['image_credentials'] = (
            self.harborproject.build_image_credentials()
            if hasattr(self, 'harborproject')
            else None
        )

        # TODO: Fix this when adding Harbor
        tfvars['deploy'] = True

        return tfvars

    def detect_config_change(self):
        '''
        Check if there is any change made in the app configuration comparing to
        configurations used in the most recent deployment
        Returns: boolean
        - True: there is
        - False: there isn't
        '''
        # If there is no successful deployment for this app,
        # return True
        try:
            k8sinstance = self.k8sinstance
        except K8sInstance.DoesNotExist:
            return True

        # Check if there is any change in app container configurations
        containers_ids = []
        for k8scontainer in k8sinstance.k8scontainer_set.all():
            try:
                container = k8scontainer.container
            except Container.DoesNotExist:
                return True

            if not container or container.application != self:
                return True

            if (container.image != k8scontainer.image or container.image_tag != k8scontainer.image_tag
                or container.port != k8scontainer.port or container.command != k8scontainer.command
                or container.environment_variables != k8scontainer.environment_variables):
                return True

            containers_ids.append(container.id)

        # Check if there is any container that is not in the most recent deployment
        for container in self.container_set.all():
            if container.id not in containers_ids:
                return True

        # Check if there is any change in app configurations
        return not (
            self.descriptive_name == k8sinstance.descriptive_name
            and self.port == k8sinstance.port
            and self.authentication == k8sinstance.authentication
            and self.routes_to_str(authenticated=True) == k8sinstance.authenticated_routes_str
            and self.routes_to_str(authenticated=False) == k8sinstance.unauthenticated_routes_str
            and self.allowed_ips == k8sinstance.allowed_ips
            and self.replicas == k8sinstance.replicas
        )

    def get_js_config(self):
        '''
        Return a dictionary of app configurations that can be used by JavaScript (use JavaScript naming formats)
        '''
        config = {
            'descriptive-name': self.descriptive_name,
            'port': self.port if self.port else "None",
            'authentication': self.authentication,
            'authenticated-routes': self.routes_to_str(authenticated=True),
            'unauthenticated-routes': self.routes_to_str(authenticated=False),
            'database': self.database if self.database != 'None' else "None",
            'allowed-ips': self.allowed_ips.split(',') if self.allowed_ips else [],
            'replicas': self.replicas,
            'containers': [{
                'id': container.id,
                'image': container.image,
                'image-tag': container.image_tag if container.image_tag else "latest",
                'port': container.port if container.port else "None",
                'command': container.command if container.command else "None",
                'environment-variables': container.environment_variables if container.environment_variables else "None"
            } for container in self.container_set.all()]
        }
        return config


    # Copy editable fields from the most recent successful deployment
    def copy_last_successful_deployment(self):
        # If there is no successful deployment for this app,
        # don't do anything
        try:
            k8sinstance = self.k8sinstance
        except K8sInstance.DoesNotExist:
            return

        # Copy app configurations
        self.descriptive_name = k8sinstance.descriptive_name
        self.port = k8sinstance.port
        self.authentication = k8sinstance.authentication
        self.allowed_ips = k8sinstance.allowed_ips
        self.replicas = k8sinstance.replicas
        self.save()

        # Copy routes
        for route in self.route_set.all():
            route.delete()

        for route in k8sinstance.authenticated_routes_str.split(','):
            if route != "":
                self.route_set.create(route=route, authenticated=True)

        for route in k8sinstance.unauthenticated_routes_str.split(','):
            if route != "":
                self.route_set.create(route=route, authenticated=False)

        # Copy containers
        containers_ids = []
        for k8scontainer in k8sinstance.k8scontainer_set.all():
            try:
                container = k8scontainer.container
            except Container.DoesNotExist:
                container = self.container_set.create()

            if not container:
                container = self.container_set.create()

            container.image = k8scontainer.image
            container.image_tag = k8scontainer.image_tag
            container.port = k8scontainer.port
            container.command = k8scontainer.command
            container.environment_variables = k8scontainer.environment_variables
            container.save()

            k8scontainer.container = container
            k8scontainer.save()

            containers_ids.append(container.id)

        # Delete containers that are not in the most recent deployment
        for container in self.container_set.all():
            if container.id not in containers_ids:
                container.delete()

    def scan_containers(self):
        '''
        Scan all containers for vulnerabilities and generate a report
        '''
        # Get Harbor configs created by admins
        harbor_config = HarborConfig.get_instance()
        severity_levels = harbor_config.get_severity_levels()

        report = {}
        for container in self.container_set.all():
            report[container.id] = container.get_scan_report()

        # Disable app deployment if there is a high vulnerability
        app_secure = True
        for vulnerabilities in report.values():
            # Make app undeployable if got an error when scanning a container
            if 'error' in vulnerabilities:
                app_secure = False
                continue

            for vulnerability in vulnerabilities:
                # Make app undeployable if there is a high or above vulnerability
                if severity_levels[vulnerability['severity']] >= severity_levels[harbor_config.severity_threshold]:
                    app_secure = False
                    break

        if app_secure != self.is_secure:
            self.is_secure = app_secure
            self.save()

        return report

    # Return all distinct group names that an application has as a list
    def group_names(self):
        return list(self.group_set.values_list('name', flat=True).distinct('name'))

    # Return all distinct group names that an application has as a JSON formated list
    # This data can be used by JavaScript
    def group_names_json(self):
        return json.dumps(self.group_names())

    # Return URL for app if deployed successfully
    def get_url(self):
        return f"https://{self.identifier}.{GeneralConfig.get_instance().domain}"

    # Create (if not exist) and get the directory path to store Terraform logs
    def get_terraform_log_path(self, type="app"):
        path = ""
        if type == "app":
            path = f"{settings.TERRAFORM_DIR}/{self.identifier}"
        elif type == "harbor":
            path = f"{settings.TERRAFORM_DIR}/{self.identifier}/harbor"

        # Create the directory if it doesn't exist
        os.makedirs(path, exist_ok=True)
        return path

    # Execute a command in a container
    def exec_command(self, command, container_id):
        # Configure API key authorization
        kube_config.load_kube_config(config_file=f"{os.environ.get('KUBE_CONFIG_DIR', '~/.kube')}/config")

        # Create a new CoreV1Api instance
        api_instance = client.CoreV1Api()

        # Get namespace
        namespace = f"strap-{self.identifier}"

        # Get the container name
        container_index = -1
        for i, container in enumerate(self.container_set.all()):
            if container.id == container_id:
                container_index = i
                break

        if container_index == -1:
            raise RuntimeError(f"Container {container_id} not found in application {self.identifier}")

        container_name = f"{self.identifier}-{container_index}"

        # Calling exec and waiting for response
        try:
            pod_name = api_instance.list_namespaced_pod(namespace=namespace).items[0].metadata.name
        except IndexError as e:
            raise RuntimeError(f"Pod not found in namespace {namespace}") from e
        res = stream.stream(api_instance.connect_get_namespaced_pod_exec,
                            pod_name,
                            namespace,
                            command=['/bin/sh', '-c', command],
                            stderr=True,
                            stdin=False,
                            stdout=True,
                            tty=False,
                            container=container_name)

        output = "\n".join([line for line in res.split("\n") if line != "" and "log.go" not in line])
        return output

    # Get the role of an user in the app
    def get_user_role(self, user: User) -> str:
        """
        Args: user - django.contrib.auth.models.User object
        Returns: str - role of the user in the app ('owner', 'editor', 'viewer', 'none')
        """
        # Check if user is the owner
        if user.username == self.owner.username:
            return 'owner'

        # Check if user is an editor
        editor_group = self.approlegroup_set.filter(role='editor').first()
        if user in editor_group.users.all():
            return 'editor'

        # Check if user is a viewer
        viewer_group = self.approlegroup_set.filter(role='viewer').first()
        if user in viewer_group.users.all():
            return 'viewer'

        return 'none'

    # Check if k8s resources are within the app's quota
    def check_quota(self, replicas=1):
        """
        Args:
            - replicas (optional): int - number of replicas to check
        Returns: boolean
            - True: if no resource exceeds the quota
            - False: if any resource exceeds the quota
        """
        quota = self.applicationquota
        return replicas <= quota.replicas

# Routes of an application
class Route(models.Model):
    route = models.CharField(max_length=100, null=False, blank=False)
    authenticated = models.BooleanField(default=False)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, null=False, blank=False)

    class Meta:
        db_table='route'
        ordering = ['route']

    # Disable invalid-repr-returned as the return value is a CharField in Django,
    # which is a string
    # pylint: disable-next=invalid-repr-returned
    def __repr__(self):
        return self.route

# Containers of an application. An app is deployed as a pod in k8s, which can support multiple containers
class Container(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    image = models.CharField(max_length=100, null=False, blank=False)
    image_tag = models.CharField(max_length=50, default="latest", blank=True)
    port = models.IntegerField(null=True, blank=True,
        validators = [validators.MinValueValidator(0), validators.MaxValueValidator(65536)])
    command = models.CharField(max_length=200, null=True, blank=True)
    environment_variables = models.CharField(max_length=7000, null=True, blank=True)
    is_main = models.BooleanField(default=False)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'container'
        ordering = ['image']

    def __repr__(self):
        return self.display_image() + ':' + self.image_tag

    def __str__(self) -> str:
        return self.display_image() + ':' + self.image_tag

    def save(self, *args, **kwargs):
        """
        Ensure that there is only 1 main container in an application
        and application's port is the same as the main container's port
        """
        if self.is_main:
            self.application.container_set.exclude(id=self.id).update(is_main=False)
            self.application.port = self.port
            self.application.save()
        super().save(*args, **kwargs)

    def display_image(self) -> str:
        """
        Display image name without the registry information (URL, project)
        """
        registry_hostname = urlparse(HarborConfig.get_instance().harbor_url).netloc
        return self.image.replace(f"{registry_hostname}/{self.application.identifier}/", "")

    # Convert environment variables into a dictionary
    # Add quotation marks to the values. Note that the values don't have quotation marks by default
    # For example, "key==value" becomes {'key': '\"value\"'}
    def get_environment_variables(self):
        if not self.environment_variables:
            return {}

        return {
            str.split('==')[0]: '"{}"'.format(str.split('==')[1].replace("\r", ""))
            for str in self.environment_variables.split('\n') if str
        }

    # Convert environment variables into a dictionary where the values don't have quotation marks
    # For example, "key==value" becomes {'key': 'value'}
    def get_environment_variables_without_quote(self):
        if not self.environment_variables:
            return {}

        return {
            str.split('==')[0]: str.split('==')[1].replace("\r", "")
            for str in self.environment_variables.split('\n') if str
        }

    # Convert environment variables into a string
    def environment_variables_to_str(self):
        if not self.environment_variables:
            return ''
        return self.environment_variables.replace('\n', ', ')

    # Scan the container for vulnerabilities and return a report
    def scan(self):

        # Send an empty list of vulnerabilities in testing environment
        if settings.TESTING:
            return []

        # Build URL and authorization headers for the API request
        harbor_config = HarborConfig.get_instance()
        harbor_project = self.application.harborproject

        # API URL to start a scan on the container
        scan_url = (f"{harbor_config.harbor_url}/api/v2.0/projects/{harbor_project.project_name}/repositories"
                    f"/{self.image.split('/')[-1]}/artifacts/{self.image_tag}/scan")

        try:
            # Start a scan on the container
            headers = harbor_config.build_authz_headers()
            response = requests.post(scan_url, headers=headers, timeout=20, json={
                "scan_type": "vulnerability"
            })
            response.raise_for_status()

            # Wait for the scan report by long polling
            poll_timeout = 0
            while "error" in self.get_scan_report() and poll_timeout < 30:
                print("Waiting for scan report")
                time.sleep(3)
                poll_timeout += 3

            # Container scanning gets timed out
            if poll_timeout >= 30:
                print(f"Container scanning for {self.image}:{self.image_tag} timed out")
                return {'error': f"Container scanning for {self.image}:{self.image_tag} timed out"}

            result = self.get_scan_report()

            return result

        # If there is an error, return a dictionary with an error message
        except requests.RequestException as e:
            return {'error': f"Error scanning container {self.image}:{self.image_tag}: {e}"}

    # Retrieve scanning report of the container
    def get_scan_report(self):
        # Build URL and authorization headers for the API request
        harbor_config = HarborConfig.get_instance()
        harbor_project = self.application.harborproject

        # API URL to get vulnerabilities log of the container
        log_url = (f"{harbor_config.harbor_url}/api/v2.0/projects/{harbor_project.project_name}/repositories"
                    f"/{self.image.split('/')[-1]}/artifacts/{self.image_tag}/additions/vulnerabilities")
        headers = harbor_config.build_authz_headers()

        VULNERABILITY_REPORT_MIME_TYPE = "application/vnd.security.vulnerability.report; version=1.1"
        try:
            # Get the scan report of the container by reading the vulnerabilities log
            response = requests.get(log_url, headers=headers, timeout=20)
            response.raise_for_status()
            if response.status_code == 200:
                # Wait for the scan report by long polling
                timeout = 30
                while (VULNERABILITY_REPORT_MIME_TYPE not in response.json()
                        and timeout > 0):
                    print("Waiting for scan report")
                    time.sleep(3)
                    response = requests.get(log_url, headers=headers, timeout=20)
                    response.raise_for_status()
                    timeout -= 3

                # Scan result is received
                if VULNERABILITY_REPORT_MIME_TYPE in response.json():
                    return (response.json()
                        [VULNERABILITY_REPORT_MIME_TYPE]
                        ["vulnerabilities"])

                # Container scanning gets timed out
                return {'error': f"Container scanning for {self.image}:{self.image_tag} timed out"}

            # Receive an error from the API
            return {
                'error': (f"Error fetching vulnerabilities of container {self.image}:{self.image_tag}. "
                          f"Return code: {response.status_code}")
            }

        # If there is an error, return a dictionary with an error message
        except requests.RequestException as e:
            return {'error': f"Error fetching vulnerabilities of container {self.image}:{self.image_tag}: {e}"}

# Store K8s resources quota for an application
class ApplicationQuota(models.Model):
    application = models.OneToOneField(Application, on_delete=models.CASCADE)
    replicas = models.IntegerField(null=False, blank=False, default=1)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'applicationquota'
        ordering = ['application']

    def __repr__(self):
        return f"{self.application.descriptive_name} Quota"

    def __str__(self) -> str:
        return f"{self.application.descriptive_name} Quota"

class RequestStatusTypes(models.TextChoices):
    NONE = 'None', 'None'               # Not submitted
    PENDING = 'Pending', 'Pending'      # Submitted but waiting for review
    APPROVED = 'Approved', 'Approved'   # Approved by admins
    REJECTED = 'Rejected', 'Rejected'   # Rejected by admins

# K8s resources quota request for an application
class ApplicationQuotaRequest(models.Model):
    application = models.OneToOneField(Application, on_delete=models.CASCADE)
    replicas = models.IntegerField(null=False, blank=False, default=1)
    justification = models.CharField(max_length=500, null=True, blank=True)
    submitted_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    status = models.CharField(max_length=15, choices=RequestStatusTypes.choices, default=RequestStatusTypes.NONE)
    admin_reply = models.CharField(max_length=500, null=True, blank=True)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'applicationquotarequest'
        ordering = ['application']

    def __repr__(self):
        return f"{self.application.descriptive_name} Quota Request"

    def __str__(self) -> str:
        return f"{self.application.descriptive_name} Quota Request"

    def get_submitter_name(self):
        """
        Return submiter's name if exists. Otherwise, returns username
        """
        return self.submitted_by.get_full_name() if self.submitted_by.get_full_name() else self.submitted_by.username

# Harbor project for an application
class HarborProject(models.Model):
    HARBOR_URL_TMPL = "{url}/api/v2.0/projects/{project}/repositories/{repo}/artifacts"

    application = models.OneToOneField(Application, on_delete=models.CASCADE)
    project_name = models.CharField(max_length=100, null=False, blank=False)
    username = models.CharField(max_length=100, null=False, blank=False)
    password = models.CharField(max_length=100, null=False, blank=False)
    images = models.CharField(max_length=2000, null=True, blank=True)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'harborproject'
        ordering = ['project_name']

    def __repr__(self):
        # pylint: disable=invalid-repr-returned
        # return value is CharField, which is a string
        return self.project_name

    # NOTE: Disable invalid-str-returned as the return value is a CharField in Django,
    # which is a string
    # pylint: disable=invalid-str-returned
    def __str__(self) -> str:
        return self.project_name
    # pylint: enable=invalid-str-returned

    def to_json(self):
        """Return a JSON representatation of the project."""
        return {
            'project_name': self.project_name,
            'username': self.username,
            'password': self.password,
            'images': self.images.split(',') if self.images else []
        }

    # Get images in the Harbor project repository as a list
    def get_images_from_harbor(self):
        # Build URL and authorization headers for the API request
        harbor_config = HarborConfig.get_instance()
        api_url = f"{harbor_config.harbor_url}/api/v2.0/projects/{self.project_name}/repositories"
        headers = harbor_config.build_authz_headers()

        try:
            # Get all repositories in the Harbor project
            response = requests.get(api_url, headers=headers, timeout=20)
            response.raise_for_status()  # Raises an HTTPError if the response was an error
            repositories = response.json()

            # Get all images in the Harbor project
            images = []
            for repo in repositories:

                # Repo name: /something1/something2/something3 -> something2/something3
                repo_name_parts = repo['name'].split('/')

                # Have to encode the repo name twice to handle "/" in the name
                repo_name = urlencode(urlencode("/".join(repo_name_parts[1:])))
                repo_url = self.HARBOR_URL_TMPL.format(
                    url=harbor_config.harbor_url,
                    project=self.project_name,
                    repo=repo_name
                )
                response = requests.get(repo_url, headers=headers, timeout=20)
                response.raise_for_status()
                artifacts = response.json()
                image_registry = urlparse(harbor_config.harbor_url).netloc
                for artifact in artifacts:
                    if artifact['tags']:
                        images.append(f"{image_registry}/{repo['name']}:{artifact['tags'][0]['name']}")
                    else:
                        images.append(f"{image_registry}/{repo['name']}")

            return {"images": images}

        # If there is an error, return an empty list
        except requests.RequestException as e:
            print(f"Error fetching Harbor project {self.project_name} repositories: {e}")
            return {"errors": e, "images": []}

    # Build credentials to pull images from private Harbor project
    def build_image_credentials(self):
        return {
            'registry': HarborConfig.get_instance().harbor_url,
            'username': self.username,
            'password': self.password
        }

# Group of an application contains users that have access
# to a certain level of authorization of that application
class Group(models.Model):
    application = models.ManyToManyField(Application, blank=True)
    name = models.CharField(max_length=100, null=False, blank=False)
    is_everyone = models.BooleanField(default=False)
    users = models.CharField(max_length=2000, null=True, blank=True)
    created_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        db_table = 'group'
        ordering = ['name']

    def __repr__(self):
        return f"Group {self.name}"

    def get_users(self):
        """Convert users string field into a list of user names"""
        if not self.users:
            return []
        return [user for user in self.users.split(',') if user != '']

    def check_user(self, user):
        """
        Args: user - str representing an user email
        Returns:
            - True if user is in this group
            - False otherwise
        """
        if self.is_everyone:
            return True
        if not self.users:
            return False
        # Disable unsupported-membership-test as self.users is a string,
        # which supports 'in' operation
        # pylint: disable-next=unsupported-membership-test
        return user in self.users.split(',')

    def application_names_str(self):
        return ', '.join(list(self.application.values_list('descriptive_name', flat=True)))

# A Kubernetes instance of an application
# This records application editable configurations when an app was last deployed
class K8sInstance(models.Model):
    AUTHENTICATION_OPTIONS = [
        (None, 'None'),
        ('uvic', 'Uvic'),
        ('uvic_social', 'Uvic + Social')
    ]

    application = models.OneToOneField(Application, on_delete=models.CASCADE, primary_key=True)
    descriptive_name = models.CharField(max_length=100, null=False, blank=False)
    port = models.IntegerField(null=True, blank=True,
        validators = [validators.MinValueValidator(0), validators.MaxValueValidator(65536)])
    authentication = models.CharField(max_length=15, choices=AUTHENTICATION_OPTIONS,
        null=True, blank=True, default=None)
    authenticated_routes_str = models.CharField(max_length=500, null=True, blank=True)
    unauthenticated_routes_str = models.CharField(max_length=500, null=True, blank=True)
    # Empty means all IPs are allowed. IPs are stored as a comma-separated list
    allowed_ips = models.CharField(max_length=300, null=True, blank=True)
    replicas = models.IntegerField(null=False, blank=False, default=1)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'k8sappinstance'
        ordering = ['-last_updated']

    def __repr__(self):
        return self.application.descriptive_name


# A deployed config of a container in a Kubernetes instance
# This records a container editable configurations when an app was last deployed
class K8sContainer(models.Model):
    k8sinstance = models.ForeignKey(K8sInstance, on_delete=models.CASCADE, null=False, blank=False)
    container = models.OneToOneField(Container, on_delete=models.SET_NULL, null=True, blank=True)
    image = models.CharField(max_length=100, null=False, blank=False)
    image_tag = models.CharField(max_length=50, default="latest", blank=True)
    port = models.IntegerField(null=True, blank=True,
        validators = [validators.MinValueValidator(0), validators.MaxValueValidator(65536)])
    command = models.CharField(max_length=200, null=True, blank=True)
    environment_variables = models.CharField(max_length=7000, null=True, blank=True)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'k8scontainer'
        ordering = ['image']

    def __repr__(self):
        return self.image + ':' + self.image_tag

    def __str__(self) -> str:
        return self.image + ':' + self.image_tag


# This model represents the deployment state of an application. It contains all the necessary information to
# deploy an application to Kubernetes. Note that this is not the deployment itself.
class Deployment(models.Model):
    application = models.OneToOneField(Application, on_delete=models.CASCADE, primary_key=True)
    log_file = models.CharField(max_length=100, default='')
    state = models.IntegerField(
        validators=[validators.MinValueValidator(0), validators.MaxValueValidator(6)],
        default=0
    )
    status = models.IntegerField(
        validators=[validators.MinValueValidator(0), validators.MaxValueValidator(11)],
        default=0
    )
    process_ids = models.CharField(max_length=15, null=True, blank=True,
    validators=[validators.int_list_validator(sep=',', allow_negative=False)])
    # Use this field to record the current state of a deployment before it gets deployed
    # This field allows 'Cancel' to set the state back to previous state
    prev_state = models.IntegerField(
        validators=[validators.MinValueValidator(0), validators.MaxValueValidator(6)],
        default=0
    )
    databases = models.CharField(max_length=200, null=True, blank=True)
    db_owner = models.CharField(max_length=200, null=True, blank=True)
    db_password = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        db_table = 'deployment'
        ordering = ['application']

    # Get state in string form
    def get_state(self):
        states = ['None', 'started', 'deployed', 'stopped', 'failed', 'deployed + started', 'deployed + failed']
        return states[self.state]

    # Get status of the deployment
    def get_status(self):
        status = ['None', 'deploying', 'succeeded', 'failed', 'canceling', 'canceled',
        'stopping', 'stopped', 'destroying', 'destroyed', 'updating', 'reverting']
        return status[self.status]

    # Return True if the deployment can be stopped/destroyed/reverted
    def is_stoppable(self):
        return self.state != 0


# Type of a notification
class NotificationTypes(models.TextChoices):
    ApplicationQuotaRequest = 'ApplicationQuotaRequest', 'ApplicationQuotaRequest'

# Status of a notification
class NotificationStatusTypes(models.TextChoices):
    NONE = 'None', 'None'
    PENDING = 'Pending', 'Pending'
    NEW = 'New', 'New'

# Keep track of user notifications
class Notification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    html = models.CharField(max_length=500, null=False, blank=False)
    url = models.CharField(max_length=500, null=True, blank=True)
    type = models.CharField(
        max_length=40,
        choices=NotificationTypes.choices,
        null=False, blank=False, default=NotificationTypes.ApplicationQuotaRequest)
    related_model_pk = models.IntegerField(null=True, blank=True)
    status = models.CharField(
        max_length=15,
        choices=NotificationStatusTypes.choices,
        null=False, blank=False, default=NotificationStatusTypes.NEW)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'notification'
        ordering = ['-created_at']

    def __repr__(self):
        name = self.user.get_full_name() if self.user.get_full_name() else self.user.username
        message = re.sub(r'<.*?>', '', self.html)[:60]
        return f"{name} at {self.created_at}: {message}"

    def __str__(self) -> str:
        return self.__repr__()
