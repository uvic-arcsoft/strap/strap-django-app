# pylint:

import environ

# pylint: disable=unused-import
# NOTE: Import these classes to be imported in urls.py
from .view_classes.dashboard import Dashboard
from .view_classes.application import (ApplicationView, ApplicationDeleteView, ApplicationAPIView,
                                       ApplicationLogView, ApplicationTerminalView)
from .view_classes.k8s import K8sResourcesView, K8sResourcesRequestView
from .view_classes.container import ContainerAPIView, ContainerScanReportView, ContainerScanView
from .view_classes.harbor import HarborAPIView
from .view_classes.group import GroupView, GroupAPIView
from .view_classes.deployment import DeploymentView, DeploymentLogView, StopDeploymentView
from .view_classes.landings import WelcomeView, AboutTeam, DefaultStrapView
from .view_classes.helpers import deployment_app_info_view
from .view_classes.notification import NotificationView
# pylint: enable=unused-import

# Enable the line below if need to specify the path of .env file
environ.Env.read_env()
