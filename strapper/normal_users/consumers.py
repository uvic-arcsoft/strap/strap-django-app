# pylint: disable=arguments-renamed,arguments-differ
# Disable arguments-differ as the missing arguments
# aren't used in the method
import json
from threading import Thread
import uuid

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

from .helper_functions import read_app_log

class DeployConsumer(WebsocketConsumer):
    def connect(self):
        self.identifier = self.scope["url_route"]["kwargs"]["identifier"]
        self.app_group_name = f"deploy_{self.identifier}"

        # Join app group
        async_to_sync(self.channel_layer.group_add)(
            self.app_group_name, self.channel_name
        )

        # Accept connection from Javascript client
        self.accept()

    # Have to keep argument 'close_code' as it is how the disconnect() method of
    # class WebSocketConsumer is defined
    # pylint: disable-next=unused-argument
    def disconnect(self, close_code):
        # Leave app group
        async_to_sync(self.channel_layer.group_discard)(
            self.app_group_name, self.channel_name
        )

    # Listen for messages from consumers (including itself)
    # Messages are received only from ApplicationDeployment and StopApplicationDeploymentView
    # and only when there is a change in the log file of the application
    # This function is currently not used
    def receive(self, log_file_content):
        log_file_content_json = json.loads(log_file_content)
        print(f'log file content: {log_file_content_json}')
        content = log_file_content_json["content"]

        # Send content to app group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name, {"type": "log_file_content", "content": content}
        )

    # Listen for log_file_content events
    # Messages are received only from ApplicationDeployment and StopApplicationDeploymentView
    # and only when there is a change in the log file of the application
    def log_file_content(self, event):
        content = event["content"]

        # Send message to WebSocket
        self.send(text_data=json.dumps({"content": content}))


class AppLogConsumer(WebsocketConsumer):
    def connect(self):
        self.identifier = self.scope["url_route"]["kwargs"]["identifier"]
        self.container_index = self.scope["url_route"]["kwargs"]["container_index"]
        self.group_name = f"app_log_{self.container_index}_{uuid.uuid4()}"

        # Join app group
        async_to_sync(self.channel_layer.group_add)(
            self.group_name, self.channel_name
        )

        # Accept connection from Javascript client
        self.accept()

        # Spawn a thread that send back log messages in real time to this channel
        self.stop_thread = False
        self.read_app_log_thread = Thread(
            target=read_app_log,
            args=(self.identifier, self.container_index, self.group_name, lambda : self.stop_thread))
        self.read_app_log_thread.start()

    # Have to keep argument 'close_code' as it is how the disconnect() method of
    # class WebSocketConsumer is defined
    # pylint: disable-next=unused-argument
    def disconnect(self, close_code):
        # Stop the app log update thread
        self.stop_thread = True
        # Leave app group
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name, self.channel_name
        )

    # Listen for app_log_updated events
    def app_log_updated(self, event):
        content = event["content"]

        # Send message to WebSocket
        self.send(text_data=json.dumps({"content": content}))
