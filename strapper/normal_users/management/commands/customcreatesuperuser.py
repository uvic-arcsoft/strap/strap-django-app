# pylint:
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

class Command(BaseCommand):
    """python manage.py createsuperuser <username> <password>
    Create a super user with an username and a password
    """

    help = 'Create a super user with an username and a password'

    def add_arguments(self, parser):
        parser.add_argument('username', nargs=1, type=str)
        parser.add_argument('password', nargs=1, type=str)

    # Disable unused-argument as this is how the handle method defined in
    # the BaseCommand class
    # pylint: disable=unused-argument
    def handle(self, *args, **options):
        username = options['username'][0]
        self.stdout.write(username)
        try:
            User.objects.get(username=username)
            self.stdout.write('Username already exists')
        except User.DoesNotExist:
            user = User.objects.create(
                username=username,
                is_staff=True,
                is_superuser=True
            )
            user.set_password(options['password'][0])
            user.save()
            self.stdout.write(f'Created superuser {username} successfully!')
