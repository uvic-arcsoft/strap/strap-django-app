# pylint:
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.conf import settings

class Command(BaseCommand):
    """python manage.py createusers
    This command reads users specified in CONFIG['AUTHZ_USERS'] and CONFIG['AUTHZ_ADMINS']
    and create normal users and admin users accordingly. Normal users have default password set to
    'testinguser'. Admin users have default password set to 'testingadmin'. To change password for these users, run
    'python manage.py changepassword [<username>]'"""

    # Command's actions
    # Disable unused-argument as this is Django standard way to use the BaseCommand class. Removing
    # will result in an unexpected argument error
    # pylint: disable=W0613
    def handle(self, *args, **options):
        # Create a normal user for each username unless that username already exists
        for username in settings.CONFIG['AUTHZ_USERS']:
            try:
                User.objects.get(username=username)
                self.stdout.write(self.style.ERROR(f"An user with username {username} already exists"))
            except User.DoesNotExist:
                User.objects.create_user(username=username, password='testinguser', is_staff=False)
                self.stdout.write(self.style.SUCCESS(f"Successfully created a new user with username {username}"))

        # Create an admin user for each username unless that username already exists
        for username in settings.CONFIG['AUTHZ_ADMINS']:
            try:
                User.objects.get(username=username)
                self.stdout.write(self.style.ERROR(f"An user with username {username} already exists"))
            except User.DoesNotExist:
                User.objects.create_user(username=username, password='testingadmin', is_staff=True)
                self.stdout.write(self.style.SUCCESS(f"Successfully created a new user with username {username}"))
