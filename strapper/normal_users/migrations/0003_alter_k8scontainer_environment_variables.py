# Generated by Django 4.1.1 on 2024-02-13 01:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('normal_users', '0002_alter_container_environment_variables'),
    ]

    operations = [
        migrations.AlterField(
            model_name='k8scontainer',
            name='environment_variables',
            field=models.CharField(blank=True, max_length=7000, null=True),
        ),
    ]
