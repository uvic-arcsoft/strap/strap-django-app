# Generated by Django 5.0.8 on 2024-12-11 23:23

import django.db.models.deletion
from django.db import migrations, models


def create_application_quotas(apps, schema_editor):
    ApplicationQuota = apps.get_model('normal_users', 'ApplicationQuota')
    Application = apps.get_model('normal_users', 'Application')
    for app in Application.objects.all():
        ApplicationQuota.objects.create(application=app)

class Migration(migrations.Migration):

    dependencies = [
        ('normal_users', '0018_k8sinstance_replicas'),
    ]

    operations = [
        migrations.CreateModel(
            name='ApplicationQuota',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('replicas', models.IntegerField(default=1)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('application', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='normal_users.application')),
            ],
            options={
                'db_table': 'applicationquota',
                'ordering': ['application'],
            },
        ),
        migrations.RunPython(create_application_quotas),
    ]
