# Generated by Django 4.1.1 on 2024-02-06 23:33

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import re


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Application',
            fields=[
                ('identifier', models.CharField(max_length=16, primary_key=True, serialize=False)),
                ('descriptive_name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=500)),
                ('authentication', models.CharField(blank=True, choices=[(None, 'None'), ('uvic', 'Uvic'), ('uvic_social', 'Uvic + Social')], default=None, max_length=15, null=True)),
                ('database', models.CharField(blank=True, choices=[('None', 'None'), ('PostgresSQL', 'PostgresSQL'), ('MySQL', 'MySQL')], default='PostgresSQL', max_length=20)),
                ('port', models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(65536)])),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'strap_application',
                'ordering': ['-last_updated'],
            },
        ),
        migrations.CreateModel(
            name='Container',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.CharField(max_length=100)),
                ('image_tag', models.CharField(blank=True, default='latest', max_length=50)),
                ('port', models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(65536)])),
                ('command', models.CharField(blank=True, max_length=200, null=True)),
                ('environment_variables', models.CharField(blank=True, max_length=5000, null=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('application', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='normal_users.application')),
            ],
            options={
                'db_table': 'strap_container',
                'ordering': ['image'],
            },
        ),
        migrations.CreateModel(
            name='Deployment',
            fields=[
                ('application', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='normal_users.application')),
                ('log_file', models.CharField(default='', max_length=100)),
                ('state', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(6)])),
                ('status', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(11)])),
                ('process_ids', models.CharField(blank=True, max_length=15, null=True, validators=[django.core.validators.RegexValidator(re.compile('^\\d+(?:,\\d+)*\\Z'), code='invalid', message=None)])),
                ('prev_state', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(6)])),
                ('databases', models.CharField(blank=True, max_length=200, null=True)),
                ('db_owner', models.CharField(blank=True, max_length=200, null=True)),
                ('db_password', models.CharField(blank=True, max_length=200, null=True)),
            ],
            options={
                'db_table': 'strap_deployment',
                'ordering': ['application'],
            },
        ),
        migrations.CreateModel(
            name='K8sInstance',
            fields=[
                ('application', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='normal_users.application')),
                ('descriptive_name', models.CharField(max_length=100)),
                ('port', models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(65536)])),
                ('authentication', models.CharField(blank=True, choices=[(None, 'None'), ('uvic', 'Uvic'), ('uvic_social', 'Uvic + Social')], default=None, max_length=15, null=True)),
                ('authenticated_routes_str', models.CharField(blank=True, max_length=500, null=True)),
                ('unauthenticated_routes_str', models.CharField(blank=True, max_length=500, null=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'strap_k8sappinstance',
                'ordering': ['-last_updated'],
            },
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('route', models.CharField(max_length=100)),
                ('authenticated', models.BooleanField(default=False)),
                ('application', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='normal_users.application')),
            ],
            options={
                'db_table': 'strap_route',
                'ordering': ['route'],
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('is_everyone', models.BooleanField(default=False)),
                ('users', models.CharField(blank=True, max_length=2000, null=True)),
                ('application', models.ManyToManyField(blank=True, to='normal_users.application')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'strap_group',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='K8sContainer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.CharField(max_length=100)),
                ('image_tag', models.CharField(blank=True, default='latest', max_length=50)),
                ('port', models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(65536)])),
                ('command', models.CharField(blank=True, max_length=200, null=True)),
                ('environment_variables', models.CharField(blank=True, max_length=5000, null=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('container', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='normal_users.container')),
                ('k8sinstance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='normal_users.k8sinstance')),
            ],
            options={
                'db_table': 'strap_k8scontainer',
                'ordering': ['image'],
            },
        ),
    ]
