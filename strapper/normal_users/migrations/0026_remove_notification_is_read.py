# Generated by Django 5.0.8 on 2025-01-13 19:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('normal_users', '0025_notification_related_model_pk_notification_status_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notification',
            name='is_read',
        ),
    ]
