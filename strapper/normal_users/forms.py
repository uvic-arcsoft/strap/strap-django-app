# pylint:
from django import forms
from django.core.validators import RegexValidator
from .models import Application, Container, Group

# Validate app identifiers
identifier_validator = RegexValidator(
    regex='^[a-z][a-z0-9-]*[a-z0-9]$',
    message=('Identifier must contain only alphanumeric characters and hyphens. '
             'It must start with an alphabet and end with an alphanumeric character.'),
    code='invalid_identifier'
)

# Form to process application configurations
class ApplicationForm(forms.ModelForm):
    # identifer is not required as form submitted when editing an app will have this field disabled
    identifier = forms.CharField(
        min_length=3,
        max_length=16,
        required=False,
        validators=[identifier_validator])
    authenticated_routes = forms.CharField(max_length=500, required=False)
    unauthenticated_routes = forms.CharField(max_length=500, required=False)

    class Meta:
        model = Application
        fields = ['descriptive_name', 'description',
                  'authentication', 'database', 'published_on_strap']

# Form to process k8s resources updates
class K8sResourcesForm(forms.Form):
    replicas = forms.IntegerField(required=True, min_value=1, max_value=10)

# Form to process requests for more k8s resources quota
class K8sResourcesRequestForm(forms.Form):
    replicas = forms.IntegerField(required=True, min_value=1, max_value=10)
    justification = forms.CharField(max_length=500, required=False)

# Form to process container configurations
class ContainerForm(forms.ModelForm):
    is_main = forms.BooleanField(required=False)

    class Meta:
        model = Container
        exclude = ['application', 'last_updated']

# Form to process groups for applications
class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'
        exclude = ['created_by']
