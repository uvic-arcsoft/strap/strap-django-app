# pylint:
import os
import time
from typing import Dict, List
import environ

from ansi2html import Ansi2HTMLConverter

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from kubernetes import client, config

# Set up Django environment since this script is outside of Django context
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'strapper.settings')
django.setup()
# pylint: disable=wrong-import-position
# NOTE: These imports must be after django.setup() to work
from django.contrib.auth.models import User
from .models import Application
# pylint: enable=wrong-import-position

# Enable the line below if need to specify the path of .env file
environ.Env.read_env()

# This is a comment
# pylint: disable-next=pointless-string-statement
'''
Args:
    str: identifier
    Deployment model instance: deployment
Returns: None

Checks the log file for the deployment every 1 second. If there is change, sends a message
as the new content to the channel layer app group. This will update the deployment log
view on the front end for that app. This function finishes if there is
no change in the log file for 20 seconds.
'''
def poll_log_file(identifier, deployment):
    # Get the channel layer app group based on passed in identifier
    layer = get_channel_layer()
    app_group_name = f"deploy_{identifier}"
    # Add this thread channel layer to app channel layer group
    async_to_sync(layer.group_add)(
        app_group_name, f'poll_log_file_{identifier}'
    )

    # Get the log file of the deployment
    log_file = deployment.log_file

    # Construct converter to convert ANSI to HTML
    conv = Ansi2HTMLConverter()

    i = 0
    cached_stamp = os.stat(log_file).st_mtime
    # Time out updating log after 20s without any changes to the log file
    # Polling for output file content changes every 1 second
    while i < 20:
        stamp = os.stat(log_file).st_mtime
        if stamp != cached_stamp:
            cached_stamp = stamp
            # Send current log output to the front end
            with open(log_file, 'r', encoding='utf-8') as log_file_stream:
                output = log_file_stream.read()
            async_to_sync(layer.group_send)(app_group_name, {
                'type': 'log_file_content',
                'content': conv.convert(output)
            })
            # Reset time-out timer every time the file changes
            i = 0
        time.sleep(1)
        i += 1

    # Discard this thread from app channel layer group
    async_to_sync(layer.group_discard)(
        app_group_name, f'poll_log_file_{identifier}'
    )

# This function reads an app pod log and send the log to an app log channel layer
def read_app_log(identifier, container_index, group_name, stop_fun):
    layer = get_channel_layer()

    # Find the pod
    config.load_kube_config(config_file=f"{os.environ.get('KUBE_CONFIG_DIR', '~/.kube')}/config")
    api_instance = client.CoreV1Api()
    namespace = f"strap-{identifier}"
    try:
        pod_name = api_instance.list_namespaced_pod(namespace=namespace).items[0].metadata.name
    except IndexError:
        async_to_sync(layer.group_send)(group_name, {
            'type': 'app_log_updated',
            'content': "App pod not found"
        })
        return

    # Get details about the pod
    pod = api_instance.read_namespaced_pod(name=pod_name, namespace=namespace)
    container = pod.spec.containers[int(container_index)]

    # Read the log
    log_stream = api_instance.read_namespaced_pod_log(
        name=pod_name,
        namespace=namespace,
        container=container.name,
        follow=True, _preload_content=False).stream()
    for line in log_stream:
        if stop_fun():
            break
        # Send the log
        async_to_sync(layer.group_send)(group_name, {
            'type': 'app_log_updated',
            'content': line.decode('utf-8')
        })
    log_stream.close()
    return

def get_apps_for_user_by_role(user: User, role: str = "all") -> Dict[str, List[Application]]:
    """
    Get all applications that an user has a certain role in sorted by their last updated time
    Args:
        user: User instance
        role: 
            - 'all': get all applications that user has any access to
            - 'owner': get all applications that user is the owner of
            - 'editor': get all applications that user is an editor of
            - 'viewer': get all applications that user is a viewer of
    Returns:
        Dictionary with keys as role and values as list of applications
    """
    if role not in ['all', 'owner', 'editor', 'viewer']:
        raise ValueError("role must be one of 'all', 'owner', 'editor', 'viewer'")

    apps = {}
    if role in ['all', 'owner']:
        # Get all applications that the current user owns
        # The applications are already sorted by their last updated time
        owned_applications = Application.objects.filter(owner=user)
        apps['owned_applications'] = owned_applications

    if role in ['all', 'editor']:
        # Get all applications that the current user is an editor of
        user_editor_groups = user.approlegroup_set.filter(role='editor')
        editor_applications = [group.app for group in user_editor_groups]
        # Sort the applications by their last updated time
        apps['editor_applications'] = sorted(editor_applications, key=lambda x: x.last_updated, reverse=True)

    if role in ['all', 'viewer']:
        # Get all applications that the current user is a viewer of
        user_viewer_groups = user.approlegroup_set.filter(role='viewer')
        viewer_applications = [group.app for group in user_viewer_groups]
        # Sort the applications by their last updated time
        apps['viewer_applications'] = sorted(viewer_applications, key=lambda x: x.last_updated, reverse=True)

    return apps
