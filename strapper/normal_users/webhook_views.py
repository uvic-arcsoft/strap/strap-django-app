# pylint:
# mywebhookapp/views.py

import os
import json
import environ

from django.contrib.sessions.models import Session
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail
from django.template import Template, Context
from django.conf import settings
from django.utils import timezone

from admin_users.models import HarborConfig
from .models import Container


# Change the line below if need to specify the path of .env file
# For example: environ.Env.read_env(os.path.join(BASE_DIR, '.env'))
environ.Env.read_env()

@csrf_exempt
def image_pushed_view(request):
    """
    Triggered when a new image is pushed to the registry
    """
    if request.method == 'POST':
        # Parse the incoming webhook payload
        webhook_data = json.loads(request.body.decode('utf-8'))

        # Process the data (you might want to save it to your database)
        print("Webhook data received:", webhook_data)

        # Respond to the webhook with a success status
        return HttpResponse('Webhook received', status=200)

    print("Method not allowed")
    # If the request is not a POST, return a method not allowed status
    return HttpResponse('Method not allowed', status=405)

@csrf_exempt
def image_high_vulnerabilities_notification(request):
    """
    Triggered when an image with vulnerabilities exceen the
    set threshold is detected in the registry
    """
    if request.method == 'POST':
        # Check if the request is from the registry
        if request.headers.get('Authorization') != os.environ.get('REGISTRY_WEBHOOK_SECRET'):
            return HttpResponse('Unauthorized', status=401)

        # If EMAIL_HOST_USER is not set in settings, disable notifying the developer
        if not settings.EMAIL_HOST_USER:
            return HttpResponse(
                'EMAIL_HOST_USER is not set in settings.py. Email notification is disabled.',
                status=200)

        # Parse the incoming webhook payload
        webhook_data = json.loads(request.body.decode('utf-8'))

        # Process the data (you might want to save it to your database)
        print("Webhook data received:", webhook_data)

        # Severity levels
        harbor_config = HarborConfig.get_instance()
        severity_levels = harbor_config.get_severity_levels()

        # Get information for the scan report
        vulnerable_artifacts = {}
        try:
            resources = webhook_data['event_data']['resources']
            for resource in resources:
                scan_result = resource['scan_overview']['application/vnd.security.vulnerability.report; version=1.1']
                severity = scan_result['severity']
                if severity_levels[severity] >= severity_levels[harbor_config.severity_threshold]:
                    print("Resource with high severity detected:", resource)
                    resource_url = resource['resource_url']
                    summary = scan_result['summary']['summary']
                    vulnerable_artifacts[resource_url] = {
                        severity: num_found
                        for severity, num_found in summary.items()
                        if severity_levels[severity] >= severity_levels[harbor_config.severity_threshold]
                    }

            print("Vulnerable artifacts:", vulnerable_artifacts)

            if len(vulnerable_artifacts) == 0:
                print("No severely vulnerable artifacts detected")
                return HttpResponse(
                    'No severely vulnerable artifacts detected. No need to notify developer.', status=200)
        except KeyError as e:
            print(f"Invalid webhook data. Missing keys: {e}")
            return HttpResponse(f"Invalid webhook data. Missing keys: {e}", status=500)

        # Get the image's owner
        # NOTE: Assuming all images are used by the same app (Might need to fix later)
        first_resource_url = list(vulnerable_artifacts.keys())[0]
        [image, tag] = first_resource_url.split(':')
        app_owner = ""
        try:
            container = Container.objects.get(image=image, image_tag=tag)
            app = container.application
            app_owner = app.owner
        except Container.DoesNotExist:
            print(f"Container not found with image: {image}:{tag}")
            return HttpResponse(f"Container not found with image: {image}:{tag}", status=404)

        # Do not send email if app owner has an active session
        active_sessions = Session.objects.filter(expire_date__gt=timezone.now())
        for session in active_sessions:
            session_data = session.get_decoded()
            if str(session_data.get('_auth_user_id', 0)) == str(app_owner.id):
                print(f"{app_owner.username} has an active session. Not sending email.")
                return HttpResponse(f"{app_owner.username} has an active session. Not sending email.", status=200)

        # Craft the email message
        interpolated_email_template = ""
        try:
            email_template = Template(harbor_config.vuln_notification_email_template)
            email_context = Context({
                "name": app_owner.first_name if app_owner.first_name else app_owner.username,
                "app_name": app.descriptive_name,
                "vuln_images": vulnerable_artifacts,
                "email_sender": os.environ.get("EMAIL_SENDER", "")
            })
            interpolated_email_template = email_template.render(email_context)
        except Exception as e:
            # Display an error message if email template interpolation fails
            print(f"Error occurred while interpolating email template: {e}")
            return HttpResponse(f"Error occurred while interpolating email template: {e}", status=500)

        # Send an email using the HTML content
        if not settings.TESTING:
            send_mail(
                "Severe vulnerabilities detected in your container images",
                "",     # Empty message body because we are using HTML content
                os.environ.get('EMAIL_SENDER'),
                [app_owner.username],
                fail_silently=False,
                html_message=interpolated_email_template,
            )

        # Respond to the webhook with a success status
        return HttpResponse(f"Contacted {app.descriptive_name}'s owner successfully!", status=200)

    print("Method not allowed")
    # If the request is not a POST, return a method not allowed status
    return HttpResponse('Method not allowed', status=405)
