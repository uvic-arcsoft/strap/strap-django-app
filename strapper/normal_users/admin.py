# pylint:
from django.contrib import admin
from .models import (Application, Container, ApplicationQuota,
                     ApplicationQuotaRequest, HarborProject,
                     Group, K8sInstance, Route, Deployment,
                     K8sContainer, Notification)

# Register your models here.
admin.site.register(Application)
admin.site.register(Container)
admin.site.register(ApplicationQuota)
admin.site.register(ApplicationQuotaRequest)
admin.site.register(HarborProject)
admin.site.register(Group)
admin.site.register(Route)
admin.site.register(Deployment)
admin.site.register(K8sInstance)
admin.site.register(K8sContainer)
admin.site.register(Notification)
