# pylint:
from urllib.parse import urlparse
from django import template

register = template.Library()

@register.filter(name='get_domain')
def get_domain(url_str):
    """Get domain name from a URL string."""
    parsed_url = urlparse(url_str)
    return parsed_url.netloc
