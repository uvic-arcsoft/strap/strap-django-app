# pylint:
from django import template

register = template.Library()

@register.filter(name='replace')
def replace(str, arg):
    """Replace old_sub with new_sub in str."""
    [old_sub, new_sub] = arg.split('|')
    return str.replace(old_sub, new_sub)
