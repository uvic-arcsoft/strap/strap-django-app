# pylint:
from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path('ws/deploy/<slug:identifier>', consumers.DeployConsumer.as_asgi()),
    path('ws/app_log/<slug:identifier>/<int:container_index>', consumers.AppLogConsumer.as_asgi()),
]
