# pylint:
from typing import Dict, Any, Union

from django.shortcuts import redirect
from django.views import View
from django.http import JsonResponse, HttpRequest

from normal_users.models import Application, Group
from normal_users.forms import GroupForm
from helpers.custom_json_response import (
    JsonResponseBadRequest, JsonResponseNotFound, JsonResponseForbidden, JsonResponseOK
)


class GroupView(View):
    '''
    This view handles all interactions with the Group model
    '''
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> Union[JsonResponse, redirect]:
        """Get or delete a group from an app"""
        identifier = kwargs.get('identifier', None)
        current_user = request.user

        # Save the previously active tab to show that tab after submission
        request.session['previously_active_app'] = identifier if identifier else 1

        app = None
        if identifier:
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound('App does not exist')

            # User must either be the owner or an editor of the app to access this view
            role = app.get_user_role(current_user)
            if role not in ['owner', 'editor']:
                return JsonResponseForbidden('User is unauthorized to access this url')

        # Handle DELETE method
        if 'delete' in request.path:
            group_id = kwargs.get('group_id', None)

            # Check if the group exists
            group = None
            if group_id:
                try:
                    group = Group.objects.get(id=group_id, created_by=current_user)
                except Group.DoesNotExist:
                    return JsonResponseForbidden("User is unauthorized to access this url")

            group.delete()
            # Display a success message
            request.session['message'] = f"Delete group <strong>{group.name}</strong> successfully"
            request.session['success'] = True
            request.session['active_tab'] = "groups"
            return redirect(f"{request.META.get('HTTP_REFERER')}")

        # Return 400 for other methods
        return JsonResponseBadRequest("Bad request. This URL doesn't support this method.")

    def post(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> Union[JsonResponse, redirect]:
        """
        Create or edit a group for an app
        
        Creates a new group if no group_id is provided
        Edits an existing group if a group_id is provided
        """
        identifier = kwargs.get('identifier', None)
        current_user = request.user

        # Save the previously active tab to show that tab after submission
        request.session['previously_active_app'] = identifier if identifier else 1

        app = None
        if identifier:
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound('App does not exist')

            # User must either be the owner or an editor of the app to create or edit app groups
            role = app.get_user_role(current_user)
            if role not in ['owner', 'editor']:
                return JsonResponseForbidden('User is unauthorized to access this url')

        ### Handle POST requests for adding existing groups to an app
        if "add_groups_to_app" in request.path:
            group_ids = request.POST.get('group_field', None).split(',')

            groups = []
            for group in Group.objects.filter(created_by=current_user):
                if str(group.id) in group_ids:
                    groups.append(group)

            for group in groups:
                app.group_set.add(group)

            # Display a success message
            request.session['message'] = f"Add groups to app <strong>{app.descriptive_name}</strong> successfully"
            request.session['success'] = True
            request.session['active_tab'] = "groups"
            return redirect(f"{request.META.get('HTTP_REFERER')}")

        ### Handle POST requests for creating and editing groups
        # Group id is received when editing a group, otherwise a group is being created
        group_id = kwargs.get('group_id', None)

        # Check if the group exists
        group = None
        if group_id:
            try:
                group = Group.objects.get(id=group_id, created_by=current_user)
            except Group.DoesNotExist:
                return JsonResponseForbidden("User is unauthorized to access this url")

        form = GroupForm(request.POST) if not group_id else GroupForm(request.POST, instance=group)
        # Form is valid
        if form.is_valid():
            if group_id:
                group = form.save()

                # Display a success message
                request.session['message'] = f"Edit group <strong>{group.name}</strong> successfully."
                request.session['success'] = True
            else:
                # Create the group
                new_group = form.save()
                new_group.created_by = current_user
                new_group.save()
                # Display a success message
                request.session['message'] =\
                    f"Create group <strong>{new_group.name}</strong> successfully."
                request.session['success'] = True
            request.session['active_tab'] = "groups"
            return redirect(f"{request.META.get('HTTP_REFERER')}")

        if group_id:
            # Form is invalid, display a failure message
            request.session['message'] =\
f"Failed to edit group <strong>{group.name}</strong>. Please fill out the group form properly."
            request.session['success'] = False
        else:
            # Form is invalid, display a failure message
            request.session['message'] =\
"Failed to create a group. Please fill out the group form properly."
            request.session['success'] = False
        request.session['active_tab'] = "groups"
        return redirect(f"{request.META.get('HTTP_REFERER')}")


class GroupAPIView(View):
    '''
    This is is not used within Strapper but used by other applications deployed by Strapper
    This is an API endpoint for other applications to get information about groups
    set up using Strapper
    '''
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        # Handle GET requests. Return names of groups that a requested user is in
        if 'get_groups_for_user' in request.path:
            user = request.GET.get('user', None)
            if not user:
                return JsonResponseBadRequest("Bad request. An user email is required.")

            identifier = kwargs.get('identifier', None)
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound('Requested app does not exist')

            group_names = []
            for group in app.group_set.all():
                if group.check_user(user):
                    group_names.append(group.name)

            return JsonResponseOK('Get groups for user successfully', groups=group_names)

        # Handle GET requests. Return all users for a requested group
        if 'get_users_for_group' in request.path:
            group_name = request.GET.get('group', None)
            if not group_name:
                return JsonResponseBadRequest("Bad request. An group name is required.")

            identifier = kwargs.get('identifier', None)
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound('Requested app does not exist')

            try:
                group = app.group_set.get(name=group_name)
            except Group.DoesNotExist:
                return JsonResponseNotFound(f'Requested group not found for app if {identifier}')

            return JsonResponseOK(
                'Get users for group successfully',
                users=group.get_users())

        return JsonResponseBadRequest("Bad request. App is not allowed to request this information.")
