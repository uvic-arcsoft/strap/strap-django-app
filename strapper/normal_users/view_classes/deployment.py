# pylint:
import os
import signal
import subprocess
import time
import re
from typing import Dict, Any

from threading import Thread

from ansi2html import Ansi2HTMLConverter

from django.conf import settings
from django.views import View
from django.http import JsonResponse, HttpRequest

from django_editablecontent.views import EditableContentView

from normal_users.models import Application, Deployment, K8sInstance, K8sContainer
from normal_users.helper_functions import poll_log_file, get_apps_for_user_by_role
from helpers.custom_json_response import (
    JsonResponseNotFound, JsonResponseBadRequest, JsonResponseUnauthorized, JsonResponseOK
)


class DeploymentView(View):
    '''
    View for deploying an application. Deploy an application to Kickstand K8s cluster with Terraform.

    Returns:
    A Json Response:
    - exit_code = 0, deployed = 1: Deployed successfully
    - exit_code != 0: Failed to deploy, or failed to access this view (Unauthorized, deployment is running)
    - exit_code = 0, deployed = 0: Doesn't deploy successfully, but doesn't fail. This can either due to
    the deployment being canceled, or no changes in configurations is detected from the last deployment.
    '''
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        # Check if the request is an AJAX call
        is_ajax = request.headers.get('x-requested-with') == 'XMLHttpRequest'

        identifier = kwargs.get('identifier', None)
        current_user = request.user

        print(f"Received a request to deploy app {identifier}")

        app = None
        # Check if the app exists
        try:
            app = Application.objects.get(identifier=identifier)
        except Application.DoesNotExist:
            return JsonResponseNotFound("App does not exist", exit_code=-1)

        # User must be an owner or an editor of the app to deploy
        role = app.get_user_role(current_user)
        if role not in ['owner', 'editor']:
            return JsonResponseUnauthorized("User is unauthorized to deploy this app", exit_code=-1)

        # Do not deploy an undeployable app
        if not app.is_deployable():
            message = ""

            # App has no containers
            if not app.container_set.all():
                message = f"App {app.descriptive_name} is not deployable. Please add at least one container first."

            # App doesn't have a main container
            elif not app.port:
                message = f"App {app.descriptive_name} is not deployable. Please set a main container first."

            # App contains unacceptable vulnerabilities
            else:
                message = f"App {app.descriptive_name} is not deployable. Please fix the vulnerabilities first."

            return JsonResponseBadRequest(message, exit_code=-1)

        # Create a deployment if the request is AJAX
        if is_ajax:
            try:
                deployment = Deployment.objects.get(application=app)
            except Deployment.DoesNotExist:
                # Create a deployment for the app if there is none
                deployment = Deployment.objects.create(
                    application=app,
                    log_file = f"{app.get_terraform_log_path()}/strap-{identifier}.log"
                )

            # if deployment is in progress, returns an error and a message as an app can't be deployed twice
            if deployment.state in (1, 5):
                return JsonResponseBadRequest(f'App {app.descriptive_name} is being deployed', exit_code=-1)

            # Only update app deployment if there is a change in app configurations
            # and app is not in a "started" state
            try:
                k8sinstance = K8sInstance.objects.get(application=app)
                app_deployed = True
            except K8sInstance.DoesNotExist:
                app_deployed = False

            if app_deployed and not app.detect_config_change() and deployment.state == 2:
                return JsonResponseOK(
                    message=('No changes detected'
                    ' in the app configuration to initiate a new deployment'),
                    exit_code=0,
                    deployed=0
                )

            # if deployment is queued, stopped or failed, start the deployment
            # Record the current state
            # Update the state to 'started'/'deployed + started', status to 'deploying'/'updating'
            deployment.prev_state = deployment.state
            deployment.state = 1 if not app_deployed else 5
            deployment.status = 1 if not app_deployed else 10
            deployment.save()

            # Create the necessary directories for the log file if they don't exist
            # NOTE: This is for a very edge case where Strapper pod is redeployed
            # and doesn't have the required directories while the deployment
            # log file is already populated in the database.
            app.get_terraform_log_path()

            with open(deployment.log_file, 'w', encoding='utf-8') as log_file_handle:
                # Run the commands, capture the outputs in a log file
                # Not using 'with' to avoid messing up the code structure
                # pylint: disable-next=R1732
                process = subprocess.Popen(
                    f"python3 terraform.py -a {identifier} {'-t 1' if settings.TESTING else ''}",
                    stdout=log_file_handle,
                    stderr=subprocess.STDOUT,
                    shell=True
                )

                # Record ids of processes that perform this deployment
                # Get deployment to avoid race condition
                deployment = Deployment.objects.get(
                    application=app,
                    log_file = f"/tmp/terraform/{identifier}/strap-{identifier}.log"
                )
                deployment.process_ids = str(deployment.process_ids or '') +\
                    f"{',' if deployment.process_ids else ''}{process.pid}"
                deployment.save()

                if not settings.TESTING:
                    # Spawn a thread in the background to update deploy log in near real time
                    poll_log_file_thread = Thread(target=poll_log_file, args=(identifier, deployment))
                    poll_log_file_thread.start()

                # Wait for the process to finish
                process.wait(timeout=settings.TERRAFORM_TIMEOUT)

                # Get deployment again as the data has been changed by StopApplicationDeploymentView
                deployment = Deployment.objects.get(
                    application=app,
                    log_file = f"/tmp/terraform/{identifier}/strap-{identifier}.log"
                )

                # Remove process ids from deployment when the process finishes
                deployment.process_ids = None
                deployment.save()

            # If process failed and the deployment is not stopped (state 3),
            # set state to 'failed'/'deployed + failed'/'stopped', set status to 'failed', return an error and a message
            if process.returncode != 0 and deployment.status != 4:
                # If state was 'stopped', set it back to 'stopped'
                if deployment.prev_state == 3:
                    deployment.state = 3
                # If state was 'None', set state to 'failed'
                elif not app_deployed:
                    deployment.state = 4
                # If state was 'deployed', set state to 'deployed + failed'
                else:
                    deployment.state = 6
                deployment.status = 3
                deployment.save()
                return JsonResponseOK(
                        message=f"Failed to {'deploy' if not app_deployed else 'update'} app {app.descriptive_name}",
                        exit_code=process.returncode,
                        deployed=0,
                )

            # If the deployment is canceled in the middle
            if process.returncode != 0 and deployment.status == 4:
                # Set state back to whatever it was before deployed
                deployment.state = deployment.prev_state
                deployment.status = 5
                deployment.save()
                return JsonResponseOK(
                        message=f'Canceled deployment for app {app.descriptive_name}',
                        exit_code=0.5, # chose 0.5 as this cannot be any process' returncode
                        deployed=0
                )

            # Else, set state to 'deployed', status to 'succeeded', return a success and a message
            deployment.state = 2
            deployment.status = 2
            # Set databases, owner and initial password
            if app.database != 'None':
                if not deployment.databases:
                    deployment.databases = f"{identifier}_dev, {identifier}_prod"
                if not deployment.db_owner:
                    deployment.db_owner = f"{identifier}_owner"
                if not deployment.db_password:
                    output = ""
                    with open(f"/tmp/terraform/{identifier}/strap-{identifier}.log", "r", encoding="utf-8") as file:
                        output = file.read()
                    postgres_info = output.split("\n\n")[-1]
                    matches = re.findall(f'"{identifier}_owner" = "[A-Za-z0-9]+"', postgres_info)
                    [_, quoted_password] = matches[0].split(" = ")
                    password = re.findall('[A-Za-z0-9]+', quoted_password)[0]
                    deployment.db_password = password
            deployment.save()

            # Create/Update K8sinstance to record this app's most recent deployed configurations
            try:
                k8sinstance = K8sInstance.objects.get(application=app)
            except K8sInstance.DoesNotExist:
                k8sinstance = K8sInstance()

            k8sinstance.application = app
            k8sinstance.descriptive_name = app.descriptive_name
            k8sinstance.port = app.port
            k8sinstance.authentication = app.authentication
            k8sinstance.authenticated_routes_str = app.routes_to_str(authenticated=True)
            k8sinstance.unauthenticated_routes_str = app.routes_to_str(authenticated=False)
            k8sinstance.allowed_ips = app.allowed_ips
            k8sinstance.save()

            # Create/update containers that is successfully deployed
            for container in app.container_set.all():
                try:
                    k8scontainer = K8sContainer.objects.get(container=container, k8sinstance=k8sinstance)
                except K8sContainer.DoesNotExist:
                    k8scontainer = K8sContainer()
                k8scontainer.k8sinstance = k8sinstance
                k8scontainer.container = container
                k8scontainer.image = container.image
                k8scontainer.image_tag = container.image_tag
                k8scontainer.port = container.port
                k8scontainer.command = container.command
                k8scontainer.environment_variables = container.environment_variables
                k8scontainer.save()

            return JsonResponseOK(
                    message=f'App {app.descriptive_name} deployed successfully',
                    exit_code=process.returncode,
                    deployed=1
            )

        return JsonResponseBadRequest('Bad request. Cannot access this view directly.')


# Displays an application deployment log
class DeploymentLogView(EditableContentView):
    template_name = 'normal_users/deploy_log.html'

    def get_context_data(self, **kwargs: Dict[str, Any]) -> Dict[str, Any]:
        identifier = kwargs.get('identifier', None)
        current_user = self.request.user

        context = super().get_context_data(**kwargs)
        available_apps = get_apps_for_user_by_role(current_user, 'all')
        context = {**context, **available_apps}

        app = None
        # Check if the app exists
        try:
            app = Application.objects.get(identifier=identifier)
        except Application.DoesNotExist:
            context['status'] = 404
            context['log'] = 'App does not exist'
            return context

        # User must have at least 'viewer' access to the app to view the deployment log
        role = app.get_user_role(current_user)
        if role == 'none':
            context['status'] = 403
            context['log'] = 'User is unauthorized to access this url'
            return context
        context['user_role'] = role

        try:
            deployment = Deployment.objects.get(application=app)
        except Deployment.DoesNotExist:
            context['status'] = 404
            context['log'] = 'No deployment for this app found'
            return context

        # Converter converts Ansi to HTML
        conv = Ansi2HTMLConverter()

        # Create an empty deployment log file if not exists
        # NOTE: This is for a very edge case where Strapper pod is redeployed
        # and doesn't have the required directories and files while the deployment
        # log file is already populated in the database.
        if not os.path.exists(deployment.log_file):
            print("WARNING: Needed to recreate the log file. Was Strapper redeployed?")
            app.get_terraform_log_path()
            with open(deployment.log_file, 'w', encoding='utf-8') as log_file_handle:
                log_file_handle.write('')

        with open(deployment.log_file, 'r', encoding='utf-8') as log_file_stream:
            output = log_file_stream.read()

        context['status'] = 200
        if settings.TESTING:
            context['log'] = output
        else:
            context['log'] = conv.convert(output)
        return context


class StopDeploymentView(View):
    '''
    View for stopping an application deployment
    There are three cases:
    - Cancel: Stop the ongoing deployment
    - Stop: stop a running deployment: quiesce the pod running the app and pull down the related service objects,
    but don't touch the database.  Leads to a "stopped" state
    - Destroy: stop running deployment, delete all kubernetes resources and the database (!).
    Leave only the application configuration

    Note: You can't do any of these without first doing the one above.
    So you can't delete until you've done the destroy, which you can't do until you've stopped the app.
    This way it's less likely the user accidentally nukes everything
    '''
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        # Check if the request is an AJAX call
        is_ajax = request.headers.get('x-requested-with') == 'XMLHttpRequest'

        identifier = kwargs.get('identifier', None)
        current_user = request.user

        # User must be the owner of the app to stop the deployment
        # try:
        #     app = Application.objects.get(identifier=identifier, owner=current_user)
        # except Application.DoesNotExist:
        #     return JsonResponse({'status': 403, 'message': 'User is unauthorized to access this url'})

        app = None
        # Check if the app exists
        try:
            app = Application.objects.get(identifier=identifier)
        except Application.DoesNotExist:
            return JsonResponseNotFound("App does not exist", exit_code=-1)

        # User must be an owner or an editor of the app to stop a deployment
        role = app.get_user_role(current_user)
        if role not in ['owner', 'editor']:
            return JsonResponseUnauthorized("User is not authorized to stop this app deployment", exit_code=-1)

        # Cancel/Stop/Destroy the deployment if request is AJAX
        if is_ajax:
            deployment = None

            try:
                deployment = Deployment.objects.get(application=app)
            except Deployment.DoesNotExist:
                # return 404 if deploy not found
                return JsonResponseNotFound('There is no deployment for this app')

            # Check if this app already has an active deployment
            try:
                K8sInstance.objects.get(application=app)
                app_deployed = True
            except K8sInstance.DoesNotExist:
                app_deployed = False

            # Cancel a deployment that is in 'started' or 'deployed + started' state
            if deployment.state in (1, 5):
                # This code needs to be here in case deployment view's terraform process
                # finishes before any of the code below start executing
                if not deployment.process_ids:
                    return JsonResponseBadRequest(
                        f'App {app.descriptive_name} deployment already finished or failed', exit_code=-1
                    )

                # Get deployment again to avoid race condition
                deployment = Deployment.objects.get(application=app)

                running_process_ids = []
                for process_id in deployment.process_ids.split(','):
                    # Get OSError if the process is not running, which means the process either finished
                    # or failed
                    try:
                        os.kill(int(process_id), 0)
                        running_process_ids.append(int(process_id))
                    except OSError:
                        continue

                # Can't stop a deployment that already finished or failed
                if len(running_process_ids) == 0 and not settings.TESTING:
                    return JsonResponseBadRequest(
                        f'App {app.descriptive_name} deployment already finished or failed', exit_code=-1
                    )

                # Set state to 'None' if not previously deployed, 'deployed' otherwise
                # This has to run before the deploying process is terminated
                # as deployment.status notifies the deploying view that the deployment
                # has is being cancelled
                deployment.state = 0 if not app_deployed else 2
                # Set status to 'cancelling'
                deployment.status = 4
                deployment.save()

                if not settings.TESTING:
                    # Terminate the all processes that is running the deployment
                    for process_id in running_process_ids:
                        os.kill(process_id, signal.SIGTERM)

                # Wait for 2 seconds to write to log file to ensure the stream in deployment's process
                # for this file is closed properly
                time.sleep(2)

                # Create a log file for the deployment if it doesn't exist
                # NOTE: This is for a very edge case where Strapper pod is redeployed
                # and doesn't have the required directories and files while the deployment
                # log file is already populated in the database.
                if not os.path.exists(deployment.log_file):
                    print("WARNING: Needed to recreate the log file. Was Strapper redeployed?")
                    app.get_terraform_log_path()
                    with open(deployment.log_file, 'w', encoding='utf-8') as log_file_handle:
                        log_file_handle.write('')

                # Write to the log file of that deployment to notify user about the stop action
                with open(deployment.log_file, 'a', encoding='utf-8') as log_file_handle:
                    log_file_handle.write('This deployment has been canceled')

                return JsonResponseOK(
                    f'Cancel deployment for app {app.descriptive_name} successfully', exit_code=0
                )

            # deployment in 'deployed + failed' state
            # Revert to the previous 'deployed' state, with all the resources created at that stage
            if deployment.state == 6:
                # Set state to 'deployed', status to 'reverting'
                deployment.state = 2
                deployment.status = 11
                deployment.save()

                # Get all changes between current app config and the last successful deployment config
                # config_diffs = app.config_diff()

                # Create a log file for the deployment if it doesn't exist
                # NOTE: This is for a very edge case where Strapper pod is redeployed
                # and doesn't have the required directories and files while the deployment
                # log file is already populated in the database.
                if not os.path.exists(deployment.log_file):
                    print("WARNING: Needed to recreate the log file. Was Strapper redeployed?")
                    app.get_terraform_log_path()

                with open(deployment.log_file, 'w', encoding='utf-8') as log_file_handle:
                    # Revert all resources created/modifier for this app in the most recent 'terraform apply'
                    # Run the commands, capture the outputs in a log file
                    # Not using 'with' to avoid messing up the code structure
                    # pylint: disable-next=R1732
                    process = subprocess.Popen(
                        f"python3 terraform.py -a {identifier} -r 1 {'-t 1' if settings.TESTING else ''}",
                        stdout=log_file_handle,
                        stderr=subprocess.STDOUT,
                        shell=True
                    )

                    if not settings.TESTING:
                        # Spawn a thread in the background to update deploy log in near real time
                        poll_log_file_thread = Thread(target=poll_log_file, args=(identifier, deployment))
                        poll_log_file_thread.start()

                    # Wait for the process to finish
                    process.wait(timeout=settings.TERRAFORM_TIMEOUT)

                # Set state to 'deployed', status to 'succeeded' if revert successfully
                if process.returncode == 0:
                    deployment.state = 2
                    deployment.status = 2
                    deployment.save()
                # Revert the state to 'deployed + failed' and status to 'failed' if reverting deployment fails
                else:
                    deployment.state = 6
                    deployment.status = 3
                    deployment.save()

                # Get app again as it's modified by the deployment process
                app = Application.objects.get(identifier=identifier, owner=current_user)

                return JsonResponseOK(
                    message=f'Revert deployment for app {app.descriptive_name} successfully'\
                    if process.returncode == 0 else f'Failed to revert deployment for app {app.descriptive_name}',
                    exit_code=process.returncode,
                    app_configs=app.get_js_config()
                )

            # Deployment in 'deployed' state. Visiting this view destroys all resources related to this deployment
            # on Kubernetes - all resources created by Helm for this app
            if deployment.state == 2:
                # Set state to 'Stopped', status to 'stopping'
                deployment.state = 3
                deployment.status = 6
                deployment.save()

                # Create a log file for the deployment if it doesn't exist
                # NOTE: This is for a very edge case where Strapper pod is redeployed
                # and doesn't have the required directories and files while the deployment
                # log file is already populated in the database.
                if not os.path.exists(deployment.log_file):
                    print("WARNING: Needed to recreate the log file. Was Strapper redeployed?")
                    app.get_terraform_log_path()

                with open(deployment.log_file, 'w', encoding='utf-8') as log_file_handle:
                    # Stop all resources for this app created by Helm
                    # Run the commands, capture the outputs in a log file
                    # Not using 'with' to avoid messing up the code structure
                    # pylint: disable-next=R1732
                    process = subprocess.Popen(
                        f"python3 terraform.py -a {identifier} -s 1 {'-t 1' if settings.TESTING else ''}",
                        stdout=log_file_handle,
                        stderr=subprocess.STDOUT,
                        shell=True
                    )

                    if not settings.TESTING:
                        # Spawn a thread in the background to update deploy log in near real time
                        poll_log_file_thread = Thread(target=poll_log_file, args=(identifier, deployment))
                        poll_log_file_thread.start()

                    # Wait for the process to finish
                    process.wait(timeout=settings.TERRAFORM_TIMEOUT)

                # Set status to 'stopped'
                if process.returncode == 0:
                    deployment.status = 7
                    deployment.save()

                # If fail to stop, revert state to 'deployed', status to 'succeeded'
                else:
                    deployment.state = 2
                    deployment.status = 2
                    deployment.save()

                return JsonResponseOK(
                    message=f'Stop deployment for app {app.descriptive_name} successfully'\
                    if process.returncode == 0 else f'Failed to stop deployment for app {app.descriptive_name}',
                    exit_code=process.returncode
                )

            # deployment in 'stopped' or 'failed' state.
            # Visiting this view destroys all resources related to this deployment
            if deployment.state in (3, 4):
                deployment.prev_state = deployment.state
                # Set state to 'None', status to 'destroying'
                deployment.state = 0
                deployment.status = 8
                deployment.save()

                # Create a log file for the deployment if it doesn't exist
                # NOTE: This is for a very edge case where Strapper pod is redeployed
                # and doesn't have the required directories and files while the deployment
                # log file is already populated in the database.
                if not os.path.exists(deployment.log_file):
                    print("WARNING: Needed to recreate the log file. Was Strapper redeployed?")
                    app.get_terraform_log_path()

                with open(deployment.log_file, 'w', encoding='utf-8') as log_file_handle:
                    # Destroy all resources for this app created by Terraform
                    # Run the commands, capture the outputs in a log file
                    # Not using 'with' to avoid messing up the code structure
                    # pylint: disable-next=R1732
                    process = subprocess.Popen(
                        f"python3 terraform.py -a {identifier} -d 1 {'-t 1' if settings.TESTING else ''}",
                        stdout=log_file_handle,
                        stderr=subprocess.STDOUT,
                        shell=True
                    )

                    if not settings.TESTING:
                        # Spawn a thread in the background to update deploy log in near real time
                        poll_log_file_thread = Thread(target=poll_log_file, args=(identifier, deployment))
                        poll_log_file_thread.start()

                    # Wait for the process to finish
                    process.wait(timeout=settings.TERRAFORM_TIMEOUT)

                # Set status to 'destroyed'
                if process.returncode == 0:
                    deployment.status = 9
                    deployment.databases = None
                    deployment.db_owner = None
                    deployment.db_password = None
                    deployment.save()

                    # Delete the last successful deployment config for this app if the app is
                    # destroyed from a 'stopped' state
                    if deployment.prev_state == 3:
                        app.k8sinstance.delete()

                # If fail to destroy, revert state to the previous
                # state (either 'stopped' or 'failed')
                # Do the same thing with status
                else:
                    deployment.state = deployment.prev_state
                    deployment.status = 7 if deployment.prev_state == 3 else 3
                    deployment.save()

                return JsonResponseOK(
                    message=f'Destroy deployment for app {app.descriptive_name} successfully'\
                    if process.returncode == 0 else f'Failed to destroy deployment for app {app.descriptive_name}',
                    exit_code=process.returncode
                )

            # View is not accessible to deployments that aren't in any of the above states
            # Return 400
            return JsonResponseBadRequest("Bad request. Can't stop this app deployment.")
        return JsonResponseBadRequest('Cannot access this page directly')
