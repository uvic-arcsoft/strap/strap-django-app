# pylint:
from typing import Union, Dict, Any

from django.views import View
from django.http import HttpRequest, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse

from normal_users.models import Notification, NotificationTypes, NotificationStatusTypes
from helpers.custom_json_response import (JsonResponseBadRequest, JsonResponseNotFound,
                                            JsonResponseUnauthorized)

class NotificationView(View):
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> Union[JsonResponse, redirect]:
        """
        Remove "New" status of a notification after being clicked on by a user
        """
        notification_id = kwargs.get('notification_id', None)
        if not notification_id:
            return JsonResponseBadRequest('No notification id provided')

        # Get the notification
        notification = None
        try:
            notification = Notification.objects.get(id=notification_id)
        except Notification.DoesNotExist:
            return JsonResponseNotFound('Notification not found')

        # User must be the recipient of the notification
        if notification.user != request.user:
            return JsonResponseUnauthorized('You are not authorized to view this notification')

        # Update the K8s resource request notification status
        if (notification.type == NotificationTypes.ApplicationQuotaRequest
            and notification.status == NotificationStatusTypes.NEW):
            notification.status = NotificationStatusTypes.PENDING

        notification.save()

        return redirect(notification.url if notification.url else reverse('dashboard'))
