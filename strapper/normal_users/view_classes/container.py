# pylint:
from typing import Union, Any, Dict

from django.shortcuts import redirect
from django.views import View
from django.http import JsonResponse, HttpRequest

from django_editablecontent.views import EditableContentView

from normal_users.models import Application, Container
from normal_users.forms import ContainerForm
from normal_users.helper_functions import get_apps_for_user_by_role
from helpers.custom_json_response import (
    JsonResponseBadRequest, JsonResponseNotFound, JsonResponseForbidden, JsonResponseOK, JsonResponseCreated
)


class ContainerAPIView(View):
    '''
    Invisible view: Manage containers for applications
    Can only access by AJAX or redirection
    '''
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> Union[JsonResponse, redirect]:
        """Get or delete a container from an app"""

        # Check if the request is an AJAX call
        is_ajax = request.headers.get('x-requested-with') == 'XMLHttpRequest'

        identifier = kwargs.get('identifier', None)
        current_user = request.user

        # Enforce certain checks
        app = None
        if identifier:
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound("Application not found")

            # User must be either the owner or an editor of the app to access this view
            role = app.get_user_role(current_user)
            if role not in ['owner', 'editor']:
                return JsonResponseForbidden("User is unauthorized to access this url")

        # Handle DELETE method
        if 'delete' in request.path:
            container_id = kwargs.get('container_id', None)

            # Check if the container exists
            container = None
            if container_id:
                try:
                    container = Container.objects.get(id=container_id, application=app)
                except Container.DoesNotExist:
                    return JsonResponseForbidden("User is unauthorized to access this url")

            container.delete()

            # Set application port to None if application has the same port as this container
            if app.port == container.port:
                app.port = None
                app.save()

            message = (
                    f"Delete container <strong>{container}</strong> for app "
                    f"<strong>{app.descriptive_name}</strong> successfully"
            )

            # Response to AJAX request
            if is_ajax:
                return JsonResponseOK(message)

            # Display a success message
            request.session['message'] = message
            request.session['success'] = True
            request.session['active_tab'] = "containers"
            return redirect(request.META.get('HTTP_REFERER'))

        # Return 400 for other methods
        return JsonResponseBadRequest("Bad request. This URL doesn't support this method.")

    def post(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> Union[JsonResponse, redirect]:
        """
        Create or edit a container for an app

        Creates a new container if no container_id is provided
        Edits an existing container if a container_id is provided
        """
        # Check if the request is an AJAX call
        is_ajax = request.headers.get('x-requested-with') == 'XMLHttpRequest'

        # Create or edit a container for an app
        identifier = kwargs.get('identifier', None)
        current_user = request.user

        # Enforce certain checks
        app = None
        if identifier:
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound("Application not found")

            # User must be either the owner or an editor of the app to access this view
            role = app.get_user_role(current_user)
            if role not in ['owner', 'editor']:
                return JsonResponseForbidden("User is unauthorized to access this url")

        # Handle POST requests for creating and editing containers
        container_id = kwargs.get('container_id', None)

        # Check if the container exists
        container = None
        if container_id:
            try:
                container = Container.objects.get(id=container_id, application=app)
            except Container.DoesNotExist:
                return JsonResponseForbidden("User is unauthorized to access container")

        form = ContainerForm(request.POST) if not container_id else ContainerForm(request.POST, instance=container)
        # Form is valid
        if form.is_valid():
            if container_id:
                container = form.save(commit=False)
                container.image_tag = container.image_tag if container.image_tag else 'latest'
                container.save()

                # Set application port to container port if this container is the main container
                if form.cleaned_data['is_main']:
                    app.port = container.port
                    app.save()
                # Remove the application port if a container is demoted from being the main container
                else:
                    if app.port == container.port:
                        app.port = None
                        app.save()

                # Reponse to AJAX request
                if is_ajax:
                    return JsonResponseOK(
                        message=f"Edit container <strong>{container}</strong> successfully.",
                        container={
                            'id': container.id,
                            'image': container.image,
                            'image_tag': container.image_tag if container.image_tag else 'latest',
                            'port': container.port if container.port else 'None',
                            'command': container.command if container.command else 'None',
                            'environment_variables': container.environment_variables \
                                if container.environment_variables else 'None'
                        }
                    )

                # Redirect to the request page and display a success message
                request.session['message'] = f"Edit container <strong>{container}</strong> successfully."
                request.session['success'] = True
                request.session['active_tab'] = "containers"
                return redirect(request.META.get('HTTP_REFERER'))

            # Create the container
            new_container = form.save(commit=False)
            new_container.application = app
            new_container.image_tag = new_container.image_tag if new_container.image_tag else 'latest'
            new_container.save()

            # Set application port to container port if this container is the main container
            if form.cleaned_data['is_main']:
                app.port = new_container.port
                app.save()

            # Reponse to AJAX request
            if is_ajax:
                return JsonResponseCreated(
                    message=f"Create container <strong>{ new_container }</strong> successfully.",
                    container={
                        'id': new_container.id,
                        'image': new_container.image,
                        'image_tag': new_container.image_tag if new_container.image_tag else 'latest',
                        'port': new_container.port if new_container.port else 'None',
                        'command': new_container.command if new_container.command else 'None',
                        'environment_variables': new_container.environment_variables\
                            if new_container.environment_variables else 'None'
                    })

            # Redirect to the request page and display a success message
            request.session['message'] = f"Create container <strong>{ new_container }</strong> successfully."
            request.session['success'] = True
            request.session['active_tab'] = "containers"
            return redirect(request.META.get('HTTP_REFERER'))

        message = (
                f"Failed to edit container <strong>{container}</strong>. "
                "Please fill out the container form properly."
        )

        # Form is invalid
        if container_id:
            # Response to AJAX request
            if is_ajax:
                return JsonResponseBadRequest(message)

            # Redirect to the request page and display a failure message
            request.session['message'] = message
            request.session['success'] = False
            request.session['active_tab'] = "containers"
            return redirect(request.META.get('HTTP_REFERER'))

        # Response to AJAX request
        if is_ajax:
            return JsonResponseBadRequest(
                "Failed to create a container. Please fill out the container form properly."
            )

        # Redirect to the request page and display a failure message
        request.session['message'] = "Failed to create a container. Please fill out the container form properly."
        request.session['success'] = False
        request.session['active_tab'] = "containers"
        return redirect(request.META.get('HTTP_REFERER'))


class ContainerScanReportView(EditableContentView):
    '''
    View to display the vulnerability scan report of a container
    '''
    template_name = "normal_users/container_scan_report.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        identifier = kwargs.get('identifier', None)
        container_id = kwargs.get('container_id', None)
        current_user = self.request.user

        app = None
        # Check if app exists
        try:
            app = Application.objects.get(identifier=identifier)
        except Application.DoesNotExist:
            return JsonResponseNotFound("Application not found")

        # User must have at least viewer access to the app to access this view
        role = app.get_user_role(current_user)
        if role == 'none' and not current_user.is_staff:
            return JsonResponseForbidden("User is unauthorized to access this url")

        # Check if the container exists
        container = None
        if container_id:
            try:
                container = Container.objects.get(id=container_id, application=app)
            except Container.DoesNotExist:
                return JsonResponseForbidden("User is unauthorized to access this url")

        # Scan the container
        report = container.get_scan_report()

        context['container'] = container
        context['report'] = report
        context = {**context, **get_apps_for_user_by_role(current_user)}

        return context

class ContainerScanView(View):
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        """
        View to scan a container for vulnerabilities
        """
        identifier = kwargs.get('identifier', None)
        container_id = kwargs.get('container_id', None)
        current_user = request.user

        app = None
        # Check if app exists
        try:
            app = Application.objects.get(identifier=identifier)
        except Application.DoesNotExist:
            error = "Application not found"
            return JsonResponseNotFound(error, error=error)

        # User must either be an admin
        # or have at least viewer access to the app to access this view
        role = app.get_user_role(current_user)
        if role == 'none' and not current_user.is_staff:
            error = "User is unauthorized to access this url"
            return JsonResponseForbidden(error, error=error)

        # Check if the container exists
        container = None
        if container_id:
            try:
                container = Container.objects.get(id=container_id, application=app)
            except Container.DoesNotExist:
                error = "Container not found"
                return JsonResponse(error, error=error)

        # Scan the container
        vulnerabilities = container.scan()

        return JsonResponseOK(
            (f"Scan container <strong>{container}</strong> for app "
            f"<strong>{app.descriptive_name}</strong> successfully"),
            vulnerabilities=vulnerabilities)
