# pylint:
import subprocess
from typing import Dict, Any

from django.conf import settings
from django.views import View
from django.http import JsonResponse, HttpRequest

from normal_users.models import Application, HarborProject
from helpers.custom_json_response import (
    JsonResponseBadRequest, JsonResponseNotFound, JsonResponseForbidden, JsonResponseOK
)


class HarborAPIView(View):
    '''
    Invisible view: Manage Harbor projects for applications
    '''
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        '''Get images in a Harbor project for an application'''
        identifier = kwargs.get('identifier', None)
        current_user = request.user

        app = None
        if identifier:
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound('App does not exist')

            # User must have at least viewer access to the app to access this view
            role = app.get_user_role(current_user)
            if role == 'none':
                return JsonResponseForbidden('User is unauthorized to access this url')

        # Get images in a Harbor project for an application
        if 'get_images' in request.path:
            try:
                harbor_project = HarborProject.objects.get(application=app)

            # If app has no Harbor project, return an empty list
            except HarborProject.DoesNotExist:
                return JsonResponseOK(
                    message='App has no Harbor project',
                    images=[]
                )

            result = harbor_project.get_images_from_harbor()
            if 'errors' in result:
                return JsonResponseBadRequest(result['errors'])
            return JsonResponseOK(
                message='Retrieved images successfully',
                images=result['images']
            )

        return JsonResponseBadRequest("Bad request. This URL doesn't support this action.")

    def post(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        '''
        Create a Harbor project for an application
        Only support AJAX requests at the moment
        '''
        identifier = kwargs.get('identifier', None)
        current_user = request.user

        app = None
        if identifier:
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound('App does not exist')

            # User must either be the owner or an editor of the app to access this view
            role = app.get_user_role(current_user)
            if role not in ['owner', 'editor']:
                return JsonResponseForbidden('User is unauthorized to access this url')

        # Create a new Harbor project
        if 'create' in request.path:
            # Create a dir for the application if it doesn't exist
            harbor_path = app.get_terraform_log_path(type='harbor')

            with open(f"{harbor_path}/harbor-tf.log", "w", encoding='utf-8') as log_file_handle:
                # Create the registry using Terraform, capture the outputs in a log file
                # Not using 'with' to avoid messing up the code structure
                # pylint: disable-next=consider-using-with
                process = subprocess.Popen(
                    (f"python3 {'harbor.py' if not settings.TESTING else 'strapper/harbor.py'} "
                    f"-a {identifier} {'-t 1' if settings.TESTING else ''} -c 1"),
                    stdout=log_file_handle,
                    stderr=subprocess.STDOUT,
                    shell=True
                )

                # Wait for the process to finish. Shouldn't take more than 5 minutes
                process.wait(timeout=310)

            if process.returncode != 0:
                return JsonResponseBadRequest(
                    "Failed to create a Harbor project. Please try again or contact us for support."
                )

            # Only return a success message if in testing mode
            if settings.TESTING:
                return JsonResponseOK("Created a Harbor project successfully.")

            # Get the new HarborProject instance created by the Terraform script
            harbor_project = HarborProject.objects.get(application=app)

            return JsonResponseOK(
                message="Created a Harbor project successfully.",
                project=harbor_project.to_json()
            )

        return JsonResponseBadRequest("Bad request. This URL doesn't support this action.")
