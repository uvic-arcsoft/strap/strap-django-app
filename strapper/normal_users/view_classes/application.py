# pylint:
import subprocess
from typing import Union, Any, Dict

from django.conf import settings
from django.shortcuts import redirect
from django.urls import reverse
from django.views import View
from django.http import JsonResponse, HttpRequest

from django_editablecontent.views import EditableContentView

from admin_users.models import GeneralConfig, HarborConfig
from access.models import TransferOwnershipRequest

from normal_users.models import Application, Group, Deployment
from normal_users.helper_functions import get_apps_for_user_by_role
from helpers.custom_json_response import (
    JsonResponseBadRequest, JsonResponseForbidden, JsonResponseNotFound, JsonResponseOK
)

# Single application view
class ApplicationView(EditableContentView):
    template_name = "normal_users/app.html"

    def get_context_data(self, **kwargs: Dict[str, Any]) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        identifier = kwargs.get('identifier', None)
        current_user = self.request.user

        available_apps = get_apps_for_user_by_role(current_user, 'all')
        context = {**context, **available_apps}

        # Check if app exists
        app = None
        try:
            app = Application.objects.get(identifier=identifier)
        except Application.DoesNotExist:
            context['status'] = 404
            context['message'] = 'Application does not exist'
            return context

        # Check if user has access to view the app
        user_role = app.get_user_role(current_user)
        if user_role == 'none':
            context['status'] = 403
            context['message'] = 'User is unauthorized to access this url'
            return context
        context['user_role'] = user_role

        # Display status messages for an action
        context['message'] = self.request.session.get('message', None)
        context['success'] = self.request.session.get('success', None)

        # Clear session message display if active
        if context['message']:
            del self.request.session['message']
            del self.request.session['success']

        if 'previously_active_app' in self.request.session:
            del self.request.session['previously_active_app']

        if 'active_tab' in self.request.session:
            context['active_tab'] = self.request.session['active_tab']
            del self.request.session['active_tab']

        context['status'] = 200
        context['application'] = app
        context['owned_applications'] = Application.objects.filter(owner=current_user)
        context['containers'] = app.container_set.all()
        context['groups'] = app.group_set.all()
        context['owned_groups'] = Group.objects.filter(created_by=current_user)
        context['deployment'] = app.deployment if hasattr(app, 'deployment') else None
        context['k8sinstance'] = app.k8sinstance if hasattr(app, 'k8sinstance') else None
        context['allowed_ips'] = app.allowed_ips.split(',') if app.allowed_ips else []
        context['general_config'] = GeneralConfig.get_instance()
        context['harbor_config'] = HarborConfig.get_instance()

        # Get role groups for the app
        viewers_group = app.approlegroup_set.filter(role='viewer').first()
        editors_group = app.approlegroup_set.filter(role='editor').first()
        context['viewers'] = viewers_group.users.all()
        context['editors'] = editors_group.users.all()
        context['viewer_usernames'] = [viewer.username for viewer in context['viewers']]
        context['editor_usernames'] = [editor.username for editor in context['editors']]

        # Get any ownership transfer pending request for the app
        transfer_request = None
        if TransferOwnershipRequest.objects.filter(app=app).exists():
            transfer_request = TransferOwnershipRequest.objects.get(app=app)
            transfer_request = transfer_request if not transfer_request.is_expired() else None
        context['transfer_request'] = transfer_request
        return context


# Invisible view: Delete an application with identifier specified in the url
class ApplicationDeleteView(View):
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> Union[JsonResponse, redirect]:
        identifier = kwargs.get('identifier', None)
        current_user = request.user

        if identifier:
            try:
                # Only owners can delete their applications
                application = Application.objects.get(\
                    identifier=identifier,\
                    owner=current_user)
                try:
                    deployment = Deployment.objects.get(application=application)
                    # Can only delete app config with deployment in 'None' state
                    if deployment.state != 0:
                        error_message = (
                            "Can only delete applications with a deployment in a None state. "
                            f"This application currently has a deployment in a {deployment.get_state()} state."
                        )
                        return JsonResponseBadRequest(error_message)

                    # Spawn a process to delete Harbor project for application
                    if not settings.TESTING:
                        harbor_path = application.get_terraform_log_path(type="harbor")
                        with open(f"{harbor_path}/harbor-tf.log", "w", encoding='utf-8') as log_file_handle:
                            # Create the registry using Terraform, capture the outputs in a log file
                            # Not using 'with' to avoid messing up the code structure
                            # pylint: disable-next=consider-using-with
                            subprocess.Popen(
                                (f"python3 {'harbor.py' if not settings.TESTING else 'strapper/harbor.py'} "
                                f"-a {identifier} {'-t 1' if settings.TESTING else ''} -d 1"),
                                stdout=log_file_handle,
                                stderr=subprocess.STDOUT,
                                shell=True
                            )

                    delete_name = application.descriptive_name
                    application.delete()
                    request.session['message'] = f'App <strong>{delete_name}</strong> deleted successfully'
                    request.session['success'] = True
                except Deployment.DoesNotExist:
                    if not settings.TESTING:
                        harbor_path = application.get_terraform_log_path(type="harbor")
                        with open(f"{harbor_path}/harbor-tf.log", "w", encoding='utf-8') as log_file_handle:
                            # Create the registry using Terraform, capture the outputs in a log file
                            # Not using 'with' to avoid messing up the code structure
                            # pylint: disable-next=R1732
                            subprocess.Popen(
                                (f"python3 {'harbor.py' if not settings.TESTING else 'strapper/harbor.py'} "
                                f"-a {identifier} {'-t 1' if settings.TESTING else ''} -d 1"),
                                stdout=log_file_handle,
                                stderr=subprocess.STDOUT,
                                shell=True
                            )

                    delete_name = application.descriptive_name
                    application.delete()
                    request.session['message'] = f'App <strong>{delete_name}</strong> deleted successfully'
                    request.session['success'] = True
            except Application.DoesNotExist:
                return JsonResponseForbidden("User is unauthorized to access this url")

        return redirect('/')


class ApplicationAPIView(View):
    '''
    Invisible view: Manage applications
    Can only access by AJAX or redirection
    '''
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> JsonResponse:
        '''
        A GET request to this view can be:
        - Scan all containers for an app
        '''
        # Check if the request is an AJAX call
        is_ajax = request.headers.get('x-requested-with') == 'XMLHttpRequest'

        identifier = kwargs.get('identifier', None)
        current_user = request.user

        # User must have at least 'viewer' access to the app to get the scan results
        app = None
        if identifier:
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound("Application does not exist")

            # Check if user has access to view the app
            user_role = app.get_user_role(current_user)
            if user_role == 'none' and not current_user.is_staff:
                return JsonResponseForbidden("User is unauthorized to access this url")

        # Scan all containers for an app
        if request.path == reverse('scan_containers_app', kwargs={'identifier': identifier}):

            # Scan all containers for the app
            report = app.scan_containers()

            # Get the latest version of the app instance
            app.refresh_from_db()

            # Response to AJAX request
            if is_ajax:
                return JsonResponseOK(
                    message=f"Scan all containers for app <strong>{app.descriptive_name}</strong> successfully",
                    report=report,
                    is_deployable=app.is_deployable()
                )

        # Invalid GET request path
        return JsonResponseBadRequest("Bad request. This URL doesn't support this action.")

# Displays an application log
class ApplicationLogView(EditableContentView):
    template_name = 'normal_users/app_log.html'

    def get_context_data(self, **kwargs: Dict[str, Any]) -> Dict[str, Any]:
        identifier = kwargs.get('identifier', None)
        current_user = self.request.user

        context = super().get_context_data(**kwargs)
        context = {**context, **get_apps_for_user_by_role(current_user, 'all')}

        # Check if app exists
        app = None
        try:
            app = Application.objects.get(identifier=identifier)
        except Application.DoesNotExist:
            context['status'] = 404
            context['log'] = 'Application does not exist'
            return context

        # User must have at least 'viewer' access to the app to view the log
        user_role = app.get_user_role(current_user)
        if user_role == 'none':
            context['status'] = 403
            context['log'] = 'User is unauthorized to access this url'
            return context
        context['user_role'] = user_role

        # App must have a deployment to have a log
        try:
            Deployment.objects.get(application=app)
        except Deployment.DoesNotExist:
            context['status'] = 404
            context['log'] = 'App is not deployed; therefore, has no log.'
            return context

        context['status'] = 200
        context['log'] = ''
        context['application'] = app
        return context


# Terminal view for users to run commands within an app pod
class ApplicationTerminalView(EditableContentView):
    template_name = 'normal_users/terminal.html'

    def get_context_data(self, **kwargs: Dict[str, Any]) -> Dict[str, Any]:
        """
        Display  a terminal console for an application
        """

        identifier = kwargs.get('identifier', None)
        current_user = self.request.user

        context = super().get_context_data(**kwargs)
        context = {**context, **get_apps_for_user_by_role(current_user, 'all')}

        # Check if app exists
        try:
            app = Application.objects.get(identifier=identifier)
        except Application.DoesNotExist:
            context['status'] = 404
            context['log'] = 'Application does not exist'
            return context

        # User must have at least 'viewer' access to the app to view the terminal
        user_role = app.get_user_role(current_user)
        if user_role == 'none':
            context['status'] = 403
            context['log'] = 'User is unauthorized to access this url'
            return context
        context['user_role'] = user_role

        # App must have a deployment to have a log
        try:
            Deployment.objects.get(application=app)
        except Deployment.DoesNotExist:
            context['status'] = 404
            context['log'] = 'App is not deployed; therefore, terminal is unavailable.'
            return context

        context["application"] = app
        context['status'] = 200
        context['log'] = \
(f'Terminal for {app.descriptive_name} is active. Type in the prompt below to run commands.'
' Press "Enter" when you want to execute a command.')
        return context

    # *args is not used in the function, therefore omitted
    # pylint: disable-next=arguments-differ
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> Union[JsonResponse, Dict[str, Any]]:
        """
        Execute commands input from the terminal console
        """

        # Check if the request is an AJAX call
        is_ajax = request.headers.get('x-requested-with') == 'XMLHttpRequest'

        if is_ajax:
            identifier = kwargs.get('identifier', None)
            current_user = request.user

            app = None
            # Check if app exists
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound("Application does not exist")

            # User must have at least 'viewer' access to the app to view the terminal
            user_role = app.get_user_role(current_user)
            if user_role == 'none':
                return JsonResponseForbidden("User is unauthorized to access this url")

            # App must have a deployment to have a log
            try:
                Deployment.objects.get(application=app)
            except Deployment.DoesNotExist:
                return JsonResponseNotFound(
                    "App is not deployed; therefore, terminal is unavailable.",
                    log="App is not deployed; therefore, terminal is unavailable."
                )

            command = request.GET.get("command", None)
            container_id = int(request.GET.get("container_id", 0))

            if not command or not container_id:
                return JsonResponseBadRequest("Bad request. Command and container id are required.")

            return JsonResponseOK("Command executed successfully", output= app.exec_command(command, container_id))

        return super().get(request, **kwargs)
