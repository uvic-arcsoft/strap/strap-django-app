# pylint:
from typing import Dict, Any

from django_editablecontent.views import EditableContentView

from normal_users.models import Application
from normal_users.helper_functions import get_apps_for_user_by_role

# Landing page for Strapper to display all published applications
class WelcomeView(EditableContentView):
    template_name = "normal_users/landings/welcome.html"

    def get_context_data(self, **kwargs: Dict[str, Any]) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)

        context['published_applications'] = Application.objects.filter(published_on_strap=True)
        context['owned_applications'] = []
        context['editor_applications'] = []
        context['viewer_applications'] = []

        # Populate owned applications if user is logged in
        if self.request.user.is_authenticated:
            current_user = self.request.user
            available_apps = get_apps_for_user_by_role(current_user, 'all')
            context = {**context, **available_apps}

        return context

# About ARCSoft and RCS page
class AboutTeam(EditableContentView):
    template_name = 'normal_users/landings/team.html'
    editable_content_names = ["about_team"]

    def get_context_data(self, **kwargs: Dict[str, Any]) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['owned_applications'] = []
        context['editor_applications'] = []
        context['viewer_applications'] = []

        if self.request.user.is_authenticated:
            current_user = self.request.user
            available_apps = get_apps_for_user_by_role(current_user, 'all')
            context = {**context, **available_apps}

        return context

# Default STRAP page
# Show this page if user visits a non-existent app in STRAP
class DefaultStrapView(EditableContentView):
    template_name = 'normal_users/landings/default_strap.html'

    def get_context_data(self, **kwargs: Dict[str, Any]) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['owned_applications'] = []
        context['editor_applications'] = []
        context['viewer_applications'] = []

        if self.request.user.is_authenticated:
            current_user = self.request.user
            available_apps = get_apps_for_user_by_role(current_user, 'all')
            context = {**context, **available_apps}
        return context
