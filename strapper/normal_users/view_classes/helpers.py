# pylint:
from django.http import JsonResponse, HttpRequest

from normal_users.models import Application, Deployment
from helpers.custom_json_response import (
    JsonResponseBadRequest, JsonResponseForbidden, JsonResponseNotFound, JsonResponseOK
)


# Helper views
# These views can be requested from templates for rendering
# but cannot be accessed directly and doesn't change any model
def deployment_app_info_view(request: HttpRequest, identifier: str) -> JsonResponse:
    """ Return the followings for an app deployment:
        - Databases
        - DB owner
        - DB initial password
    """
    # Check if the request is an AJAX call
    is_ajax = request.headers.get('x-requested-with') == 'XMLHttpRequest'

    if is_ajax:
        current_user = request.user

        # Check if the app exists
        app = None
        try:
            app = Application.objects.get(identifier=identifier)
        except Application.DoesNotExist:
            return JsonResponseNotFound('App does not exist')

        # User must either be the owner or an editor of the app to access this view
        role = app.get_user_role(current_user)
        if role not in ['owner', 'editor']:
            return JsonResponseForbidden('User is unauthorized to access this url')

        # Get the deployment for the app
        try:
            deployment = Deployment.objects.get(application=app)
        except Deployment.DoesNotExist:
            # return 404 if deploy not found
            return JsonResponseNotFound('There is no deployment for this app')

        # Return the database info
        return JsonResponseOK(
            message='Retrieved deployment info successfully',
            databases=deployment.databases,
            dbowner=deployment.db_owner,
            dbpassword=deployment.db_password,
            app_url=app.get_url()
        )

    return JsonResponseBadRequest("Can't access this URL directly")
