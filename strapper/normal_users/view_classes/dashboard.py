# pylint:
import json
import ipaddress
from typing import Union, Any, Dict

from django.shortcuts import redirect
from django.http import JsonResponse, HttpRequest
from django.urls import reverse

from django_editablecontent.views import EditableContentView

from admin_users.models import GeneralConfig
from access.models import TransferOwnershipRequest

from normal_users.models import Application, Group
from normal_users.forms import ApplicationForm
from normal_users.helper_functions import get_apps_for_user_by_role
from helpers.custom_json_response import (JsonResponseNotFound, JsonResponseForbidden)

# Users manage all their applications using this Dashboard
class Dashboard(EditableContentView):
    template_name = "normal_users/dashboard/index.html"

    def get_context_data(self, **kwargs: Dict[str, Any]) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        current_user = self.request.user

        # Get all applications that the user has a certain role in
        available_apps = get_apps_for_user_by_role(current_user, 'all')
        context = {**context, **available_apps}

        # Get all exisiting IDs. This is for ensuring users cannot input an exisiting id into
        # the app create form
        existing_identifiers = []
        for application in Application.objects.all():
            existing_identifiers.append(application.identifier)
        context['existing_identifiers'] = json.dumps(existing_identifiers)

        # Display message for editing and creating applications
        context['message'] = self.request.session.get('message', None)
        context['success'] = self.request.session.get('success', None)

        # Make the previsouly active app tab active again
        context['previously_active_app'] = self.request.session.get('previously_active_app', None)

        # Get all groups that this user owns
        context['owned_groups'] = Group.objects.filter(created_by=current_user)

        # Clear session message display if active
        if context['message']:
            del self.request.session['message']
            del self.request.session['success']

        if 'previously_active_app' in self.request.session:
            del self.request.session['previously_active_app']

        if 'active_tab' in self.request.session:
            # context['active_tab'] = self.request.session['active_tab']
            del self.request.session['active_tab']

        # Get the general configurations made by admins
        context['general_config'] = GeneralConfig.get_instance()

        # Get all non-expired app ownership transfer requests that this user has received
        context['received_transfer_requests'] = [
            transfer_request
            for transfer_request in TransferOwnershipRequest.objects.filter(receiver=current_user)
            if not transfer_request.is_expired()
        ]
        return context

    def post(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> Union[JsonResponse, redirect]:
        """
        Handle either creating or editing an application

        Creates a new application if no identifier is provided
        Edits an existing application if an identifier is provided
        """
        # Receive an identifier if an app is being edited
        identifier = kwargs.get('identifier', None)
        application_form = ApplicationForm(request.POST)
        current_user = request.user

        # Enforce checks for editing an app
        if identifier:
            # Check if the app exists
            app = None
            try:
                app = Application.objects.get(identifier=identifier)
            except Application.DoesNotExist:
                return JsonResponseNotFound("App does not exist")

            # User must be an owner or an editor of the app to edit it
            role = app.get_user_role(current_user)
            if role not in ['owner', 'editor']:
                return JsonResponseForbidden("User is unauthorized to edit this app")

        # R1705 disabled as this 'else' makes the code more understandable
        # pylint: disable=R1705
        # Form is valid
        if application_form.is_valid():
            form_data = application_form.cleaned_data
            new_application = None

            # In case of editing an application
            if identifier:
                new_application = Application.objects.get(identifier=identifier)
            # In case of creating a new application
            else:
                # If the identifier already exists
                if len(Application.objects.filter(identifier=form_data['identifier'])) > 0:
                    # Display message for failed app creation
                    request.session['message'] = "Attempt to create app failed. \
                    An app with this identifier already exists. \
                    For further support, please create an issue <a target='_blank' \
                    href='https://gitlab.com/uvic-arcsoft/strap-django-app/-/issues'>here</a>"
                    request.session['success'] = False
                    return redirect(request.META.get('HTTP_REFERER'))

                # If the identifier is one of the reserved app names
                # Display message for failed app creation
                general_config = GeneralConfig.get_instance()
                if form_data['identifier'] in general_config.get_reserved_app_names():
                    request.session['message'] = f"Attempt to create app failed. \
                    Identifier <strong>{form_data['identifier']}</strong> is a reserved name. \
                    For further support, please create an issue <a target='_blank' \
                    href='https://gitlab.com/uvic-arcsoft/strap-django-app/-/issues'>here</a>"
                    request.session['success'] = False
                    return redirect(request.META.get('HTTP_REFERER'))

                new_application = Application()
                new_application.identifier=form_data['identifier']

            new_application.descriptive_name=form_data['descriptive_name']
            new_application.description=form_data['description']
            new_application.authentication=form_data['authentication']
            new_application.published_on_strap=form_data['published_on_strap']

            if form_data['database']:
                new_application.database = form_data['database']
            if not identifier:
                new_application.owner=current_user

            # Set app's limit IP addresses/networks if any
            invalid_ips = set()
            if request.POST.get('ip_access', 'open') == 'open':
                new_application.allowed_ips = None
            else:
                allowed_ips = []

                # Add preset IPs that user selected
                general_config = GeneralConfig.get_instance()
                preset_ips = general_config.get_preset_ips()
                for name in preset_ips.keys():
                    if request.POST.get(name, "") == "on":
                        allowed_ips.extend(preset_ips[name])

                # Add other IPs that user selected
                if request.POST.get('other_ips', False):
                    allowed_ips.extend(request.POST.getlist('allowed_ips', []))

                # Remove invalid IPs
                filtered_allowed_ips = set()
                for ip in allowed_ips:
                    if ip.strip() == '':
                        continue
                    try:
                        ipaddress.ip_network(ip, strict=False)
                        filtered_allowed_ips.add(ip)
                    except ValueError:
                        invalid_ips.add(ip)
                        continue

                # Remove duplicates
                new_application.allowed_ips = ",".join(filtered_allowed_ips)

            new_application.save()

            # Clear all existing routes of app before readding
            # when editing
            if identifier:
                for route in new_application.route_set.all():
                    route.delete()

            # Construct new routes (authenticated and unauthenticated) and add to the application
            new_application.create_routes(form_data['authenticated_routes'].split(','), authenticated=True)
            new_application.create_routes(form_data['unauthenticated_routes'].split(','), authenticated=False)

            # Display message for successful app creation
            request.session['message'] =\
f"App <strong>{new_application.descriptive_name}</strong> {'created' if not identifier else 'edited'} successfully."
            request.session['success'] = True

            # Notify users of invalid IPs
            if invalid_ips:
                request.session['message'] += ("<br>The following IPs/networks were invalid and therefore, "
                                f"couldn't be added: <strong>{', '.join(invalid_ips)}</strong>")

            # Redirect to the app page
            return redirect(reverse('get_app', kwargs={'identifier': new_application.identifier}))

        # Form is invalid
        else:
            # Print out invalid input values for application form if any
            for field in application_form:
                print("Field Error of ApplicationForm:", field.name,  field.errors)

            # Display message for failed app creation
            request.session['message'] = f"Attempt to {'create' if not identifier else 'edit'} app failed. \
            App form was not filled out properly. This might due to an application error, which has \
            been logged. For further support, please create an issue <a target='_blank' \
            href='https://gitlab.com/uvic-arcsoft/strap-django-app/-/issues'>here</a>"
            request.session['success'] = False
            return redirect(request.META.get('HTTP_REFERER'))
        # pylint: enable=R1705
