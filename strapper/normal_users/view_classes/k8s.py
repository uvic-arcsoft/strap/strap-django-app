# pylint:
from typing import Union, Dict, Any
import os
import logging
import json
import environ
import requests

from django.views import View
from django.http import HttpRequest, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib.auth.models import User
from django.conf import settings

from normal_users.models import (Application, RequestStatusTypes, Notification,
                                 NotificationTypes, NotificationStatusTypes)
from normal_users.forms import K8sResourcesForm, K8sResourcesRequestForm

from helpers.custom_json_response import (JsonResponseBadRequest,
                JsonResponseNotFound, JsonResponseForbidden)

# Load environment variables
environ.Env.read_env()

logger = logging.getLogger(__name__)

class K8sResourcesView(View):
    def post(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> redirect:
        """
        Handle K8s resources updates
        """
        app_id = kwargs.get('identifier', None)
        if not app_id:
            return JsonResponseBadRequest('No app id provided')

        # Get the app
        app = None
        try:
            app = Application.objects.get(identifier=app_id)
        except Application.DoesNotExist:
            return JsonResponseNotFound('App not found')

        # User must have at least 'editor' access to update K8s resources
        user_role = app.get_user_role(request.user)
        if user_role not in ['editor', 'owner']:
            return JsonResponseForbidden('You are not authorized to update K8s resources for this app')

        # Validate the submitted request
        form = K8sResourcesForm(request.POST)
        if not form.is_valid():
            return JsonResponseBadRequest(form.errors)

        # Check if updated K8s resources are within the quota
        data = form.cleaned_data
        replicas = data['replicas']
        if not app.check_quota(replicas=replicas):
            return JsonResponseBadRequest('Quota exceeded')

        # Update the K8s resources
        app.replicas = replicas
        app.save()

        # Display success message
        self.request.session['success'] = True
        self.request.session['message'] = 'K8s resources updated successfully'

        return redirect(reverse('get_app', kwargs={'identifier': app_id}))


class K8sResourcesRequestView(View):
    def get(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> Union[redirect, JsonResponse]:
        """
        Handle requests for more K8s resources quota for apps
        """
        app_id = kwargs.get('identifier', None)
        if not app_id:
            return JsonResponseBadRequest('No app ID provided')

        # Get the app
        app = None
        try:
            app = Application.objects.get(identifier=app_id)
        except Application.DoesNotExist:
            return JsonResponseNotFound('App not found')

        # Handle requests for canceling K8s quota requests:
        if request.path == reverse('cancel_k8s_resource_request', kwargs={'identifier': app_id}):
            # User must have at least 'editor' access to cancel K8s resource requests
            user_role = app.get_user_role(request.user)
            if user_role not in ['editor', 'owner']:
                return JsonResponseForbidden('You are not authorized to cancel resource requests for this app')

            # Cancel the K8s resources request
            k8s_request = app.applicationquotarequest
            k8s_request.replicas = app.replicas
            k8s_request.justification = ''
            k8s_request.status = RequestStatusTypes.NONE
            k8s_request.save()

            # Set status of all request's related notifications to 'None'
            notifications = Notification.objects.filter(
                type=NotificationTypes.ApplicationQuotaRequest,
                related_model_pk=k8s_request.id
            )

            for notification in notifications:
                if notification.status != NotificationStatusTypes.NONE:
                    notification.status = NotificationStatusTypes.NONE
                    notification.save()

            # Display success message
            self.request.session['success'] = True
            self.request.session['message'] = 'K8s resource request canceled successfully'

            return redirect(reverse('get_app', kwargs={'identifier': app_id}))

        # Display the form to request more K8s resources
        return JsonResponseBadRequest('Invalid URL')

    def post(self, request: HttpRequest, **kwargs: Dict[str, Any]) -> redirect:
        """
        Handle requests for more K8s resources quota for apps
        """
        app_id = kwargs.get('identifier', None)
        if not app_id:
            return JsonResponseBadRequest('No app id provided')

        # Get the app
        app = None
        try:
            app = Application.objects.get(identifier=app_id)
        except Application.DoesNotExist:
            return JsonResponseNotFound('App not found')

        # User must have at least 'editor' access to request K8s resources
        user_role = app.get_user_role(request.user)
        if user_role not in ['editor', 'owner']:
            return JsonResponseForbidden('You are not authorized to request K8s resources for this app')

        # Validate the submitted request
        form = K8sResourcesRequestForm(request.POST)
        if not form.is_valid():
            return JsonResponseBadRequest(form.errors)

        # Check if it is a new request or an update to an existing request
        action = "new"      # "new" or "updated"
        k8s_request = app.applicationquotarequest
        if k8s_request.status == RequestStatusTypes.PENDING:
            action = "updated"

        # Create a new request that will be reviewed by the admins
        data = form.cleaned_data
        replicas = data['replicas']
        justification = data.get('justification', '')
        k8s_request.replicas=replicas
        k8s_request.justification=justification
        k8s_request.submitted_by=request.user
        k8s_request.status=RequestStatusTypes.PENDING
        k8s_request.save()

        # Notify the admins
        admins = User.objects.filter(is_staff=True)
        for admin in admins:
            Notification.objects.create(
                user=admin,
                html=f'{action.capitalize()} resource request for app <strong>{app.descriptive_name}</strong>',
                url=f"{reverse('admin_home_page')}?active_tab=k8s",
                type=NotificationTypes.ApplicationQuotaRequest,
                related_model_pk=k8s_request.id,
                status=NotificationStatusTypes.NEW
            )

        # Send a notification to Slack channel
        if not settings.TESTING:
            try:
                webhook_url = os.environ.get('WEBHOOK_URL')
                payload_raw = os.environ.get('RESOURCE_REQUEST_PAYLOAD')
                if payload_raw is None:
                    logger.warning("Could not read RESOURCE_REQUEST_PAYLOAD environment variable")
                else:
                    payload = payload_raw.format(
                        action=action.capitalize(),
                        app=app.descriptive_name,
                        user=request.user.username,
                        detailsUrl=(f"{request.scheme}://{request.get_host()}"
                                    f"{reverse('admin_home_page')}?active_tab=k8s"))
                    payload = json.loads(payload)
                    response = requests.post(
                                webhook_url,
                                data=json.dumps(payload),
                                headers={"Content-Type": "application/json"},
                                timeout=10)
                    response.raise_for_status()
            except Exception as e:
                logger.error("Failed to send a notification to Slack channel: %s", e)

        # Display success message
        self.request.session['success'] = True
        self.request.session['message'] =\
            "K8s resources request submitted successfully. Waiting for an admin's approval"

        return redirect(reverse('get_app', kwargs={'identifier': app_id}))
