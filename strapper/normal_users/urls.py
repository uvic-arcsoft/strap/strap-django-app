# pylint:
from django.urls import path
from . import views, webhook_views

urlpatterns = [
    # Dashboard
    path('', views.Dashboard.as_view(), name='dashboard'),

    # Landing pages
    path('welcome/', views.WelcomeView.as_view(), name='welcome'),
    path('team/', views.AboutTeam.as_view(), name='about_team'),
    path('notfound/', views.DefaultStrapView.as_view(), name='default_strap'),

    # Applications
    path('<slug:identifier>/', views.ApplicationView.as_view(), name='get_app'),
    path('edit/<slug:identifier>/', views.Dashboard.as_view(), name='edit_app'),
    path('delete/<slug:identifier>/', views.ApplicationDeleteView.as_view(), name='delete_app'),
    path('scan_containers/<slug:identifier>/', views.ApplicationAPIView.as_view(), name='scan_containers_app'),

    # K8s resources
    path('k8s_resources/<slug:identifier>/', views.K8sResourcesView.as_view(), name='update_k8s_resources'),
    path(
        'k8s_resources_request/<slug:identifier>/',
        views.K8sResourcesRequestView.as_view(),
        name='request_k8s_resources'
    ),
    path(
        'k8s_resources_request/cancel/<slug:identifier>/',
        views.K8sResourcesRequestView.as_view(),
        name='cancel_k8s_resource_request'
    ),

    # Containers
    path('container/create/<slug:identifier>/', views.ContainerAPIView.as_view(), name='create_container'),
    path('container/edit/<slug:identifier>/<int:container_id>/', views.ContainerAPIView.as_view(),
        name='edit_container'),
    path('container/delete/<slug:identifier>/<int:container_id>/', views.ContainerAPIView.as_view(),
        name='delete_container'),
    path('container/scan_report/<slug:identifier>/<int:container_id>/', views.ContainerScanReportView.as_view(),
        name='scan_report_container'),
    path('container/scan/<slug:identifier>/<int:container_id>/', views.ContainerScanView.as_view(),
        name='scan_container'),

    # Harbor projects
    path('harbor/create/<slug:identifier>/', views.HarborAPIView.as_view(), name='create_harbor_project'),
    path('harbor/get_images/<slug:identifier>/', views.HarborAPIView.as_view(), name='get_harbor_images'),

    # Groups
    path('group/create/<slug:identifier>/', views.GroupView.as_view(), name='create_group'),
    path('group/create/', views.GroupView.as_view(), name='create_group_to_app'),
    path('group/edit/<slug:identifier>/<int:group_id>/', views.GroupView.as_view(), name='edit_group'),
    path('group/edit/<int:group_id>/', views.GroupView.as_view(), name='edit_group'),
    path('group/delete/<slug:identifier>/<int:group_id>/', views.GroupView.as_view(), name='delete_group'),
    path('group/delete/<int:group_id>/', views.GroupView.as_view(), name='delete_group'),
    path('group/add_groups_to_app/<slug:identifier>/', views.GroupView.as_view(), name='add_groups_to_app'),

    # Deployments
    path('deploy/<slug:identifier>/', views.DeploymentView.as_view(), name='deploy_app'),
    path('deploy_log/<slug:identifier>/', views.DeploymentLogView.as_view(), name='deploy_log_app'),
    path('stop_deploy/<slug:identifier>/', views.StopDeploymentView.as_view(), name='stop_deploy_app'),

    # App logs and terminal
    path('app_log/<slug:identifier>/', views.ApplicationLogView.as_view(), name='app_log'),
    path('terminal/<slug:identifier>/', views.ApplicationTerminalView.as_view(), name='terminal'),


    # API endpoints for apps deployed by STRAP
    path('api/group/get_groups_for_user/<slug:identifier>/', views.GroupAPIView.as_view(), name='get_groups_for_user'),
    path('api/group/get_users_for_group/<slug:identifier>/', views.GroupAPIView.as_view(), name='get_users_for_group'),

    # Non-page urls, helper views/functions
    path('app_info/<slug:identifier>/', views.deployment_app_info_view, name='app_info'),

    # Notifications
    path('notification/<int:notification_id>/', views.NotificationView.as_view(), name='get_notification'),

    # Webhook
    path('webhook/registry/image_pushed/', webhook_views.image_pushed_view, name='webhook_image_pushed'),
    path(
        'webhook/registry/image_high_vulnerabilities/',
        webhook_views.image_high_vulnerabilities_notification,
        name='webhook_image_high_vulnerabilities'
    ),
]
