# pylint:
import sys
import os
import argparse
import json
import subprocess
import environ

from exceptions import DeploymentFailure

import django
django.setup()

# Change the line below if need to specify the path of .env file
# For example: environ.Env.read_env(os.path.join(BASE_DIR, '.env'))
environ.Env.read_env()

# TODO: Has to import this here as django.setup() needs to be called before
# Application model can be imported
# pylint: disable=wrong-import-position
from normal_users.models import Application, HarborProject
from admin_users.models import HarborConfig, ObjectStorageConfig, GeneralConfig
# pylint: enable=wrong-import-position

BASE_DIR = '/tmp/terraform'

# Get the application name from command line argument -a
parser = argparse.ArgumentParser(description='Fake running terraform')
parser.add_argument('-a', '--app', required=True, help='Specify app name to run terraform on')
parser.add_argument('-c', '--create', required=False, default=0,
    help='Set this to a non-zero value to create a harbor project for the app')
parser.add_argument('-d', '--destroy', required=False, default=0,
    help='Set this to a non-zero value to destroy a harbor project for the app')
parser.add_argument('-t', '--test', required=False, default=0,
    help='If set to a non-zero, the file is run in test mode. Default is 0')

args = vars(parser.parse_args())
app_id = args['app']

# Create mode
create = args['create']

# Destroy mode
destroy = args['destroy']

# Test mode
test = args['test']

# Fake run terraform in unit test environment
print("App id inside harbor.py: %s", app_id)
if test:
    print(f'Manage harbor project for app: {app_id}')

    # Fake create harbor project successfully
    if app_id == "successfulapp":
        print("Creating harbor project for successfulapp")

    # Fake create harbor project failed
    else:
        print("Creating harbor project failed")
        raise DeploymentFailure(description=f'Failed to create Harbor project for {app_id}')

    sys.exit(0)

# Harbor configurations made by admins
general_config = GeneralConfig.get_instance()
harbor_config = HarborConfig.get_instance()
object_storage_config_strap = ObjectStorageConfig.get_instance(object_storage_pk="strap")

harbor_path = f"{BASE_DIR}/{app_id}/harbor"

# create Terraform variable definitions file in JSON
testing = False

deploy_vars = {
    "source": harbor_config.harbor_module,
    "harbor_url": harbor_config.harbor_url,
    "harbor_username": harbor_config.harbor_username,
    "harbor_password": harbor_config.harbor_password,
    "domain": general_config.domain,
    "webhook_auth_header": os.environ.get("REGISTRY_WEBHOOK_SECRET"),
    "app_name": app_id
}
app_tfconfig = {
    "module": {
        "strap_harbor": deploy_vars
    },
    "output": {
        "registry": {
            "value": "${module.strap_harbor.registry}",
            "sensitive": True
        }
    }
}

# Cannot use "with" because temporary file has to exist when running
# Terraform and can only be deleted afterwards.
# pylint: disable=consider-using-with
config_file = open(f"{harbor_path}/app.tf.json", "w", encoding="utf-8")
print("Terraform config file: %s", config_file.name)
json.dump(app_tfconfig, config_file, ensure_ascii=False, indent=4)
config_file.close()

# Write Terraform config file for storing state files in S3 in Minio server
if os.environ.get("TERRAFORM_BACKEND_ENABLED", "False") == "True":
    # Export neccesary environment variables for Terraform backend
    # based on the configurations we have in Strapper
    # There environment variables only last for the current process and its subprocesses
    os.environ["AWS_ACCESS_KEY_ID"] = object_storage_config_strap.object_storage_access_key
    os.environ["AWS_SECRET_ACCESS_KEY"] = object_storage_config_strap.object_storage_secret_key
    os.environ["AWS_ENDPOINT_URL_S3"] = object_storage_config_strap.object_storage_url
    os.environ["AWS_REGION"] = object_storage_config_strap.object_storage_region

    backend_tfconfig = {
        "terraform": {
            "backend": {
                "s3": {
                    "bucket": object_storage_config_strap.object_storage_bucket,
                    "key": f"harbor/{app_id}.tfstate",
                    "skip_region_validation": "true",
                    "skip_requesting_account_id": "true",
                    "use_path_style": "true",
                    "skip_credentials_validation": "true",
                    "skip_metadata_api_check": "true",
                    "skip_s3_checksum": "true"
                }
            }
        }
    }
    backend_config_file = open(f"{harbor_path}/backend.tf.json", "w", encoding="utf-8")
    json.dump(backend_tfconfig, backend_config_file, ensure_ascii=False, indent=4)
    backend_config_file.close()

# if not (destroy or revert or stop or log):
# Use os.system here because there is no need to stop
# these processes when stopping a deployment as these processes
# don't make changes to Kubernetes resources
# Run 'terraform init'
init_result = os.system(f"cd {harbor_path} && terraform init -upgrade -force-copy")
# If init fails, try running with "-reconfigure" flag
if init_result != 0:
    print("Failed to initialize Terraform. Trying again with -reconfigure flag")
    init_result = os.system(f"cd {harbor_path} && terraform init -upgrade -force-copy -reconfigure")
    if init_result != 0:
        raise DeploymentFailure(description=f"Failed to initialize Terraform for {app_id}'s Harbor project")

# Validate terraform configs
os.system(f"cd {harbor_path} && terraform validate")

# TODO: "plan" should be set elsewhere.  This command is the basis for
# either the plan or apply operation (and maybe destroy at some point).
# "plan" should create a plan file and "apply" should use this file.
action = "plan" if testing else "apply"
auto_approve = "-auto-approve" if not testing else ""

if create:
    # Run "terraform apply"
    process = subprocess.Popen(
        ["terraform", f"-chdir={harbor_path}", action, "-no-color", "-input=false", auto_approve,
        f"-state=harbor/{app_id}.tfstate"]
    )

    process.wait(timeout=310)

    app = Application.objects.get(identifier=app_id)

    # Raise an error if creating harbor project fails
    if process.returncode != 0:
        raise DeploymentFailure(description=f'Failed to create Harbor project for {app.descriptive_name}')

    # Get robot account credentials from terraform output
    robot_account = subprocess.check_output(
        ["terraform", f"-chdir={harbor_path}", "output", "-json", "registry"]
    )
    robot_account = json.loads(robot_account)
    username = robot_account['account']['username']
    password = robot_account['account']['password']

    # Create a HarborProject instance
    HarborProject.objects.create(
        application=app,
        project_name=app_id,
        username=f"{harbor_config.harbor_robot_account_name_prefix}{username}+{username}",
        password=password
    )

    sys.exit()

if destroy:
    # Run "terraform destroy"
    process = subprocess.Popen(
        ["terraform", f"-chdir={harbor_path}", "destroy", "-no-color", "-input=false", auto_approve,
        f"-state=harbor/{app_id}.tfstate"]
    )

    process.wait(timeout=310)

    # Raise an error if destroying harbor project fails
    if process.returncode != 0:
        raise DeploymentFailure(description=f'Failed to destroy Harbor project for {app_id}')

    sys.exit()
