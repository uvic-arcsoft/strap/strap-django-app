# pylint:
from django.db import models
from django.contrib.auth.models import User

from normal_users.models import Application

# Create your models here.
class Wizard(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=False, blank=False)
    step = models.IntegerField(default=1, blank=True)
    is_active = models.BooleanField(default=False, blank=True)
    app = models.OneToOneField(Application, on_delete=models.CASCADE, null=True, blank=True)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'wizard'
        ordering = ['-last_updated', 'user']

    def __repr__(self) -> str:
        return str(self)

    def __str__(self) -> str:
        return (f"{self.user.username} - step {str(self.step)} - "
                f"{self.app.descriptive_name if self.app else 'No app'}")

    def cal_progress(self) -> int:
        """
        Calculate the progress percentage based on step to display
        on Bootstrap progress bar
        """
        return (self.step - 1) * 20

    def clear(self) -> None:
        """
        Clear the wizard
        """
        self.step = 1
        self.is_active = False
        self.app = None
        self.save()
