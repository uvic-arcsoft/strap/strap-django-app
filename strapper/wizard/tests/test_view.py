# pylint:

from django.test import TestCase, Client, override_settings
from django.contrib.auth.models import User
from django.urls import reverse
from wizard.models import Wizard

# pylint: disable=no-name-in-module
# TODO: Figure out why no-name-in-module is being raised
# when the code is working fine
from strapper.test_settings import TEST_SETTINGS
# pylint: enable=no-name-in-module

# Test cases for wizard views
@override_settings(**TEST_SETTINGS)
class WizardViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="user@example.org", password="testpassword")
        # Log the user in
        self.client = Client(HTTP_X_FORWARDED_USER="user@example.org")
        self.client.get(reverse("login"), follow=True)

    # Go through step by step of the wizard and test the result
    def test_wizard_workflow(self):
        # Visit getting started page
        response = self.client.get(reverse("getting_started"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "wizard/getting_started.html")

        context = response.context
        self.assertIn("owned_applications", context)
        self.assertIn("general_config", context)

        # Visit step 1
        response = self.client.get(reverse("wizard_view", args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "wizard/basics.html")

        # Step 1 should create a new default wizard
        wizard = Wizard.objects.get(user=self.user)
        self.assertEqual(wizard.step, 1)
        self.assertTrue(wizard.is_active)
        self.assertIsNone(wizard.app)

        # Create an app with step 1
        response = self.client.post(reverse("wizard_view", args=[1]), {
            "identifier": "testapp",
            "descriptive_name": "Test App",
            "description": "This is a test app",
            "published_on_strap": True
        }, follow=True)
        self.assertEqual(response.status_code, 200)

        # We should end up at step 2
        self.assertTemplateUsed(response, "wizard/authentication.html")
        wizard.refresh_from_db()
        self.assertEqual(wizard.step, 2)
        self.assertTrue(wizard.is_active)
        self.assertIsNotNone(wizard.app)

        # Check current app info
        app = wizard.app
        self.assertEqual(app.identifier, "testapp")
        self.assertEqual(app.descriptive_name, "Test App")
        self.assertEqual(app.description, "This is a test app")
        self.assertTrue(app.published_on_strap)

        # Create authentication with step 2
        response = self.client.post(reverse("wizard_view", args=[2]), {
            "authentication": "uvic_social",
            "unauthenticated_routes": "/login,/logout",
            "authenticated_routes": "/",
        }, follow=True)
        self.assertEqual(response.status_code, 200)

        # We should end up at step 3
        self.assertTemplateUsed(response, "wizard/database.html")
        wizard.refresh_from_db()
        self.assertEqual(wizard.step, 3)

        # Check current app info
        app = wizard.app
        self.assertEqual(app.authentication, "uvic_social")
        self.assertEqual(app.route_set.count(), 3)
        self.assertEqual(app.routes_to_str(authenticated=False), "/login,/logout")
        self.assertEqual(app.routes_to_str(authenticated=True), "/")

        # Create a database with step 3
        response = self.client.post(reverse("wizard_view", args=[3]), {
            "database": "PostgreSQL",
        }, follow=True)
        self.assertEqual(response.status_code, 200)

        # We should end up at step 4
        self.assertTemplateUsed(response, "wizard/registry.html")
        wizard.refresh_from_db()
        self.assertEqual(wizard.step, 4)

        # Check current app info
        app = wizard.app
        self.assertEqual(app.database, "PostgreSQL")

        # Visit step 5
        response = self.client.get(reverse("wizard_view", args=[5]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "wizard/containers.html")
        wizard.refresh_from_db()
        self.assertEqual(wizard.step, 5)

        # Visit step 6
        response = self.client.get(reverse("wizard_view", args=[6]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "wizard/review.html")
        wizard.refresh_from_db()
        self.assertEqual(wizard.step, 6)

        # Visit a different page should clear the wizard
        response = self.client.get(reverse("dashboard"))
        self.assertEqual(response.status_code, 200)

        wizard.refresh_from_db()
        self.assertEqual(wizard.step, 1)
        self.assertFalse(wizard.is_active)
        self.assertIsNone(wizard.app)
