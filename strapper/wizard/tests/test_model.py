# pylint:
from django.test import TestCase
from django.contrib.auth.models import User
from wizard.models import Wizard

from normal_users.models import Application

# Test cases for the Wizard model
class WizardModelTest(TestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(username="testuser1", password="testpassword")
        # Create a wizard without an app
        self.wizard_no_app = Wizard.objects.create(user=self.user1, step=1, is_active=False)

        # Create a wizard with an app
        self.user2 = User.objects.create_user(username="testuser2", password="testpassword")
        self.app = Application.objects.create(identifier="testapp", descriptive_name="Test App", owner=self.user2)
        self.wizard_with_app = Wizard.objects.create(user=self.user2, step=2, is_active=True, app=self.app)

    # Test creating an instance without an app
    def test_wizard_no_app_creation(self):
        self.assertTrue(isinstance(self.wizard_no_app, Wizard))
        self.assertEqual(str(self.wizard_no_app), "testuser1 - step 1 - No app")

    # Test creating an instance with an app
    def test_wizard_with_app_creation(self):
        self.assertTrue(isinstance(self.wizard_with_app, Wizard))
        self.assertEqual(str(self.wizard_with_app), "testuser2 - step 2 - Test App")

    # Test the __repr__ method
    def test_wizard_repr(self):
        self.assertEqual(repr(self.wizard_no_app), str(self.wizard_no_app))
        self.assertEqual(repr(self.wizard_with_app), str(self.wizard_with_app))

    # Test the cal_progress method
    # Calculate the progress percentage based on step to display
    # on Bootstrap progress bar
    def test_wizard_cal_progress(self):
        self.assertEqual(self.wizard_no_app.cal_progress(), 0)
        self.assertEqual(self.wizard_with_app.cal_progress(), 20)

    # Test the clear method
    # Clear the wizard
    def test_wizard_clear(self):
        self.wizard_no_app.clear()
        self.assertEqual(self.wizard_no_app.step, 1)
        self.assertFalse(self.wizard_no_app.is_active)
        self.assertIsNone(self.wizard_no_app.app)

        self.wizard_with_app.clear()
        self.assertEqual(self.wizard_with_app.step, 1)
        self.assertFalse(self.wizard_with_app.is_active)
        self.assertIsNone(self.wizard_with_app.app)
