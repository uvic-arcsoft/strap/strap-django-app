# pylint:
from django.urls import path
from . import views

urlpatterns = [
    path('getting_started', views.GettingStartedView.as_view(), name="getting_started"),
    path('step/<int:step>', views.wizard_view, name="wizard_view")
]
