# pylint:
import json

from django.http import HttpRequest
from django.http.response import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse

from django_editablecontent.views import EditableContentView
from admin_users.models import GeneralConfig
from normal_users.models import Application
from normal_users.forms import ApplicationForm
from .models import Wizard

def wizard_view(request, step):
    """
    A view that guides user through the process of using Strapper with the following steps:
    - Step 1: Basics
    - Step 2: Authentication
    - Step 3: Database
    - Step 4: Image registry
    - Step 5: Containers
    - Step 6: Review
    """
    step_views = {
        1: BasicsView,
        2: AuthenticationView,
        3: DatabaseView,
        4: RegistryView,
        5: ContainersView,
        6: ReviewView,
    }

    # Default to ReviewView if step is not in the dictionary
    view = step_views.get(step, ReviewView)

    return view.as_view()(request, step=step)


class GettingStartedView(EditableContentView):
    """
    A view that displays the getting started page
    """
    template_name = 'wizard/getting_started.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Get all applications that the current user owns
        user = self.request.user
        owned_applications = Application.objects.filter(owner=user)
        context['owned_applications'] = owned_applications

        # Get the general configurations made by admins
        context['general_config'] = GeneralConfig.get_instance()

        return context


class BaseStepView(EditableContentView):
    """
    Base class for the step views
    """
    user = None
    wizard = None

    def setup(self, request: HttpRequest, *args, **kwargs) -> None:
        """
        Initialize attributes for the view
        """
        if not self.user:
            self.user = request.user
        # If user has never started the wizard, create a new wizard object
        if not self.wizard:
            if not Wizard.objects.filter(user=self.user).exists():
                Wizard.objects.create(user=self.user, is_active=True)
            self.wizard = Wizard.objects.get(user=self.user)

        return super().setup(request, *args, **kwargs)

    def dispatch(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        # Set wizard as active
        if not self.wizard.is_active:
            self.wizard.is_active = True
            self.wizard.save()

        # For all subsequent steps, there must be an app created using the wizard in step 1
        step = kwargs.get('step', 1)
        if step > 1 and not self.wizard.app:
            request.session['message'] = 'Please create an application first'
            request.session['success'] = False
            return redirect(reverse('wizard_view', kwargs={'step': 1}))

        # Mark the current step as completed if the previous step is completed
        if step > 1 and self.wizard.step == step - 1:
            self.wizard.step = step
            self.wizard.save()

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['active_step'] = kwargs.get('step', 1)

        # Get the current state of the wizard
        context['wizard'] = self.wizard

        # Get all applications that the current user owns
        owned_applications = Application.objects.filter(owner=self.user)
        context['owned_applications'] = owned_applications

        # Get all exisiting IDs. This is for ensuring users cannot input an exisiting id into
        # the app create form
        existing_identifiers = []
        for application in Application.objects.all():
            existing_identifiers.append(application.identifier)
        context['existing_identifiers'] = json.dumps(existing_identifiers)

        # Display message for editing and creating applications
        context['message'] = self.request.session.get('message', None)
        context['success'] = self.request.session.get('success', None)

        # Clear the message and success from the session
        if 'message' in self.request.session:
            del self.request.session['message']
            del self.request.session['success']

        # Get the general configurations made by admins
        context['general_config'] = GeneralConfig.get_instance()

        # User will be the owner of the app that is being created
        context['user_role'] = 'owner'

        return context

class BasicsView(BaseStepView):
    """
    Step 1: Basics
    """
    template_name = 'wizard/basics.html'

    # NOTE: Must define the arguments as they receive a step argument
    # from wizard_view
    # pylint: disable-next=unused-argument
    def post(self, request, *args, **kwargs):
        """
        Process the application form
        """
        # Get the application form
        form = None
        if self.wizard.app:
            form = ApplicationForm(request.POST, instance=self.wizard.app)
        else:
            form = ApplicationForm(request.POST)

        if form.is_valid():
            # Save the form
            app = form.save(commit=False)

            # Create a new app
            if form.cleaned_data['identifier']:
                app.identifier = form.cleaned_data['identifier']

            app.owner = self.user
            app.save()

            # Add the application to the wizard
            if not self.wizard.app:
                self.wizard.app = app

            self.wizard.save()

            # Set the success message for app creation
            if form.cleaned_data['identifier']:
                request.session['message'] = f'App {self.wizard.app.descriptive_name} created successfully'
                request.session['success'] = True

            # Set success message for app edition
            else:
                request.session['message'] = f'App {self.wizard.app.descriptive_name} edited successfully'
                request.session['success'] = True

            return redirect(reverse('wizard_view', kwargs={'step': 2}))

        # Set the error message for app creation
        if not request.POST.get('identifier', None):
            request.session['message'] = f"Error creating application: {form.errors}"
            request.session['success'] = False

        # Set the error message for app edition
        else:
            request.session['message'] = f"Error editing application: {form.errors}"
            request.session['success'] = False

        return redirect(reverse('wizard_view', kwargs={'step': 1}))

class AuthenticationView(BaseStepView):
    """
    Step 2: Authentication
    """
    template_name = 'wizard/authentication.html'

    # NOTE: Must define the arguments as they receive a step argument
    # from wizard_view
    # pylint: disable-next=unused-argument
    def post(self, request, *args, **kwargs):
        """
        Process the authentication form
        """
        # Save authentication method
        authentication = request.POST.get('authentication', None)
        self.wizard.app.authentication = authentication
        self.wizard.app.save()

        # Clear all existing routes of app before readding
        # when editing
        self.wizard.app.route_set.all().delete()

        # Save authenticated routes
        for route in request.POST.get('authenticated_routes', '').split(','):
            if route and not self.wizard.app.has_route(route):
                self.wizard.app.route_set.create(route=route, authenticated=True)

        # Save unauthenticated routes
        for route in request.POST.get('unauthenticated_routes', '').split(','):
            if route and not self.wizard.app.has_route(route):
                self.wizard.app.route_set.create(route=route, authenticated=False)

        # Set the success message
        request.session['message'] = 'Authentication settings saved successfully'
        request.session['success'] = True

        # Move to the next step
        return redirect(reverse('wizard_view', kwargs={'step': 3}))


class DatabaseView(BaseStepView):
    """
    Step 3: Database
    """
    template_name = 'wizard/database.html'

    # NOTE: Must define the arguments as they receive a step argument
    # from wizard_view
    # pylint: disable-next=unused-argument
    def post(self, request, *args, **kwargs):
        """
        Process the database form
        """
        # Save database method
        database = request.POST.get('database', None)
        self.wizard.app.database = database
        self.wizard.app.save()

        # Set the success message
        request.session['message'] = 'Database settings saved successfully'
        request.session['success'] = True

        # Move to the next step
        return redirect(reverse('wizard_view', kwargs={'step': 4}))

class RegistryView(BaseStepView):
    """
    Step 4: Image registry
    """
    template_name = 'wizard/registry.html'

class ContainersView(BaseStepView):
    """
    Step 5: Containers
    """
    template_name = 'wizard/containers.html'

class ReviewView(BaseStepView):
    """
    Step 6: Review
    """
    template_name = 'wizard/review.html'
