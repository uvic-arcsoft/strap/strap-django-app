# pylint:
import os
import environ

from django.http import HttpResponseNotFound
from django.urls import reverse

# Load environment variables
environ.Env.read_env()

class BlockExternalUrlsMiddleware():
    """
    This middleware returns a 404 response for all non-Strapper URLs except for the notfound page
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if (os.environ.get('APP_HOST', 'strapper.example.org') not in request.get_host()
            and request.path != reverse('default_strap')):
            return HttpResponseNotFound("Page not found")

        response = self.get_response(request)
        return response
