# pylint:
from django.conf import settings

from wizard.models import Wizard

# Exclude paths from the middleware
EXCLUDED_PATHS = [
    "/login",
    "/logout",
    "/admin-django",
    "/api",
    "/webhook",
    "/wizard",
    "/content",
    "/fileupload",
    "/harbor/get_images",
    settings.STATIC_URL,
    settings.MEDIA_URL if settings.MEDIA_URL else "/media",
    "/favicon.ico"
]

class WizardMiddleware():
    """
    Clear the wizard when visiting pages that are not part of
    the excluded paths
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Clear the wizard when visiting pages that are not part of
        # the excluded paths
        if (not any(path in request.path for path in EXCLUDED_PATHS)
        and request.user.is_authenticated):
            user = request.user
            if (request.method == "GET"
                and Wizard.objects.filter(user=user).exists() and user.wizard.is_active):
                user.wizard.clear()

        response = self.get_response(request)

        return response
