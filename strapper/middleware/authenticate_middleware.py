# pylint:
from django.shortcuts import redirect
from django.conf import settings
from django.urls import reverse

# This paths are unaffected by the middleware
ADMIN_PATH = "/admin-django/"
NON_LOGIN_REQUIRED_PATHS = [
    reverse('login'),
    reverse('logout'),
    reverse('welcome'),
    reverse('about_team'),
    reverse('default_strap'),
]

class StarterAuthenticateMiddleware():
    """
    This middleware checks if user is authenticated before every view.
    An unauthenticated user will be redirected to the login page
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # redirect user to login page if user is not authenticated
        login_required = (request.path not in NON_LOGIN_REQUIRED_PATHS
            and ADMIN_PATH not in request.path
            and 'help' not in request.path
            and 'api' not in request.path
            and 'webhook' not in request.path
            and not settings.EDITABLECONTENT_TESTING
            and not request.user.is_authenticated)
        if login_required:
            return redirect(reverse('welcome'))


        response = self.get_response(request)

        return response
