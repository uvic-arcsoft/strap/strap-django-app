# pylint:
from django.contrib.auth.models import User

from django_editablecontent.views import EditableContentView

from normal_users.models import Application

# Using Strapper page
class UsingStrapperPage(EditableContentView):
    template_name = 'help/using_strapper.html'
    editable_content_names = ["using_strapper"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['owned_applications'] = []
        if self.request.session.get('user', None):
            current_user = User.objects.get(id=int(self.request.session.get('user', '-1')))

            # Get all applications that the current user owns
            owned_applications = Application.objects.filter(owner=current_user)
            context['owned_applications'] = owned_applications
        return context


# About STRAP page
class AboutStrapPage(EditableContentView):
    template_name = 'help/about_strap.html'
    editable_content_names = ["about_strap"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['owned_applications'] = []
        if self.request.session.get('user', None):
            current_user = User.objects.get(id=int(self.request.session.get('user', '-1')))

            # Get all applications that the current user owns
            owned_applications = Application.objects.filter(owner=current_user)
            context['owned_applications'] = owned_applications
        return context
