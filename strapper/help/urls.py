# pylint:
from django.urls import path
from . import views

urlpatterns = [
    path('using_strapper', views.UsingStrapperPage.as_view(), name="help_using_strapper"),
    path('about_strap/', views.AboutStrapPage.as_view(), name="help_about_strap"),
]
