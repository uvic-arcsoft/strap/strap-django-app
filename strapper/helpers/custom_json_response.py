# pylint:
from http import HTTPStatus
from django.http import JsonResponse

class JsonResponseBase(JsonResponse):
    def __init__(self, message: str, http_status: HTTPStatus, **kwargs):
        response_data = {
            'status': http_status.value,
            'title': http_status.phrase,
            'message': message,
        }
        response_data.update(kwargs)
        super().__init__(data=response_data, status=http_status.value)

class JsonResponseBadRequest(JsonResponseBase):
    def __init__(self, message: str, **kwargs):
        super().__init__(message, HTTPStatus.BAD_REQUEST, **kwargs)

class JsonResponseNotFound(JsonResponseBase):
    def __init__(self, message: str, **kwargs):
        super().__init__(message, HTTPStatus.NOT_FOUND, **kwargs)

class JsonResponseForbidden(JsonResponseBase):
    def __init__(self, message: str, **kwargs):
        super().__init__(message, HTTPStatus.FORBIDDEN, **kwargs)

class JsonResponseUnauthorized(JsonResponseBase):
    def __init__(self, message: str, **kwargs):
        super().__init__(message, HTTPStatus.UNAUTHORIZED, **kwargs)

class JsonResponseOK(JsonResponseBase):
    def __init__(self, message: str, **kwargs):
        super().__init__(message, HTTPStatus.OK, **kwargs)

class JsonResponseCreated(JsonResponseBase):
    def __init__(self, message: str, **kwargs):
        super().__init__(message, HTTPStatus.CREATED, **kwargs)

class JsonResponseInternalServerError(JsonResponseBase):
    def __init__(self, message: str, **kwargs):
        super().__init__(message, HTTPStatus.INTERNAL_SERVER_ERROR, **kwargs)
        