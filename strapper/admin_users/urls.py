# pylint:
from django.urls import path
from . import views

urlpatterns = [
    path('', views.AdminHomePage.as_view(), name="admin_home_page"),
    path(
        'general_config/update',
        views.GeneralConfigAPIView.as_view(),
        name="update_general_config"
    ),
    path(
        'database_config/update/<slug:database_pk>',
        views.DatabaseConfigAPIView.as_view(),
        name="update_database_config"
    ),
    path(
        'keycloak_config/update',
        views.KeycloakConfigAPIView.as_view(),
        name="update_keycloak_config"
    ),
    path(
        'harbor_config/update',
        views.HarborConfigAPIView.as_view(),
        name="update_harbor_config"
    ),
    path(
        'harbor_config/test_email',
        views.HarborConfigAPIView.as_view(),
        name='send_test_email'
    ),
    path(
        'apps_scan_reports',
        views.AppsScanReportView.as_view(),
        name='apps_scan_reports'
    ),
    path(
        'object_storage_config/update/<slug:object_storage_pk>',
        views.ObjectStorageConfigAPIView.as_view(),
        name="update_object_storage_config"
    ),
    path(
        'k8s_config/update',
        views.K8sConfigAPIView.as_view(),
        name="update_k8s_config"
    ),
    path(
        'process_k8s_resource_request/<int:request_id>',
        views.K8sConfigAPIView.as_view(),
        name="process_k8s_resource_request"
    ),
]
