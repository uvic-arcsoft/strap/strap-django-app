# pylint: disable=unused-import, too-many-ancestors
# unused-import: disabled this import is default it sets the page up
# too-many-ancestors: from inheriting from Django's model classes
import json
from typing import Dict, List
import base64
import requests

from requests.exceptions import HTTPError

from django.conf import settings
from django.db import models

# Base class for defining a singleton model
# We cannot create instances of this class, but we can inherit from it to create singleton models
class SingletonModel(models.Model):
    class Meta:
        abstract = True

    @classmethod
    def get_instance(cls):
        instance, _ = cls.objects.get_or_create(pk=1)
        return instance

    def save(self, *args, **kwargs):
        self.pk = 1
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass  # Prevent deletion of the singleton instance


# General configurations for STRAPPER
# There can ONLY be 1 instance for this model
class GeneralConfig(SingletonModel):
    # Reserved names are separated by new lines and stripped all whitespaces
    reserved_app_names = models.CharField(max_length=5000, null=True, blank=True)
    terraform_module = models.CharField(max_length=400, null=True, blank=True)
    helm_chart = models.CharField(max_length=400, null=True, blank=True)
    domain = models.CharField(max_length=100, null=True, blank=True)

    # JSON string of a dictionary with keys as names and values as lists of IP addresses/networks
    # Example: json.dumps({
    #   "My VPN": ["0.0.0.0/0", "1.1.1.1/1"],
    #   "Bob's workstation": ["2.2.2.2/2"]
    # })
    preset_ips = models.CharField(max_length=1000, null=True, blank=True)

    class Meta:
        db_table = "general_config"

    def save(self, *args, **kwargs):
        # Strip all whitespaces from reserved app names
        if self.reserved_app_names:
            self.reserved_app_names = "\n".join([name.strip() for name in self.reserved_app_names.split("\n")])
        super().save(*args, **kwargs)

    def __repr__(self) -> str:
        return "General configurations"

    def __str__(self) -> str:
        return "General configurations"

    # Return all reserved app names as a list
    def get_reserved_app_names(self) -> List[str]:
        return self.reserved_app_names.split("\n") if self.reserved_app_names else []

    # Return all preset IPs as a dictionary
    def get_preset_ips(self) -> Dict[str, List[str]]:
        if not self.preset_ips:
            self.preset_ips = json.dumps({})
            self.save()
            return {}

        try:
            return json.loads(self.preset_ips)
        except json.JSONDecodeError:
            self.preset_ips = json.dumps({})
            self.save()
            return {}


# Database configurations for STRAP
# There can ONLY be 2 instances for this model: one with pk "psql" and one with pk "mysql"
class DatabaseConfig(models.Model):
    supported_databases = ["psql", "mysql"]

    # Explicitly define the primary key to make it a string instead of an integer (default)
    database_pk = models.CharField(max_length=20, primary_key=True)
    database_name = models.CharField(max_length=100, null=True, blank=True)
    database_username = models.CharField(max_length=100, null=True, blank=True)
    database_host = models.CharField(max_length=100, null=True, blank=True)
    database_password = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        db_table = "database_config"

    def __repr__(self) -> str:
        if self.database_pk == "psql":
            return "Database configurations for PostgreSQL"
        return "Database configurations for MySQL"

    def __str__(self) -> str:
        if self.database_pk == "psql":
            return "Database configurations for PostgreSQL"
        return "Database configurations for MySQL"

    @classmethod
    def get_instance(cls, database_pk: str):
        if database_pk not in cls.supported_databases:
            raise ValueError("Invalid primary key for DatabaseConfig. Must be either 'psql' or 'mysql'")
        instance, _ = cls.objects.get_or_create(database_pk=database_pk)
        return instance

    def save(self, *args, **kwargs):
        if self.database_pk not in self.supported_databases:
            raise ValueError("Invalid primary key for DatabaseConfig. Must be either 'psql' or 'mysql'")
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass    # Prevent deletion


# Keycloak configurations for STRAP
# There can ONLY be 1 instance for this model
class KeycloakConfig(SingletonModel):
    keycloak_url = models.CharField(max_length=200, null=True, blank=True)
    keycloak_base_path = models.CharField(max_length=100, null=True, blank=True)
    keycloak_client_id = models.CharField(max_length=200, null=True, blank=True)
    keycloak_client_secret = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        db_table = "keycloak_config"

    def __repr__(self) -> str:
        return "Keycloak configurations"

    def __str__(self) -> str:
        return "Keycloak configurations"

class Severities(models.TextChoices):
    CRITICAL = 'Critical', 'Critical'
    HIGH = 'High', 'High'
    MEDIUM = 'Medium', 'Medium'
    LOW = 'Low', 'Low'
class RescanIntervalTypes(models.TextChoices):
    NONE = 'None', 'None'
    HOURLY = 'Hourly', 'Hourly'
    DAILY = 'Daily', 'Daily'
    WEEKLY = 'Weekly', 'Weekly'

# Harbor configurations for STRAP
class HarborConfig(SingletonModel):
    cron_intervals = {
        'None': '0 0 0 * * *',
        'Hourly': '0 0 * * * *',
        'Daily': '0 0 0 * * *',
        'Weekly': '0 0 0 * * 0'
    }

    # Harbor instance
    harbor_module = models.CharField(max_length=400, null=True, blank=True)
    harbor_url = models.CharField(max_length=200, null=True, blank=True)
    harbor_username = models.CharField(max_length=100, null=True, blank=True)
    harbor_password = models.CharField(max_length=100, null=True, blank=True)
    harbor_robot_account_name_prefix = models.CharField(max_length=100, blank=True, default="robot$")

    # Container scanning
    severity_threshold = models.CharField(max_length=20, default="High", blank=True, choices=Severities.choices)
    unknown_criticality = models.CharField(max_length=20, default="High", blank=True, choices=Severities.choices)
    rescan_interval = models.CharField(
        max_length=20, default=RescanIntervalTypes.NONE, blank=True, choices=RescanIntervalTypes.choices)
    vuln_notification_email_template = models.TextField(blank=True)

    class Meta:
        db_table = "harbor_config"

    def save(self, *args, **kwargs):
        if not self.harbor_robot_account_name_prefix:
            self.harbor_robot_account_name_prefix = "robot$"  # Default value

        super().save(*args, **kwargs)

        # Update remote registry scan all interval
        if not settings.TESTING and self.harbor_url and self.harbor_username and self.harbor_password:
            headers = self.build_authz_headers()
            update_scan_schedule_url = f"{self.harbor_url}/api/v2.0/system/scanAll/schedule"
            try:
                response = requests.put(
                    update_scan_schedule_url,
                    headers=headers,
                    json={
                        "schedule": {
                            "type": self.rescan_interval,
                            "cron": self.cron_intervals[self.rescan_interval]
                        }
                    },
                    timeout=10
                )
                response.raise_for_status()
            except HTTPError as http_err:
                print(f"HTTP error occurred: {http_err}")
                raise

    def __repr__(self) -> str:
        return "Harbor configurations"

    def __str__(self) -> str:
        return "Harbor configurations"

    def get_severity_levels(self) -> Dict[str, int]:
        levels = {
            'None': 0,
            'Low': 1,
            'Medium': 2,
            'High': 3,
            'Critical': 4
        }
        levels['Unknown'] = levels[self.unknown_criticality]
        return levels

    # Build authorization headers for Harbor API
    def build_authz_headers(self):
        # Headers for authorization with Harbor API
        token = base64.b64encode(
            bytes(
                f"{self.harbor_username}:{self.harbor_password}", 
            'ascii'))
        authz_header = f"Basic {token.decode('ascii')}"
        return {
            'Authorization': authz_header,
            'Content-Type': 'application/json',
            'accept': 'application/json'
        }


# Object storage configurations for STRAP
# There can ONLY be 2 instances for this model: one with pk "strap" and one with pk "strapplications"
class ObjectStorageConfig(models.Model):
    # Explicitly define the primary key to make it a string instead of an integer (default)
    object_storage_pk = models.CharField(max_length=20, primary_key=True)

    # NOTE:
    # - Bucket for STRAP is used as a bucket to store terraform state files
    # - Bucket for STRAP applications is used as a bucket prefix
    # for the buckets set up by Terraform to store application files
    object_storage_bucket = models.CharField(max_length=100, null=True, blank=True)
    object_storage_region = models.CharField(max_length=100, null=True, blank=True)
    object_storage_url = models.CharField(max_length=200, null=True, blank=True)
    object_storage_access_key = models.CharField(max_length=100, null=True, blank=True)
    object_storage_secret_key = models.CharField(max_length=150, null=True, blank=True)

    class Meta:
        db_table = "object_storage_config"

    def __repr__(self) -> str:
        if self.object_storage_pk == "strap":
            return "Object storage configurations for STRAP"
        return "Object storage configurations for STRAP applications"

    def __str__(self) -> str:
        if self.object_storage_pk == "strap":
            return "Object storage configurations for STRAP"
        return "Object storage configurations for STRAP applications"

    @classmethod
    def get_instance(cls, object_storage_pk: str):
        if object_storage_pk not in ["strap", "strapplications"]:
            raise ValueError("Invalid primary key for ObjectStorageConfig. Must be either 'strap' or 'strapplications'")
        instance, _ = cls.objects.get_or_create(object_storage_pk=object_storage_pk)
        return instance

    def save(self, *args, **kwargs):
        if self.object_storage_pk not in ["strap", "strapplications"]:
            raise ValueError("Invalid primary key for ObjectStorageConfig. Must be either 'strap' or 'strapplications'")
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass  # Prevent deletion


# K8s configurations for STRAP
# There can ONLY be 1 instance for this model
class K8sConfig(SingletonModel):
    k8s_server_url = models.CharField(max_length=200, null=True, blank=True)
    k8s_client_key = models.TextField(null=True, blank=True)
    k8s_client_certificate = models.TextField(null=True, blank=True)

    class Meta:
        db_table = "k8s_config"

    def __repr__(self) -> str:
        return "K8s configurations"

    def __str__(self) -> str:
        return "K8s configurations"
