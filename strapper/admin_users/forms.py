# pylint: disable=too-many-ancestors
# too-many-ancestors: We use Django's built-in classes, which already have this issue
from django import forms
from django.db import models
from .models import GeneralConfig, DatabaseConfig, KeycloakConfig, HarborConfig, ObjectStorageConfig, K8sConfig

# Form to process general configurations
class GeneralConfigForm(forms.ModelForm):
    class Meta:
        model = GeneralConfig
        fields = '__all__'
        exclude = ['preset_ips']

# Form to process database configurations
class DatabaseConfigForm(forms.ModelForm):
    class Meta:
        model = DatabaseConfig
        fields = '__all__'
        exclude = ['database_pk']

# Form to process keycloak configurations
class KeycloakConfigForm(forms.ModelForm):
    class Meta:
        model = KeycloakConfig
        fields = '__all__'

# Form to process harbor configurations
class HarborConfigForm(forms.ModelForm):
    class Meta:
        model = HarborConfig
        fields = '__all__'

# Form to process object storage configurations
class ObjectStorageConfigForm(forms.ModelForm):
    class Meta:
        model = ObjectStorageConfig
        fields = '__all__'
        exclude = ['object_storage_pk']

# Form to process k8s configurations
class K8sConfigForm(forms.ModelForm):
    class Meta:
        model = K8sConfig
        fields = '__all__'

class K8sResourceRequestProcessActions(models.TextChoices):
    APPROVE = 'approve', 'approve'
    REJECT = 'reject', 'reject'

# Form to process K8s resource request approvals or rejections
class K8sResourceRequestProcessForm(forms.Form):
    replicas = forms.IntegerField(min_value=1, max_value=10, required=True)
    reply = forms.CharField(max_length=500, required=False)
    action = forms.ChoiceField(
        choices=K8sResourceRequestProcessActions.choices,
        required=True)
