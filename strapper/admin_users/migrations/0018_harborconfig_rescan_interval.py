# Generated by Django 5.0.3 on 2024-03-28 20:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('admin_users', '0017_alter_harborconfig_severity_threshold'),
    ]

    operations = [
        migrations.AddField(
            model_name='harborconfig',
            name='rescan_interval',
            field=models.IntegerField(blank=True, default=60),
        ),
    ]
