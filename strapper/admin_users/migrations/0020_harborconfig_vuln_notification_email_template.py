# Generated by Django 5.0.7 on 2024-08-14 23:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('admin_users', '0019_harborconfig_harbor_robot_account_name_prefix'),
    ]

    operations = [
        migrations.AddField(
            model_name='harborconfig',
            name='vuln_notification_email_template',
            field=models.TextField(blank=True),
        ),
    ]
