# Generated by Django 5.0.3 on 2024-03-21 18:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('admin_users', '0009_objectstorageconfig'),
    ]

    operations = [
        migrations.RenameField(
            model_name='objectstorageconfig',
            old_name='object_storage_password',
            new_name='object_storage_access_key',
        ),
        migrations.RemoveField(
            model_name='objectstorageconfig',
            name='object_storage_username',
        ),
        migrations.AddField(
            model_name='objectstorageconfig',
            name='object_storage_secret_key',
            field=models.CharField(blank=True, max_length=150, null=True),
        ),
    ]
