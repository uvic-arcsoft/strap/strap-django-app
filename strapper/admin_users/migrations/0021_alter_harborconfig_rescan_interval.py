# Generated by Django 5.0.8 on 2024-10-04 18:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('admin_users', '0020_harborconfig_vuln_notification_email_template'),
    ]

    operations = [
        migrations.AlterField(
            model_name='harborconfig',
            name='rescan_interval',
            field=models.CharField(blank=True, choices=[('None', 'None'), ('Hourly', 'Hourly'), ('Daily', 'Daily'), ('Weekly', 'Weekly')], default='None', max_length=20),
        ),
    ]
