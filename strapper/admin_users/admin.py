# pylint: disable=W0611
# W0611 disabled this import is default it sets the page up
from django.contrib import admin
from .models import GeneralConfig, DatabaseConfig, KeycloakConfig, HarborConfig, ObjectStorageConfig, K8sConfig

# Register your models here.
admin.site.register(GeneralConfig)
admin.site.register(DatabaseConfig)
admin.site.register(KeycloakConfig)
admin.site.register(HarborConfig)
admin.site.register(ObjectStorageConfig)
admin.site.register(K8sConfig)
