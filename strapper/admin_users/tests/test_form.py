# pylint: disable=W0611
# W0611 disabled this import is default it sets the page up
from django.test import TestCase
from admin_users.forms import (GeneralConfigForm, DatabaseConfigForm,
                                KeycloakConfigForm, HarborConfigForm,
                                ObjectStorageConfigForm, K8sConfigForm)

# Test cases for GeneralConfigForm
class GeneralConfigFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = GeneralConfigForm({
            'reserved_app_names': 'app1,app2,app3',
            'terraform_module': 'module1',
            'helm_chart': 'chart1',
            'domain': 'test.example.com',
        })
        self.assertEqual(form.is_valid(), True)

# Test cases for DatabaseConfigForm
class DatabaseConfigFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = DatabaseConfigForm({
            'database_pk': 'psql',
            'database_name': 'mydb',
            'database_username': 'myuser',
            'database_host': 'localhost',
            'database_password': 'mypassword',
        })
        self.assertEqual(form.is_valid(), True)

# Test cases for KeycloakConfigForm
class KeycloakConfigFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = KeycloakConfigForm({
            'keycloak_url': 'http://localhost:8080/auth',
            'keycloak_baswe_path': '',
            'keycloak_client_id': 'myclient',
            'keycloak_client_secret': 'mysecret',
        })
        self.assertEqual(form.is_valid(), True)

# Test cases for HarborConfigForm
class HarborConfigFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = HarborConfigForm({
            'harbor_module': 'module1',
            'harbor_url': 'http://localhost:8080',
            'harbor_username': 'myuser',
            'harbor_password': 'mypassword',
        })
        self.assertEqual(form.is_valid(), True)

# Test cases for ObjectStorageConfigForm
class ObjectStorageConfigFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = ObjectStorageConfigForm({
            'object_storage_pk': 'strap',
            'object_storage_bucket': 'mybucket',
            'object_storage_region': 'us-east-1',
            'object_storage_url': 'http://localhost:8080',
            'object_storage_access_key': 'myaccesskey',
            'object_storage_secret_key': 'mysecretkey',
        })
        self.assertEqual(form.is_valid(), True)

# Test cases for K8sConfigForm
class K8sConfigFormTestCases(TestCase):
    # Form should be valid
    def test_valid_form(self):
        form = K8sConfigForm({
            'k8s_server_url': 'http://localhost:8080',
            'k8s_client_key': 'mykey',
            'k8s_client_certificate': 'mycertificate',
        })
        self.assertEqual(form.is_valid(), True)
