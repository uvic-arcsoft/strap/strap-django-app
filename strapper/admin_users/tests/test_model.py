# pylint: disable=W0611
# W0611 disabled this import is default it sets the page up
import json
from django.test import TestCase, override_settings
from django.db.utils import IntegrityError

from admin_users.models import (GeneralConfig, DatabaseConfig, KeycloakConfig,
                    HarborConfig, ObjectStorageConfig, K8sConfig)

# pylint: disable=no-name-in-module
# TODO: Figure out why no-name-in-module is being raised
# when the code is working fine
from strapper.test_settings import TEST_SETTINGS
# pylint: enable=no-name-in-module

@override_settings(**TEST_SETTINGS)
class ModelTestCases(TestCase):
    def setUp(self):
        self.general_config = GeneralConfig.objects.create(
            reserved_app_names="app1\napp2\napp3",
            terraform_module="git::https://git.example.com/test-terraform-module.git",
            helm_chart="test-chart",
            domain="test.domain.org",
            preset_ips=json.dumps({
                "My VPN": ['1.1.1.1/1', '2.2.2.2/2']
            })
        )

        self.psql_database_config = DatabaseConfig.objects.create(
            database_pk = "psql",
            database_name = "test_db",
            database_username = "test_user",
            database_host = "test_host",
            database_password = "test_password"
        )

        self.mysql_database_config = DatabaseConfig.objects.create(
            database_pk = "mysql",
            database_name = "test_db",
            database_username = "test_user",
            database_host = "test_host",
            database_password = "test_password"
        )

        self.keycloak_config = KeycloakConfig.objects.create(
            keycloak_url = "http://keycloak.example.com",
            keycloak_base_path = "/auth",
            keycloak_client_id = "test-client",
            keycloak_client_secret = "test-secret"
        )

        self.harbor_config = HarborConfig.objects.create(
            harbor_module = "git::https://git.example.com/test-harbor-module.git",
            harbor_url = "http://harbor.example.com",
            harbor_username = "test-user",
            harbor_password = "test-password"
        )

        self.strap_object_storage_config = ObjectStorageConfig.objects.create(
            object_storage_pk = "strap",
            object_storage_bucket = "strap-bucket",
            object_storage_region = "us-east-1",
            object_storage_url = "http://strap.example.com",
            object_storage_access_key = "strap-access-key",
            object_storage_secret_key = "strap-secret-key"
        )

        self.strapplications_object_storage_config = ObjectStorageConfig.objects.create(
            object_storage_pk = "strapplications",
            object_storage_bucket = "strapplications-bucket",
            object_storage_region = "us-east-1",
            object_storage_url = "http://strapplications.example.com",
            object_storage_access_key = "strapplications-access-key",
            object_storage_secret_key = "strapplications-secret-key"
        )

        self.k8s_config = K8sConfig.objects.create(
            k8s_server_url = "http://k8s.example.com",
            k8s_client_key = "test-client-key",
            k8s_client_certificate = "test-client-certificate"
        )


# Test cases for the GeneralConfig model
class GeneralConfigModelTestCases(ModelTestCases):
    # Create a valid instance
    def test_create_valid(self):
        self.assertEqual(GeneralConfig.objects.count(), 1)
        self.assertEqual(self.general_config.reserved_app_names, "app1\napp2\napp3")
        self.assertEqual(self.general_config.terraform_module, "git::https://git.example.com/test-terraform-module.git")
        self.assertEqual(self.general_config.helm_chart, "test-chart")
        self.assertEqual(self.general_config.domain, "test.domain.org")

    # Test creating a second instance
    # This shoud fail because there can only be 1 instance
    def test_create_second_instance(self):
        with self.assertRaises(IntegrityError):
            GeneralConfig.objects.create(
                reserved_app_names="app1\napp2\napp3",
                terraform_module="git::https://git.example.com/test-terraform-module.git",
                helm_chart="test-chart",
                domain="test.domain.org"
            )

    # Test __repr__
    def test_repr(self):
        self.assertEqual(repr(self.general_config), "General configurations")

    # Test __str__
    def test_str(self):
        self.assertEqual(str(self.general_config), "General configurations")

    # Test get_reserved_app_names
    # Get list of identifiers that an app cannot have
    def test_get_reserved_app_names(self):
        self.assertEqual(self.general_config.get_reserved_app_names(), ["app1", "app2", "app3"])

    # Test get_preset_ips
    def test_get_preset_ips(self):
        self.assertEqual(self.general_config.get_preset_ips(),
        {
            "My VPN": ['1.1.1.1/1', '2.2.2.2/2']
        })
        self.general_config.preset_ips = None
        self.assertEqual(self.general_config.get_preset_ips(), {})

# Test cases for the DatabaseConfig model
class DatabaseConfigModelTestCases(ModelTestCases):
    # Create a valid instance
    def test_create_valid(self):
        self.assertEqual(DatabaseConfig.objects.count(), 2)
        self.assertEqual(self.psql_database_config.database_pk, "psql")
        self.assertEqual(self.psql_database_config.database_name, "test_db")
        self.assertEqual(self.psql_database_config.database_username, "test_user")
        self.assertEqual(self.psql_database_config.database_host, "test_host")
        self.assertEqual(self.psql_database_config.database_password, "test_password")

        self.assertEqual(self.mysql_database_config.database_pk, "mysql")
        self.assertEqual(self.mysql_database_config.database_name, "test_db")
        self.assertEqual(self.mysql_database_config.database_username, "test_user")
        self.assertEqual(self.mysql_database_config.database_host, "test_host")
        self.assertEqual(self.mysql_database_config.database_password, "test_password")

    # Create an invalid instance where database_pk is not in supported_databases
    def test_create_invalid(self):
        with self.assertRaises(ValueError):
            DatabaseConfig.objects.create(
                database_pk = "invalid",
                database_name = "test_db",
                database_username = "test_user",
                database_host = "test_host",
                database_password = "test_password"
            )

    # Test get_instance
    # Get the instance with the given primary key
    def test_get_instance(self):
        self.assertEqual(DatabaseConfig.get_instance("psql"), self.psql_database_config)
        self.assertEqual(DatabaseConfig.get_instance("mysql"), self.mysql_database_config)

    # Test __repr__
    def test_repr(self):
        self.assertEqual(repr(self.psql_database_config), "Database configurations for PostgreSQL")
        self.assertEqual(repr(self.mysql_database_config), "Database configurations for MySQL")

    # Test __str__
    def test_str(self):
        self.assertEqual(str(self.psql_database_config), "Database configurations for PostgreSQL")
        self.assertEqual(str(self.mysql_database_config), "Database configurations for MySQL")


# Test cases for the KeycloakConfig model
class KeycloakConfigModelTestCases(ModelTestCases):
    # Create a valid instance
    def test_create_valid(self):
        self.assertEqual(KeycloakConfig.objects.count(), 1)
        self.assertEqual(self.keycloak_config.keycloak_url, "http://keycloak.example.com")
        self.assertEqual(self.keycloak_config.keycloak_base_path, "/auth")
        self.assertEqual(self.keycloak_config.keycloak_client_id, "test-client")
        self.assertEqual(self.keycloak_config.keycloak_client_secret, "test-secret")

    # Test creating a second instance
    # This shoud fail because there can only be 1 instance
    def test_create_second_instance(self):
        with self.assertRaises(IntegrityError):
            KeycloakConfig.objects.create(
                keycloak_url = "http://keycloak2.example.com",
                keycloak_base_path = "/auth",
                keycloak_client_id = "test-client-2",
                keycloak_client_secret = "test-secret-2"
            )

    # Test __repr__
    def test_repr(self):
        self.assertEqual(repr(self.keycloak_config), "Keycloak configurations")

    # Test __str__
    def test_str(self):
        self.assertEqual(str(self.keycloak_config), "Keycloak configurations")


# Test cases for the HarborConfig model
class HarborConfigModelTestCases(ModelTestCases):
    # Create a valid instance
    def test_create_valid(self):
        self.assertEqual(HarborConfig.objects.count(), 1)
        self.assertEqual(self.harbor_config.harbor_module, "git::https://git.example.com/test-harbor-module.git")
        self.assertEqual(self.harbor_config.harbor_url, "http://harbor.example.com")
        self.assertEqual(self.harbor_config.harbor_username, "test-user")
        self.assertEqual(self.harbor_config.harbor_password, "test-password")
        self.assertEqual(self.harbor_config.severity_threshold, "High")
        self.assertEqual(self.harbor_config.unknown_criticality, "High")
        self.assertEqual(self.harbor_config.rescan_interval, "None")

    # Test creating a second instance
    # This shoud fail because there can only be 1 instance
    def test_create_second_instance(self):
        with self.assertRaises(IntegrityError):
            HarborConfig.objects.create(
                harbor_module = "git::https://git.example.com/test-harbor-module.git",
                harbor_url = "http://harbor2.example.com",
                harbor_username = "test-user-2",
                harbor_password = "test-password-2"
            )

    # Test __repr__
    def test_repr(self):
        self.assertEqual(repr(self.harbor_config), "Harbor configurations")

    # Test __str__
    def test_str(self):
        self.assertEqual(str(self.harbor_config), "Harbor configurations")

    # Test get_severity_levels
    # Get the severity levels for vulnerabilities
    def test_get_severity_levels(self):
        levels = self.harbor_config.get_severity_levels()
        self.assertEqual(levels['None'], 0)
        self.assertEqual(levels['Low'], 1)
        self.assertEqual(levels['Medium'], 2)
        self.assertEqual(levels['High'], 3)
        self.assertEqual(levels['Critical'], 4)
        self.assertEqual(levels['Unknown'], levels[self.harbor_config.unknown_criticality])


# Test cases for the ObjectStorageConfig model
class ObjectStorageConfigModelTestCases(ModelTestCases):
    # Create a valid instance
    def test_create_valid(self):
        self.assertEqual(ObjectStorageConfig.objects.count(), 2)
        self.assertEqual(self.strap_object_storage_config.object_storage_pk, "strap")
        self.assertEqual(self.strap_object_storage_config.object_storage_bucket, "strap-bucket")
        self.assertEqual(self.strap_object_storage_config.object_storage_region, "us-east-1")
        self.assertEqual(self.strap_object_storage_config.object_storage_url, "http://strap.example.com")
        self.assertEqual(self.strap_object_storage_config.object_storage_access_key, "strap-access-key")
        self.assertEqual(self.strap_object_storage_config.object_storage_secret_key, "strap-secret-key")

        self.assertEqual(self.strapplications_object_storage_config.object_storage_pk, "strapplications")
        self.assertEqual(self.strapplications_object_storage_config.object_storage_bucket, "strapplications-bucket")
        self.assertEqual(self.strapplications_object_storage_config.object_storage_region, "us-east-1")
        self.assertEqual(
            self.strapplications_object_storage_config.object_storage_url,
            "http://strapplications.example.com")
        self.assertEqual(
            self.strapplications_object_storage_config.object_storage_access_key,
            "strapplications-access-key")
        self.assertEqual(
            self.strapplications_object_storage_config.object_storage_secret_key,
            "strapplications-secret-key")

    # Create an invalid instance where object_storage_pk is not in supported_object_storage
    def test_create_invalid(self):
        with self.assertRaises(ValueError):
            ObjectStorageConfig.objects.create(
                object_storage_pk = "invalid",
                object_storage_bucket = "test-bucket",
                object_storage_region = "us-east-1",
                object_storage_url = "http://example.com",
                object_storage_access_key = "test-access-key",
                object_storage_secret_key = "test-secret-key"
            )

    # Test get_instance
    # Get the instance with the given primary key
    def test_get_instance(self):
        self.assertEqual(
            ObjectStorageConfig.get_instance(object_storage_pk="strap"),
            self.strap_object_storage_config)
        self.assertEqual(
            ObjectStorageConfig.get_instance(object_storage_pk="strapplications"),
            self.strapplications_object_storage_config)

    # Test __repr__
    def test_repr(self):
        self.assertEqual(
            repr(self.strap_object_storage_config),
            "Object storage configurations for STRAP")
        self.assertEqual(
            repr(self.strapplications_object_storage_config),
            "Object storage configurations for STRAP applications")

    # Test __str__
    def test_str(self):
        self.assertEqual(
            str(self.strap_object_storage_config),
            "Object storage configurations for STRAP")
        self.assertEqual(
            str(self.strapplications_object_storage_config),
            "Object storage configurations for STRAP applications")


# Test cases for the K8sConfig model
class K8sConfigModelTestCases(ModelTestCases):
    # Create a valid instance
    def test_create_valid(self):
        self.assertEqual(K8sConfig.objects.count(), 1)
        self.assertEqual(self.k8s_config.k8s_server_url, "http://k8s.example.com")
        self.assertEqual(self.k8s_config.k8s_client_key, "test-client-key")
        self.assertEqual(self.k8s_config.k8s_client_certificate, "test-client-certificate")

    # Test creating a second instance
    # This shoud fail because there can only be 1 instance
    def test_create_second_instance(self):
        with self.assertRaises(IntegrityError):
            K8sConfig.objects.create(
                k8s_server_url = "http://k8s2.example.com",
                k8s_client_key = "test-client-key-2",
                k8s_client_certificate = "test-client-certificate-2"
            )

    # Test __repr__
    def test_repr(self):
        self.assertEqual(repr(self.k8s_config), "K8s configurations")

    # Test __str__
    def test_str(self):
        self.assertEqual(str(self.k8s_config), "K8s configurations")
