# pylint:
import json

from django.test import TestCase, Client, override_settings
from django.contrib.auth.models import User
from django.urls import reverse

from admin_users.models import (GeneralConfig, DatabaseConfig, KeycloakConfig,
                                HarborConfig, ObjectStorageConfig, K8sConfig)
from admin_users.forms import K8sResourceRequestProcessActions

from normal_users.models import Application, RequestStatusTypes

# pylint: disable=no-name-in-module
# TODO: Figure out why no-name-in-module is being raised
# when the code is working fine
from strapper.test_settings import TEST_SETTINGS
# pylint: enable=no-name-in-module

# General set up for view test cases
@override_settings(**TEST_SETTINGS)
class ViewTestCase(TestCase):
    def setUp(self):
        # Create an admin user
        self.admin_user = User.objects.create_superuser(
            username='admin@example.org',
            password='testpassword'
        )

        # Log the admin user in
        self.client = Client(HTTP_X_FORWARDED_USER="admin@example.org", HTTP_REFERER="/admin")

        # Create an unauthorized client
        self.user = User.objects.create(
            username='user@example.org',
            password='testpassword',
            is_staff=False,
            is_active=True
        )
        self.unauthz_client = Client(HTTP_X_FORWARDED_USER="user@example.org")

        # Create a general config
        self.general_config = GeneralConfig.objects.create(
            reserved_app_names='app1,app2,app3',
            terraform_module='module1',
            helm_chart='chart1',
            domain='test.example.com'
        )

        # Create a database config
        self.database_config_psql = DatabaseConfig.objects.create(
            database_pk='psql',
            database_name='mydb',
            database_username='myuser',
            database_host='localhost',
            database_password='mypassword'
        )

        # Create a keycloak config
        self.keycloak_config = KeycloakConfig.objects.create(
            keycloak_url='http://localhost:8080/auth',
            keycloak_base_path='',
            keycloak_client_id='myclient',
            keycloak_client_secret='mysecret'
        )

        # Create a harbor config
        self.harbor_config = HarborConfig.objects.create(
            harbor_module='module1',
            harbor_url='http://localhost:8080',
            harbor_username='myuser',
            harbor_password='mypassword'
        )

        # Create an object storage config
        self.object_storage_config_strap = ObjectStorageConfig.objects.create(
            object_storage_pk='strap',
            object_storage_bucket='mybucket',
            object_storage_region='myregion',
            object_storage_url='http://localhost:9000',
            object_storage_access_key='myaccesskey',
            object_storage_secret_key='mysecretkey'
        )

        # Create a k8s config
        self.k8s_config = K8sConfig.objects.create(
            k8s_server_url='http://localhost:8080',
            k8s_client_key='mykey',
            k8s_client_certificate='mycert',
        )

        # Log the 2 clients in
        self.client.get(reverse('login'), follow=True)
        self.unauthz_client.get(reverse('login'), follow=True)


class AdminHomePageViewTestCase(ViewTestCase):
    """
    Test cases for admin homepage
    """
    def test_admin_home_page(self):
        response = self.client.get(reverse('admin_home_page'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'admin_users/dashboard.html')
        context_data = response.context_data

        # Dashboard should contain all configs
        self.assertEqual(context_data['general_config'], self.general_config)
        self.assertEqual(context_data['database_config_psql'], self.database_config_psql)
        self.assertEqual(context_data['keycloak_config'], self.keycloak_config)
        self.assertEqual(context_data['harbor_config'], self.harbor_config)
        self.assertEqual(context_data['object_storage_config_strap'], self.object_storage_config_strap)
        self.assertEqual(context_data['k8s_config'], self.k8s_config)


class GeneralConfigAPIViewTestCase(ViewTestCase):
    """
    Test cases for general config API view
    """
    def test_update_general_config(self):
        """
        Test updating general configurations
        """
        response = self.client.post(
            reverse('update_general_config'),
            {
                'reserved_app_names': 'app1,app2',
                'terraform_module': 'module2',
                'helm_chart': 'chart2',
                'domain': 'example.com',
                'preset_ip_name': ['My VPN', 'Test network', 'Invalid network'],
                'preset_ips': [
                    '1.1.1.1,2.2.2.2',    # All valid IPs
                    '2.1.2/2,3.3.3.3',       # One invalid IP
                    '3.2.3,4.3.2/1'           # All invalid IPs
                ]
            },
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'admin_users/dashboard.html')

        # Check if the general config has been updated
        updated_general_config = GeneralConfig.objects.get(pk=self.general_config.pk)
        self.assertEqual(updated_general_config.reserved_app_names, 'app1,app2')
        self.assertEqual(updated_general_config.terraform_module, 'module2')
        self.assertEqual(updated_general_config.helm_chart, 'chart2')
        self.assertEqual(updated_general_config.domain, 'example.com')
        preset_ips = updated_general_config.get_preset_ips()
        self.assertIn('My VPN', preset_ips)
        self.assertIn('Test network', preset_ips)
        self.assertNotIn('Invalid network', preset_ips)
        self.assertEqual(set(preset_ips['My VPN']), set(['1.1.1.1', '2.2.2.2']))
        self.assertEqual(preset_ips['Test network'], ['3.3.3.3'])

        # Check template context
        context_data = response.context_data
        self.assertEqual(context_data['general_config'], updated_general_config)
        self.assertIn(
            ("General configurations have been updated.<br>The following IPs/networks were invalid and therefore, "
             "couldn't be added:<br>"),
            context_data['message'])
        self.assertEqual(context_data['success'], True)
        self.assertNotIn('active_tab', context_data)

    def test_unauthz_update_general_config(self):
        """
        Test updating general configurations without authorization
        """
        response = self.unauthz_client.post(
            reverse('update_general_config'),
            {
                'reserved_app_names': 'app1,app2',
                'terraform_module': 'module2',
                'helm_chart': 'chart2',
                'domain': 'example.com'
            },
            follow=True
        )

        # Should receive a 403 JsonResponse
        content = json.loads(response.content)
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], "You are not authorized to access this view.")


class DatabaseConfigAPIViewTestCase(ViewTestCase):
    """
    Test cases for database config API view
    """
    def test_update_database_config(self):
        response = self.client.post(
            reverse('update_database_config', kwargs={'database_pk': 'psql'}),
            {
                'database_name': 'mydb2',
                'database_username': 'myuser2',
                'database_host': 'localhost2',
                'database_password': 'mypassword2'
            },
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'admin_users/dashboard.html')

        # Check if the database config has been updated
        updated_database_config = DatabaseConfig.objects.get(pk='psql')
        self.assertEqual(updated_database_config.database_name, 'mydb2')
        self.assertEqual(updated_database_config.database_username, 'myuser2')
        self.assertEqual(updated_database_config.database_host, 'localhost2')
        self.assertEqual(updated_database_config.database_password, 'mypassword2')

        # Check template context
        context_data = response.context_data
        self.assertEqual(context_data['database_config_psql'], updated_database_config)
        self.assertEqual(context_data['message'], "PostgreSQL database configurations have been updated.")
        self.assertEqual(context_data['success'], True)
        self.assertEqual(context_data['active_tab'], 'database')

    def test_unauthz_update_database_config(self):
        """
        Test updating database configurations without authorization
        """
        response = self.unauthz_client.post(
            reverse('update_database_config', kwargs={'database_pk': 'psql'}),
            {
                'database_name': 'mydb2',
                'database_username': 'myuser2',
                'database_host': 'localhost2',
                'database_password': 'mypassword2'
            },
            follow=True
        )

        # Should receive a 403 JsonResponse
        content = json.loads(response.content)
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], "You are not authorized to access this view.")

    def test_update_database_config_with_wrong_database_pk(self):
        """
        Test updating database configurations with missing database_pk
        """
        response = self.client.post(
            reverse('update_database_config', kwargs={'database_pk': 'not_supported_db'}),
            {
                'database_name': 'mydb2',
                'database_username': 'myuser2',
                'database_host': 'localhost2',
                'database_password': 'mypassword2'
            },
            follow=True
        )

        # Should receive a 400 JsonResponse
        content = json.loads(response.content)
        self.assertEqual(content['status'], 400)
        self.assertEqual(content['message'], "Invalid primary key for DatabaseConfig. Must be either 'psql' or 'mysql'")


class KeycloakConfigAPIViewTestCase(ViewTestCase):
    """
    Test cases for keycloak config API view
    """
    def test_update_keycloak_config(self):
        response = self.client.post(
            reverse('update_keycloak_config'),
            {
                'keycloak_url': 'http://localhost:8080/auth2',
                'keycloak_base_path': 'path2',
                'keycloak_client_id': 'myclient2',
                'keycloak_client_secret': 'mysecret2'
            },
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'admin_users/dashboard.html')

        # Check if the keycloak config has been updated
        updated_keycloak_config = KeycloakConfig.objects.get(pk=self.keycloak_config.pk)
        self.assertEqual(updated_keycloak_config.keycloak_url, 'http://localhost:8080/auth2')
        self.assertEqual(updated_keycloak_config.keycloak_base_path, 'path2')
        self.assertEqual(updated_keycloak_config.keycloak_client_id, 'myclient2')
        self.assertEqual(updated_keycloak_config.keycloak_client_secret, 'mysecret2')

        # Check template context
        context_data = response.context_data
        self.assertEqual(context_data['keycloak_config'], updated_keycloak_config)
        self.assertEqual(context_data['message'], "Keycloak configurations have been updated.")
        self.assertEqual(context_data['success'], True)
        self.assertEqual(context_data['active_tab'], 'keycloak')

    def test_unauthz_update_keycloak_config(self):
        """
        Test updating keycloak configurations without authorization
        """
        response = self.unauthz_client.post(
            reverse('update_keycloak_config'),
            {
                'keycloak_url': 'http://localhost:8080/auth2',
                'keycloak_base_path': 'path2',
                'keycloak_client_id': 'myclient2',
                'keycloak_client_secret': 'mysecret2'
            },
            follow=True
        )

        # Should receive a 403 JsonResponse
        content = json.loads(response.content)
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], "You are not authorized to access this view.")


class HarborConfigAPIViewTestCase(ViewTestCase):
    """
    Test cases for harbor config API view
    """
    def test_update_harbor_config(self):
        response = self.client.post(
            reverse('update_harbor_config'),
            {
                'harbor_module': 'module2',
                'harbor_url': 'http://localhost:8080/2',
                'harbor_username': 'myuser2',
                'harbor_password': 'mypassword2'
            },
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'admin_users/dashboard.html')

        # Check if the harbor config has been updated
        updated_harbor_config = HarborConfig.objects.get(pk=self.harbor_config.pk)
        self.assertEqual(updated_harbor_config.harbor_module, 'module2')
        self.assertEqual(updated_harbor_config.harbor_url, 'http://localhost:8080/2')
        self.assertEqual(updated_harbor_config.harbor_username, 'myuser2')
        self.assertEqual(updated_harbor_config.harbor_password, 'mypassword2')
        self.assertEqual(updated_harbor_config.severity_threshold, 'High')
        self.assertEqual(updated_harbor_config.unknown_criticality, 'High')
        self.assertEqual(updated_harbor_config.rescan_interval, "None")

        # Check template context
        context_data = response.context_data
        self.assertEqual(context_data['harbor_config'], updated_harbor_config)
        self.assertEqual(context_data['message'], "Harbor configurations have been updated.")
        self.assertEqual(context_data['success'], True)
        self.assertEqual(context_data['active_tab'], 'harbor')

    def test_send_test_email(self):
        # Valid email template
        harbor_config = HarborConfig.get_instance()
        harbor_config.vuln_notification_email_template = """
            <p>Hi {{ name }},</p>
            <p>We have detected severe vulnerabilities in the container images used by your application, {{ app_name }}. Please fix the followings as soon as possible:</p>
            <ul>
                {% for resource_url, vulnerabilities in vuln_images.items %}
                    <li>
                        <strong>{{ resource_url }}</strong>: 
                        {% for severity, num_found in vulnerabilities.items %}
                            {{ num_found }} {{severity}} vulnerabilities{% if not forloop.last %}, {% endif %}
                        {% endfor %}
                    </li>
                {% endfor %}
            </ul>
            <p>Please either fix the vulnerabilities or use a different image to upgrade your app. If you have any questions, feel free to contact us at {{ email_sender }}.</p>
            <p>
            Best regards,<br>
            <strong>UVic ARCSoftware team</strong>
            </p>
        """
        harbor_config.save()

        response = self.client.post(reverse('send_test_email'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'admin_users/dashboard.html')

        context_data = response.context_data
        self.assertEqual(
            context_data['message'],
            f"Sent a test email to <strong>{self.admin_user.username}</strong> successfully!")
        self.assertEqual(context_data['success'], True)

        # Invalid email template
        harbor_config.vuln_notification_email_template = """
            {% if name %}
            <p>Hi {{ name }},</p>
            <p>We have detected severe vulnerabilities in the container images used by your application, {{ app_name }}. Please fix the followings as soon as possible:</p>
        """
        harbor_config.save()

        response = self.client.post(reverse('send_test_email'), follow=True)
        content = json.loads(response.content)
        self.assertEqual(content['status'], 500)
        self.assertIn("Error occurred while interpolating email template:", content['message'])


    def test_unauthz_update_harbor_config(self):
        """
        Test updating harbor configurations without authorization
        """
        response = self.unauthz_client.post(
            reverse('update_harbor_config'),
            {
                'harbor_module': 'module2',
                'harbor_url': 'http://localhost:8080/2',
                'harbor_username': 'myuser2',
                'harbor_password': 'mypassword2'
            },
            follow=True
        )

        # Should receive a 403 JsonResponse
        content = json.loads(response.content)
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], "You are not authorized to access this view.")


class ObjectStorageConfigAPIViewTestCase(ViewTestCase):
    """
    Test cases for object storage config API view
    """
    def test_update_object_storage_config(self):
        response = self.client.post(
            reverse('update_object_storage_config', kwargs={'object_storage_pk': 'strap'}),
            {
                'object_storage_bucket': 'mybucket2',
                'object_storage_region': 'myregion2',
                'object_storage_url': 'http://localhost:9000/2',
                'object_storage_access_key': 'myaccesskey2',
                'object_storage_secret_key': 'mysecretkey2'
            },
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'admin_users/dashboard.html')

        # Check if the object storage config has been updated
        updated_object_storage_config = ObjectStorageConfig.objects.get(pk='strap')
        self.assertEqual(updated_object_storage_config.object_storage_bucket, 'mybucket2')
        self.assertEqual(updated_object_storage_config.object_storage_region, 'myregion2')
        self.assertEqual(updated_object_storage_config.object_storage_url, 'http://localhost:9000/2')
        self.assertEqual(updated_object_storage_config.object_storage_access_key, 'myaccesskey2')
        self.assertEqual(updated_object_storage_config.object_storage_secret_key, 'mysecretkey2')

        # Check template context
        context_data = response.context_data
        self.assertEqual(context_data['object_storage_config_strap'], updated_object_storage_config)
        self.assertEqual(context_data['message'], "Object storage configurations for STRAP have been updated.")
        self.assertEqual(context_data['success'], True)
        self.assertEqual(context_data['active_tab'], 'object_storage')

    def test_unauthz_update_object_storage_config(self):
        """
        Test updating object storage configurations without authorization
        """
        response = self.unauthz_client.post(
            reverse('update_object_storage_config', kwargs={'object_storage_pk': 'strap'}),
            {
                'object_storage_bucket': 'mybucket2',
                'object_storage_region': 'myregion2',
                'object_storage_url': 'http://localhost:9000/2',
                'object_storage_access_key': 'myaccesskey2',
                'object_storage_secret_key': 'mysecretkey2'
            },
            follow=True
        )

        # Should receive a 403 JsonResponse
        content = json.loads(response.content)
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], "You are not authorized to access this view.")

    def test_update_object_storage_config_with_wrong_object_storage_pk(self):
        """
        Test updating object storage configurations with missing object_storage_pk
        """
        response = self.client.post(
            reverse('update_object_storage_config', kwargs={'object_storage_pk': 'not_supported_object_storage'}),
            {
                'object_storage_bucket': 'mybucket2',
                'object_storage_region': 'myregion2',
                'object_storage_url': 'http://localhost:9000/2',
                'object_storage_access_key': 'myaccesskey2',
                'object_storage_secret_key': 'mysecretkey2'
            },
            follow=True
        )

        # Should receive a 400 JsonResponse
        content = json.loads(response.content)
        self.assertEqual(content['status'], 400)
        self.assertEqual(
            content['message'],
            "Invalid primary key for ObjectStorageConfig. Must be either 'strap' or 'strapplications'"
        )


class K8sConfigAPIViewTestCase(ViewTestCase):
    """
    Test cases for k8s config API view
    """
    def test_update_k8s_config(self):
        response = self.client.post(
            reverse('update_k8s_config'),
            {
                'k8s_server_url': 'http://localhost:8080/2',
                'k8s_client_key': 'mykey2',
                'k8s_client_certificate': 'mycert2'
            },
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'admin_users/dashboard.html')

        # Check if the k8s config has been updated
        updated_k8s_config = K8sConfig.objects.get(pk=self.k8s_config.pk)
        self.assertEqual(updated_k8s_config.k8s_server_url, 'http://localhost:8080/2')
        self.assertEqual(updated_k8s_config.k8s_client_key, 'mykey2')
        self.assertEqual(updated_k8s_config.k8s_client_certificate, 'mycert2')

        # Check template context
        context_data = response.context_data
        self.assertEqual(context_data['k8s_config'], updated_k8s_config)
        self.assertEqual(context_data['message'], "K8s configurations have been updated.")
        self.assertEqual(context_data['success'], True)
        self.assertEqual(context_data['active_tab'], 'k8s')

    def test_unauthz_update_k8s_config(self):
        """
        Test updating k8s configurations without authorization
        """
        response = self.unauthz_client.post(
            reverse('update_k8s_config'),
            {
                'k8s_server_url': 'http://localhost:8080/2',
                'k8s_client_key': 'mykey2',
                'k8s_client_certificate': 'mycert2'
            },
            follow=True
        )

        # Should receive a 403 JsonResponse
        content = json.loads(response.content)
        self.assertEqual(content['status'], 403)
        self.assertEqual(content['message'], "You are not authorized to access this view.")

    def test_process_k8s_resource_request(self):
        """
        Test approving/denying a K8s resource request
        """
        user = User.objects.create(username="appuser@example.org", password="testpassword")
        app = Application.objects.create(
            identifier="testapp",
            descriptive_name="Test App",
            description="Test App Description",
            owner=user
        )
        request = app.applicationquotarequest
        request.replicas=3
        request.justification="Test justification"
        request.submitted_by=user
        request.status=RequestStatusTypes.PENDING
        request.save()

        # Test approving the request and changing some resources
        response = self.client.post(
            reverse('process_k8s_resource_request', kwargs={'request_id': request.id}),
            {
                'action': K8sResourceRequestProcessActions.APPROVE,
                'replicas': 5,
                'reply': 'I have approved your request'
            },
            follow=True
        )
        self.assertEqual(response.status_code, 200)

        app.refresh_from_db()
        quota = app.applicationquota
        self.assertEqual(quota.replicas, 5)

        request = app.applicationquotarequest
        self.assertEqual(request.replicas, 5)
        self.assertEqual(request.status, RequestStatusTypes.APPROVED)
        self.assertEqual(request.admin_reply, 'I have approved your request')

        # Rerequest for a new test
        request.replicas = 6
        request.status = RequestStatusTypes.PENDING
        request.save()

        # Test rejecting the request
        response = self.client.post(
            reverse('process_k8s_resource_request', kwargs={'request_id': request.id}),
            {
                'action': K8sResourceRequestProcessActions.REJECT,
                'reply': 'I have denied your request'
            },
            follow=True
        )
        self.assertEqual(response.status_code, 200)

        app.refresh_from_db()
        quota = app.applicationquota
        self.assertEqual(quota.replicas, 5)
