# pylint:
import os
import json
import ipaddress
from typing import Any
import environ
from requests.exceptions import HTTPError

from django.views import View
from django.urls import reverse
from django.shortcuts import redirect
from django.template import Template, Context
from django.core.mail import send_mail
from django.conf import settings
from django.http import HttpRequest
from django.template.loader import render_to_string

from django_editablecontent.views import EditableContentView

from normal_users.models import (Application, ApplicationQuotaRequest,
                            RequestStatusTypes, Notification, NotificationTypes,
                            NotificationStatusTypes)
from normal_users.helper_functions import get_apps_for_user_by_role
from helpers.custom_json_response import (
    JsonResponseBadRequest, JsonResponseForbidden, JsonResponseInternalServerError
)

from mixins import AdminRequiredMixin
from .forms import (GeneralConfigForm, DatabaseConfigForm, KeycloakConfigForm,
                    HarborConfigForm, ObjectStorageConfigForm, K8sConfigForm,
                    K8sResourceRequestProcessActions, K8sResourceRequestProcessForm)
from .models import GeneralConfig, DatabaseConfig, KeycloakConfig, HarborConfig, ObjectStorageConfig, K8sConfig

# Load environment variables
environ.Env.read_env()

# Create your views here.
# Admin Page: Only authenticated admin users can visit this page
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class AdminHomePage(AdminRequiredMixin, EditableContentView):
    template_name = 'admin_users/dashboard.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        current_user = self.request.user
        harbor_config = HarborConfig.get_instance()

        general_config = GeneralConfig.get_instance()
        context["general_config"] = general_config
        context["preset_ips"] = general_config.get_preset_ips()
        context["database_config_psql"] = DatabaseConfig.get_instance(database_pk="psql")
        context["database_config_mysql"] = DatabaseConfig.get_instance(database_pk="mysql")
        context["keycloak_config"] = KeycloakConfig.get_instance()
        context["harbor_config"] = harbor_config
        context["object_storage_config_strap"] = ObjectStorageConfig.get_instance(object_storage_pk="strap")
        context["object_storage_config_strapplications"] = (
            ObjectStorageConfig.get_instance(object_storage_pk="strapplications")
        )
        context["k8s_config"] = K8sConfig.get_instance()
        context = {**context, **get_apps_for_user_by_role(current_user, 'all')}
        context["disable_email"] = not bool(settings.EMAIL_HOST_USER)
        context["email_sender"] = os.environ.get("EMAIL_SENDER", "")

        # Craft a test email template to notify developers for severe vulnerabilities
        # in their images
        interpolated_email_template = ""
        try:
            email_template = Template(harbor_config.vuln_notification_email_template)
            email_context = Context({
                "name": current_user.first_name if current_user.first_name else current_user.username,
                "app_name": "My App",
                "vuln_images": {
                    'myapp_web:1.2.0': {
                        'High': 5,
                        'Critical': 2,
                    },
                    'myapp_server:1.1.0': {
                        'High': 3,
                        'Critical': 1,
                    },
                    'myapp_db:1.0.0': {
                        'High': 1,
                        'Critical': 0,
                    },
                },
                "email_sender": os.environ.get("EMAIL_SENDER", "")
            })
            interpolated_email_template = email_template.render(email_context)
        except Exception as e:
            # Display an error message if email template interpolation fails
            print(f"Error occurred while interpolating email template: {e}")
            interpolated_email_template = f"Error occurred while interpolating email template: {e}"
        context["interpolated_email_template"] = interpolated_email_template

        # Display pending K8s resource requests from users
        context["pending_k8s_resource_requests"] = (
            ApplicationQuotaRequest.objects.filter(status=RequestStatusTypes.PENDING)
        )

        # Display success or error message if there is any
        context['message'] = self.request.session.get('message', None)
        context['success'] = self.request.session.get('success', None)

        # Clear session message display if active
        if context['message']:
            del self.request.session['message']
            del self.request.session['success']

        # Open the currently active tab. Usually to handle redirection from API views
        if 'active_tab' in self.request.session:
            context['active_tab'] = self.request.session['active_tab']
            del self.request.session['active_tab']

        # Open a tab if passed as a param in the URL
        if 'active_tab' in self.request.GET:
            context['active_tab'] = self.request.GET['active_tab']

        return context


class GeneralConfigAPIView(View):
    """
    API view to modify general configs
    Can only be accessed by redirection
    """
    def post(self, request):
        # User must be an admin to access this view
        if not request.user.is_staff:
            return JsonResponseForbidden("You are not authorized to access this view.")

        # Update general configurations
        if request.path == reverse("update_general_config"):
            general_config = GeneralConfig.get_instance()
            general_config_form = GeneralConfigForm(request.POST, instance=general_config)
            if general_config_form.is_valid():
                # We will process the preset IPs separately below
                general_config.preset_ips = None
                general_config_form.save()

                ip_names = request.POST.getlist('preset_ip_name', [])
                ips = request.POST.getlist('preset_ips', [])

                # Validate IP names and addresses/networks
                # - Ip names must not be empty
                # - IP addresses/networks must be a valid IPv4 or IPv6 address/network
                # Only save valid IPs and notify users about invalid IPs
                preset_ips = {}
                error_ips = {}
                for i, ips_str in enumerate(ips):
                    valid_ips = set()
                    invalid_ips = set()
                    for ip in ips_str.split(","):
                        if ip.strip() == "":
                            continue
                        try:
                            ipaddress.ip_network(ip)
                            valid_ips.add(ip)
                        except ValueError:
                            invalid_ips.add(ip)

                    if len(valid_ips) > 0 and ip_names[i].strip() != "":
                        preset_ips[ip_names[i]] = list(valid_ips)
                    if len(invalid_ips) > 0:
                        error_ips[ip_names[i]] = list(invalid_ips)

                general_config.preset_ips = json.dumps(preset_ips)
                general_config.save()

                # Display a success message
                request.session['message'] = "General configurations have been updated."
                request.session['success'] = True

                # Notify users about invalid IP addresses
                if len(error_ips):
                    request.session['message'] +=\
                "<br>The following IPs/networks were invalid and therefore, couldn't be added:<br><ul>"
                    for ip_name, invalid_ips in error_ips.items():
                        request.session['message'] += f"<li><strong>{ip_name}: {', '.join(invalid_ips)}</strong></li>"
                    request.session['message'] += "</ul>"

            # Invalid fields found in POST request
            # Display an error message
            else:
                request.session['message'] = general_config_form.errors
                request.session['success'] = False

        return redirect(request.META.get('HTTP_REFERER'))


class DatabaseConfigAPIView(View):
    """
    API view to modify database configs
    Can only be accessed by redirection
    """
    def post(self, request, **kwargs: Any):
        # User must be an admin to access this view
        if not request.user.is_staff:
            return JsonResponseForbidden("You are not authorized to access this view.")

        database_pk = kwargs.get("database_pk", "")

        # Can only update either configurations for PostgreSQL or MySQL
        if database_pk not in DatabaseConfig.supported_databases:
            return JsonResponseBadRequest(
                "Invalid primary key for DatabaseConfig. Must be either 'psql' or 'mysql'"
            )

        # Update database configs
        if request.path == reverse(
                "update_database_config",
                kwargs={"database_pk": database_pk}
            ):
            database_config = DatabaseConfig.get_instance(database_pk=database_pk)
            database_form = DatabaseConfigForm(request.POST, instance=database_config)
            if database_form.is_valid():
                database_form.save()

                # Display a success message
                request.session['message'] = (
                    f"{'PostgreSQL' if database_pk == 'psql' else 'MySQL'} "
                    "database configurations have been updated."
                )
                request.session['success'] = True

            # Invalid fields found in POST request
            # Display an error message
            else:
                request.session['message'] = database_form.errors
                request.session['success'] = False

            # Open the database tab
            request.session['active_tab'] = "database"

        return redirect(request.META.get('HTTP_REFERER'))


class KeycloakConfigAPIView(View):
    """
    API view to modify keycloak configs
    Can only be accessed by redirection
    """
    def post(self, request):
        # User must be an admin to access this view
        if not request.user.is_staff:
            return JsonResponseForbidden("You are not authorized to access this view.")

        # Update keycloak configs
        if request.path == reverse("update_keycloak_config"):
            keycloak_config = KeycloakConfig.get_instance()
            keycloak_form = KeycloakConfigForm(request.POST, instance=keycloak_config)
            if keycloak_form.is_valid():
                keycloak_form.save()

                # Display a success message
                request.session['message'] = "Keycloak configurations have been updated."
                request.session['success'] = True

            # Invalid fields found in POST request
            # Display an error message
            else:
                request.session['message'] = keycloak_form.errors
                request.session['success'] = False

            # Open the keycloak tab
            request.session['active_tab'] = "keycloak"

        return redirect(request.META.get('HTTP_REFERER'))


class HarborConfigAPIView(View):
    """
    API view to modify harbor configs
    Can only be accessed by redirection
    """
    def post(self, request):
        current_user = request.user

        # User must be an admin to access this view
        if not current_user.is_staff:
            return JsonResponseForbidden("You are not authorized to access this view.")

        # Update harbor configs
        if request.path == reverse("update_harbor_config"):
            harbor_config = HarborConfig.get_instance()
            harbor_form = HarborConfigForm(request.POST, instance=harbor_config)
            if harbor_form.is_valid():
                try:
                    harbor_form.save()
                except HTTPError as http_err:
                    # Display an error message if harbor API request fails
                    request.session['message'] = f"Error occurred while calling Harbor API: {http_err}"
                    request.session['success'] = False
                    return redirect(request.META.get('HTTP_REFERER'))

                # Display a success message
                request.session['message'] = "Harbor configurations have been updated."
                request.session['success'] = True

            # Invalid fields found in POST request
            # Display an error message
            else:
                request.session['message'] = harbor_form.errors
                request.session['success'] = False

            # Open the harbor tab
            request.session['active_tab'] = "harbor"

        # Send a test email to notify developers about severe vulnerabilities
        # found in their images
        if request.path == reverse("send_test_email"):
            if not settings.EMAIL_HOST_USER:
                request.session['message'] = (
                    "<strong>EMAIL_HOST_USER</strong> is not set in <strong>settings.py</strong>. "
                    "Can't send a test email.")
                request.session['success'] = False

                # Open the harbor tab
                request.session['active_tab'] = "harbor"
                return redirect(request.META.get('HTTP_REFERER'))

            harbor_config = HarborConfig.get_instance()

            # Interpolate the email template with test data
            # and output HTML
            interpolated_email_template = ""
            try:
                email_template = Template(harbor_config.vuln_notification_email_template)
                email_context = Context({
                    "name": current_user.first_name if current_user.first_name else current_user.username,
                    "app_name": "My App",
                    "vuln_images": {
                        'myapp_web:1.2.0': {
                            'High': 5,
                            'Critical': 2,
                        },
                        'myapp_server:1.1.0': {
                            'High': 3,
                            'Critical': 1,
                        },
                        'myapp_db:1.0.0': {
                            'High': 1,
                            'Critical': 0,
                        },
                    },
                    "email_sender": os.environ.get("EMAIL_SENDER", "")
                })
                interpolated_email_template = email_template.render(email_context)
            except Exception as e:
                # Display an error message if email template interpolation fails
                print(f"Error occurred while interpolating email template: {e}")
                return JsonResponseInternalServerError(
                    f"Error occurred while interpolating email template: {e}"
                )

            # Send an email using the HTML content
            if not settings.TESTING:
                send_mail(
                    "Severe vulnerabilities detected in your container images",
                    "",     # Empty message body because we are using HTML content
                    os.environ.get('EMAIL_SENDER'),
                    [current_user.username],
                    fail_silently=False,
                    html_message=interpolated_email_template,
                )

            request.session['message'] = f"Sent a test email to <strong>{current_user.username}</strong> successfully!"
            request.session['success'] = True

            # Open the harbor tab
            request.session['active_tab'] = "harbor"

        return redirect(request.META.get('HTTP_REFERER'))

class AppsScanReportView(EditableContentView):
    template_name = 'admin_users/apps_scan_report.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        current_user = self.request.user
        context = {**context, **get_apps_for_user_by_role(current_user, 'all')}
        context["apps"] = Application.objects.all()

        return context

class ObjectStorageConfigAPIView(View):
    """
    API view to modify object storage configs
    Can only be accessed by redirection
    """
    def post(self, request, **kwargs: Any):
        # User must be an admin to access this view
        if not request.user.is_staff:
            return JsonResponseForbidden("You are not authorized to access this view.")

        object_storage_pk = kwargs.get("object_storage_pk", "")

        # Can only update either configurations for STRAP or STRAP applications
        if object_storage_pk not in ["strap", "strapplications"]:
            return JsonResponseBadRequest(
                "Invalid primary key for ObjectStorageConfig. Must be either 'strap' or 'strapplications'"
            )

        # Update object storage configs
        if request.path == reverse(
                "update_object_storage_config",
                kwargs={"object_storage_pk": object_storage_pk}
            ):
            object_storage_config = ObjectStorageConfig.get_instance(
                object_storage_pk=object_storage_pk
            )
            object_storage_form = ObjectStorageConfigForm(request.POST, instance=object_storage_config)
            if object_storage_form.is_valid():
                object_storage_form.save()

                # Display a success message
                request.session['message'] = (
                    "Object storage configurations for "
                    f"{'STRAP' if object_storage_pk == 'strap' else 'STRAP applications'} have been updated."
                )
                request.session['success'] = True

            # Invalid fields found in POST request
            # Display an error message
            else:
                request.session['message'] = object_storage_form.errors
                request.session['success'] = False


            # Open the object storage tab
            request.session['active_tab'] = "object_storage"

        return redirect(request.META.get('HTTP_REFERER'))


class K8sConfigAPIView(View):
    """
    API view to modify k8s configs
    Can only be accessed by redirection
    """
    def post(self, request: HttpRequest, **kwargs: Any) -> redirect:
        # User must be an admin to access this view
        if not request.user.is_staff:
            return JsonResponseForbidden("You are not authorized to access this view.")

        # Update k8s configs
        if request.path == reverse("update_k8s_config"):
            k8s_config = K8sConfig.get_instance()
            k8s_form = K8sConfigForm(request.POST, instance=k8s_config)
            if k8s_form.is_valid():
                k8s_form.save()

                # Display a success message
                request.session['message'] = "K8s configurations have been updated."
                request.session['success'] = True

            # Invalid fields found in POST request
            # Display an error message
            else:
                request.session['message'] = k8s_form.errors
                request.session['success'] = False

            # Open the k8s tab
            request.session['active_tab'] = "k8s"
            return redirect(request.META.get('HTTP_REFERER'))

        # Approve or reject pending K8s resource requests
        request_id = kwargs.get('request_id', None)
        if request.path == reverse("process_k8s_resource_request", kwargs={"request_id": request_id}):
            if not request_id:
                return JsonResponseBadRequest('No request id provided')

            # Process POST request
            form = K8sResourceRequestProcessForm(request.POST)
            if not form.is_valid():
                request.session['message'] = form.errors
                request.session['success'] = False
                return redirect(request.META.get('HTTP_REFERER'))

            data = form.cleaned_data
            action = data['action']
            replicas = data['replicas']
            reply = data['reply']
            k8s_request = ApplicationQuotaRequest.objects.get(id=request_id)
            app = k8s_request.application
            quota = app.applicationquota

            email = {
                "subject": "",
                "html": ""
            }

            # Approve the request
            if action == K8sResourceRequestProcessActions.APPROVE:
                quota.replicas = replicas
                quota.save()

                # Check if an admin has modified the request before approving
                request_modified = False
                if k8s_request.replicas != replicas:
                    request_modified = True

                # Update the request status
                k8s_request.replicas = replicas
                k8s_request.status = RequestStatusTypes.APPROVED
                k8s_request.admin_reply = reply
                k8s_request.save()

                # Display a success message
                request.session['message'] = (f'Resource request for {app.descriptive_name} '
                                              f'{"adjusted and " if request_modified else ""}approved')
                request.session['success'] = True

                # Construct an email content to notify the submitter
                email['subject'] = (f'Strapper: Resource request for {app.descriptive_name} '
                                    f'{"adjusted and " if request_modified else ""}approved')
                email['html'] = render_to_string(
                    'emails/k8s_resource_request_approval.html',
                    {
                        'k8s_request': k8s_request,
                        'app': app,
                        'quota': quota,
                        'request_modified': request_modified,
                    }
                )

            # Reject the request
            elif action == K8sResourceRequestProcessActions.REJECT:
                # Update the request status
                k8s_request.status = RequestStatusTypes.REJECTED
                k8s_request.admin_reply = reply
                k8s_request.save()

                # Display a success message
                request.session['message'] = f'Resource request for {app.descriptive_name} denied'
                request.session['success'] = True

                # Construct an email content to notify the submitter
                email['subject'] = f'Strapper: Resource request for {app.descriptive_name} denied'
                email['html'] = render_to_string(
                    'emails/k8s_resource_request_rejection.html',
                    {
                        'k8s_request': k8s_request,
                        'app': app,
                        'support_email': os.environ.get('EMAIL_SENDER')
                    }
                )

            # Set status of all request's related notifications to 'None'
            notifications = Notification.objects.filter(
                type=NotificationTypes.ApplicationQuotaRequest,
                related_model_pk=k8s_request.id
            )

            for notification in notifications:
                if notification.status != NotificationStatusTypes.NONE:
                    notification.status = NotificationStatusTypes.NONE
                    notification.save()

            # Send an email to the submitter
            if not settings.TESTING:
                send_mail(
                    email['subject'],
                    "",     # Empty message body because we are using HTML content
                    os.environ.get('EMAIL_SENDER'),
                    [k8s_request.submitted_by.username],
                    fail_silently=False,
                    html_message=email['html'],
                )

            # Open the k8s tab
            request.session['active_tab'] = "k8s"
            return redirect(request.META.get('HTTP_REFERER'))

        return JsonResponseBadRequest('Invalid URL')
