# pylint:
from django.shortcuts import redirect

# Custom mixin: Redirect unauthenticated users to the login page if they are not logged in
# (not currently used anywhere, provided as another option to the authenticate_middleware)
class LoginRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("/login")
        return super().dispatch(request, *args, **kwargs)

# Custom mixin: Redirect normal users to the homepage if they access an admin page
class AdminRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            return redirect("/")
        return super().dispatch(request, *args, **kwargs)
