# Build Docker containers for STRAPPER on development server
To build the container for the main Django app (Strapper), `Helm` and `Terrafrom` binaries are **required**. They should be included in the `deployments` directory. The current version is as follow:
- Helm: [v3.10.2](https://github.com/helm/helm/releases/tag/v3.10.2)
- Terraform: [v1.3.6](https://developer.hashicorp.com/terraform/downloads?product_intent=terraform)

To build the container for the main Django app, from the **root** directory, run:

    docker build -f deployments/Dockerfile -t deployments_web:latest .

To stand up all the required containers (Django app, Postgresql container for database, Selenium container for automated testing), in `strapper/strapper`, make sure `.env` and `.env.db` are written properly, then run:

    docker-compose -f deployments/docker-compose.yml up -d

If `DEBUG=False` in `strapper/strapper/settings.py`, run:

    docker-compose -f deployments/docker-compose.yml exec web python manage.py migrate
    docker-compose -f deployments/docker-compose.yml exec web python manage.py collectstatic --no-input